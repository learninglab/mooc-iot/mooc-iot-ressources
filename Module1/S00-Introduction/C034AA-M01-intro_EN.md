
# Module 1. Internet of Things: General Presentation

Objective: At the end of this module you will be able to describe the IoT system from the device to the cloud.

Hands-on activities (TP):
TP1 : Welcome to the IoT-LAB testbed training

## Contents of Module 1

### 1.1. Introduction to the Internet of Things
- 1.1.0. Introduction
- 1.1.1. What is the Internet of Things?
- 1.1.2. The architecture of IoT solutions
- 1.1.3. The challenges facing the Internet of Things

### 1.2. Which Radio Technology for Which  Application?
- 1.2.0 Introduction
- 1.2.1 Frequency bands
- 1.2.2 Specifications
- 1.2.3 Technologies
- 1.2.4 Overview and comparative assessment

### 1.3. Examples of IoT Applications
- 1.3.0. Introduction
- 1.3.1. Smart watches
- 1.3.2. Sensors for connected agriculture
- 1.3.3. Sensor networks in smart buildings

### 1.4. FIT IoT-Lab Usage
- 1.4.0 Introduction
- 1.4.1 Why have such a platform?
- 1.4.2 What does FIT IoT-LAB offer?
- 1.4.3 How to use FIT IoT-LAB?
- 1.4.4 The use of FIT IoT-LAB in this MOOC
- TP1. Welcome to the IoT-LAB testbed training
