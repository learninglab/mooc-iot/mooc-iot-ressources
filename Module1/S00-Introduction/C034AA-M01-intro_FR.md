
# Module 1. Internet des Objets: Présentation générale

Objectif : À la fin de ce module, vous serez capable de décrire la chaîne IoT dans son ensemble, de l'objet au cloud.

Activités pratiques (TP) :
TP1 : Welcome to the IoT-LAB testbed training


## Sommaire du Module 1

### 1.1. Introduction à l'Internet des Objets
- 1.1.0. Introduction
- 1.1.1. Qu'est-ce que l'Internet des objets ?
- 1.1.2. Architecture des solutions IoT
- 1.1.3. Défis de l’Internet des Objets

### 1.2. Quelle technologie radio pour quelle application ?
- 1.2.0 Introduction
- 1.2.1 Les bandes de fréquences
- 1.2.2 Les caractéristiques
- 1.2.3 Les technologies
- 1.2.4 Pour résumer

### 1.3. Exemples d'applications IoT
- 1.3.0. Introduction
- 1.3.1. La montre connectée
- 1.3.2. Des capteurs pour l’agriculture connectée
- 1.3.3. Un réseau de capteurs dans un bâtiment intelligent

### 1.4. Utilisation de FIT IoT-LAB
- 1.4.0 Introduction
- 1.4.1 Pourquoi une telle plateforme ?
- 1.4.2 Que propose FIT IoT-LAB ?
- 1.4.3 Comment utilise-t-on FIT IoT-LAB ?
- 1.4.4 L’utilisation de FIT IoT-LAB dans ce MOOC
- TP1. Welcome to the IoT-LAB testbed training
