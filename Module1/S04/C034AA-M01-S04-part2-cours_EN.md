# 1.4.2. What does FIT IoT-LAB offer?

IoT-LAB now enables access to 1,500 nodes for experimenting, deployed across various different sites, the majority of which are in France. This deployment makes IoT-LAB:

- multi-platform, in that it offers various different experimentation boards
- multi-radio, in that boards don’t all have the same radio chips, and some have more than one
- multi-topology, in that the physical deployments are all different (in grids, in several corridors, dense, scattered, over several levels, etc.)
- multi-OS, in that the different boards have one or more embedded OS support.


<figure>
    <img src="Images/boards.jpeg" alt="Different experiment boards from retail">
    <figcaption>Fig. 1: Different experiment boards from retail <em>© J. Vandaele</em></figcaption>
</figure>

<figure>
    <img src="Images/grenoble.jpg" alt="Grenoble">
    <figcaption>Fig. 2: Deployment of the platform in Grenoble <em>© Inria / Photo L. Boissieux</em></figcaption>
</figure>

<figure>
    <img src="Images/lille.jpg" alt="Lille">
    <figcaption>Fig. 3: Deployment of the platform in Lille <em>© Inria / Photo C. Morel</em></figcaption>
</figure>

<figure>
    <img src="Images/saclay.jpg" alt="Saclay">
    <figcaption>Fig. 4: Deployment of the platform in Saclay <em>© Inria / Photo C. Morel</em></figcaption>
</figure>

<figure>
    <img src="Images/strasbourg.jpg" alt="Strasbourg">
    <figcaption>Fig. 5: Deployment of the platform in Strasbourg <em>© ICube / Photo G. Schreiner</em></figcaption>
</figure>

More than anything else, IoT-LAB is a platform that is open to all users: students, teachers, makers, researchers, manufacturers, etc. It is free to access - all that’s needed is for users to create a user account and to respect the usage rules.
