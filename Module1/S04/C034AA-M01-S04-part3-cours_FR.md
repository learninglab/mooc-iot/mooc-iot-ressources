
# 1.4.3. Comment utilise-t-on FIT IoT-LAB ?

Pour une utilisation simple de la plateforme, un portail Web permet en quelques étapes la création d'une expérience sur plusieurs centaines de cartes. Sur chaque site, un serveur accessible par SSH (on parle de frontale SSH) permet un accès à un environnement de développement et des accès aux données récoltées ou aux liens séries des cartes.
La plateforme dispose également de nombreux outils d'interface en ligne de commandes pour des usages avancés ou l'automatisation de tâches. La plateforme expose également une API ouverte pour les développeurs.

Des tutoriels sont disponibles sur le site Internet de la plateforme pour découvrir toutes ces fonctionnalités : https://www.iot-lab.info/learn/

<figure>
    <img src="Images/design-infra-total.png" alt="Overview">
    <figcaption>Fig. 1 : Aperçu des interactions avec la plateforme</figcaption>
</figure>
