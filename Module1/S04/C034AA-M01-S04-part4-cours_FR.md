# 1.4.4. L'utilisation de FIT IoT-LAB dans ce MOOC

Les activités pratiques vous seront proposées presque exclusivement à travers l'environnement JupyterLab, qui vous permettra dans une seule interface de suivre les instructions présentes dans des notebooks, d'éditer le code des applications, d'interagir avec la plateforme et avec les noeuds d'expérimentation depuis des terminaux. Cet environnement a été personnalisé en intégrant tous les outils IoT-LAB nécessaires; vous n'aurez donc à installer aucun outil et pourrez tout faire depuis votre navigateur web.

Pour chaque participant, un compte dédié à ce MOOC sera créé. Les paramètres d'authentification (accès à l'API et clés SSH) seront installés automatiquement pour vous dans votre environnement JupyterLab. Une fois de plus, vous n'aurez donc rien à paramétrer vous même.

Les ingénieurs de l'équipe de développement d'IoT-LAB (Alexandre Abadie, Frédéric Saint-Marcel, Guillaume Schreiner et Julien Vandaële) font partie de l'équipe pédagogique de ce MOOC. Vous avez donc pour vous aider les experts de cette plateforme ; n'hésitez pas à leurs poser vos questions sur le forum.

Pour un exemple concret, découvrez en vidéo comment déployer 
et interagir avec une expérience depuis le portail web d'IoT-LAB et comment faire la même chose depuis l'environnement du MOOC.