# 1.4.1. Why have such a platform?

The FIT IoT-LAB platform comes from the academic world.
In the early 2000s, researchers working in the field
of sensor networks had to validate their research through the use of
network simulators. However, problems linked to wave propagation
and interference also required validation through experimentation.
They then had to purchase, and sometimes even design, electronic cards incorporating a microcontroller, sensors and a radio chip.
Today, this might seem relatively straightforward, but 15
years ago, the makers movement and Arduino or Raspberry Pi circuit boards
had yet to democratise programmable electronics for the wider public
as they have since.

On top of the financial cost of the equipment, there was also the time
taken up by experiments. And once they had
designed the program to run on the circuit board, they then needed to deploy
the experiment. This involved:

1. programming the circuit boards one by one, connecting each in turn to a computer
2. recharging the batteries used to power the circuit boards
3. deploying the different circuit boards, following a physical topology enabling radio communication
4. implementing a solution for retrieving the experiment logs (e.g. storing the output from the main network node’s serial link in a text file).

Any bugs or modifications in the application to be tested would mean having
to start the whole process over again. Throw in the upscaling test,
featuring a sizeable number of nodes in the network, and you get an idea of just how
complicated an experimental validation was.

In this context, in 2008, researchers began work on an innovative
research project aimed at creating an open, large-scale
experimentation platform for wireless sensor networks. The goal was to save time in the
experimental phases, to test easily on a large-scale and to be able to replay
experiments (e.g. for reproducible scientific purposes).

This platform is now part of the [FIT](https://fit-equipex.fr) infrastructure (Future Internet), a large-scale experimentation environment thanks to the federation of the following platforms:

- FIT Wireless for experimenting on a wide range of wireless network problems (Wi-Fi, 5G and cognitive radio)
- FIT Cloud, linked to cloud design, the development of OpenStack components and services
- FIT IoT-LAB, dedicated to experiments for the Internet of Things, particularly embedded technology.

<figure>
    <img src="Images/banner-fit.png" alt="FIT">
    <figcaption>Fig. 1: The FIT federation <em>© FIT Equipex</em></figcaption>
</figure>
