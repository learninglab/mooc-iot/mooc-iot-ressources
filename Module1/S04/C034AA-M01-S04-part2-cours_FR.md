
# 1.4.2. Que propose FIT IoT-LAB ?

IoT-LAB donne aujourd'hui accès à 1500 noeuds pour l'expérimentation, déployés sur différents sites, principalement en France. À travers ce déploiement IoT-LAB est :

- multi-plateforme, proposant plusieurs modèles de cartes d'expérimentation ;
- multi-radio, car les cartes n'ont pas toutes les mêmes puces radio, voire en possèdent parfois plusieurs ;
- multi-topologie, les déploiements physiques étant tous différents (en grille, dans plusieurs couloirs, dense, éparse, sur plusieurs niveaux, ...)
- multi-OS, les différentes cartes disposant d'un support dans un ou plusieurs OS embarqués.


<figure>
    <img src="Images/boards.jpeg" alt="Différentes cartes d'expérimentation issues du commerce">
    <figcaption>Fig. 1 : Différentes cartes d'expérimentation issues du commerce <em>© J. Vandaele</em></figcaption>
</figure>

<figure>
    <img src="Images/grenoble.jpg" alt="Grenoble">
    <figcaption>Fig. 2 : Déploiement de la plateforme à Grenoble <em>© Inria / Photo L. Boissieux</em></figcaption>
</figure>

<figure>
    <img src="Images/lille.jpg" alt="Lille">
    <figcaption>Fig. 3 : Déploiement de la plateforme à Lille <em>© Inria / Photo C. Morel</em></figcaption>
</figure>

<figure>
    <img src="Images/saclay.jpg" alt="Saclay">
    <figcaption>Fig. 4 : Déploiement de la plateforme à Saclay <em>© Inria / Photo C. Morel</em></figcaption>
</figure>

<figure>
    <img src="Images/strasbourg.jpg" alt="Strasbourg"> 
    <figcaption>Fig. 5 : Déploiement de la plateforme à Strasbourg <em>© ICube / Photo G. Schreiner</em></figcaption>
</figure>

IoT-LAB est avant tout une plateforme ouverte à tous les utilisateurs : étudiants, enseignants, makers, chercheurs et industriels. Son accès est gratuit. Elle requiert juste la création d'un compte utilisateur et le respect des règles d'utilisation.
