
# 1.4.3. How to use FIT IoT-LAB

For a simple use of the platform, there is a web portal letting you create an experiment on several hundred boards in just a few steps. On each site, a server accessible via SSH (known as SSH frontend) is used to access a development environment, as well as to access collected data or boards serial links.
The platform also has a number of command line interface tools for advanced uses or for task automation. The platform also features an open API for developers.

Some tutorials are available on the platform website, introducing all these features:
https://www.iot-lab.info/learn/

<figure>
    <img src="Images/design-infra-total.png" alt="Overview">
    <figcaption>Fig. 1: Overview of interactions with the platform</figcaption>
</figure>
