# 1.4.0. Introduction

This MOOC offers activities linked to experimenting with and programming connected objects. Practical application will involve real hardware, but no purchases will be necessary - instead, you will use hardware made available by an experimentation platform for the IoT: FIT IoT-LAB.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>The purpose of this sequence is to give you a brief overview of the FIT IoT-LAB platform, explaining its origin and what it was designed for, its features, how to use it and, more specifically, how you will be using it in the context of this MOOC.
</p>
</div>

___Within reach of sensors___  
_Wireless sensor networks are becoming increasingly widespread in the Internet of Things, particularly in smart cities. 
This video, in French only, lets you discover the research taking place in this field and the resulting need for experimentation. 
We present here the FIT IoT-LAB experimentation platform, resulting from an ANR project started in 2008. It is currently deployed 
on six sites in France, namely Strasbourg, Lille, Saclay, Paris, Lyon and Grenoble. There are now more than 2,300 sensor nodes 
available to users for experiments in embedded wireless communications._

<iframe width="791" height="445" src="https://www.youtube.com/embed/eGc5Vcci3kY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
