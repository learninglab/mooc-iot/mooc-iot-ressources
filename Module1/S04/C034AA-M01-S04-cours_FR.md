
# 1.4.0. Introduction

Ce MOOC propose des activités d'expérimentation et de programmation d'objets connectés. La mise en pratique se fera sur du matériel réel mais sans achat nécessaire. En effet, vous utiliserez du matériel mis à disposition par une plateforme d'expérimentation pour l'IoT : FIT IoT-LAB.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Cette séquence a pour but de vous présenter rapidement la plateforme FIT IoT-LAB en expliquant son origine et son but, ce qu'elle propose, comment l'utiliser et plus précisément comment vous l'utiliserez dans le cadre de ce MOOC.
</p>
</div>

___À portée de capteurs___  
_Les réseaux de capteurs sont de plus en plus utilisés dans l'Internet des Objets, notamment dans les villes intelligentes. Découvrez dans cette vidéo (durée 6'13 - réalisation Inria, 2018) les recherches en cours dans ce domaine et les besoins en expérimentation qui en découlent.
Nous vous y présentons la plateforme d'expérimentation FIT IoT-LAB (à 3'09) et l'évolution de son déploiement à Lille (à 3'51) pour répondre le mieux possible aux besoins de ses utilisateurs._

<iframe width="791" height="445" src="https://www.youtube.com/embed/eGc5Vcci3kY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
