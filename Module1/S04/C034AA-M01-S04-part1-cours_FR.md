
# 1.4.1. Pourquoi une telle plateforme ?

La plateforme FIT IoT-LAB est issue du monde académique.
En effet, au début des années 2000, les chercheurs travaillant dans le domaine
des réseaux de capteurs devaient déjà valider leurs travaux par l'utilisation de
simulateurs réseaux. Cependant, les problématiques liées à la propagation des ondes
et aux interférences nécessitaient également une validation par
l'expérimentation. Ils devaient alors faire l'acquisition, voire parfois même la
conception, de cartes électroniques embarquant un microcontrôleur, des capteurs
et une puce radio. Cela peut paraitre aujourd'hui assez simple et peu onéreux, mais il y a 15 ans, le mouvement des makers et les cartes Arduino ou Raspberry Pi
n'avaient pas encore démocratisé l'électronique programmable grand public comme
c'est le cas aujourd'hui.

Au coût financier de l'équipement, il fallait ajouter le coût en temps que
demandaient les expérimentations. Et après avoir
conçu le programme devant s'exécuter sur la carte, il leur fallait déployer
l'expérimentation. Cela revenait donc à :

1. programmer les cartes une à une, en les connectant à tour de rôle à un ordinateur ;
2. recharger les batteries devant alimenter les cartes ;
3. déployer les différentes cartes, selon une topologie physique devant permettre les communications radio ;
4. et mettre en place une solution pour récupérer les logs de l'expérimentation (e.g. stockage dans un fichier texte de la sortie du lien série du noeud principal du réseau).

Un bogue ou une simple modification dans l'application à tester, et c'était
l'ensemble de ces étapes qui était à reprendre. Ajoutez à cela le test du passage à
l'échelle avec un grand nombre de noeuds dans le réseau et vous comprenez que la
validation par expérimentation devenait une opération très compliquée à réaliser.

C'est dans ce contexte que, dès 2008, des chercheurs ont porté un projet de
recherche innovant visant à créer une plateforme ouverte d'expérimentation à
grande échelle pour les réseaux de capteurs. Le but : gagner du temps dans les
phases d'expérimentation, tester facilement à grande échelle et pouvoir rejouer
des expériences (par exemple dans un but de science reproductible).

Cette plateforme fait aujourd'hui partie de l'infrastructure [FIT](https://fit-equipex.fr) (Future Internet), qui offre un environnement d'expérimentation à grande échelle grâce à la fédération des plateformes suivantes :

- FIT Wireless pour l'expérimentation sur un large éventail de problèmes de réseaux sans fil (WiFi, 5G et radio cognitive) ;
- FIT Cloud liée à la conception de cloud, au développement de composants OpenStack et de services ;
- FIT IoT-LAB dédiée à l'expérimentation pour l'Internet des Objets, et plus particulièrement l'embarqué.

<figure>
    <img src="Images/banner-fit.png" alt="FIT">
    <figcaption>Fig. 1 : La fédération FIT <em>© FIT Equipex</em></figcaption>
</figure>
