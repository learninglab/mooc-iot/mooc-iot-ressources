# 1.4.4. The use of FIT IoT-LAB in this MOOC

Practical activities will be available almost exclusively through the JupyterLab environment, one single interface that will enable you to follow the instructions in the notebooks, to edit the code for applications and to interact with the platform and with experiment nodes from terminals. This environment has been customized by integrating all of the necessary IoT-LAB tools, meaning you won’t have to install anything and can do everything from your web browser.

A dedicated MOOC account will be created for each participant. The authentication parameters (access to the API and SSH keys) will be installed automatically for you in your JupyterLab environment. Once again, you will not have to configure anything yourself.

The engineers from the IoT-LAB’s development team (Alexandre Abadie, Frédéric Saint-Marcel, Guillaume Schreiner and Julien Vandaële) are part of the educational team for this MOOC. They are experts on this platform, and will be happy to answer any questions you may have on the forum.

For a concrete example, discover in this video how to deploy
and interact with an experience from the IoT-LAB web portal and how to do the same from the MOOC environment.