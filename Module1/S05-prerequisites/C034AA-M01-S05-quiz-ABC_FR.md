_Pour chaque question, il n'y a pas de mauvaise réponse.
Suggestion : Demander au participant de se positionner selon son expérience d'utilisation d'une ligne de code ou de commande qu'il sera amené à voir ou écrire dans les cours et les TP._


# Question 1.

Est-ce que vous avez déjà entendu parler du système Linux ?
(a) Je l'ai déjà installé sur mon ordinateur
(b) Je l'ai utilisé quelque fois
(c) J'en ai vaguement entendu parlé, est-ce que c'est un pingouin ?

# Question 2.

Quand je dois lancer un outil en ligne de commande dans un terminal:
(a) Je trouve ça facile
(b) Je ne suis pas très à l'aise mais je peux m'ens sortir
(c) Un outil en ligne de commande ? Qu'est-ce que c'est ?

# Question 3.

Est-ce que vous connaissez le protocol Secure Shell (SSH)?
(a) Oui, je l'utilise très souvent pour me connecter à des systèmes Linux
(b) Je l'ai utilisé quelquefois dans le passé
(c) Je ne sais pas me connecter à un système distant

# Question 4.

A propos de ma connaissance sur l'architecture matérielle d'un ordinateur:
(a) Je connais plusieurs architectures de CPU (x86, ARM, PowerPC, etc)
(b) Je sais faire la différence entre RAM et ROM
(c) Je ne connais pas les éléments principaux qui composent un ordinateur (CPU, memory, etc)

# Question 5.

A propos de ma connaissance sur les protocoles réseaux:
(a) Je connais la différence entre les protocoles UDP and TCP
(b) J'ai déjà entendu parler du modèle OSI
(c) Je ne sais même pas ce qu'est une address IP!

# Question 6.

Quand j'écris du code C:
(a) Je maitrise la syntaxe ou je peux faire quelques erreurs de syntaxe mais j'arrive à les corriger
(b) J'ai déjà compilé et exécuté du code C
(c) Je n'ai jamais écrit une seule ligne de code C

# Question 7.

A propos de mon niveau en anglais:
(a) Je le parle courammment
(b) Je peux lire de la documentation technique en anglais sans problème
(c) Je ne comprends pas l'anglais

# Resultat du quiz selon profil A,B ou C
Message plutôt a (OK, GO) :
Vous avez toutes les connaissances requises pour suivre le Mooc sans difficultés et même de faire
l'évaluation par les pairs à la fin du module 3. Amusez-vous bien!

Message plutôt b (Accrochez-vous, consulter ressources):
Vos connaissances techniques pourraient être un peu limitées pour les activités pratiques mais vous
devriez pouvoir suivre ce Mooc et compléter l'évaluation par les pairs à la fin du
module 3.
Si vous n'avez jamais écrit de code C, nous vous recommandons de lire quelques
ressources en ligne (par exemple: https://www.learn-c.org/).
Si vous n'avez jamais utilisé Linux, vous pouvez suivre un tutoriel sur
l'utilisation des commandes Linux (https://ubuntu.com/tutorials/command-line-for-beginners#1-overview).

Message plutôt c (Eviter évaluation par les pairs, ressources):
Nous avons identifié que vos connaissances techniques pourraient ne pas être suffisantes pour
compléter l'évaluation par les pairs à la fin du module 3. Mais vous pouvez tout de même
suivre et valider le Mooc sans l'évaluation par les pairs.
