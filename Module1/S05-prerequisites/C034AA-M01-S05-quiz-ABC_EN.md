_Pour chaque question, il n'y a pas de mauvaise réponse.
Suggestion : Demander au participant de se positionner selon son expérience d'utilisation d'une ligne de code ou de commande qu'il sera amené à voir ou écrire dans les cours et les TP._


# Question 1.

Have you ever used a Linux system ?
(a) I have already installed it on my computer
(b) I've already used it from time to time
(c) I've heard about it but I think it's a penguin

# Question 2.

When I have to run a command line tool in a terminal:
(a) I feel easy
(b) I don't feel comfortable but I can get by
(c) A command line tool! What is that ?

# Question 3.

Do you know the Secure Shell (SSH) protocol?
(a) Yes, I use it all the time to connect to my Linux computers
(b) I used it a few times in the past
(c) I don't know how to connect to a remote computer

# Question 4.

About my knowledge on computer hardware and architecture:
(a) I know several CPU architectures (x86, ARM, PowerPC, etc)
(b) I know the difference between RAM and ROM
(c) I don't know what are the main elements of a computer (CPU, memory, etc)

# Question 5.

About my knowledge on network protocols:
(a) I know the difference between UDP and TCP protocols
(b) I already heard about the OSI protocol stack
(c) I don't even know what is an IP address!

# Question 6.

When I write C code:
(a) I'm fluent or I can make some syntax mistakes but I know how to fix them
(b) I already built and executed C code
(c) I never wrote a single line of C

# Question 7.

About my english level:
(a) I'm fluent
(b) I can read technical documentation without problem
(c) I don't understand english

# Resultat du quiz selon profil A,B ou C
Message plutôt a (OK, GO) :
You have all required knowledge to follow the Mooc without problems and even for the pair evualation at the end of module 3. Enjoy!

Message plutôt b (Accrochez-vous, consulter ressources):
Your technical skills might be a bit limited for practical activities but you should still be able to follow this Mooc and to complete the pair evaluation at the end of module 3.
If you never wrote C code, we recommend that read some online resources (https://www.learn-c.org/).
If you never used Linux, please follow a tutorial about Linux commands (https://ubuntu.com/tutorials/command-line-for-beginners#1-overview)


Message plutôt c (Eviter évaluation par les pairs, ressources):
We identified that your technical skills might not be sufficient enough for you to complete the pair evualation at the end of module 3. But you can still
follow and validate the Mooc without it.
