# Question 1.1.1
Quelle(s) définition(s) vous semble(nt) correspondre à l’Internet des Objets :
[X] Un moyen de donner une vie numérique à un objet 
[X] Un ensemble d'objets physiques connectés entre eux 
[ ] Il possède une puissance de traitement proche d'un ordinateur.
[ ] La gestion d’une base de données 


# Question 1.1.2
Parmi les défis suivants, lesquels sont parmi les grands défis d’aujourd’hui du monde IoT ?
[X] Les objets sont contraints matériellement et ne peuvent réaliser de calculs complexes.
[X] Les applications utilisent des systèmes hétérogènes et nombreux.
[X] Avoir de nombreux objets distribués et captant différents types d’information représente un défi sociétal d’acceptation et de compréhension 
[X] Déployer tous ces objets implique la collecte de plus en plus de données qu’il faut stocker et traiter 
 

# Question 1.1.3
Nous déployons de plus en plus d’objets contraints sans fil. En quoi la communication de ces objets représente un défi ? 
Cochez les affirmations exactes : 
[X] Les communications sans fil sont énergivores. 
[X] La bande passante est limitée. 
[X] Les objets communiquants sont distribués et ne savent pas nécessairement quels sont les autres objets en présence.
[X] Les objets sont potentiellement mobiles.


# Question 1.1.4
Une architecture matérielle complète IoT comprend les composants suivants :
[X] Des bases de données 
[X] Des serveurs d’application qui utilisent les données
[X] Des passerelles
[X] Les objets contraints
[ ] Des vélos 
