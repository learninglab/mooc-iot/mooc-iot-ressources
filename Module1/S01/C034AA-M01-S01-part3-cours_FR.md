# 1.1.3. Défis de l'Internet des Objets

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.1.3a Interopérabilité des solutions IoT](#113a-interopérabilité-des-solutions-iot)
- [1.1.3b Contraintes matérielles](#113b-contraintes-matérielles)
- [1.1.3c Topologie dynamique](#113c-topologie-dynamique)
- [1.1.3d Stockage, traitement et transport des données](#113d-stockage-traitement-et-transport-des-données)
- [1.1.3e Sécurité des objets connectés](#113e-sécurité-des-objets-connectés)
- [1.1.3f Problématiques sociétales](#113f-problématiques-sociétales)
<!-- TOC END -->

Comme nous l'avons vu précédemment, l'Internet des Objets se retrouve dans un
grand nombre de domaines d'applications, qu'ils soient industriels ou liés à
notre vie quotidienne et, pour répondre aux attentes des usagers, il doit
faire face à de nombreux défis.

## 1.1.3a Interopérabilité des solutions IoT

Ces dernières années ont vu l'émergence de petits et grands acteurs dans le
secteur des objets connectés. En particulier, les géants actuel du web se sont
lancés dans une course technologique pour faire en sorte qu'un maximum de
déploiements d'objets connectés utilisent leurs solutions.
C'est le cas par exemple d'Amazon avec sa plateforme AWS IoT, de Microsoft avec
Azure IoT ou encore Google avec Google Cloud IoT.
L'utilisation des offres de Cloud de Google/Amazon/Microsoft sont pratiques car
elles rassemblent toutes les briques logicielles nécessaires, parfois de l'objet jusqu'au
Cloud, pour développer une application complètes IoT : serveur de gestion des
données, analyse des données avec des algorithmes d'apprentissage statistique,
hébergement d'application de visualisation en ligne, etc.

Cette évolution favorise malheureusement le fonctionnement en _silos_ des
solutions IoT. En effet, les utilisateurs d'une application donnée se trouve en
quelque sorte _captifs_, c'est-à-dire que les objets et les données qu'ils
produisent ne peuvent être gérés que par le biais de cette application. Il
n'est généralement pas possible de réutiliser un objet programmé pour une
certaine application avec une autre application.

<figure>
    <img src="Images/Vertical-silos-of-IoT-service-deployment.png" alt="Fonctionnement en silos des solutions IoT"><br />
    <figcaption>Fig. 1 : Fonctionnement en silos des solutions IoT.</figcaption>
</figure>

_[Source:](https://arxiv.org/pdf/1410.4977.pdf) P. Desai, A. Sheth and P. Anantharam, "Semantic Gateway as a Service Architecture for IoT Interoperability," 2015 IEEE International Conference on Mobile Services, New York, NY, 2015, pp. 313-319._

Les autres problèmes liés à ces silos concernent le manque d'utilisation de
standards au niveau des protocoles de communication.

Face à cette problématique d'interopérabilité des solutions IoT, un des enjeux
majeurs est d'arriver à développer l'utilisation de standards de communication.
Il en existe déjà un certain nombre, généralement proposé par l'[IETF](https://www.ietf.org/)  (_Internet Engineering Task Force_), un
organisme de standardisation international. Ces standards s'appliquent en
particulier aux réseaux de capteurs, comme nous le verrons dans ce Mooc.

D'autres spécifications existent, poussées par certaines entreprises pour
promouvoir leur technologie.
Par exemple, c'est le cas de LoRaWAN avec la [LoRa Alliance](https://lora-alliance.org/), une
organisation qui regroupe de nombreux acteurs de l'Internet des objets :
opérateurs de télécommunications, fabricants d'objets, fabricants de puces.

C'est aussi le cas du [ThreadGroup](https://www.threadgroup.org/) qui promeut des spécifications pour les
réseaux de capteurs. Ces spécifications du ThreadGroup réutilisent certains des
standards de l'IETF.

## 1.1.3b Contraintes matérielles

Un autre défi concerne la partie matérielle des objets eux-mêmes, en particulier
ceux fonctionnant avec des microcontrôleurs.
En effet, ce type de plateforme dispose de caractéristiques intrinsèques très
contraintes : quelques dizaines de kilo octets de RAM, une mémoire flash d'une
centaine de kilo octets. Et pourtant, ils doivent être capables de faire
fonctionner des applications de plus en plus complexes - capacités de
communication, sécurité, mise-à-jour -, être de plus en plus intelligents.
Cette problématique est résolue par l'utilisation de logiciels adaptés et
d'optimisations multiples pour "faire tenir" ces applications sur des
microcontrôleurs.

Un autre aspect du défi lié aux contraintes matérielles est la gestion de
l'énergie sur les objets connectés. En effet, la plupart des objets déployés
dans une application IoT doivent pouvoir fonctionner sur batterie le plus
longtemps possible.
Les microcontrôleurs disposent de plusieurs niveaux de fonctionnement
permettant de consommer très peu d'énergie. On parle généralement de mode
endormi. Pour diminuer la consommation et économiser au maximum la batterie, il
faut donc tirer le meilleur parti de cette caractéristique. Comme il s'agit
avant tout d'électronique, il faut aussi s'assurer que la conception matérielle
n'engendre pas de fuite de courant.
Et tout ça, en garantissant le fonctionnement d'applications de plus en plus
complexes.

## 1.1.3c Topologie dynamique

La topologie d'un réseau IoT est très dynamique. En effet, chaque objet est indépendant et peut s'allumer et s'éteindre à tout moment. De plus, les objets peuvent être mobiles et chaque mobilité est différente en trajectoire et vitesse, car propre à chacun  et lié à l'objet ou la personne qui le porte. Cette dynamique
ajoute un défi à l'acheminement des données et au placement des services. En effet, comment envoyer un message à un objet sans savoir où il est vraiment ? Comment s'assurer d'une route de transmission de données entre noeuds si à tout moment un peut apparaitre ou disparaitre d'un voisinage radio ?
Cet aspect est également un défi pour  le placement de services dans le _edge computing_ dans lequel on cherche à déployer les services au plus près des objets, en périphérie de réseau. Les services doivent donc dynamiquement suivre la mobilité des objets et leurs besoins sans connaissance a priori de leurs déplacements.

## 1.1.3d Stockage, traitement et transport des données

Le déploiement d'un nombre croissant d'objets connectés (les estimations parlent de [38 milliards d'objets d'ici à 2025](https://www.mac4ever.com/actu/143604_il-y-aurait-22-milliards-d-objets-connectes-dans-le-monde-strategy-analytics)) est amené à produire une quantité gigantesque de données (certaines estimations parlent de [175 milliards de téraoctets d'ici à 2025](https://www.aucoeurdesinfras.fr/liot-ou-comment-traiter-le-volume-de-donnees-en-temps-reel/6247/)).

Tout d'abord, il faut être en mesure de stocker de manière fiable et pérenne
toutes ces données, par exemple dans des bases de données. L'expérience acquise
depuis les débuts de l'informatique sur ces sujets, en particulier sur les
systèmes de gestion de base de données, doit permettre de faire face à
cette problématique. Pour être en mesure de stocker de manière fiable et
pérenne ces volumes de données, il faudra continuer à améliorer les systèmes de
base de données existants

L'internet de objets doit répondre à un besoin de rapidité d'accès à
l'information. Cette information ne peut pas toujours être tirée directement
des données elles-même : en effet, il faut très souvent traiter ces données, les
recouper entre elles pour en extraire une information de plus haut-niveau
utilisable.
Ces traitements sont parfois complexes et nécessitent une grande capacité de
calcul. Aujourd'hui, les offres liées aux plateformes de _cloud computing_
donnent accès très facilement à ces ressources de calcul (serveurs de calcul,
cartes GPU, etc) et doivent permettre de produire des résultats de traitement en
temps réels.

Enfin, il est possible de mieux répartir les ressources de calcul entre
les différents éléments de la chaîne IoT : les calculs les plus intensifs
s'effectuent dans les _cloud_, les calculs plus légers peuvent s'effectuer à un
niveau intermédiaire (par exemple, sur une station de base) et les calculs les
plus élémentaires s'effectuent sur les objets eux-mêmes.
Cette meilleure répartition permet de diminuer l'utilisation des infrastructures
de transport des données (radio, filaire) et donc d'augmenter le nombre d'objets
pouvant être connectés.

## 1.1.3e Sécurité des objets connectés

Tous ces capteurs déployés, dans un bâtiment intelligent, dans une
ville, chez les particuliers, etc., fournissent des données de natures très
variées (température, luminosité, pression, accélération, proximité, humidité) et parfois très sensibles (présence de
personnes).
Si ces données sont interceptées, elles peuvent être exploitées par un tiers malveillant et causer des problèmes de différentes natures : dysfonctionnement ou arrêt complet de l'application, atteinte physique sur une ou plusieurs personnes, espionnage industriel en vue d'obtenir un avantage concurrentiel, etc.

La protection de ces données est donc un enjeu majeur pour les applications de
l'Internet des Objets pour garantir son développement sur le long terme.
Cette protection peut se faire à plusieurs niveaux :

* au niveau des communications entre objets,
* au niveau des applications fonctionnant sur les objets eux-mêmes,
* au niveau de la partie matérielle de l'objet.

En fonction du niveau de protection (communication, logiciel, matériel), la
difficulté des attaques (et des protections) varie.

Cet aspect sera abordé en détail dans le module 6 et des solutions seront
apportées pour mettre en place des mécanismes de protection.

## 1.1.3f Problématiques sociétales

Au delà des aspects techniques, l'Internet des Objets doit faire face à des
problèmes d'ordres sociétaux et environnementaux.
Le défi principal réside dans la conséquence sur la vie quotidienne qu'implique
le déploiement des solutions IoT.
En effet, les objets connectés gagnent de plus en plus d'espaces dans la
vie quotidienne de la population et même dans tous les domaines d'activités.

Par exemple, il peut arriver que certains utilisateurs, par manque de
connaissance sur le fonctionnement de ces objets (un assistant vocal, une borne
domotique), ne réalisent pas qu'ils partagent des informations personnelles,
parfois même intimes.
Autre exemple : le déploiement de solutions IoT en zone urbaine dans le but de
rendre les villes plus intelligentes peut remonter des données sur l'usage de
certaines infrastructures par les habitants de la ville. Ces systèmes doivent
permettre d'améliorer la gestion des transports, des infrastructures, de faire
des économies, d'améliorer le vivre ensemble.
Mais même si les objectifs initiaux sont louables et importants et que ces
données sont supposées être anonymes, rien ne garantit que le recoupement
d'information ne puisse permettre de désanonymiser ces données.
Il peut donc y avoir un problème de respect de la vie privée lié à l'Internet
des Objets.

D'un point de vue environnemental, l'Internet des Objets pourrait avoir un impact
négatif important. En effet, comme nous l'avons mentionné précédemment, il est
prévu des déploiements de milliards d'objets connectés. Quel sera l'impact sur
l'environnement de tous ces objets disséminés lorsqu'ils arriveront en fin de
vie ? Sera-t-il possible de les reconditionner pour d'autres fonctions plutôt
que d'en reproduire de nouveaux ?

Références (en anglais) :  
[Is Your IoT Device Harming the Environment?](https://innovationatwork.ieee.org/iot-environmental-impact-ieee-wake-up-radio/)  
[Reducing the environmental impact of global IoT](https://www.itproportal.com/features/reducing-the-environmental-impact-of-global-iot/)
