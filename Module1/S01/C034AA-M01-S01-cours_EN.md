# 1.1.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence you will discover what the Internet of Things is, the main areas it is used in, its hardware and software components and the technological challenges facing it.
</p>
</div>

Video presentation of the Internet of Things (IoT): what is the IoT? What are its features?
