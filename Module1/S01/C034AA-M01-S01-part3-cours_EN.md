# 1.1.3. The challenges facing the Internet of Things

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.1.3a The interoperability of IoT solutions](#113a-the-interoperability-of-iot-solutions)
- [1.1.3b Hardware constraints](#113b-hardware-constraints)
- [1.1.3c Dynamic topology](#113c-dynamic-topology)
- [1.1.3d Storing, processing and transporting data](#113d-storing-processing-and-transporting-data)
- [1.1.3e The security of connected objects](#113e-the-security-of-connected-objects)
- [1.1.3f Societal problems](#113f-societal-problems)
<!-- TOC END -->

As we have already seen, the Internet of Things can be found in a
wide range of applications, whether in industry or in
our daily lives, and in order to meet users’ expectations, there are a number
of challenges it must face.

## 1.1.3a The interoperability of IoT solutions

The past few years have seen the emergence of both small and large stakeholders
in the connected objects sector. The current giants of the internet in particular
are engaged in a technological arms race in order to ensure that as many connected
objects as possible use their solutions.
Examples of this include Amazon’s AWS IoT platform, Microsoft’s
Azure IoT or Google’s Google Cloud IoT.
Using Google/Amazon/Microsoft cloud technology is practical because
all the necessary software building blocks are there in one place, sometimes from the object up to
the cloud, enabling a comprehensive IoT application: data management
servers, data analysis with machine learning algorithms,
hosting for online virtualisation applications, etc.

Unfortunately, this development has seen an increase in IoT solutions
operating in _silos_. Essentially, users of a given application find themselves
captive, to a certain extent, i.e. the objects and the data which they
produce can only be managed through this application. Under
normal circumstances, it is not possible to reuse an object that has been programmed for
one application with another application.

<figure>
    <img src="Images/Vertical-silos-of-IoT-service-deployment.png" alt="Vertical silos of IoT service deployment"><br />
    <figcaption>Fig. 1: Vertical silos of IoT service deployment.</figcaption>
</figure>

_[Source:](https://arxiv.org/pdf/1410.4977.pdf) P. Desai, A. Sheth and P. Anantharam, "Semantic Gateway as a Service Architecture for IoT Interoperability," 2015 IEEE International Conference on Mobile Services, New York, NY, 2015, pp. 313-319._

Another issue with these silos is the lack of standards used in relation to communication protocols.

When it comes to solving the problem of the interoperability of IoT solutions, one of the major
challenges involves finding a way of developing the use of communications standards.
A few already exist, the majority of which have been proposed by the [IETF](https://www.ietf.org/) (_Internet Engineering Task Force_), an
international standards body. These standards are applicable
to sensor networks in particular, as we will see in this MOOC.

Other specifications have been pushed by certain companies in order
to promote their technology.
LoRaWAN, for example, did this with the [LoRa Alliance](https://lora-alliance.org/), an
organisation made up of various different stakeholders from the IoT world:
telecommunications operators, manufacturers of objects, manufacturers of chips, etc.

Another example is the [ThreadGroup](https://www.threadgroup.org/), which promotes specifications for
sensor networks. These ThreadGroup specifications reuse certain
IETF standards.

## 1.1.3b Hardware constraints

Another challenge relates to the hardware aspect of the objects themselves, particularly
those which operate using microcontrollers.
The inherent characteristics of these platforms are highly
restrictive: a few dozen kilobytes of RAM, flash memory of
around one hundred kilobytes. Despite this, they must be capable of
operating increasingly complex applications - communication
capacities, security, updates - while being increasingly more intelligent.
This problem can be solved by using adapted software and
multiple optimisations in order to keep these applications going on
microcontrollers.

Another aspect of the challenge linked to hardware constraints is how energy
is managed on connected objects. Indeed, the majority of objects deployed
in IOT applications must be capable of running on batteries for as
long as possible.
Microcontrollers have a number of different operating levels,
meaning they consume very little energy. We generally refer to sleep
mode. Limiting consumption and saving as much battery life as possible
means getting the most out of this feature. Given that it is
first and foremost electronic, it is also vital to ensure that the hardware design
does not lead to any current leakage.
All this has to be done while ensuring that increasingly complex applications are able to
function.


## 1.1.3c Dynamic topology

The topology of an IoT network is very dynamic. Indeed, each object is independent and can turn on and off at any time. In addition, objects can move and each mobility is different in trajectory and speed, since it is specific to each device and linked to the object or the person carrying it. This dynamic raises an additional challenge to data routing and service placement. Indeed, how to send a message to an object without knowing where it really is? How to build a forwarding path between nodes if at any time one can appear or disappear from a radio neighborhood?
This aspect is also a challenge for the placement of services in edge computing in which we aim to deploy services as close as possible to objects, at the edge of the network. Services must therefore dynamically adapts to the mobility of objects and their needs without a priori knowledge of their movements.


## 1.1.3d Storing, processing and transporting data

The deployment of a growing number of connected objects (estimates suggest the number will rise
to [38 billion connected objects by 2025](https://www.mac4ever.com/actu/143604_il-y-aurait-22-milliards-d-objets-connectes-dans-le-monde-strategy-analytics))
will produce vast quantities of data (some estimates suggest there could be [175 billion terabytes of data by 2025](https://www.aucoeurdesinfras.fr/liot-ou-comment-traiter-le-volume-de-donnees-en-temps-reel/6247/)).

Most importantly, it must be possible to store all of this data in a way that is
both reliable and sustainable, such as in databases, for example. The experience acquired
since the early days of computing on these subjects, particularly on
database management systems, should make it possible to tackle
this issue. However, the volumes could be so large that it
may prove necessary for existing solutions to be improved.

The Internet of Things must meet the need of rapid access
to information. This information can’t always be taken directly
from the data itself: in many cases the data has to be processed
and cross-checked in order to extract high-quality, usable
information.
This processing can sometimes be complex, requiring substantial computational
capacities. Today, _cloud computing_ platforms
make it easy to access these processing resources (processing servers,
GPU cards, etc.) and should make it possible to produce processing results
in real-time.

Lastly, there are better ways of distributing processing resources
among the different components of the IoT chain: the most intensive processing
takes place in the _cloud_, less intensive processing can take place at a
middle level (on base stations, for example), and the most basic
processing can take place on the objects themselves.
Distributing resources in this way helps reduce the use of data
transport infrastructure (radio, wired technologies), thus increasing the number of objects
that can be connected.

## 1.1.3e The security of connected objects

All sensors deployed in smart buildings,
towns or people’s homes supply various different types of
data (temperature, brightness, pressure, acceleration, proximity, humidity, etc.), which is often highly sensitive (the presence of
individuals, for example).
If this data is intercepted, it can be exploited by malevolent
third parties, causing a whole host of problems:
errors
with the application or a total shut-down; physical harm caused to individuals,
industrial espionage with a view towards gaining a competitive advantage,
etc.

Protecting this data is a major concern for Internet of Things
applications if its long-term development is to be guaranteed.
This protection can take place on a number of levels:

* at the level of objects communicating with each other,
* at the level of the applications on the objects themselves,
* at the level of the hardware of the object.

The complexity of attacks (and the relevant protection) will vary
depending on the level of protection (communication, software materials, etc.)

This aspect will be covered in greater detail in module 6, and solutions will be
addressed in order to put protective mechanisms in place.

## 1.1.3f Societal problems

Aside from technical aspects, the Internet of Things is also faced with
societal and environmental issues.
Chief among these are the consequences of the deployment of IoT solutions
on everyday life.
Connected objects are becoming increasingly prevalent in
all aspects of our everyday lives.

Certain users, for example, through a lack of
awareness of how these objects work (voice assistants, home
automation systems), don't realise that they are sharing personal and sometimes
private information.
Meanwhile, the deployment of IoT solutions in urban areas with the goal of
making towns and cities smarter can feed back information on the use of
infrastructure by residents. These systems are
designed to improve the management of transport and infrastructure, to save
money and to improve community life.
However, even if the initial goals were laudable and worthwhile and the
data supposedly anonymous, there is nothing to guarantee that the way in which information
is corroborated won’t deanonymise this data.
There are, therefore, privacy issues linked to the Internet
of Things.

From an environmental point of view, meanwhile, the Internet of Things could have a serious
negative impact. As we mentioned previously, it is
expected that billions of connected objects will be deployed in the near future. What will be the environmental
impact of all these objects once they are no longer fit for
use? Will it be possible to repurpose them instead
of producing new ones?

References:  
[Is Your IoT Device Harming the Environment?](https://innovationatwork.ieee.org/iot-environmental-impact-ieee-wake-up-radio/)  
[Reducing the environmental impact of global IoT](https://www.itproportal.com/features/reducing-the-environmental-impact-of-global-iot/)
