# 1.1.2. The architecture of IoT solutions

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.1.2a Introduction](#112a-introduction)
- [1.1.2b Constrained objects](#112b-constrained-objects)
- [1.1.2c Gateways](#112c-gateways)
- [1.1.2d IoT platforms](#112d-iot-platforms)
<!-- TOC END -->

## 1.1.2a Introduction
IoT solutions typically implement **constrained objects or devices** (i.e. things) that pass their data through **gateways** through the network to reach servers that host **IoT platforms** and to which various applications are connected. We will therefore define these 3 layers.

Given the rapid development of the Internet of Things, it soon became vital to determine a standard architecture for IoT solutions.
It was with this in mind that the Internet Architecture Board (IAB) published RFC 7542[1]. This document describes four models of interactions common to the architecture of all IoT solutions:

* communication between objects: this model is based on wireless communication between two constrained objects,
* communication between objects and IoT platforms: in this model, data measured by sensors is sent to IoT platforms via a network,
* the communication of objects with gateways: this model enables data to be sent from sensors to IoT platforms via an intermediary,
* back-end data-sharing: this model is used to share data between several IoT solutions. It is based on the “programmable web” concept with APIs.

<figure>
    <img src="Images/IoT-arch.png" alt=""><br />
    <figcaption>Fig. 1: IoT solutions architecture[2]</figcaption>
</figure>

## 1.1.2b Constrained objects
Objects, whether these are sensors used to feed back physical data or actuators to act on the physical environment, are the basis of IoT solutions. These objects are where data come from. Generally speaking, these objects are limited in terms of their size and are battery-operated, reducing their level of autonomy. They operate using microcontrollers (MCUs), which also limits their processing capacities.

The role of these objects is to support specific tasks. They may be comprised of the following in terms of software:

1. An operating system for the IoT, tailored specifically for constrained objects and embedded systems
2. A hardware abstraction layer (HAL), simplifying access to the features of the MCU: flash memory, GPIOs, serial interface, etc.
3. A communication layer, enabling the object to communicate via a wired or wireless protocol such as Bluetooth, Z-Wave, Thread, CAN bus, MQTT, CoAP, etc.
4. Remote management, which can be used to control objects, to update firmware or to monitor battery levels

## 1.1.2c Gateways
A gateway is a relay point between a network of sensors and actuators and an external network. These may rely upon specific materials or extra features added to certain constrained objects.

Gateways can offer capacities for processing data and storage on the periphery of constrained objects (what’s known as _Edge Computing_), the goal being to tackle any problems linked to latency or network reliability. When it comes to connectivity between constrained objects, gateways will also be responsible for dealing with issues of interoperability.

## 1.1.2d IoT platforms
IoT platforms are the software solutions and services required in order to implement IoT solutions. These are run on servers hosted either in the cloud (such as AWS, Google Cloud, Microsoft Azure, etc.) or within the company’s own infrastructure. These solutions must be capable of adapting to scaling up at both a horizontal level (the number of connected objects) and a vertical level (the variety of the solutions offered). IoT platforms enable interconnectivity with companies’ existing information systems.

The key features of these platforms are as follows:

1. Connectivity and message routing
2. Registering and managing objects
3. Data storage and management
4. Incident management, data analysis and representation
5. Using an API to integrate applications


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<div>In this MOOC, we will be focusing on these first two layers: <b>constrained objects and gateways</b>.
<ul>
<li>From a <b>hardware</b> perspective, we will explore the field of embedded systems, explaining what constrained objects are comprised of: microcontrollers, sensors/actuators, data buses and radio chips.</li>
<li>From a <b>software</b> perspective, we will take a look at what solutions there are for programming constrained objects, before taking a closer look at an example with the embedded operating system RIOT.</li>
<li>From a <b>network</b> perspective, we will present the different wireless communication standards and different network layers currently available for the IoT.</li>
<li>From a <b>security</b> perspective, we will focus on the mechanisms that can be employed in order to guard against network or software attacks.</li>
</ul>
For each of these, you will see how solutions adapt to the limited capacities of the materials and tackle the issue of energy consumption.</div>
</div>

References:

[1] Architectural Considerations in Smart Object Networking: https://tools.ietf.org/html/rfc7452

[2] The illustration comes from a white paper published by the Eclipse IoT Working Group, entitled [The Three Software Stacks Required for IoT Architectures](https://www.yumpu.com/en/document/view/56749045/the-three-software-stacks-required-for-iot-architectures). Feel free to read this document, which provides a more detailed description of the software architectures in these 3 layers and the existing Open Source solutions._
