# 1.1.1. Qu'est-ce que l'Internet des objets ?



L'Internet des Objets constitue une vraie révolution dans le monde du numérique. Mais que signifie-t-il vraiment ? Selon l'Union internationale des télécommunications, l'Internet des objets (IdO ou IoT en anglais) est une « infrastructure mondiale pour la société de l'information, qui permet de disposer de services évolués en interconnectant des objets (physiques ou virtuels) grâce aux technologies de l'information et de la communication interopérables existantes ou en évolution ».
On peut aussi lui donner une définition plus simple avec un ensemble d'objets physiques connectés, possédant une identité numérique et capables de communiquer les uns avec les autres.
Ils forment d'une certaine façon une passerelle entre le monde physique et le monde virtuel.
L’IoT commence ainsi dans le monde physique avec par exemple des capteurs qui mesurent des données (température, pression atmosphérique, luminosité) qui sont ensuite transmises sur Internet grâce à la connexion et l’intégration des systèmes entre eux. Les données sont ensuite traitées et stockées pour être analysées et exploitées. L’évolution de l’IoT est rapide et [les prévisions pour le nombre d’objets IoT](https://www.ericsson.com/en/mobility-report/internet-of-things-forecast) en 2022 sont de l'ordre de 18 milliards (parmi presque 29 milliards d'objets connectés).
Nous assistons au déploiement massif d'un ensemble d'objets interconnectés dotés de capacités de communication, de détection et d'activation dont les applications sont multiples.
On retrouve l'utilisation de l'IoT dans tous les grands domaines d'application et on peut citer quelques exemples comme :

 * les villes intelligentes avec la gestion du traffic et la sécurisation de l'espace public,
 * l'agriculture connectée avec la surveillance médicale des animaux ou le suivi de la qualité du sol,
 * l'industrie du futur avec la surveillance des équipements et la maintenance prédictive,
 * la logistique avec le suivi des conteneurs dans le transport maritime.

<figure>
    <img src="Images/C034AA-M01-S01.png" alt="" width="750"><br />
    <figcaption>Fig. 1 : Domaines d'application de l'IoT</figcaption>
</figure>
