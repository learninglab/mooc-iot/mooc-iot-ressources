# 1.1.2. Architecture des solutions IoT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.1.2a Introduction](#112a-introduction)
- [1.1.2b Les objets contraints](#112b-les-objets-contraints)
- [1.1.2c Les passerelles](#112c-les-passerelles)
- [1.1.2d Les plateformes IoT](#112d-les-plateformes-iot)
<!-- TOC END -->

## 1.1.2a Introduction
Les solutions IoT mettent généralement en oeuvre des **objets contraints** qui font transiter leurs données par des **passerelles** à travers le réseau pour atteindre des serveurs qui hébergent des **plateformes IoT** et auxquelles se connectent différentes applications. Nous définirons donc ces 3 couches.

Au vu du développement rapide de l'internet des objets, il était important de définir une architecture type pour uniformiser les solutions IoT.
C'est dans ce cadre que le comité Internet Architecture Board (IAB) a édité la RFC 7542[1]. Ce document décrit quatre modèles d'interactions
communs dans les architectures des solutions IoT :

* la communication entre objets : ce modèle est basé sur une communication sans-fil entre deux objets contraints,
* la communication entre les objets vers les plateformes IoT : avec ce modèle les données mesurées par les capteurs sont envoyées vers les plateformes IoT via un réseau,
* la communication des objets vers une passerelle : ce modèle permet d'envoyer les données des capteurs vers les plateformes IoT via un intermédiaire,
* le partage de données en "back-end" : ce modèle permet de partager les données entre plusieurs solutions IoT. Il est basé sur la notion de « web programmable » avec les API.

<figure>
    <img src="Images/IoT-arch.png" alt=""><br />
    <figcaption>Fig. 1 : Architecture des solutions IoT[2]</figcaption>
</figure>


## 1.1.2b Les objets contraints
Les objets, qu'il s'agisse de capteurs pour remonter des données physiques ou d'actionneurs pour agir sur l'environnement physique, sont à la base des solutions IoT. Ce sont de ces objets que proviennent les données. Ces objets sont généralement contraints en taille et équipés de batteries, leur laissant une autonomie limitée. Ils fonctionnent avec un microcontrôleur (MCU), limitant aussi leurs capacités de traitement.

Ces objets ont pour rôle de supporter des tâches spécifiques. La partie logicielle qu'ils exécutent pourra comporter :

1. Un système d'exploitation pour l'IoT, particulièrement adapté pour les objets contraints et le domaine de l'embarqué
2. Une couche d'abstraction matérielle (Hardware Abstraction Layer - HAL), qui simplifie les accès aux fonctionnalités du MCU : mémoire flash, GPIOs, interface série, etc.
3. Une couche de communication, fournissant des solutions pour permettre à l'objet de communiquer via un protocole filaire ou sans fil comme Bluetooth, Z-Wave, Thread, CAN bus, MQTT, CoAP, etc.
4. Une gestion à distance, permettant de le contrôler, de mettre à jour son firmware, ou de surveiller son niveau de batterie

## 1.1.2c Les passerelles
Les passerelles (_gateway_ en anglais) sont le point de relais entre un réseau de capteurs et d'actionneurs et un réseau externe. Elles peuvent reposer sur du matériel spécifique ou des fonctionnalités supplémentaires ajoutées à certains objets contraints.

Une passerelle peut offrir des capacités de traitement des données et de stockage à la périphérie des objets contraints (on parle de _Edge Computing_) pour faire face à d'éventuels problèmes de latence et de fiabilité du réseau. Pour la connectivité entre objets contraints, la passerelle sera aussi en charge de traiter les problèmes d'interopérabilité.

## 1.1.2d Les plateformes IoT
Les plateformes IoT représentent les solutions logicielles et les services requis pour mettre en oeuvre les solutions IoT. Elles s'exécutent sur des serveurs, hébergés dans des infrastructures cloud (comme AWS, Google Cloud, Microsoft Azure, etc.) ou dans l'infrastructure de l'entreprise. Ces solutions doivent pouvoir s'adapter aux passages à l'échelle aussi bien au niveau horizontal (nombre d'objets connectés) qu'au niveau vertical (variété des solutions apportées). Les plateformes IoT permettent une interconnexion avec le système d'information existant de l'entreprise.

Les fonctionnalités-clés de ces plateformes sont :

1. La connectivité et le routage des messages
2. L'enregistrement des objets et leur gestion
3. La gestion et le stockage des données
4. La gestion des événements, l'analyse et la représentation des données
5. L'utilisation d'une API pour l'intégration des applications


<div class="ill-alert alert-grey">
<div>Dans ce MOOC, nous nous intéressons à ces deux premières couches : <b>les objets contraints et les passerelles</b>.
<ul>
<li>D'un point de vue <b>matériel</b>, nous entrerons dans le domaine de l'embarqué et détaillerons ce qui compose les objets contraints : micro-contrôleur, capteurs/actionneurs, bus de données et puces radios.</li>
<li>D'un point de vue <b>logiciel</b>, nous verrons quelles solutions existent pour programmer un objet contraint et étudierons en détail un exemple avec le système d'exploitation embarqué RIOT.</li>
<li>D'un point de vue <b>réseau</b>, nous vous présenterons les différentes couches réseaux et les standards de communication sans fil disponibles aujourd'hui pour l'IoT.</li>
<li>D'un point de vue <b>sécurité</b>, nous nous intéresserons aux mécanismes pouvant être mis en place pour se défendre contre des attaques de type réseau ou de type logiciel.</li>
</ul>
À chaque fois, vous verrez comment les solutions s'adaptent aux capacités restreintes du matériel et répondent à la problématique de la consommation énergétique.</div>
</div>

Références:

[1] Architectural Considerations in Smart Object Networking: https://tools.ietf.org/html/rfc7452

[2] Le schéma est issu d'un livre blanc publié par l'Eclipse IoT Working Group, intitulé [The Three Software Stacks Required for IoT Architectures](https://www.yumpu.com/en/document/view/56749045/the-three-software-stacks-required-for-iot-architectures). N'hésitez pas à lire ce document qui décrit en plus les architectures logicielles dans ces 3 couches et les solutions Open Source existantes.
