# 1.1.1. What is the Internet of Things?



The Internet of Things is a revolutionary development in the digital world. But what actually is it? According to the International Telecommunication Union, the Internet of Things (IoT) is a a "global infrastructure for the information society, enabling advanced services by interconnecting (physical and virtual) things based on existing and evolving
interoperable information and communication technologies.”
A simpler definition would be to say that it is a set of connected physical objects with digital identities which are capable of communicating with each other.
These objects form a gateway between the physical world and the virtual world.
The IoT begins in the physical world with sensors used to measure data (temperature, atmospheric pressure, brightness, etc.), which is then transmitted to the internet via the connection and integration of the systems with each other. This data is then processed and stored in order for it to be analysed and used. The IoT is evolving rapidly - [forecasts](https://www.ericsson.com/en/mobility-report/internet-of-things-forecast) indicate there will be somewhere in the region of 18 billion IoT objects by 2022 (among almost 29 billions of connected objects).
We are witnessing the mass deployment of a set of interconnected objects equipped with communication, detection and activation capacities, which can be used for a range of different applications.
IoT is used everywhere, with some examples including:

 * smart cities: traffic management and security in public spaces,
 * connected agriculture: medical surveillance on animals and soil quality monitoring,
 * industry of the future: equipment surveillance and predictive maintenance,
 * logistics: container tracking in maritime transport.

<figure>
    <img src="Images/C034AA-M01-S01.png" alt="" width="750"><br />
    <figcaption>Fig. 1: IoT application domains</figcaption>
</figure>
