
# 1.1.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez découvrir comment se définit l'Internet des Objets, quels sont ses principaux domaines d'application, ses composants matériels et logiciels, ainsi que ses enjeux technologiques.
</p>
</div>

Présentation en vidéo de l'Internet des Objets (IoT) : qu'est-ce que l'IoT et quelles sont ses caractéristiques ?
