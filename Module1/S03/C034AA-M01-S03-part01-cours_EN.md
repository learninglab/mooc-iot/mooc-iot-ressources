# 1.3.1. Smart watches

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.3.1a Introduction](#131a-introduction)
- [1.3.1b How it works](#131b-how-it-works)
- [1.3.1c BLE protocol](#131c-ble-protocol)
- [1.3.1d Watch battery life](#131d-watch-battery-life)
<!-- TOC END -->

## 1.3.1a Introduction

Smart watches are now widely used by athletes in order to track heart data (heart rate, blood pressure, etc.) during exercise or to count the number of steps taken over the course of a day.

How is this data exchanged between these watches and the internet? Which protocols are used? What constraints are there?

## 1.3.1b How it works

For this type of Internet of Things application, information acquired by sensors on watches is sent directly via radio, using a _Bluetooth Low Energy_ connection (also known as _Bluetooth LE_ or _BLE_), to smartphones, users’ mobile terminals.
This information is then available to consult directly on the mobile terminal via a dedicated application and/or on a website using a computer.
In this second example, data is sent by the terminal to an IoT platform through its internet connection: the terminal is connected to the internet via either an operator network (3G, 4G and soon 5G) or a local Wi-Fi network.
The protocols used in order to send this data to IoT platforms generally depend on applications: there is no real standard for this part of the chain.

<figure style="text-align:center">
    <img src="Images/C034AA-M01-S03.png" style="width:400px"><br />
    <figcaption>Fig. 1: General overview</figcaption>
</figure>

## 1.3.1c BLE protocol

Given that smart watches are targeted at the wider public, they must be easy to use with the terminals already used by potential users, using a radio protocol and applications which are compatible with both the watch and the mobile terminal.
For a number of years now, all smartphones have had BLE technology built-in, enabling them to communicate with other terminals or objects. As such, it is this method of communication which is used. For more information on the specifications of the BLE protocol, see https://www.bluetooth.com/

All you really need to know is that Bluetooth LE operates in the 2.4 GHz ISM band. Aside from this physical communication medium, the protocol specifies access profiles in order to ensure interoperability between different classes of objects. A full list of these profiles can be found in the _Generic Access Profile_, from the physical layer up to the _Generic Profile Attribute_ (or GATT).

<figure style="text-align:center">
    <img src="Images/bluetooth-profiles.png" alt="" style="width:400px"/><br/>
    <figcaption>Fig. 2: Bluetooth profiles</figcaption>
</figure>

Source: https://www.bluetooth.com/specifications/bluetooth-core-specification, Vol 0, Section 6

More specifically, the BLE specification outlines a set of GATT profiles/services designed to ensure interoperability between objects (in this case, between smart watches and smartphones).
A GATT profile introduces the concept of clients and servers and what actions are possible between clients and servers: read-only, modification, notification. In order to receive/access data, a client (a smartphone) must establish a connection with the server (the smart watch).

A list of all the GATT profiles and services outlined within the BLE specification can be found on this page:
https://www.bluetooth.com/specifications/gatt/.

In order to exchange data on heart rate, for example, the corresponding GATT profile is _HRP_(Heart Rate Profile).

Lastly, data exposed by the server are known as _attributes_. An _attribute_ is either a service (listed in the same location as GATT profiles) or a characteristic. Each characteristic contains a description, a unique value and descriptors used to qualify this value.

In our example, heart rate is exposed with the characteristic _Heart Rate Measurement_ (code _0x2A37_).

A full list of characteristics can be found on this page:
https://www.bluetooth.com/specifications/gatt/characteristics/

This feature enables any application operating on the smartphone and designed to display the heart rate supplied by a BLE sensor to process the data obtained from the smart watch via BLE by implementing the compatible profile GATT HRP, which will contain a  characteristic such as _Heart Rate Measurement_.

Other documentation resources for BLE: https://blog.groupe-sii.com/le-ble-bluetooth-low-energy/

## 1.3.1d Watch battery life

Given that watches normally have an ergonomic interface and a screen for their primary function (which is to tell the time), it is not really possible to obtain consumption as low as it is for other classes of objects with better self-sufficiency, but it is reasonable to expect self-sufficiency of at least 24 hours, or even several days. In all cases, users of these
watches will employ them on a sufficiently regular basis in order to be able to recharge them, as needed.
