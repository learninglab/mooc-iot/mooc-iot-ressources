
# 1.3.0. Introduction

Présentation en vidéo d'exemples d'application IoT : la montre connectée, le contrôle des besoins en eau des plantes, et la gestion de la qualité de l'air dans un bâtiment.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>
Dans cette séquence, nous vous présentons en détail 3 cas concrets d'applications IoT utilisant des technologies radio différentes et donc 3 manières différentes de communiquer avec Internet.
</p>
</div>

1. La **montre connectée**, largement répandue chez le grand public, présente le cas classique de l'**objet communiquant avec un smartphone ou un ordinateur** faisant office de passerelle vers Internet.

2. Des **capteurs de température et d'humidité du sol** déployés dans une culture pour contrôler les besoins en eau des plantes : ce cas présente l'utilisation des **réseaux cellulaires basse consommation et longue distance** pour envoyer les informations, en particulier le réseau **LoRaWAN**.

3. Des **capteurs mesurant la qualité de l'air dans un bâtiment** et communiquant avec le système de gestion technique du bâtiment pour contrôler le système de ventilation: ce cas présente le fonctionnement d'un **réseau de capteurs sans fil à courte portée**.
