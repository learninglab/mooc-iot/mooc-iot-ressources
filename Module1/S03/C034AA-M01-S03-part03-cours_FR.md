# 1.3.3. Un réseau de capteurs dans un bâtiment intelligent

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.3.3a Introduction](#133a-introduction)
- [1.3.3b Principe de fonctionnement](#133b-principe-de-fonctionnement)
- [1.3.3c Les protocoles existants](#133c-les-protocoles-existants)
- [1.3.3d Autonomie des capteurs](#133d-autonomie-des-capteurs)
<!-- TOC END -->

## 1.3.3a Introduction

Les objets connectés peuvent aussi être utilisés pour améliorer le suivi et le
contrôle d'une maison ou d'un bâtiment. L'objectif peut être d'optimiser leur
consommation énergétique en mesurant précisément la température en plusieurs
points du bâtiment pour mieux contrôler le chauffage (ou la climatisation), de
prévoir l'usure et donc la maintenance de certains équipements (on parle ici de
maintenance prédictive dans l'industrie 4.0) ou encore d'améliorer la qualité
de l'air en activant le système de ventilation si besoin.
Et cette liste est bien loin d'être exhaustive !


Cependant, dans un bâtiment existant, il est trop coûteux de devoir tirer de
nouveaux câbles pour installer ces nouveaux capteurs. C'est pour cela qu'on
privilégie les solutions utilisant des technologies de communication par
radiofréquence de courte portée avec des capteurs fonctionnant sur batterie :
ce genre de capteurs peut alors être déployé n'importe où dans le bâtiment,
dans les couloirs, dans les bureaux, dans les salles techniques, etc.

Le réseau de capteurs remonte les mesures des différents capteurs vers le
système de gestion technique du bâtiment ou une application dans le Cloud. De là
sont commandées les parties fonctionnelles du bâtiment : chaufferie,
ventilation, éclairage. Un système d'alerte peut aussi être déclenché si une
maintenance est nécessaire.

Comment fonctionne un réseau de capteurs ? Comment le connecter au système de
gestion technique d'un bâtiment ?

## 1.3.3b Principe de fonctionnement

Le but d'un réseau de capteurs est de compenser :
* l'absence d'infrastructure (pas de réseau filaire),
* la courte portée radio
en utilisant chaque capteur du réseau comme relais (on parle aussi de _noeud_ ou de _routeur_).

De cette manière, il est alors possible de transporter une information sur une distance
plus grande que la portée radio d'un seul capteur en effectuant plusieurs
_sauts_. Ce genre de réseau est également appelé réseau _mesh_.

Il suffit ensuite de placer une passerelle (aussi appelée _noeud puits_)
suffisamment proche du réseau pour router les données du réseau vers le réseau
interne du bâtiment voire même directement vers Internet.

Les données, une fois sur le réseau interne du bâtiment/Internet, peuvent
transiter jusqu'à des serveurs applicatifs, un utilisateur ou un système de
gestion technique de bâtiment et ainsi servir à prendre une décision : par exemple,
démarrer le système de ventilation si la qualité de l'air dépasse un certain
seuil.

<figure style="text-align:center">
    <img src="Images/wsn-architecture_EN.png" alt="" style="width:400px"/><br/>
    <figcaption>Fig. 1 : Schéma de principe d'un réseau de capteurs</figcaption>
</figure>

*Source: <a href="https://fr.wikipedia.org/wiki/R%C3%A9seau_de_capteurs_sans_fil">https://fr.wikipedia.org/wiki/R%C3%A9seau_de_capteurs_sans_fil</a>*

## 1.3.3c Les protocoles existants

Pour ce type d'installation, l'utilisation de la bande ISM 2.4GHz offre les
meilleurs débits et une portée maximum d'environ 100 mètres.

Il existe plusieurs solutions de réseau de capteurs de type mesh fonctionnant
dans cette bande comme par exemple BLE mesh, WiFi ou le 802.15.4.

La norme 802.15.4 a d'ailleurs été développée spécifiquement pour ce cas
d'utilisation et a été standardisé par l'IETF (_Internet Enginnering Task
Force_) pour les réseaux de capteurs à basse consommation.

C'est pourquoi nous nous intéresserons ici à ce protocole : il est tout à
fait adapté à une utilisation pour un bâtiment intelligent.

Pour plus de détails sur 802.15.4, voir https://fr.wikipedia.org/wiki/IEEE_802.15.4.

Comme nous le verrons plus en détail dans le module 4 de ce Mooc, les réseaux
802.15.4 fonctionnent sur le même principe que les réseaux IP avec un modèle en
couches. Certaines de ces couches ont dû être adaptées pour pouvoir être
utilisable avec des capteurs très contraints (contraints = très peu de mémoire,
quelques dizaines de kilooctets).

Au-dessus de la couche radio 802.15.4, on aura :

* une couche d'adressage pour attribuer une addresse IP (plus précisément IPv6) aux capteurs,
* une couche de routage pour le multi-sauts,
* une couche de transport,
* et une couche applicative.

Toutes ces couches s'appuient sur des protocoles adaptés aux réseaux de capteurs
utilisant des objets très contraints, c'est-à-dire disposant de capacités de
calcul très limitées (de l'ordre de quelques dizaines de MHz), de mémoires très
faibles (quelques kilos octets) et consommant très peu d'énergie.
L'avantage du modèle en couches est qu'il rend le réseau de capteurs
interopérable avec les réseaux informatiques conventionnels : un capteur peut
communiquer directement avec un serveur et vice-versa. En utilisant le même
protocole applicatif, il est donc possible de faire remonter des données des
capteurs jusqu'aux équipements qui gèrent le bâtiment.

## 1.3.3d Autonomie des capteurs

Dans un réseau 802.15.4, les capteurs restent en mode endormi la plupart du
temps, la latence de ce type de réseau est donc importante.
Même si cela engendre des contraintes de routage fortes - la topologie du réseau
devant se réadapter en fonction de l'état d'éveil des capteurs - l'autonomie
globale des capteurs peut aller jusqu'à plusieurs années.
