# 1.3.1. La montre connectée

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.3.1a Introduction](#131a-introduction)
- [1.3.1b Principe de fonctionnement](#131b-principe-de-fonctionnement)
- [1.3.1c Protocole BLE](#131c-protocole-ble)
- [1.3.1d Autonomie de la montre](#131d-autonomie-de-la-montre)
<!-- TOC END -->

## 1.3.1a Introduction

La montre connectée est aujourd'hui très utilisée par les sportifs pour suivre par exemple leurs données cardiaques (nombre de pulsations, tension, etc) pendant un effort ou pour compter le nombre de pas effectués au cours d'une journée.

Comment ces données sont-elles échangées entre la montre et Internet ? Quels protocoles sont utilisés ? Quelles sont les contraintes à respecter ?

<figure style="text-align:center">
    <img src="Images/C034AA-M01-S03.png" style="width:400px"><br/>
    <figcaption>Fig. 1 : Schéma de principe</figcaption>
</figure>

## 1.3.1b Principe de fonctionnement

Sur ce genre d'application de l'Internet des Objets, les informations acquises par les capteurs présents sur la montre sont directement envoyées par radio, en utilisant une connexion _Bluetooth Low Energy_ (aussi appelée _Bluetooth LE_ ou encore _BLE_), à un smartphone, le terminal mobile de l'utilisateur.
Toutes ces informations sont ensuite consultables directement sur le terminal mobile via une application dédiée et/ou depuis un site sur le web via un ordinateur.
Dans ce second cas, les données sont envoyées par le terminal vers une plateforme IoT en passant par sa connexion Internet : cette connexion à Internet s'effectue alors en utilisant un réseau d'opérateur (3G, 4G et bientôt 5G) ou un réseau local WiFi.
Les protocoles utilisés pour communiquer ces données vers les plateformes IoT sont en général dépendant des applications : il n'existe pas vraiment de standard pour cette partie de la chaîne.

## 1.3.1c Protocole BLE

La montre connectée étant destinée à un public large, elle doit pouvoir être facilement utilisable avec les terminaux que les potentiels utilisateurs utilisent déjà et donc utiliser un protocole radio et des applications compatibles entre la montre et le terminal mobile.
Depuis plusieurs années, tout smartphone dispose nativement de la technologie BLE pour communiquer avec d'autres terminaux ou objets. C'est donc ce moyen de communication qui est utilisé. Pour plus d'informations sur les spécifications du protocole BLE, voir https://www.bluetooth.com/

Pour simplifier, il faut retenir que le Bluetooth LE fonctionne dans la bande ISM des 2.4GHz. Puis au-dessus de ce support de communication physique, le protocole spécifie des profils d'accès pour garantir l'interopérabilité des différentes classes d'objets. L'ensemble de ces profils est défini dans le _Generic Access Profile_ de la couche physique jusqu'au _Generic Profile Attribute_ (ou GATT).

<figure style="text-align:center">
    <img src="Images/bluetooth-profiles.png" alt="" style="width:400px"/><br/>
    <figcaption>Fig. 2 : Les profiles Bluetooth</figcaption>
</figure>

*Source: <a href=https://www.bluetooth.com/specifications/bluetooth-core-specification>https://www.bluetooth.com/specifications/bluetooth-core-specification, Vol 0, Section 6</a>*

Plus précisément, la spécification BLE décrit un ensemble de profils/services GATT destinés à assurer l'interopérabilité entre les objets (dans notre cas, la montre et le smartphone).
Un profil GATT introduit la notion de client et de serveur et les actions possibles entre le client et le serveur : consultation (lecture), modification, notification. Pour recevoir/accéder à des données, un client (le smartphone) établit une connexion avec le serveur (la montre).

Pour information, tous les profils et services GATT décrits dans la spécification BLE sont listés sur cette page: https://www.bluetooth.com/specifications/gatt/.

Par exemple, pour échanger des données liées à la fréquence cardiaque, le profil GATT correspondant est _HRP_ (Heart Rate Profile).

Enfin, les données exposées par le serveur sont appelées _attributes_. Un _attribute_ est soit un service (listé au même endroit que les profils GATT) soit une caractéristique. Chaque caractéristique contient la description, la valeur (unique) et des descripteurs pour qualifier la valeur.

Dans notre exemple, la fréquence cardiaque sera exposée avec la caractéristique _Heart Rate Measurement_ (code _0x2A37_).

La liste des caractéristiques est décrite sur cette page : https://www.bluetooth.com/specifications/gatt/characteristics/

Grâce à ce fonctionnement, toute application fonctionnant sur le smartphone et destinée à afficher la fréquence cardiaque d'un capteur BLE sera en mesure de traiter les données obtenues depuis la montre connectée via BLE, car celle-ci implémente le profil compatible GATT HRP contenant une caractéristique de type _Heart Rate Measurement_.

Autres ressources de documentation pour BLE : https://blog.groupe-sii.com/le-ble-bluetooth-low-energy/

## 1.3.1d Autonomie de la montre

La montre possédant généralement un écran et une interface ergonomique pour son utilisation primaire (à savoir donner l'heure), il n'est pas vraiment possible d'obtenir une consommation aussi basse que pour d'autres classes d'objets plus autonomes mais on peut raisonnablement penser qu'une autonomie d'au moins 24h jusqu'à plusieurs jours est courante. Dans tous les cas, l'utilisateur de la
montre la manipule suffisamment régulièrement pour pouvoir la recharger au besoin.
