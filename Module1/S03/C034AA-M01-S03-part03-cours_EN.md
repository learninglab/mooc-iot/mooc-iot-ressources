# 1.3.3. Sensor networks in smart buildings

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.3.3a Introduction](#133a-introduction)
- [1.3.3b How it works](#133b-how-it-works)
- [1.3.3c Existing protocols](#133c-existing-protocols)
- [1.3.3d Sensor autonomy](#133d-sensor-autonomy)
<!-- TOC END -->

## 1.3.3a Introduction

Connected objects can also be used to improve the way in which
houses or buildings
are controlled and monitored. The objective may be to optimise their
energy consumption by precisely measuring the temperature at various
points in the building in order to better control heating (or air conditioning); to
anticipate wear and maintenance on certain items of equipment (what is known
as predictive maintenance in the context of industry 4.0); or even to improve air
quality by activating the ventilation system, should this prove necessary.
And this is far from an exhaustive list!


In existing buildings, however, it is too costly to have to draw
new cables in order to install new sensors. It is for this reason that
preference is given to communication technology using
short-range radio frequencies, with battery-operated sensors.
These types of sensors can be deployed anywhere in buildings:
in corridors, in offices, in machine rooms, etc.

Sensor networks feed measurements from different sensors back to
buildings’ technical management systems or an application in the cloud. These are
used to control the functional aspects of the building: heating,
ventilation, lighting, etc. An alert system can also be triggered
to indicate when maintenance is required.

How do sensor networks work? How are they connected to buildings’
technical management systems?

## 1.3.3b How it works

Sensor networks are designed to compensate :

* a lack of infrastructure (no wired network),  
* short radio ranges by using each sensor
in the network as a relay (also known as _nodes_ or _routers_).

In this way, it is possible to transmit information over longer
distances than the radio range of one single sensor through what are known as
_hops_. This type of network is also known as a _mesh_ network.

All that then has to be done is to position a gateway (also called a _well node_)
sufficiently close to the network in order to route data from the network towards the building’s
internal network, or even directly to the internet.

Once on the building’s internal network/the internet, this data can
then be sent to application servers, users or buildings’
technical management systems, and can thus be used to take decisions: e.g.
to start up ventilation systems in the event of air quality exceeding a certain
threshold.

<figure style="text-align:center">
    <img src="Images/wsn-architecture_EN.png" alt="" style="width:400px"/><br/>
    <figcaption>Fig. 1 : Wireless sensor network overview</figcaption>
</figure>

*Source: <a href="https://en.wikipedia.org/wiki/Wireless_sensor_network">https://en.wikipedia.org/wiki/Wireless_sensor_network</a>*

## 1.3.3c Existing protocols

For this type of installation, use of the 2.4 GHz ISM band offers the
best speeds and a maximum range of roughly 100 metres.

There are a number of mesh type sensor network solutions operating
within this band, such as BLE mesh, Wi-Fi or
802.15.4

802.15.4. is a standard that was developed specifically for this use
case, having been standardised by the IETF (_Internet Engineering Task
Force_) for low-energy consumption sensor networks.

This protocol is perfectly suited to use in smart buildings,
which is why we will be focusing on it here.

For more details on 802.15.4., please go to https://en.wikipedia.org/wiki/IEEE_802.15.4.

As we will see in greater detail in module 4 of this MOOC, 802.15.4.
sensors operate on the same principle as IP networks, with a multi-layer
model. Some of these layers had to be adapted in order to make them
compatible with highly constrained sensors (very low memory,
only a few dozen kilobytes).

Above the 802.15.4. radio layer, there is:

* an address layer for allocating IP addresses (more specifically IPv6) to sensors
* a routing layer for multi-hops
* a transport layer
* an application layer

All these layers rely on protocols designed for sensor networks
using highly constrained objects, i.e. objects with extremely
limited processing capacities (a few dozen MHz), very low
memory (a few kilobytes) and low energy consumption.
The advantage of the multi-layer model is that it makes sensor networks
interoperable with conventional IT networks, enabling sensors to
communicate directly with servers and vice-versa. By using the same
application protocol, it is possible to relay data from
sensors to the equipment used to manage the building.

## 1.3.3d Sensor autonomy

In 802.15.4. networks, sensors remain in standby mode for the majority of the
time, meaning that latency is high for this type of network.
Even if this leads to significant routing constraints - the topology of the network
has to readjust depending on whether or not the sensors are awake - generally speaking,
the sensors can remain self-sufficient for up to several years.
