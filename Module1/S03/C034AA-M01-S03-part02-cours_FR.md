# 1.3.2. Des capteurs pour l'agriculture connectée

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.3.2a Introduction](#132a-introduction)
- [1.3.2b Principe de fonctionnement](#132b-principe-de-fonctionnement)
- [1.3.2c Le réseau LoRaWAN](#132c-le-réseau-lorawan)
- [1.3.2d Autonomie des capteurs](#132d-autonomie-des-capteurs)
<!-- TOC END -->

## 1.3.2a Introduction

Pouvoir contrôler le taux d'humidité du sol dans un jardin ou, à plus grande échelle, sur une exploitation agricole est un cas d'utilisation classique des objets connectés. Dans ce cas d'usage, les données remontées par les capteurs vont pouvoir être utilisées pour simplement remonter une alerte en cas de sécheresse et/ou actionner des systèmes d'arrosage automatique.
Les données étant disponibles en temps réel, ce genre de système peut ainsi être utilisé pour optimiser l'arrosage de certaines cultures (par goutte à goutte) et donc réaliser des économies à la fois au niveau financier, qu'au niveau de la ressource en eau qui peut parfois être très limitée selon les régions (exemple: sécheresse dans la vallée centrale en Californie).

Plusieurs questions se posent pour déployer ce genre de système : comment faire pour remonter ces données depuis des zones éloignées, c'est-à-dire sur de grandes distances ? Quels sont les protocoles utilisés ? Quels sont les contraintes que l'objet doit respecter pour ce genre de système ?

## 1.3.2b Principe de fonctionnement

Pour ce genre d'application, la tendance actuelle est à l'utilisation de technologies cellulaires où l'objet communique avec une ou plusieurs passerelles elles-même connectées à Internet.
La communication entre l'objet et les passerelles s'effectue sur des bandes de fréquences permettant l'envoi de données sur une longue distance allant jusqu'à plusieurs kilomètres.
Dans les réseaux cellulaires, on trouve 2 cas de figures:

* **Quand la bande de fréquence appartient à un opérateur :** dans ce cas, l'émission de signal ne peut se faire qu'en respectant le protocole de communication pour lequel la bande est destinée et avec un accord commercial avec l'opérateur.
  C'est le même principe que celui de la téléphonie mobile. La bande des 900MHz, initialement allouée pour le GSM (la 2G) et aujourd'hui de moins en moins utilisée par la téléphonie, trouve dans l'Internet des Objets un nouveau domaine d'utilisation. Le protocole NB-IoT (Narrow Band IoT) réutilise cette bande partagée entre plusieurs opérateurs pour permettre de transmettre des données IoT moyennant un abonnement.

* **Quand la bande de fréquence est ouverte :** c'est le cas de la bande ISM qui peut être utilisée par n'importe qui, moyennant certaines restrictions définies par un organisme public de régulation. En Europe, cet organisme s'appelle l'_ETSI_ (European Telecommunications Standards Institute).
  Pour l'Europe, les bandes ISM généralement utilisées pour les applications
  liées à l'agriculture connectée sont les bandes des 433MHz et des 868MHz. Pour
  pouvoir émettre sur ces bandes de fréquences, il faut respecter 2 contraintes
  définies par l'ETSI et qui varient selon les bandes (et sous-bandes) utilisées :
  * la puissance d'émission ne doit pas dépasser un certain seuil,
  * la durée d'émission par heure, aussi appelée _duty cycle_, doit rester
    inférieure à un seuil. Cette limite est exprimée en % d'une heure.

  Deux principales technologies cellulaires  utilisent ces bandes ISM pour transmettre les données IoT : **SigFox** et **LoRaWAN**.

Dans tous les cas, l'objet communique par radiofréquences avec une station de base (ou passerelle), sur une bande de fréquence donnée et en utilisant un protocole de communication lié à la technologie utilisée. Ensuite, la station de base relaie les données à un serveur de l'opérateur à qui elle appartient.
La communication entre la station de base et le serveur utilise les protocoles de communication classiques de l'internet (réseaux IP).
Enfin, ce serveur se charge d'envoyer les données à son destinataire via un protocole applicatif prédéfini. Ce protocole garantit en général une certaine confidentialité des données et un accès facile à ces données (par exemple comme pour le web via HTTP, mais il existe de nombreux autres protocoles que nous ne détaillerons pas ici, comme MQTT).

Au niveau applicatif, pour ce genre de réseau, il n'y a pas de spécification particulière pour le formatage des données et donc pas de compatibilité entre 2 applications différentes sauf si elles connaissent a priori le format des données générées par l'objet.

## 1.3.2c Le réseau LoRaWAN

Un réseau LoRaWAN est un réseau cellulaire fonctionnant sur les bandes ISM 433MHz ou 868MHz en Europe. Toujours en Europe, sur ces bandes, le _duty cycle_ est en général de 1% mais il peut être supérieur ou inférieur sur certaines sous-bandes et la puissance d'émission ne peut pas dépasser 14dBm pour le 868MHz
ou 20dBm pour le 433MHz.
La technologie radio (définissant la modulation et le codage de l'information) utilisée pour communiquer entre les objets/capteurs et les stations de base s'appelle LoRa (pour _Long Range_) et permet d'envoyer de petites données avec des débits très faibles allant de 292 bits par seconde à 50 kilobits par seconde.
Le protocole LoRaWAN définit ensuite des spécifications au-dessus de la technologie LoRa pour connecter un objet à un réseau cellulaire et lui permettre d'envoyer et de recevoir des données.


La figure 1 montre l'architecture d'un réseau LoRaWAN :

* Les objets qui communiquent en LoRa avec des stations de base ou passerelles.
* Les passerelles qui sont connectées par le réseau classique Internet à un serveur de réseau. Ce serveur se trouve _dans le cloud_ et est géré par l'opérateur du réseau LoRaWAN.
* Les applications sont enfin branchées au serveur de réseau et permettent aux
  utilisateurs d'accéder aux données, soit via le web, soit via d'autres
  protocoles applicatifs comme MQTT.

<figure style="text-align:center">
    <img src="Images/C034AA-M01-S03_Devices.png" alt="" style="width:400px"/><br/>
    <figcaption>Fig. 1 : Schéma de principe d'un réseau LoRaWAN</figcaption>
</figure>

Dans un réseau LoRaWAN, on programme sur l'objet une liste d'identifiants et de
clés pour lui permettre de s'authentifier auprès du serveur du réseau. Cet
aspect est important pour permettre au serveur de réseau de déterminer si un
objet appartient bien à une application gérée par l'opérateur du réseau. Toute
donnée reçue par le serveur et n'appartenant pas à une application par
l'opérateur sera simplement rejetée.

Quelques mots sur l'opérateur du réseau : il en existe un grand nombre donc on
ne les listera pas tous ici. Par exemple en France, certains opérateurs comme Orange ou
Objenious (Bouygues Télécom) sont des sous-entités de grands opérateurs de
téléphonie mobile. Ils proposent donc une couverture quasi complète du
territoire et une facturation soit au nombre d'objets, soit au nombre de
messages.
Pour débuter, il est parfois plus simple d'utiliser des opérateurs dont l'accès
au réseau est ouvert, comme [Loriot](https://loriot.io) ou
[The Things Network](https://www.thethingsnetwork.org). Pour ce type d'opérateur, la
couverture est assurée par des bénévoles déployant leurs propres gateways. La
continuité de service n'est donc pas assurée. Cela dit, l'accès au réseau est
gratuit et beaucoup moins restrictif en terme de nombre de messages et d'objets.

## 1.3.2d Autonomie des capteurs

Dans un réseau LoRaWAN, on rencontre 3 classes d'objets:
* **les objets de classe A** sont les objets ayant la consommation d'énergie
  la plus faible. En effet, ils passent la plupart du temps dans un état
  endormi, avec une consommation d'énergie quasi nulle, et se réveillent
  périodiquement pour envoyer une donnée. Après l'envoi de la donnée (on parle
  d'_uplink_), ils ouvrent 2 fenêtres d'écoute pour recevoir du serveur de
  réseau d'éventuelles données (on parle dans ce cas de _downlink_).
  Cette classe d'objets a une latence très élevée du fait qu'ils ne peuvent
  recevoir une donnée qu'après un envoi. Leur avantage reste leur niveau de
  consommation et ils sont particulièrement adaptés aux systèmes de contrôle
  déployés dans des zones éloignées et vastes.

* **les objets de classe B** ouvrent périodiquement des fenêtres d'écoute et
  sont donc joignables plus souvent par le serveur de réseau. Cependant, ils
  consomment plus d'énergie puisqu'ils se réveillent plus régulièrement.

* **les objets de classe C** sont tout le temps réveillés et lorsqu'ils
  n'émettent pas de données, ils restent en écoute d'éventuels messages envoyés
  par le serveur de réseau. Leur avantage est qu'ils offrent une très faible
  latence : un message envoyé par le serveur sera reçu immédiatement par
  l'objet, par contre, ils consomment énormément d'énergie, ce qui empêche leur
  utilisation pour une application où les objets fonctionnent sur batterie.

On l'aura compris, pour une application d'agriculture connectée comme présentée
ici, seul un objet de classe A pourra être utilisé : en effet, il est important
de pouvoir déployer l'objet et d'être certain que son autonomie sera maximale.

De nos jours, certains microcontrôleurs consomment environ 1 micro ampère en
mode de fonctionnement minimal. Alimentés avec de petites batterie de 2000mAh,
cela leur garantit une autonomie de plusieurs dizaines d'années !
