# 1.3.0. Introduction

Video presentation of examples of IoT applications: smart watches, monitoring plant water requirements and managing air quality in buildings.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>
In this video, we will take a detailed look at 3 concrete examples of IoT applications using different types of radio technology and 3 different ways of communicating with the internet.
</p>
</div>

1. **Smart watches**, which have become commonplace, are a classic example of **objects communicating with smartphones or computers**, serving as a gateway to the internet. 

2. **Temperature and soil moisture sensors** deployed in farming in order to monitor plant water requirements: this presents an example of the use of **low-energy and long-distance cellular networks** for sending information, particularly **LoRaWAN** networks.

3. **Sensors measuring air quality in buildings** and communicating with the building’s technical management system in order to control the ventilation system: this example shows how **short-range wireless sensor networks** operate.
