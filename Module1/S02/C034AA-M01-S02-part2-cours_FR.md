# 1.2.2. Les caractéristiques

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.2.2a Débit, quantité de données, portée, énergie](#122a-débit-quantité-de-données-portée-énergie)
- [1.2.2b Couverture et fiabilité](#122b-couverture-et-fiabilité)
<!-- TOC END -->

Les principales caractéristiques à prendre en compte lors du choix d'une technologie radio sont les suivantes :

- le débit et la quantité de données à envoyer,
- la portée de communication,
- la consommation énergétique,
- la couverture,
- la fiabilité.

Certaines sont liées. En effet, par exemple, de façon générale, des fréquences hautes permettent un plus grand débit mais sur des distances plus courtes.
Nous détaillons ces caractéristiques en citant certaines technologies radio. Nous donnerons plus tard, en [section 1.3](https://fun-mooc.fr/courses/course-v1:inria+41020+session03/jump_to_id/9da8b279013746d69680df6797452283) des exemples d'application qui utilisent telle ou telle technologie.

## 1.2.2a Débit, quantité de données, portée, énergie

Chaque technologie de communication radio se caractérise principalement par sa portée et son débit.
Ces derniers sont généralement liés aux bandes de fréquences qu'elles utilisent. Plus les fréquences sont hautes, plus la portée est courte mais plus le débit est élevé.
Egalement, plus la portée est grande ou plus le débit est élevé, plus la consommation énergétique est grande.
La figure ci-dessous positionne les technologies radio les plus connues en fonction de ces caractéristiques.

<figure>
    <img src="Images/debitportee.png" alt="Lien débit / portée" width="3750"><br />
    <figcaption>Fig. 1 : Lien débit / portée</figcaption>
</figure>

Avant de choisir une technologie de communication radio, il faut donc considérer tous ces critères en fonction des besoins de l'application. Le débit et la portée de communication sont importants mais ne sont pas les seuls critères qui doivent guider un choix.
En effet, si un appareil communicant possède une forte autonomie énergétique (possibilité de le recharger souvent ou alimenté en permanence), on négligera l'aspect consommation.
Si, au contraire, il repose sur une batterie qu'on peut difficilement recharger ou remplacer, l'aspect consommation sera primordial dans le choix de la technologie.
De la même façon, on choisira une technologie en fonction de la quantité de données que l'on doit envoyer. Si les données sont nombreuses ou imposantes (comme un flux vidéo par exemple), une technologie bas débit telle que LoRA ou SigFox est à éviter.

## 1.2.2b Couverture et fiabilité

Comme évoqué dans la section précédente, certaines bandes de fréquences sont nécessairement sous l'autorité d'un opérateur qui contrôle les différents accès au canal de communication et assure une certaine fiabilité des communications.
Certaines technologies bien qu'utilisant les bandes ISM sont également opérées. C'est le cas de SigFox, LoRA, NB-IoT et parfois même du WiFi. En effet, un opérateur tel que l'opérateur français Orange vous proposera également du WiFi ou du LoRA, même si chacun est libre de déployer son propre réseau WiFi ou LoRA.

Passer par un opérateur apporte donc de grands avantages mais cela implique qu'il couvre la zone dans laquelle votre réseau doit être déployé. On parle de couverture. Si l'infrastructure cellulaire, LoRA, WiFi ou autre, n'est pas déployée, vous ne pourrez pas utiliser le réseau de l'opérateur, il faudra donc envisager de déployer vous-même votre infrastructure et/ou d'utiliser des routages adhoc multisauts.

Ceci a bien évidemment un coût supplémentaire à intégrer dans le choix de la technologie.
Un particulier ne pourra pas déployer une infrastructure pour une technologie opérant sur des fréquences sous licences (telles que les technologies cellulaires). Le choix se limite donc aux technologies utilisant les bandes ISM permettant le déploiement d'une nouvelle infrastructure telles que le WiFi, LoRA ou les réseaux adhoc (ZigBee, Zwave, etc).
