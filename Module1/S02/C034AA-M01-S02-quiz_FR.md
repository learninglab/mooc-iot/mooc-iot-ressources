# Question 1.2.1
Quelles sont les caractéristiques d’une technologie radio qui peuvent influer sur le choix de l’une d’elle ?
[X] Sa portée
[X] Son débit 
[X] Sa couverture 
[X] Sa consommation d’énergie 
[X] Sa fiabilité 


# Question 1.2.2
Cochez les affirmations exactes concernant les fréquences ISM :
[X] Elles sont libres d’utilisation 
[ ] Elles ne permettent que des technologies ayant de hauts débits 
[X] Elles sont parfois opérées. 
 

# Question 1.2.3
Quelles sont les affirmations correctes ? 
[ ] Wifi est une technologie sous licence 
[ ] LoRA est une technologie plus performante que ZigBee 
[ ] SigFox est une technologie longue portée 
[X] BlueTooth utilise des licences libres 


# Question 1.2.4
Quelles est la technologie la plus performante ?
[ ] LoRA
[ ] Wifi
[ ] 5G 
[ ] Une autre 
[X] Aucune, ça dépend


# Question 1.2.5
Vous souhaitez déployer une application qui surveille les données météorologiques de sites isolés. Vos données ne représentent que quelques octets que vous souhaitez collecter une fois par jour. Quelle serait la solution la plus adaptée ? 
[ ] Wifi 
[ ] 5G 
[X] Une technologie LPWAN
[ ] Bluetooth 

