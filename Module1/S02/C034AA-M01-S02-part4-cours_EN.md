# 1.2.4 Overview and comparative assessment

To summarise, no technology is intrinsically better than any other. Different criteria have to be taken into consideration depending on application needs, chief among these being speed, range, power consumption and the type of infrastructure.

The table below provides a comparative assessment based on these criteria.

| Technology | energy consumption<sup>*</sup>       |     speed     |        range | infrastructure | operator/autonomous |
| :------------ | :------------: | :------------- | :------------- |  :------------- |  :-------------: |
| **RFID/NFC** | **** | NFC: 106 kbits/s to 424 kbits/s | between 10 cm and 100m | point-to-point | autonomous |
| **Wi-Fi** | * | 1.3 Gbits/s | > 100m |  star| autonomous |  
| **Bluetooth** | *** | 1 Mbit/s | > 100m | point-to-point, mesh | autonomous |  
| **Zigbee** | **** | 256 kbit/s | > 100m | point-to-point, star, mesh | autonomous |  
| **4G** | * | 600 Mbits/s | > 30km | cellular | operator |  
| **NB IoT** | *** | 250 kbit/s | > 30km | cellular | operator |  
| **Sigfox** | **** | roughly 800 bits/s | > 10km | cellular | operator | 
| **LoRa** | **** | between 250 and 5470 bit/s | > 10km | point-to-point, star | both |

<sup>*</sup> _: **** means the lowest power consumption and * means the highest power consumption_

The following diagram will help you to choose the most appropriate technology for your application. In many cases, more than one solution is possible, even if you don’t necessarily select the ideal solution, provided the chosen technology delivers the desired service.

<figure>
    <img src="Images/choixTechno-EN.png" alt="What technology to choose?" width="850"><br />
    <figcaption>Fig. 1: Technology selection aid</figcaption>
</figure>
