# What radio technology for what application ?


There are a large number of radio technologies, each with its advantages and drawbacks, and each one being the most appropriate for a given application. So how do you know which one to choose?
This is what we will try to see according to different application needs.

To be able to answer this question, we first need to understand some of the characteristics of radio technologies such as the frequencies they use, the bit rates they offer, the ranges they reach, and so on.

## Sommaire
* [Frequency bands](#Partie-1)
* [Characteristics](#Partie-2)
* [Technologies](#Partie-3)
* [To sum up](#Partie-4)


## Frequency bands


These radio technologies have in common the fact of using radio frequencies. The radio spectrum is highly regulated and most frequency ranges are reserved (police, military, aeronautical, meteorological, etc.) and licensed (mobile telephony, etc.).
In France, it is the [ARCEP] (www.arcep.fr) (Regulatory Authority of Electronic Communications and Post) which is in charge of this regulation. The distribution of the spectrum in France is available here: https://www.anfr.fr/gestion-fr-frequences-sites/tnrbf/frise-interactive/. These attributions vary from one country to another.

However, there are some free frequency ranges. These are the ISM bands (Industry, Science and Medical).

### ISM bands


There are several ISM frequency bands [^ 1], all over the spectrum, the most popular being :

* 2,4GHz (WiFi, BlueTooth, micro-wave oven, etc.),
* 5GHz (WiFi),
* 433 MHz (babyphones, alarms, home monitoring, LoRA etc.), 868 MHz (home monitoring,  SigFox, LoRA, etc.)


[^1]: See also https://www.ebds.eu/applications-docs/documentation/fréquences-libres-en-france/

![ISM](Images/ISM.png)

The advantage of ISM bands is that they are free to use. However, they are subject to regulations in terms of transmission power and time of use, which vary according to the band concerned. For example, for the low band, the transmission time is limited to 1% of the time per hour, or 36s per hour.
The disadvantage of ISM bands is that they are very busy. Two nearby devices that communicate at the same time on the same frequency, even if they use different protocols (such as BlueTooth and WiFi), can interfere with each other. Frequencies are a rare shared resource.

### Under-licensed bands

To ensure that a communication will not be scrambled, it is necessary to be able to control the entities authorized to communicate on the same frequencies and, for this purpose, to use licensed frequency bands.
This is what telephone companies do: they buy licenses, in other words, rights to use exclusively a part of the spectrum of radio. Thus, only their subscribers can use these frequencies (via the SIM card which controls the use of this or that frequency) [^2].

[^2]: When changing phone operators, you often need to "unlock" your phone. Indeed it is "blocked" on the frequency of an operator. This frequency is not the same as that of the new operator, so you have to allow the phone to connect to a new frequency range. Operators then manage the allocation of resources among their subscribers.


![frequencies en France](Images/frequences.png)

The advantage of using licensed frequencies, and thus to pass via an operator, is that the latter guarantees a quality of service and a good connection. The disadvantage is that you have to subscribe a contract and depend on an operator and its infrastructure.



## Characteristics

The main features to consider when choosing a radio technology are:
- the rate and quantity of data to be sent,
- the range of communication,
- the energy consumption,
- the coverage,
- the reliability.

Some are related. In fact, for example, in general, high frequencies allow greater throughput but over shorter distances.

### Throughput, data rate, range, energy

Each radio communication technology is characterized mainly by its range and throughput.
These are usually related to the frequency bands they use. The higher the frequencies, the shorter the range but the higher the rate.
Also, the greater the range, the higher the flow, the higher the energy consumption.
The figure below positions the most known radio technologies according to these characteristics.

![Relation throughput / range](Images/debitporteeEN.png)

Before choosing a radio communication technology, all these criteria must be considered according to the needs of the application. The flow and the range of communication are important but they are not the only criteria that should guide a choice.
Indeed, if a communicating device has a high energy autonomy (possibility of recharging or continuously supplied), we neglect the consumption aspect.
If on the other hand it is based on a battery that can hardly be recharged or replaced, the consumption aspect will be paramount in the choice of technology.

In the same way, one will choose a technology according to the quantity of data which one must send. If the data is numerous or imposing (like a video stream for example), a low-speed technology such as LoRA or SigFox is to be banned.

### Coverage and reliability

As mentioned in the previous section, certain frequency bands are necessarily under the authority of an operator who controls the various accesses to the communication channel and ensures a certain reliability of the communications.
Some technologies, while using ISM bands, are also operated. This is the case of SigFox, LoRA, NB-IoT or WiFi. Indeed, an operator such as the French operator Orange will also offer you WiFi or LoRA even if everyone is free to deploy their own WiFi or LoRA network.

Going through an operator brings great benefits but this means that it must cover the area in which your network is to be deployed. We talk about coverage. If the cellular infrastructure, LoRA, WiFi or other, is not deployed, you will not be able to use your network, so you should consider deploying your infrastructure yourself or using multihop adhoc routing.

This obviously has an additional cost to integrate into the choice of technology.
An individual will not be able to deploy an infrastructure for technology operating on licensed frequencies (such as cellular technologies). The choice is therefore limited to technologies using ISM bands for the deployment of a new infrastructure such as WiFi, LoRA or adhoc networks (ZigBee, Zwave, etc.).


## Technologies

There are a very large number of radio communication technologies. We will not be exhaustive, especially since new ones appear every day.
We will therefore mainly highlight the most used today.
We will not go into the details of these technologies in this MOOC because several other MOOCs have been dedicated to them and will be more complete. If you are interested, we invite you to visit these other resources.

We will classify the different technologies into two broad categories:
- short-range technologies
- long-range technologies


### Short Range technologies

Short-range technologies generally allow the establishment of Local Area Networks (LANs). They are, for the most part, associated with high speeds, this is the case of WiFi for example.
Short-range technologies are mostly used:
* by Internet applications (WiFi),
* for point-to-point information exchanges or access authorizations (RFID, NFC, Bluetooth),
* for home automation (ZigBee, ZWave),
* or for connected objects (Bluetooth, WiFi, etc.).


Here are some interesting links to find out more:
- [Local Area Networks (LAN)](https://www.fun-mooc.fr/courses/course-v1:univ-toulouse+101008+session01/about)
- [WiFi networks](https://www.dailymotion.com/video/x43ldag)
- [Connected objects](https://www.fun-mooc.fr/courses/course-v1:univ-toulouse+101003+session03/about)



### Long Range technologies

Long-range technologies have two main types of applications:
- mobile telephony and its derivatives (SMS, mobile applications, etc.),
- the collection of IoT information.

These two applications differ mainly in the needs in terms of data rates and QoS (latency, jitter, etc.). For example, mobile telephony requires very low latency and jitter [^ 3], while the recovery of environmental data can lead to longer delays.

In the first case, one will prefer cellular technology, while in the second case, LPWAN technologies can meet the needs.

[^3]: Jitter is the variation of latency over time.

#### Cellular technologies

The technologies that we call cell phones are associated with mobile technologies, from 2G to 5G, through intermediate generations (edge, LTE, etc.).
To get to know more about these technologies, we invite you to visit the following links and MOOCs:

- [2G, 3G, 4G](https://www.youtube.com/watch?v=BOvXV5rreDE)
- [Understand 4G](https://www.fun-mooc.fr/courses/MinesTelecom/04010/session01/about)
- [5G](http://blogs.univ-poitiers.fr/f-launay/)
- [5G](https://www.coursera.org/lecture/smart-device-mobile-emerging-technologies/4-7-5g-architecture-6KzD8) _(On line course)_

#### LPWAN (Low Power Area Network) technologies

LPWAN networks are networks offering long ranges but at the cost of low rates and use of the legislatively regulated medium.
There are several LPWAN technologies, the best known today being SigFox, LoRA / LoRAWAN, Ingenu, Weightless, NB-IoT and LTE-M.
These technologies differ in the frequencies they use and their access protocols to the medium (MAC and PHY).
Some are managed by operators, others can be deployed by the individual, so they differ in terms of coverage (not all operators cover the same areas).
Some LPWAN technologies allow multi-hop routing and mesh networking, while others only allow data to be sent to the operator's base station (star topology).

The table below summarizes the main differences between the main LPWAN networks:

|  | SigFox | LoRA | Ingenu |  Weightless (W/N/P) | NB-IoT |
| ------ | ------ | ------ | ------ |  ------ | ------ |
| Frequencies | 868MHz (EU) | 433MHz & 868MHz (EU) | 2,4GHz | W : 470-790MHz  | 200kHz |
||902MHz (US) | 915MHz (US) 430MHz (Asia)  | |N&P: 868 MHz (EU) 915 MHz (EU) |  |
| Range | 10-50km | 5-50km| 15km| 5 km (W), 3km (N), 2km (P)| 5-10km |
| Number of channels  | 360 | 10 (EU) 80 (US) | 40| 24| |
| Topologies |star | star, mesh | star, tree | star | star|


To go further :

- [LPWAN technologies](https://www.youtube.com/playlist?list=PLnMKNibPkDnECerjxSNJzMJ9njyqovVB9)

- [LPWAN et 5G](https://www.telecom-evolution.fr/fr/formations-courtes/technologies-de-connectivite-longue-portee-lpwan-lte-m-nb-iot-5g)



## To sum up

To summarize, we note that there is no better technology than another in the absolute. Different criteria are to be taken into account according to the needs of the application, in priority flow, scope, consumption, coverage, legislation or operator costs.
The following diagram will help you choose the most appropriate technology for your application. Note that in some cases, you can choose a different technology, there is no unique solution, as soon as the expected service is achieved.

![What technology to choose?](Images/choixTechnoEN.png)
