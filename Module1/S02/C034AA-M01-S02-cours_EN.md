# 1.2.0. Introduction
There are many different types of radio technology, each with their own advantages and disadvantages and each better suited to specific applications. With that in mind, how can we know which application to choose?

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>When it comes to selecting the most appropriate type of radio technology, we first need to understand some of the characteristics of radio technology, such as the frequencies they use, the speeds they offer, the ranges they provide, etc.
This is what we will try to show in this sequence for different application needs.
</p>
</div>