# 1.2.4 Synthèse et bilan comparatif

Pour résumer, on retiendra qu'il n'existe aucune technologie meilleure qu'une autre dans l'absolu. Différents critères sont à prendre en compte en fonction des besoins de l'application, en priorité le débit, la portée, la consommation d'énergie et le type d'infrastructure.

Le tableau ci-dessous dresse un bilan comparatif selon ces critères.

| Technologie | consommation<sup>*</sup>       |     débits     |        portée | infrastructure | opérateur/autonome |
| :------------ | :------------: | :------------- | :------------- |  :------------- |  :-------------: |
| **RFID/NFC** | **** | NFC: 106 kbits/s à 424 kbits/s | entre 10 cm et 100m | point à point | autonome |
| **Wi-Fi** | * | 1,3 Gbits/s | < 100m |  étoile| autonome |  
| **Bluetooth** | *** | 1 Mbit/s | < 100m | point à point, maillé | autonome |  
| **Zigbee** | **** | 256 kbit/s | < 100m | point à point, étoile, maillé | autonome |  
| **4G** | * | 600 Mbits/s | > 30km | cellulaire | opérateur |  
| **NB IoT** | *** | 250 kbit/s | > 30km | cellulaire | opérateur |  
| **Sigfox** | **** | environ 800 bits/s | > 10km | cellulaire | opérateur | 
| **LoRa** | **** | de 250 à 5470 bit/s | > 10km | point à point, étoile | les deux |

<sup>*</sup> _: **** indique une consommation faible et * indique une consommation élevée_

Le diagramme suivant vous aidera à choisir la technologie la plus appropriée pour votre application. Dans beaucoup de cas, plusieurs solutions sont bien sûr possibles, même si vous ne choisissez pas forcément la solution idéale, tant que la technologie choisie rend le service désiré.

<figure>
    <img src="Images/choixTechno.png" alt="Aide au choix de technologie" width="850"><br />
    <figcaption>Fig. 1 : Aide au choix de technologie</figcaption>
</figure>
