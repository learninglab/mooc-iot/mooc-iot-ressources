# 1.2.2. Specifications

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.2.2a Speed, data quantity, range, energy](#122a-speed-data-quantity-range-energy)
- [1.2.2b Coverage and reliability](#122b-coverage-and-reliability)
<!-- TOC END -->

The most important aspects to take into consideration when choosing between different types of radio technology are as follows:

- the speed and the quantity of the data to be sent
- the communication range
- energy consumption
- coverage
- reliability

Some of these are linked. Generally speaking, higher frequencies enable greater speeds, but over shorter distances. We detail below these characteristics and cite some technologies that meet them. We will see later in [Section 1.3](https://fun-mooc.fr/courses/course-v1:inria+41020+session03/jump_to_id/9da8b279013746d69680df6797452283) some applications that use some or other technology.


## 1.2.2a Speed, data quantity, range, energy

Different types of radio communication technology have different ranges and speeds.
These both tend to be linked to the frequency bands they use. The higher the frequency, the shorter the range but the higher the speed.
Similarly, the bigger the range or the higher the speed, the more energy will be consumed.
The figure below outlines these specifications for the most widely-used types of radio technology.

<figure>
    <img src="Images/debitportee-EN.png" alt="Speed / Range link" width="750"><br />
    <figcaption>Fig. 1: Speed / Range link</figcaption>
</figure>

Before making a choice in terms of radio communication technology, you therefore have to consider all these criteria, taking application needs into account. The speed and the range of communication are both important, but are not the only criteria that should guide your choice.
If a communicating appliance is energy self-sufficient (i.e., if it can be recharged often or permanently supplied), then the consumption aspect can be overlooked.
On the other hand, if it uses a type of battery that is difficult to recharge or replace, then consumption will be a key factor in determining the choice of technology.
Similarly, the quantities of data to be sent must also be taken into account. For high quantities of data (such as video streams, for example), low speed options such as LoRA or SigFox can be ruled out.

## 1.2.2b Coverage and reliability

As discussed previously, operators control access to communication channels for certain frequency bands, guaranteeing reliable communication.
Certain types of technology using ISM bands are also operated. This is the case for SigFox, NB-IoT and, sometimes, Wi-Fi. An operator such as the French operator Orange will also offer you Wi-Fi or LoRA, although individuals are also free to set up their own Wi-Fi or LoRA network.

Going through an operator has considerable advantages, but the zone in which your network is to be deployed must be covered by the operator. This is called coverage. If the cellular infrastructure - LoRA, Wi-Fi, etc. - is not deployed, you will not be able to use its network, and instead will need to consider deploying your own infrastructure or using ad-hoc multi-hop routing.

This will incur an additional expense when it comes to selecting the type of technology.
Individual customers will not be able to deploy infrastructure for technology operating on licensed frequencies (like cellular technology). The choice is therefore limited to technology using ISM bands enabling the deployment of new infrastructure, such as Wi-Fi, LoRA or ad-hoc networks (ZigBee, Zwave, etc.).
