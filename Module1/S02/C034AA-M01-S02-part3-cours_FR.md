# 1.2.3. Les technologies

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.2.3a Les technologies courte portée (short range)](#123a-les-technologies-courte-portée-short-range)
- [1.2.3b Les technologies longue portée (long range)](#123b-les-technologies-longue-portée-long-range)
<!-- TOC END -->

Il existe un très grand nombre de technologies de communication radio. Nous ne serons donc pas exhaustifs, d'autant plus que de nouvelles apparaissent régulièrement. Nous allons donc principalement mettre en avant les plus utilisées aujourd'hui.

On distingue deux groupes de technologies radio selon leur portée : les technologies **courte portée** radio avec une portée inférieure à 100 m et **longue portée** radio avec une portée supérieure à 1 km.
En fonction de l’application envisagée, la portée radio est souvent déterminante pour envisager un déploiement extérieur ou intérieur dans un grand bâtiment sans la contrainte de déployer une infrastructure lourde.

## 1.2.3a Les technologies courte portée (short range)

Les technologies à courte portée permettent généralement l'établissement de réseaux locaux (ou LAN - Local Area Network). Elles sont, pour la plupart, associées à de hauts débits, c'est le cas du WiFi par exemple.
Les technologies courte portée sont surtout utilisées :

* par des applications Internet (WiFi),
* pour les échanges d'information point à point ou les autorisations d'accès (RFID, NFC, Bluetooth),
* pour la domotique (ZigBee, ZWave),
* ou encore pour les objets connectés (Bluetooth, WiFi, etc.).

Voyons en détail quelques unes de ces technologies.

### Wi-Fi
Le Wi-Fi est un protocole de communication sans fil standardisé par la norme
IEEE 802.11.  

Il utilise la bande ISM des 2,4 GHz et 5 Ghz. Les premières versions 802.11a et 802.11b apparues à la fin des années 90 proposent des débits théoriques de 11 Mbits/s.

La version actuellement commercialisée 802.11ac offre des débits jusqu’à 1,3 Gbits/s théorique. La future version 802.11ax sera 20% plus rapide.

Historiquement lié aux ordinateurs portables, le Wi-Fi s’est démocratisé à l’ensemble des appareils mobiles communicants. Le réseau sans fil fonctionne principalement en étoile, reliant une station à un point d’accès. Le point d’accès est généralement connecté sur un backbone filaire ethernet pour router le trafic.

D’autres approches ont tenté une infrastructure de type réseaux maillés sur un backbone sans fil sur une bande de fréquence dédiée (5Ghz) (réf Roofnet). L’inconvénient principal du Wi-Fi reste sa consommation d’énergie qui impose une batterie conséquente. La norme 802.11ah (2017) optimisée pour réduire la consommation d’énergie dans le cadre d’application IoT n’a pas réussi à s’imposer face aux autres technologies du marché.

### Bluetooth
En parallèle du Wi-Fi, le Bluetooth apparaît à la fin des années 90.  Il est
porté par le consortium Bluetooth SIG (Special Interest Group).

Son but initial est de relier un terminal de type téléphone à un ordinateur. En 1999 sort la version 1.0 et le premier téléphone compatible. Tout comme le Wi-Fi, il utilise la bande ISM des 2,4 GHz pour une portée de 100m mais avec des canaux d’une largeur beaucoup plus petite, induisant de ce fait une bande passante beaucoup plus faible inférieure à 1 Mbits/s.

Potentiellement, ces deux technologies radio risquent de s’interférer à cause des recouvrement des canaux. La norme spécifie de nombreux profils recouvrant des cas d’usages variés (modem, transfert de
fichier, casque audio). L’usage courant du bluetooth est complètement
décentralisé et ne nécessite pas une infrastructure lourde. La version 4.0 ou BLE  (Bluetooth Low Energy) réduit énormément la consommation d’énergie.

De plus en plus d’objet connectés autonome adoptent cette technologie pour s’interfacer avec des terminaux mobiles. Cependant, la connectivité est dépendante de la présence de la portée radio entre le terminal et l’objet.

### Zigbee
Le Zigbee est un standard haut niveau basé sur la norme IEEE 802.15.4 (Low-Rate
Wireless PANs) apparue au début des années 2000.

Son but est de proposer un réseau sans fil à bas débit et faible consommation. Tout comme le Wi-Fi et le Bluetooth, il opère également sur la bande ISM des 2,4 GHz pour une portée de maximum 100m. Il peut fonctionner sur des topologies pair-à-pair ou en étoile.
Le Zigbee est porté par la Zigbee Alliance qui permet de certifier les produits respectant les spécifications du standard. De nombreux profils existent pour des applications comme la domotique (lumière, chauffage), capteurs médicaux,
détecteurs de fumée.

L’avantage du Zigbee est de pouvoir créer un réseau maillée
entre objets afin d’étendre la surface de couverture de l’application déployée.
Généralement une passerelle Zigbee/IP permet d’échanger l’information de manière permanente, en temps réel entre les objets et un cloud.

### RFID, NFC
Le RFID (Radio Frequency IDentification) est une technologie d’échange
d’informations sans fil entre un lecteur et un tag. Le lecteur envoie une onde
électromagnétique qui va télé-alimenter le tag pour envoyer une réponse en
retour, généralement un identifiant stockée dans une petite mémoire embarquée.
Le lecteur peut également écrire une donnée dans le tag. Ce dernier peut avoir
différents mode de fonctionnement : passif, semi-actif ou actif et atteindre des
portées radio de 10m à 200m.

L’utilisation principale de cette technologie est
l’identification et la traçabilité des objets. Le coût d’un tag permet de
marquer à grande échelle les objets que l’on souhaite suivre. Le cas d’usage le
plus connu est l’antivol apposé sur les produits de consommation vendus en
magasin. Le NFC (Near Field Communication) autorise des communications sur une
distance courte de moins de 10 cm.

De nombreux smartphones et tablettes sont équipés de cette technologie qui permet trois types d’échanges : l’émulation de
carte (paiement, transport), le mode lecteur, et le mode pair à pair. La bande
passante varie de 106 kbits/s à 424 kbits/s . Les échanges sécurisés à courte
portée autorise des applications comme les transactions bancaires, la
récupération des clés du réseau Wi-Fi.


## 1.2.3b Les technologies longue portée (long range)

Les technologies longue portée ont principalement deux types d'applications :

- la téléphonie mobile et ses dérivés (SMS, applications mobiles, etc.) ;
- la collecte d'informations IoT.

Ces deux applications diffèrent principalement dans les besoins en termes de débits et de QoS (latence, gigue, etc). La téléphonie mobile requiert par exemple une latence et une gigue très faibles alors que la remontée de simples données environnementales autorise en général des délais plus importants. Certaines technologies permettent le routage multi-sauts et l'établissement de réseau maillé (mesh) alors que d'autres permettent exclusivement un envoi des données vers la station de base de l'opérateur (étoile).

Pour des déploiements IoT qui dépassent l’échelle du bâtiment, les technologies
radio longue portée comme NB IoT, Sigfox et LoRa autorisent le passage à
l’échelle en terme de portée radio avec des débits suffisants pour des
applications de télémétries classiques ou pour des actionneurs distants. Le
critère de couverture radio totalement garantie ne peut être atteint que si
l’utilisateur déploie son propre réseau d’antenne afin de combler les
éventuelles zones blanches. Dans ce cas de figure, seule LoRa permet de gagner son indépendance en opérant soi-même sa propre infrastructure.

Voyons en détail quelques unes de ces technologies.

### 4G / NB IoT
Les connexions cellulaires autorisent des communication radio sur plusieurs
kilomètres de distance. Les opérateurs proposent des couvertures radio sur la
quasi-totalité du territoire. Les terminaux utilisateurs sont accrochés à une
cellule qui partage la bande passante entre les terminaux actifs, par exemple
maximum 600 Mbits/s mutualisés pour la norme 4G LTE-A Cat 12.

L’inconvénient
majeur de cette technologie radio est la forte consommation énergétique. Afin de
pallier ce problème, Narrowband Internet of Things (NB IoT) propose de réduire
cette consommation d’énergie au détriment de la bande passante. Les
inconvénients majeures des connexions cellulaires sont la dépendance vis à vis
des opérateurs qui possèdent les licences d’exploitation relatives aux  bandes
de fréquences utilisées.
Dans la cas d’une zone blanche, il est impossible
d’étendre la couverture par ses propres moyens.

Enfin, le modèle économique par
d’abonnement peut être potentiellement très onéreux par rapport au nombre
d’objets connectés déployés.

### SigFox
Sigfox est un opérateur qui a développé et déployé sa propre technologie radio
utilisant la bande ISM des 868 MHz.

Cette bande est contrainte à une utilisation
particulière en terme de puissance et de temps d’utilisation du médium physique
: il est interdit de monopoliser plus de 1% du temps le canal radio. Tout en
respectant cette contrainte, la modulation radio utilisée par Sigfox arrive à
franchir énormément d’obstacles. Toutefois, la taille des paquets est limitée à
12 octets pour un maximum de 140 messages par jour.

L’avantage majeur est la
très faible consommation énergétique mais on retrouve les mêmes inconvénients
que les technologies cellulaires (abonnements, zones blanches).

### LoRa
Tout comme Sigfox, LoRa (Long-Range) est une technologie radio utilisant la
bande ISM des 868 MHz en Europe.

Initialement développée par Cycléo en 2009 puis
racheté par Semtech en 2012, LoRa fonctionne sur une modulation radio
propriétaire qui permet d’atteindre des portées radio jusqu’à 5 km en zone
urbaine dense et 15 km en zone dégagée.

Les puces radio propriétaires sont
vendues à bas coût pour viser des déploiements à large échelle supérieur à
plusieurs milliers d’objets. La modulation LoRa propose différentes puissances
d’émissions appelées « Spreading Factor » (SF) allant de 7 à 12. Plus le SF est
fort, moins la bande passante est élevée. Le SF contraint la taille maximale
d’un paquet allant de 51 à 222 octets maximum. Afin d’augmenter la robustesse
aux interférences et de garantir l’intégrité des paquets, un CRC et un code
correcteur (Coding Rate) sont inclus dans la trame LoRa.
