
# 1.2.0. Introduction
Il existe un grand nombre de technologies radio, chacune ayant ses avantages et ses inconvénients et chacune étant plus appropriée pour une application donnée. Alors, comment savoir laquelle choisir ?

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Pour pouvoir choisir la technologie radio la plus appropriée, il nous faut d'abord bien comprendre quelques caractéristiques des technologies radio comme les fréquences qu'elles utilisent, les débits qu'elles offrent, les portées qu'elles proposent, etc.
Dans cette séquence, c’est ce que nous allons essayer de voir en fonction des différents besoins applicatifs.
</p>
</div>
