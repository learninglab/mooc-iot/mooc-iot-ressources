# 1.2.1. Les bandes de fréquences

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [1.2.1a Les bandes ISM](#121a-les-bandes-ism)
- [1.2.1b Les bandes licenciées](#121b-les-bandes-licenciées)
<!-- TOC END -->

Les technologies radio ont en commun le fait d’utiliser des fréquences radio. Le spectre radio est très régulé et la plupart des plages de fréquences sont réservées (police, armée, aéronautique, météorologie, etc.) et sous licence (téléphonie mobile, etc.).  
En France, c’est l’[ARCEP](https://www.arcep.fr) (Autorité de Régulation des Communications Electroniques et des Postes) qui est en charge de cette régulation. [1]  
Vous pouvez consulter la [répartition du spectre des fréquences en France](https://www.anfr.fr/gestion-des-frequences-sites/tnrbf/frise-interactive/) sur le site de l’Agence nationale des fréquences (ANFR). Ces attributions changent d’un pays à l’autre.

Il existe cependant quelques plages de fréquences libres d’utilisation. Il s’agit des bandes ISM (Industrie, Science et Médical).

## 1.2.1a Les bandes ISM


Il existe plusieurs bandes de fréquences ISM, un peu partout sur le spectre, les plus prisées étant les bandes :

* 2,4GHz (WiFi, Bluetooth, four à micro-ondes, etc.),
* 5GHz (WiFi),
* 433 MHz (babyphones, alarmes, domotique, LoRA etc.), 868 MHz (domotique,  SigFox, LoRA, etc.)

<figure>
    <img src="Images/ISM_2-EN.png" alt="" width="800"><br />
    <figcaption>Fig. 1 : Bandes de fréquences ISM (Source : EBDS Wireless & Antennas - http://www.ebds.eu)</figcaption>
</figure>  
  
L’avantage des bandes ISM est que justement elles sont libres d’utilisation. Elles sont cependant soumises à des régulations, en termes de puissance d'émission et de temps d'utilisation, qui varient suivant la bande concernée. Par exemple, pour la bande basse, le temps d’émission est limité à 1% du temps par heure, soit 36s par heure.

L’inconvénient des bandes ISM est qu’elles sont très sollicitées. Deux appareils proches communiquant en même temps sur la même fréquence, même s’ils utilisent des protocoles différents (comme Bluetooth et WiFi), peuvent se brouiller mutuellement. Les fréquences représentent une ressource partagée rare.


## 1.2.1b Les bandes licenciées

Pour garantir qu’une communication ne sera pas brouillée, il faut pouvoir contrôler les entités autorisées à communiquer sur les mêmes fréquences et, pour cela, utiliser des bandes de fréquences sous licence.
C’est ce que font les opérateurs téléphoniques : ils achètent des licences, autrement dit, des droits d’utilisation exclusifs d’une largeur du spectre radio. Ainsi, seuls leurs abonnés peuvent utiliser ces fréquences (via la carte SIM qui contrôle l’usage de telle ou telle fréquence)[2].


<figure>
    <img src="Images/frequences-EN.png" alt="fréquences en France" width="800"><br />
    <figcaption>Fig. 2 : Fréquences en France</figcaption>
</figure>

L’avantage d’utiliser des fréquences sous licence, et donc de passer via un opérateur, est que ce dernier garantit une qualité de service et une bonne connexion. L’inconvénient est qu’il faut souscrire un forfait et qu’on dépend d’un opérateur et de son infrastructure.

<hr/>
[1] Le spectre est alloué au niveau international dans le cadre de l’UIT lors de conférences périodiques CMR / WRC (la dernière a eu lieu en octobre 2019). L’ARCEP alloue des fréquences dans ce cadre. Une utilisation sans contrainte pour un pays d’une bande est décidée dans les CMR.

[2] Lorsqu’on change d’opérateur on a souvent besoin de « désimlocker » son téléphone. En effet celui-ci est « bloqué » sur la fréquence d’un opérateur. Cette fréquence n’étant pas la même que celle du nouvel opérateur, il faut donc autoriser le téléphone à se connecter sur une nouvelle plage de fréquences. Les opérateurs gèrent ensuite l'attribution des ressources entre leurs abonnés.  
