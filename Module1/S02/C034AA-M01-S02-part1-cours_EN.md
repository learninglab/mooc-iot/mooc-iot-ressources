# 1.2.1. Frequency bands

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.2.1a ISM bands](#121a-ism-bands)
- [1.2.1b Licensed bands](#121b-licensed-bands)
<!-- TOC END -->

What the different types of radio technology have in common is that they all use radio frequencies. The radio spectrum is highly regulated and the majority of frequency ranges are either reserved (police, army, aeronautics, meteorology, etc.) or licensed (mobile operators, etc.).  
In France, the [ARCEP](https://www.arcep.fr) (the French Electronic Communications and Postal Regulatory Authority) is in charge of these regulations.[1]   
A [breakdown of the radio spectrum frequencies in France](https://www.anfr.fr/gestion-des-frequences-sites/tnrbf/frise-interactive/) is available to view on the French National Frequency Agency (ANFR) website. These allocations change from one country to the next.

There are, however, a number of frequency ranges which are free to use. These are called ISM bands (Industry, Science and Medical).

## 1.2.1a ISM bands


There are several ISM frequency bands across the spectrum, the most used being:

* 2.4 GHz (Wi-Fi, Bluetooth, microwaves, etc.)
* 5 GHz (Wi-Fi)
* 433 MHz (baby monitors, alarms, home automation, LoRA, etc.), 868 MHz (home assistants, SigFox, LoRA, etc.)

<figure>
    <img src="Images/ISM_2-EN.png" alt="" width="800"><br />
    <figcaption>Fig. 1: ISM radio bands (Source : EBDS Wireless & Antennas - http://www.ebds.eu)</figcaption>
</figure>

The advantage of ISM bands is that they are free to use. They are subject to regulations, however, in terms of their emitting power and usage time, both of which will vary depending on the band being used. For the low band, for example, the emitting time is limited to 1% of the time per hour, or 36 seconds.

The disadvantage of ISM bands is that they are in high demand. Two neighbouring appliances communicating at the same time on the same frequency, even when using different protocols (such as Bluetooth and Wi-Fi), may interfere with each other. Frequencies are a rare shared resource.


## 1.2.1b Licensed bands

In order to prevent interference, you must be able to control who is authorised to communicate over the same frequencies, which requires the use of licensed frequency bands.
This is what telephone operators do: they buy licenses, giving them exclusive rights to certain bandwidths of the radio spectrum. What this means is that only their customers can use these frequencies (via their SIM cards, which control the use of a given frequency)[2].


<figure>
    <img src="Images/frequences-EN.png" alt="frequencies in France" width="800"><br />
    <figcaption>Fig. 2: Frequencies in France</figcaption>
</figure>

The advantage of going through operators and using licensed frequencies is that these operators are able to guarantee quality of service and a good connection. The disadvantage is that you have to pay tariffs that depend on operators and their infrastructure.

<hr/>
[1] Spectrum is allocated in an international agreement. This is discussed during periodic CMR / WRC conferences (the last held in October 2019) in which each country can assign independently frequencies assigned to a usage.

[2] When changing operator, it is often necessary to unlock your phone’s SIM. This is because it is “locked” on to the frequency of a certain operator. Given that this frequency is not the same as that of the new operator, the telephone has to be authorised to connect to a new frequency range. Operators are then responsible for managing the allocation of resources among customers.  
