# 1.2.3. Technology

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [1.2.3a Short-range](#123a-short-range)
- [1.2.3b Long-range](#123b-long-range)
<!-- TOC END -->

There are many different types of radio communication technology. We are not aiming to be exhaustive here, especially as new types are appearing all the time. Instead, we will focus on those most widely used today.

A distinction is made between two different types of radio technology, depending on their range: **short-range** radio technology, which has a range of less than 100 m; and **long-range** radio technology, which has a range in excess of 1 km. Depending on how the technology is to be used, radio range is often a determining factor when it comes to considering either external deployment or deployment in a large building without the need to deploy any heavy infrastructure.

## 1.2.3a Short-range

Short-range technology is generally used to set up local networks or LANs (Local Area Networks). For the most part, these are associated with high speeds, as is the case with Wi-Fi, for example.
Short-range technology is primarily used:

* by internet applications (Wi-Fi),
* for point-to-point information exchange or access authorisation (RFID, NFC, Bluetooth),
* for home automation (ZigBee, ZWave),
* for connected objects (Bluetooth, Wi-Fi, etc.).

Let's take a closer look at some of these technologies.

### Wi-Fi
Wi-Fi is a wireless communication protocol governed by the IEEE 802.11 standard. It uses the 2.4 GHz and 5 GHz ISM bands.

The first versions, 802.11a and 802.11b, were introduced in the late 1990s, offering theoretical speeds of 11 Mbits/s.  
The version currently available, 802.11ac, offers theoretical speeds of up to 1.3 Gbits/s. The future version, 802.11ax, will be 20% quicker.

Historically linked to laptop computers, Wi-Fi has spread to all communicating mobile devices. Wireless networks operate primarily in stars, linking one station to one point of access. Generally speaking, the point of access is connected to a wired Ethernet backbone for the purposes of routing traffic.

Other approaches have involved attempting to build a mesh network type infrastructure on a wireless backbone on a dedicated frequency band (5 GHz) (ref. Roofnet).

The main disadvantage of Wi-Fi remains its energy consumption, which means a sizeable battery is required. The 802.11ah standard (2017), which was optimised in order to reduce energy consumption in IoT applications, failed to establish itself in the face of competition from other technology available on the market.

### Bluetooth
Like Wi-Fi, Bluetooth also first emerged towards the end of the 1990s. It is overseen by the consortium Bluetooth SIG (Special Interest Group) and was initially designed to link a telephone-type terminal to a computer.

1999 saw the release of version 1.0 and the first compatible phone. In much the same way as Wi-Fi, Bluetooth uses the 2.4 GHz ISM band, giving it a range of 100 m, but its channels are much narrower, meaning its bandwidth is much lower (less than 1 Mbit/s).

There is a potential risk of these two types of radio technology interfering with each other as a result of channel overlap. The standard specifies a number of profiles covering a variety of use cases (modems, file transfer, audio headsets, etc.).

Current use of Bluetooth is completely decentralised and does not require any heavy infrastructure. The 4.0 version of Bluetooth, BLE (Bluetooth Low Energy) significantly reduces energy consumption. An increasing number of autonomous connected objects have been adopting this technology in order to be able to interface with mobile terminals. However, connectivity depends on the presence of the radio range between the terminal and the object.

### Zigbee
Zigbee is a high level standard based on the IEEE 802.15.4 standard (Low-Rate Wireless PANs), which first appeared in the early 2000s. It was designed to offer a low-speed, low-energy consumption wireless network.

In much the same way as Wi-Fi and Bluetooth, Zigbee also operates on the 2.4 GHz ISM band, with a maximum range of 100 m. It is capable of operating on peer-to-peer topologies or star networks.

Zigbee is overseen by the Zigbee Alliance, whose role it is to certify that products respect the specifications of the standard. There are a range of different profiles for applications, including home automation (lighting, heating), medical sensors and smoke detectors. The advantage of Zigbee is that it can be used to create a mesh network between objects, extending the surface coverage of the application deployed. Generally speaking, Zigbee/IP gateways can be used for permanent, real-time information exchange between objects and a cloud.

### RFID, NFC
RFID (Radio Frequency Identification) is technology used to enable a reader and a tag to exchange information wirelessly.

The reader remotely supplies the tag with an electromagnetic wave. The tag then sends a response back, normally in the form of a username stored in a small embedded memory. The reader can also write data into the tag. These tags can have different operating modes - passive, semi-active, or active - and can reach radio ranges between 10 m and 200 m.

This technology is mainly used to identify and track objects. The cost of a tag makes it possible to mark objects you wish to follow on a large-scale. The best-known use case is as an anti-theft device attached to items sold in shops.

NFC (Near Field Communication) authorises communication over short distances (less than 10 cm). Many smartphones and tablets are equipped with this technology, which can be used for three types of exchange: card emulation (payment, transport), read mode, and peer-to-peer mode. The bandwidth ranges from 106 kbits/s to 424 kbits/s. Secure, short-range exchanges can be used for applications such as bank transactions and for recovering Wi-Fi network keys.


## 1.2.3b Long-range

Long-range technology is primarily used for two types of application:

- mobile communication and connected services (text messaging, mobile applications, etc.),
- collecting IoT information.

These two applications differ chiefly in terms of their speed and QoS requirements (latency, jitter, etc.). Very low latency and jitter is required for mobile communications, but longer delays are generally tolerated when it comes to relaying environmental data. Some types of LPWAN are capable of multi-hop routing and setting up mesh networks, while others can only be used to send data to operators’ base stations (star networks).

For IoT deployments going beyond building level, long-range radio technology such as NB IoT, Sigfox and LoRA enable scaling-up in terms of radio range, with speeds fast enough for conventional range-finding applications or for remote actuators. Fully-guaranteed radio coverage can only be achieved if the user deploys their own antenna network in order to fill in any potential blank zones. In such cases, only LoRa enables independence by operating its own infrastructure itself.

Let's take a closer look at some of these technologies.

### 4G / NB IoT
Cellular connections enable radio communication over distances of several kilometres. Operators provide radio coverage almost everywhere in the country. User terminals are hooked up to a
cell which shares the bandwidth between two active terminals, e.g.
a maximum of 600 Mbits/s is shared for the LTE-A Cat 12 4G standard.

The main disadvantage of this type of radio technology is its high levels of energy consumption. In order
to compensate for this, Narrowband Internet of Things (NB IoT) provides a way of reducing this energy consumption, to the detriment of the bandwidth.

The main disadvantage of cellular connections is how dependent they are on operators, who have operating licenses relating to the frequency bands used. For blank zones, it is not possible to extend coverage using your own means. Lastly, subscription-based economic models can prove very costly in relation to the number of connected objects deployed.

### SigFox
Sigfox is an operator which developed and deployed its own type of radio technology using the 868 MHz ISM band.
There are usage restrictions on this
band in terms of power and time spent using the physical medium: monopolising more than 1% of the radio channel is prohibited.

While managing to respect this constraint, the radio modulation used by Sigfox has been able to
overcome a whole host of obstacles. However, the size of packets is limited to 12 bytes, capping the number of messages per day at 140. The main advantage is its
extremely low energy consumption, but the disadvantages are the same as for cellular technology (subscriptions, blank zones, etc.).

### LoRa
Like Sigfox, LoRa (Long-Range) is a type of radio technology which uses the
868 MHz ISM band in Europe.

Initially developed by Cycléo in 2009, before being
acquired by Semtech in 2012, LoRa operates on an owner radio modulation, enabling it to achieve radio ranges of up to 5 km in dense urban areas and 15 km in open areas. Owner radio chips are sold at a low cost in the hope of attracting large-scale deployment projects with
several thousand objects.

LoRa modulation offers different emitting
powers known as “spreading factors” (SF), ranging from 7 to 12. The stronger the
SF, the lower the bandwidth will be. The SF limits the maximum size
of packets, which ranges from 51 to 222 bytes. In order to enhance its ability
to withstand interference and to protect the integrity of packets, a CRC and a corrector
code (Coding Rate) are included in the LoRa frame.
