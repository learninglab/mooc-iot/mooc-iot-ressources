﻿# 2.3.1. The connected object IoT-LAB M3

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.3.1a Synoptic](#231a-synoptic)
- [2.3.1b MCU](#231b-mcu)
- [2.3.1c External hardware](#231c-external-hardware)
- [2.3.1d Power supply](#231d-power-supply)
- [2.3.1e Programming](#231e-programming)
<!-- TOC END -->

## 2.3.1a Synoptic

<figure>
<img src="Images/iotlab_open_m3.png" alt="" style="width:400px"/>
<figcaption>Fig. 1: IoT-LAB M3 board</figcaption>
</figure>

<figure>
<img src="Images/archi_open_m3_EN.png" alt="" style="width:600px"/>
<figcaption>Fig. 2: IoT-LAB M3 architecture</figcaption>
</figure>

## 2.3.1b MCU

The connected object **IoT-LAB M3** takes its name from the **CPU** it is equipped with: the
[Cortex M3](https://developer.arm.com/ip-products/processors/cortex-m/cortex-m3)
manufactured by [ARM](https://www.arm.com). The Cortex M3 is a 32-bit CPU set to a maximum speed of 72 MHz. The Cortex M3 is integrated into the STM32 **MCU**
(reference
[STM32F103REY](https://www.st.com/en/microcontrollers-microprocessors/stm32f103re.html))
manufactured by [STMicroelectronics](https://www.st.com/), more commonly
known as ST. The STM32 MCU is equipped with 64 KB of **RAM** and 256 KB of **ROM**. These technical specifications put the connected object IoT-LAB M3 in the category of **constrained objects** employing the use of embedded operating systems. An example of a connected object employing the use of this same MCU is the Apple TV 4 remote control.

In terms of its dimensions, the M3 has a *form factor* (i.e. the CAD design of the embedded system) 4 cm wide and 5 cm long. It should be borne in mind that the object IoT-LAB M3 was designed as a generic development board.
Depending on the use case, a manufacturer could further optimise the CAD in order to reduce components to the size of a thumbnail.

<figure>
<img src="Images/C034AA-M02-S04-Circuit.png" alt="" style="width:600px"/>
<br />
<br />
<img src="Images/C034AA-M02-S04_Circuit2.png" alt="" style="width:600px"/>
<figcaption>Fig. 3: External IoT-LAB M3 components</figcaption>
</figure>

## 2.3.1c External hardware

In order to communicate with the different external components, the object IoT-LAB M3 uses the data buses **I2C** and **SPI** of its SMT32 MCU.

### Radio chips

The radio chip
[AT86RF231](Docs/AT86RF231.pdf) manufactured
by [Atmel](https://fr.wikipedia.org/wiki/Atmel) (purchased in 2016 by
[Microchip](https://www.microchip.com)) communicates over the 2.4 GHz ISM frequency band. This radio chip was designed to implement the MAC layer of the IEEE 802.15.4 standard, used by the Zigbee communication protocol, among others. The maximum bandwidth is 256 kbits/s. A small, 9 mm
long ceramic antenna is welded onto the circuit. This type of radio technology falls into the “short-range” category, with maximum distances of 40/50 meters indoors and up to around 100 meters outdoors. Depending on these technical specifications, the radio chip consumes up to
14 mA when transmitting at maximum power. It is interconnected with the MCU on the SPI bus, enabling rapid data exchange. It is also connected to a GPIO port on the MCU in order to control the waking up of the radio chip in the event of it being set to standby mode.

### External NOR flash memory

A 128-Mbits external NOR flash memory (N25Q128A13E1240F) is connected to the MCU via the SPI bus in order to enable rapid data transfer. This memory is useful when it comes to storing data acquired by sensors while a program is running. Another use case is for OTA (Over The Air) updates to the MCU’s firmware. Some embedded operating systems
partition this ROM memory in order to download and store different versions of the firmware.


### Sensors

4 sensors connected to the MCU via the I2C bus are embedded into the IoT-LAB M3:

- the light sensor
  [ISL29020](Docs/ISL29020.pdf)
  : this measures ambient light intensity in lux.
- the pressure sensor
  [LPS331AP](Docs/LPS331AP.pdf):
  this measures atmospheric pressure in hPa.
- the accelerometer/magnetometer
  [LSM303DLHC](Docs/LSM303DLHC.pdf):
  this provides feedback on an object’s acceleration, and can be used to detect movement. By determining a threshold, it generates a change of state on one of the MCU’s digital inputs/outputs in order to create an **interrupt**, which can be used to bring the MCU out of standby mode.
- the gyroscope
  [L3G4200D](Docs/L3G4200D.pdf):
  this measures the orientation of an object in space and can be used, for example, to determine the orientation of the screen of a tablet or a smartphone.

### Actuators

3 LEDs (red, green and orange) are connected to the MCU via the **digital
inputs/outputs** on the IoT-LAB M3. Locally, these are used to notify MCU actions or states. Remotely, on the IoT-LAB experiment platform, these LEDs enable us to illustrate the monitoring of energy consumption by changing their state periodically (on/off).

## 2.3.1d Power supply

The M3 node can be supplied in 3.3 V in two different ways:

* via the USB port
* via the external battery

In terms of the energy consumption of the different hardware devices, it should be noted that, at full power, the MCU consumes 14 mA, to which we must add the energy consumption of the other hardware devices. The radio chip, for example, consumes 14 mA when transmitting and 12 mA when receiving.

Typical use cases for constrained connected objects often require the use of a battery. In order to maximise the lifespan of objects, the energy-saving strategy employed involves switching the MCU and its various different components to standby mode once the object has finished processing. In the
case of the STM32 MCU, consumption is reduced to 1 mA when in sleep mode, and remains below 1 μA in standby mode. The AT86RF231 radio chip supports the same type of features, with comparable energy savings.

## 2.3.1e Programming

The JTAG interface can be used to reprogram and debug the MCU from a computer.
It can be accessed via the FTDI component, which is located behind the USB and IoT-LAB connectors. The latter is used for the purposes of integrating the IoT-LAB M3 object into the IoT-LAB platform. This FTDI component also features an UART port, which displays messages on the user’s terminal.
