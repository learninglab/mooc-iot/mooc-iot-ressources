# 2.3.2. La passerelle IoT-LAB

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [Intégration matérielle](#intégration-matérielle)
- [Interaction à distance](#interaction-à-distance)
<!-- TOC END -->


## Intégration matérielle
L'objet connecté présenté précédemment est typiquement identique à des cartes de développement IoT communément branchées à un ordinateur via un port USB afin de les programmer et déboguer. Dans un contexte d'expérimentation d'applications ou de protocoles réseaux à grande échelle, il devient fastidieux
de reprogrammer une dizaine voire une centaine d'objets de cette manière.

La plateforme FIT IoT-LAB apporte une solution à ce problème en intégrant chaque carte d'expérimentation appelée **Open Node** (ON) derrière un mini-ordinateur appelé **Gateway** (GW). On utilise ici le terme de passerelle, mais au sens matériel, car elle met en lien la carte d'expérimentation avec les autres composants matériels de la plateforme. Ce sera cette passerelle qui aura notamment la charge de programmer la carte d'expérimentation. En plus de cette passerelle intermédiaire, un système embarqué autonome appelé **Control Node** (CN) est capable de surveiller l'*Open Node* pour mesurer sa consommation énergétique, le bruit et le trafic radio. L'intérêt d'utiliser un MCU dédié est de garantir une exécution temps réel de ces mesures. Enfin, le noeud IoT-LAB complet est relié via un réseau privé à un *serveur IoT-LAB de site* qui va pouvoir l'administrer à distance.

<figure>
    <img src="Images/iotlab-node.png" alt="iotlab-node"/>
    <figcaption>Fig. 1 : Noeud IoT-LAB complet = ON + GW + CN</figcaption>
</figure>

## Interaction à distance
Pour faciliter le déploiement, la Gateway embarque un module PoE (Power Over Ethernet) qui, comme son nom l'indique, permet d'apporter l'alimentation électrique par le port Ethernet en plus de la liaison réseau. Étant le seul lien au matériel, c'est donc aussi par cette liaison que passent les liens habituels d'interaction avec la carte d'expérimentation:
- le lien série de l'Open Node est redirigé vers une socket TCP servie par la Gateway sur le port 20000;
- le lien de déboguage de l'Open Node est redirigé vers une seconde socket TCP servie par la Gateway sur le port 3333;

ainsi que les données remontées par le Control Node:
- les données du sniffer sont écrites dans une socket TCP servie par la Gateway sur le port 30000;
- les données de surveillance automatique (i.e. monitoring) sont eux écrits dans directement dans des fichiers dans l'espace de travail de l'utilisateur.

Des règles de filtrage réseau dynamiques rendent ces liens accessibles par l'utilisateur ayant réservé ces noeuds d'expérimentation, depuis la frontale SSH du site concerné, pour la durée de l'expérience.

<figure>
    <img src="Images/design-hardware.png" alt="interaction à distance"/>
    <figcaption>Fig. 2 : Liens redirigés par la carte Gateway</figcaption>
</figure>

Visitez le site web d'IoT-LAB pour plus d'information sur la [carte IoT-LAB Gateway](https://github.com/iot-lab/iot-lab/wiki/Hardware-Iotlab-gateway).
