
# 2.4.2. The connected object IoT-LAB A8

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.4.2a Synoptic](#242a-synoptic)
- [2.4.2b MCU](#242b-mcu)
- [2.4.2c External hardware](#242c-external-hardware)
<!-- TOC END -->


<img src="Images/iotlab_open_a8.png" alt="" style="width:400px"/>

*Fig.: IoT-LAB A8* board


## 2.4.2a Synoptic

<img src="Images/archi_open_a8.png" alt="" style="width:400px"/>

*Fig.: IoT-LAB A8* architecture

## 2.4.2b MCU

As is the case with the IoT-LAB M3 object, the IoT-LAB A8 object takes its name from the CPU it is equipped with: the [Cortex
A8](https://developer.arm.com/ip-products/processors/cortex-a/cortex-a8), manufactured by ARM. Based on Armv7 architecture, the Cortex A8 is a 32-bit CPU set to a frequency of 600 MHz, consuming less than 300 mW. The Cortex A8 is integrated into a system-on-module chip
[VAR-SOM-AM35](http://www.ti.com/product/AM3505) made by [Texas
Instruments](http://www.ti.com), which, like an MCU, is equipped with all the components it needs to be run. This module is itself embedded into a
[Variscite](https://www.variscite.com/wp-content/uploads/2017/12/VAR-SOM-AM35-Datasheet.pdf) *DDR2 SODIMM connector* form factor circuit board, designed specifically for industrial applications.

<img src="Images/variscite.jpeg" alt="" style="width:400px"/>

*Fig.: Variscite board*

This industrial board is inserted into a circuit equipped with all required external components.

<img src="Images/iotlab_open_a8_components.png" alt="" style="width:400px"/>

*Fig.: External IoT-LAB A8* components

The final assembly has 256 MB of RAM. All these technical characteristics put the IoT-LAB A8 object in the **user terminal** (e.g. smartphones) or **gateway** (e.g. home automation systems) class of objects.

## 2.4.2c External hardware

### M3 co microcontroller

What sets the IoT-LAB A8 connected object apart is that it is equipped with a clone of the IoT-LAB M3 object. As a result, it features the same sensors/actuators, as well as the same radio chip, enabling it to communicate wirelessly with other M3 objects within radio range. The M3 is linked to the A8 via the I2C data bus and the
GPIO inputs/outputs. This M3 co microcontroller can be programmed from the A8 through an intermediary FTDI component.

### GPS

A sub-assembly of the IoT-LAB A8 nodes are equipped with GPS as an optional
extra, enabling the system time to be synchronized with satellite signals with a
precision under the microsecond. This feature allows to precisely draw the
events timeline between IoT-LAB devices equipped with extra GPS.

### Ethernet

The IoT-LAB A8 object features an Ethernet interface, enabling it to connect to a LAN and to communicate with the internet in IPv4/IPv6 via a standard router.
