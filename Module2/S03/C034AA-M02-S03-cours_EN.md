# 2.3.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence, we will take a detailed look at the components and characteristics of the IoT-LAB M3 connected objects used on the FIT IoT-LAB platform, as well as how you can access them. You will learn how to analyse the different components of the FIT IoT-LAB’s main connected objects and how to access them remotely.
</p>
</div>

When designing the FIT IoT-LAB platform, we also designed our own hardware. We will detail here the hardware architecture of the IoT-LAB M3 board, which has been deployed in large numbers to allow large-scale experiments. We will also briefly explain how we achieved the hardware integration of the experiment boards into the platform for remote use.

<figure style="float: left;">
  <img src="Images/iotlab_open_m3.png" alt="" style="width:400px"/>
  <figcaption>Fig. 1: Carte IoT-LAB M3</figcaption>
</figure>

<br style="clear: both;">

Video presentation of the connected objects of the FIT IoT-LAB platform: What are the embedded components? How can we interact remotely with these objects?
