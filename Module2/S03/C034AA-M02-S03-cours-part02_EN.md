# 2.3.2. The IoT-LAB gateway

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [Hardware integration](#hardware-integration)
- [Remote interaction](#remote-interaction)
<!-- TOC END -->


## Hardware integration
The connected object shown previously is typically identical to IoT development boards commonly connected to a computer via a USB port for programming and debugging. In a context of experimenting with applications or network protocols on a large scale, it becomes tedious to reprogram a dozen or even a hundred objects in this way.

The FIT IoT-LAB platform provides a solution to this problem by integrating each experimental board called **Open Node** (ON) behind a mini-computer called **Gateway** (GW). The term Gateway is used here,  in a hardware sense, as it links the experimental board with the other hardware components of the platform. This gateway will be in charge of programming the experimental board. In addition to this intermediate gateway, an autonomous on-board system called **Control Node** (CN) is able to monitor the *Open Node* to measure its energy consumption, radio noise and traffic. The advantage of using a dedicated MCU is to guarantee real-time execution of these measurements. Finally, the complete IoT-LAB node is connected via a private network to a *IoT-LAB site server* which will be able to manage it remotely.

<figure>
    <img src="Images/iotlab-node.png" alt="iotlab-node"/>
    <figcaption>Fig. 1: Complete IoT-LAB node = ON + GW + CN</figcaption>
</figure>

## Remote interaction
To facilitate deployment, the Gateway includes a PoE (Power Over Ethernet) module which, as its name suggests, allows power to be supplied via the Ethernet port in addition to the network link. Being the only link to the hardware, it is also through this link that the usual interaction links with the experimental board pass:
- the serial link of the Open Node is redirected to a TCP socket served by the Gateway on port 20000;
- the Open Node debug link is redirected to a second TCP socket served by the Gateway on port 3333;

as well as the data sent back by the Control Node:
- the sniffer data is written to a TCP socket served by the Gateway on port 30000;
- the automatic monitoring data is written directly to files in the user's workspace.

Dynamic network filtering rules make these links accessible to the user who has reserved these experimentation nodes, from the SSH frontend of the site concerned, for the duration of the experiment.

<figure>
    <img src="Images/design-hardware.png" alt="remote interaction"/>
    <figcaption>Fig. 2: Links redirected by the Gateway board</figcaption>
</figure>


Visit the IoT-LAB website for more information about the [IoT-LAB Gateway](https://github.com/iot-lab/iot-lab/wiki/Hardware-Iotlab-gateway).
