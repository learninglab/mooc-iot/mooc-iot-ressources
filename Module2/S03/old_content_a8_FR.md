
# 2.4.2. L'objet connecté IoT-LAB A8

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.4.2a Synoptique](#242a-synoptique)
- [2.4.2b MCU](#242b-mcu)
- [2.4.2c Périphériques externes](#242c-périphériques-externes)
<!-- TOC END -->

## 2.4.2a Synoptique

<figure style="float: left;">
  <img src="Images/iotlab_open_a8.png" alt="" style="width:400px"/>
  <figcaption>Fig. : Carte IoT-LAB A8</figcaption>
</figure>

<figure style="float: right;">
  <img src="Images/archi_open_a8.png" alt="" style="width:400px"/>
  <figcaption>Fig. : Architecture IoT-LAB A8</figcaption>
</figure>

<br style="clear: both;">

## 2.4.2b MCU

Tout comme l'objet IoT-LAB M3, l'objet IoT-LAB A8 tire son nom du CPU qu'il embarque : le [Cortex
A8](https://developer.arm.com/ip-products/processors/cortex-a/cortex-a8) du fabricant ARM. Basé sur une architecture Armv7, le Cortex A8 est un CPU 32 bits cadencé à 600 MHz pour une consommation de moins de 300 mW. Le Cortex A8 est intégré dans une puce de type System-on-module
[VAR-SOM-AM35](http://www.ti.com/product/AM3505) de chez [Texas
Instruments](http://www.ti.com) qui tout comme un MCU embarque l'ensemble des composants indispensables à son exploitation. Ce module est lui-même embarqué dans une carte
[Variscite](https://www.variscite.com/wp-content/uploads/2017/12/VAR-SOM-AM35-Datasheet.pdf) au form factor type *DDR2 SODIMM connector* adapté aux applications industrielles.

<img src="Images/variscite.jpeg" alt="" style="width:400px"/>

*Fig. : Carte Variscite*

Enfin, cette carte industrielle est insérée sur un circuit embarquant tous les composants externes nécessaires.

<img src="Images/iotlab_open_a8_components.png" alt="" style="width:400px"/>

*Fig. : Composants externes IoT-LAB A8*

Le montage final comprend 256 Mo de RAM. L'ensemble des caractéristiques techniques placent l'objet IoT-LAB A8 dans la famille des objets de type **terminal utilisateur** (par ex. un smartphone) ou **passerelle** (par ex. une box domotique).

## 2.4.2c Périphériques externes

### Co micro-contrôleur M3

L'originalité de l'objet connecté IoT-LAB A8 est d'embarquer un clone de l'objet IoT-LAB M3. De ce fait, il dispose des mêmes capteurs/actionneurs et surtout de la même puce radio, le rendant ainsi communicant en sans fil avec les autres objets M3 à portée radio. Le M3 est relié au A8 via le bus de données I2C, les
entrées/sorties GPIO. Ce co micro-contrôleur M3 est programmable depuis le A8 grâce à un composant FTDI intermédiaire.

### GPS

Un sous-ensemble des noeuds IoT-LAB A8 embarquent en option un GPS qui autorise
une synchronisation du temps système avec les signaux des satellites à la
microseconde. Cette fonctionnalité permet de précisément dater les événements
entre les différents objets IoT-LAB équipés d'un GPS.

### Ethernet

L'objet IoT-LAB A8 dispose d'une interface Ethernet permettant la connexion à un LAN et de communiquer en IPv4/IPv6 avec Internet via un routeur classique.
