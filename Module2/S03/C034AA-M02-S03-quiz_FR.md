# Question 2.4.1

L'objet connecté IoT-LAB M3 tire son nom :

( ) De son MCU ?
(X) De son CPU ?
( ) D'une troisième révision matérielle ?

[explanation]
Il tire son nom du **CPU** Cortex M3 qui est embarqué dans un MCU STM32.
[explanation]

#Question 2.4.2

L'objet IoT-LAB M3 peut-il communiquer en radio avec :

[X] Un autre objet IoT-LAB M3 ?
[X] Un autre objet avec une puce radio compatible IEEE 802.15.4 ?
[ ] Un smartphone ?
[ ] Un ordinateur ?

[explanation]
L'objet IoT-LAB M3 embarque une puce radio compatible IEEE 802.15.4. A ce
titre, il est interopérable au niveau radio avec un autre objet qui embarque la
même technologie radio. Par défaut, un smartphone ou un
ordinateur embarque les technologies radio Wi-Fi (IEEE 802.11) et Bluetooth qui
ne sont pas intéropérables avec les puce IEEE 802.15.4.
[explanation]

#Question 2.4.3

Pour tester les applications IoT à grande échelle, comment sont reprogrammés
(flash du firmware) les objets connectés IoT-LAB M3 ?

( ) Grâce à un serveur sur lequel tous les objets sont branchés via USB ?
(X) Grâce à des mini-ordinateurs sur lesquels est branché un objet via USB
ou le port IoT-LAB ?

[explanation]
Les objets IoT-LAB M3 ne sont pas directement branchés en USB au serveur
d'expérimentation. Ils sont chacun branchés derrière un **mini-ordinateur**
appelé **IoT-LAB Gateway** qui embarque tous les outils logiciels pour
reprogrammer à distance l'objet. La IoT-LAB Gateway est atteinte à distance
depuis le serveur d'expérimentation via une connectivité IP au dessus d'un
réseau filaire Ethernet, de plus elle est alimentée en PoE.
[explanation]
