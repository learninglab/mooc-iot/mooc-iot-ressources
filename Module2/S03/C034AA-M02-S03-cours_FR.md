# 2.3.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence, nous présentons en détail les composants et caractéristiques de la carte IoT-LAB M3 que vous utiliserez dans les activités pédagogiques. Vous aurez ainsi vu un exemple concret des différents composants d'un objet connecté.
</p>
</div>

Lors de la conception de la plateforme FIT IoT-LAB, nous avons également conçu notre propre matériel. Nous allons détailler ici l'architecture matérielle de la carte IoT-LAB M3, qui a été déployé en grand nombre pour permettre les expérimentations à large échelle. Nous expliquerons également succinctement de quelle manière est réalisée l'intégration matérielle des cartes d'expérimentation dans la plateforme pour une utilisation à distance.

<figure style="float: left;">
  <img src="Images/iotlab_open_m3.png" alt="" style="width:400px"/>
  <figcaption>Fig. 1 : Carte IoT-LAB M3</figcaption>
</figure>

<br style="clear: both;">

Présentation en vidéo des objets connectés de la plateforme FIT IoT-LAB : Quels sont les composants embarqués ? Comment pouvons-nous interagir à distance avec ces objets ?
