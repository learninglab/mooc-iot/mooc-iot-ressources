# 2.3.1. L'objet connecté IoT-LAB M3

## Sommaire

<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.3.1a Synoptique](#231a-synoptique)
- [2.3.1b MCU](#231b-mcu)
- [2.3.1c Périphériques externes](#231c-périphériques-externes)
- [2.3.1d Alimentation](#231d-alimentation)
- [2.3.1e Programmation](#231e-programmation)
<!-- TOC END -->

## 2.3.1a Synoptique
<figure>
  <img src="Images/iotlab_open_m3.png" alt="" style="width:400px"/>
  <figcaption>Fig. 1 : Carte IoT-LAB M3</figcaption>
</figure>

<figure>
  <img src="Images/archi_open_m3_FR.png" alt="" style="width:600px"/>
  <figcaption>Fig. 2 : Architecture IoT-LAB M3</figcaption>
</figure>




## 2.3.1b MCU

L'objet connecté **IoT-LAB M3** tire son nom du **CPU** dont il est équipé : le
[Cortex M3](https://developer.arm.com/ip-products/processors/cortex-m/cortex-m3)
du fabricant [ARM](https://www.arm.com). Le Cortex M3 est un CPU 32 bits cadencé à une vitesse maximum de 72 MHz. Le Cortex M3 est intégré au **MCU** STM32
(référence
[STM32F103REY](https://www.st.com/en/microcontrollers-microprocessors/stm32f103re.html))
du fabricant [STMicroelectronics](https://www.st.com/) ou plus communément
appelé ST. Le MCU STM32 embarque 64ko de **RAM** et 256ko de **ROM**. Ces caractéristiques techniques placent l'objet connecté IoT-LAB M3 dans la catégorie des **objets contraints** utilisant des systèmes d'exploitation embarqués. Un exemple d'objet connecté utilisant ce même MCU est la télécommande de l'Apple TV 4.

Au niveau des dimensions, le M3 a un *form factor* (ie design de la CAO du système embarqué) d'une taille de 4 cm de large sur 5 cm de long. Gardons à l'esprit que l'objet IoT-LAB M3 a été conçu comme une carte générique de développement.
Selon le cas d'usage, un industriel pourrait encore optimiser la CAO pour réduire l'encombrement des composants à la taille de l'ongle d'un pouce.

<figure>
<img src="Images/C034AA-M02-S04-Circuit.png" alt="" style="width:600px"/>
<br />
<br />
<img src="Images/C034AA-M02-S04_Circuit2.png" alt="" style="width:600px"/>
<figcaption>Fig. 3 : Composants externes IoT-LAB M3</figcaption>
</figure>

## 2.3.1c Périphériques externes

Afin de communiquer avec les différents composants externes, l'objet IoT-LAB M3 utilise les bus de données **I2C** et **SPI** de son MCU SMT32.

### Puce radio

La puce radio
[AT86RF231](Docs/AT86RF231.pdf) du
fabricant  [Atmel](https://fr.wikipedia.org/wiki/Atmel) (racheté en 2016 par
[Microchip](https://www.microchip.com)) communique sur la bande de fréquence ISM 2.4 GHz. Cette puce radio a été conçue pour implémenter la couche MAC du standard IEE 802.15.4 utilisée entre autre par le protocole de communication Zigbee. La bande passante maximale est 256 kbits/s . Une antenne céramique très
petite d'une longueur de 9 mm est soudée sur le circuit. On classe cette technologie radio dans la catégorie *courte portée* pour des distances maximum de quelques dizaines de mètres en intérieur jusqu'à une centaine de mètre en extérieur. Selon ces spécifications techniques, la puce radio consomme jusqu'à
14 mA pour une transmission à puissance maximale. Elle est interconnectée avec le MCU sur le bus SPI qui permet des échanges de données rapides. Elle est également branchée sur un port GPIO du MCU afin de gérer le réveil de la puce radio dans le cas d'une mise en veille.

### Mémoire NOR Flash externe

Une mémoire NOR Flash externe (N25Q128A13E1240F) de 128 Mbits est connecté au MCU via le bus SPI afin d'autoriser des transferts de données rapides. Cette mémoire est utile pour stocker de manière persistante les données acquises par les capteurs au cours de l'exécution du programme. Un autre cas d'usage est la mise à jour OTA (Over The Air) du firmware du MCU. Certains OS embarqués
partitionnent cette mémoire ROM pour télécharger et stocker différentes versions de firmware.


### Capteurs

4 capteurs connectés au MCU via le bus I2C sont embarqués dans le IoT-LAB M3 :

- le capteur de lumière
  [ISL29020](Docs/ISL29020.pdf) :
  il mesure l'intensité lumineuse environnante en lux.
- le capteur de pression
  [LPS331AP](Docs/LPS331AP.pdf) :
  il mesure la pression atmosphérique en hPa.
- l'accéléromètre/magnétomètre
  [LSM303DLHC](Docs/LSM303DLHC.pdf) :
  il remonte l'accélération d'un objet. Son utilisation permet de détecter des mouvements. En définissant un seuil, il génère un changement d'état sur une entrée/sortie digitale du MCU afin de créer une **interruption**, utile par exemple pour sortir le MCU de veille.
- le gyromètre
  [L3G4200D](Docs/L3G4200D.pdf) :
  il mesure l'orientation de l'objet dans l'espace, par exemple pour déterminer l'orientation de l'écran d'une tablette ou d'un smartphone.

### Actionneurs

3 LEDS (rouge, vert, orange) sont connectées au MCU via les **entrées/sorties
digitales** sur le IoT-LAB M3. En local, elles sont utiles pour notifier un état ou une action du MCU. A distance sur la plateforme d'expérimentation IoT-LAB, les LEDs vont nous permettre d'illustrer le monitoring de consommation d'énergie en changeant périodiquement leur état allumé ou éteint.

## 2.3.1d Alimentation

Le noeud M3 peut être alimenté en 3,3V de deux manières différentes :

* via le port USB
* via la batterie externe

Au niveau des consommations des différentes périphériques, on notera que le MCU consomme à pleine puissance 14 mA auquel il faut ajouter la consommation des autres périphériques. Par exemple le puce radio consomme 14 mA en émission et 12 mA en réception.

Les cas d'usages typiques des objets connectés contraints requierent souvent l'utilisation d'une batterie. Afin de maximiser la durée de vie de l'objet, la stratégie d'économie d'énergie consiste à basculer le MCU ainsi que ses divers composants dans un état de veille dès que l'objet a fini un traitement. Dans le
cas du MCU STM32, on arrive à une consommation de 1 mA dans le mode SLEEP, et en-dessous de 1 μA dans le mode STANDBY. La puce radio AT86RF231 supporte le même type de fonctionnalités pour des gains énergétiques comparables.

## 2.3.1e Programmation

L'interface JTAG permet de reprogrammer et débugger le MCU depuis un ordinateur.
Elle est accessible via le composant FTDI qui est placé derrière les connecteurs USB et IoT-LAB. Ce dernier est utilisé pour l'intégration de l'objet IoT-LAB M3 dans la plateforme IoT-LAB. Ce composant FTDI expose également un port UART qui permet d'afficher des messages sur le terminal utilisateur.
