# 2.2.2. Le bus de données I2C

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.2.2a Fonctionnement](#222a-fonctionnement)
- [2.2.2b Cas d'usages](#222b-cas-dusages)
<!-- TOC END -->

## 2.2.2a Fonctionnement

Le bus **I2C** (Inter-Integrated Circuit) a été développé par Philipps dans les
années 1980 afin de pouvoir relier un MCU aux différents circuits intégrés d'un
téléviseur moderne. Historiquement, on le retrouve donc dans énormément de
circuits dédiés à l'audio/vidéo.

Il s'agit d'un bus série qui permet de faire communiquer entre eux des composants
électroniques grâce à trois fils ou lignes. L'implémentation du protocole utilise
deux lignes de signaux plus une de référence reliée à la masse.

* la ligne **SDA** (Serial Data) : sert à transférer les données
* la ligne **SCL** (Serial Clock) : permet de cadencer temporellement l'envoi des
messages sur la ligne SDA
* la ligne **GND** (Ground ou masse) : sert de référence au niveau haut et bas des
lignes SDA et SCL

Les échanges de données sont  **bi-directionnels**, mais sur un modèle
Maître-Esclave. Les messages sont toujours à l'initiative du Maître. Le bus I2C
peut également être multi-maîtres. Chaque abonné (Maître ou Esclave) possède une
adresse sur 7 bits autorisant un maximum de 128 adresses sur le même bus. Chaque
octet transféré est acquitté.

Les débits du bus I2C peuvent atteindre 100 kbits/s (standard) pour la première
version du protocole, jusqu'à 400 kbits/s (fast)  et 3,4 Mbits/s (HS) pour les
dernières révisions du protocole.

Les lignes SDA et SCL utilisent deux niveaux logiques : bas ou "0" et haut ou "1".
Le mode d'état au repos du bus I2C correspond à l'état niveau haut pour les
lignes SDA et SCL.

### Synchronisation SDA et SCL

Les signaux entre SDA et SCL sont **synchronisés**. Une donnée de la ligne SDA est
considérée comme valide si la ligne SCL est au niveau haut. La ligne SDA
maintient la donnée (0 ou 1) tant que le SCL est au niveau haut. Une fois SCL
repassé au niveau bas, SDA peut alors changer de valeur pour le bit suivant de
l'octet à transférer.

<figure>
    <img src="Images/i2c_chrono_base.png" alt="" width="400">
    <figcaption>Fig. 1 : Transmissions de bits 1 puis 0 sur le bus I2C</figcaption>
</figure>

### Déroulement d'une communication sur le bus I2C

1. Prise de parole sur le bus par le Maître
2. Condition de départ (Start) par le Maître : passage de SDA de 1 à 0 tout en
gardant SCL à 1. La ligne SCL est cadencée par le Maître.
3. Envoi de l'adresse du destinataire (7 bits) + mode d'accès lecture/écriture (1
bit) par le Maître
4. Acquittement par l'Esclave

<figure>
    <img src="Images/i2c_rw.png" alt="" width="500"><br />
    <figcaption>Fig. 2 : I2C phase d'adressage</figcaption>
</figure>

5a. Transfert des données en lecture par l'Esclave puis  Acquittement du Maître

<figure>
    <img src="Images/i2c_lecture.png" alt="" width="500"><br />
    <figcaption>Fig. 3 : I2C lecture des données de l'Esclave vers le Maître</figcaption>
</figure>

5b. Transfert des données en écriture par le Maître puis Acquittement de l'Esclave

<figure>
    <img src="Images/i2c_ecriture.png" alt="" width="500"><br />
    <figcaption>Fig. 4:  I2C écriture des données du Maître vers l'Esclave</figcaption>
</figure>


6. Condition d'arrêt (Stop) par le Maître : passage de SDA de 0 à 1 tout en gardant
SCL à 1.

## 2.2.2b Cas d'usages

* Interroger un capteur I2C par le MCU
* Relier deux MCU en filaire pour échanger des données. Attention, la
communication est uniquement à l'initiative du Maitre. Par exemples :
  * Deux MCUs identiques : Arduino UNO <-> Arduino UNO
  * Deux MCUs différents : Arduino UNO <-> Raspberry Pi
* Lire et écrire des données dans une Mémoire Flash EEPROM reliée en I2C avec le MCU
