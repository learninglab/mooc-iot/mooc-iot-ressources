# Question 2.2.1

A quoi sert un bus de donnée ?

[X] A échanger des données entre un MCU et un composant (par ex. un capteur)
[X] A échanger des données entre deux MCU identiques
[X] A échanger des données entre deux MCU différents
[X] A échanger des données entre un MCU et Internet via une puce communicante
 branchée sur un bus de communication

[explanation]
Tous les choix sont valables.
[explanation]

#Question 2.2.2

Quel est le bus de donnée d'un objet connecté contraint autorisant les
transferts les plus rapides ?

() UART ?
() I2C ?
(X) SPI ?

[explanation]
SPI avec un débit maximum de 20 Mbits/s.
[explanation]

#Question 2.2.3

Quels sont les bus de données fiables qui acquitte les échanges de données ?

() UART
(X) I2C
() SPI

[explanation]
L'UART propose un bit de parité permettant de détecter une corruption lors du
transfert de donnée mais n'implémente pas d'acquittement de message par défaut.
On peut toutefois l'implémenter de manière logicielle avec des caractères
spéciaux XON/XOFF.  Le port série RS232 utilise deux lignes supplémentaires
RTS/CTS permettant de faire du contrôle de flux de donnée en matériel.

SPI ne propose pas d'acquittement par défaut pour les échanges de données. La
fiabilisation est à la discrétion du programmeur.

Enfin, I2C acquitte par défaut chaque échange de donnée en le Master et le
Slave.
[explanation]
