# 2.2.0 UART, I2C and SPI data buses

Current microcontrollers offer multiple types of data bus. The
role of these buses is to link the microcontroller’s inputs/outputs to the different
pieces of hardware of which an embedded system is comprised. Generally speaking, the type of hardware
will determine the type of data bus that is required.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>This sequence will introduce you to the different types of data bus, providing concrete examples of how they can be used with electronic components. You will learn how to use existing data buses for different components.
</p>
</div>
