# 2.2.4 Overview and elements of choice

UARTs offer the flexibility of point-to-point, bi-directional communication,
which can come from either of the two extremities. However, the format of the
frames is relatively rigid, and speeds can be insufficient in certain
cases.

Unlike UARTs, I2C and SPI data buses enable data exchange at
much faster speeds. However, these both require a
Master/Slave operating model, which can be an obstacle when it comes to initiating
communication. The I2C protocol specifies the data format and ensures
secure exchanges through acknowledgement. SPI, on the other hand, does not specify any message
format in particular, leaving this entirely up to the designer of the application.
This makes SPI much more flexible when it comes to transporting data, in addition to
being much quicker.

In terms of the number of components supported, I2C supports 128 addresses, divided up
into series of three lines. The Master of the SPI bus must have the right number
of pins for the SS lines linked to each of their Slaves.
