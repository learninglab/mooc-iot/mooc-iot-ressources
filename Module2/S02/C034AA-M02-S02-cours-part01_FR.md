# 2.2.1. Le bus de données UART

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.2.1a Fonctionnement](#221a-fonctionnement)
- [2.2.1b Cas d'usages](#221b-cas-dusages)
<!-- TOC END -->

## 2.2.1a Fonctionnement
*Universal Asynchronous Receiver Transmitter* ou **UART** est un bus de
communication point à point qui permet de faire transiter des données entre 2
MCUs ou 1 MCU et un circuit intégré (voir exemples ci-dessous).

L'UART utilise deux fils distincts **TX** et **RX** pour des flux de messages montants
et descendants (**bi-directionnels**) plus un fil de référence **GND** relié à la
masse.

La transmission est réalisée octet par octet. Le code embarqué de l'émetteur copie
l'octet dans l'UART émetteur qui va le transférer bit par bit vers l'UART du
récepteur. Une fois que les bits sont capturés pour reconstruire un octet complet,
ce dernier est alors remonté au code embarqué du récepteur.

L'implémentation du protocole utilise deux niveaux de tension type TTL pour
coder l'état d'un bit :

* le niveau logique haut ou  "1" correspondant à 5V ou 3.3V (VCC) en fonction du
niveau de voltage de l'alimentation du MCU
* le niveau logique bas ou  "0" correspondant à 0V (GND)

Le niveau logique par défaut est le 1 (pas de trafic). Une trame UART est
décomposée en bits de la manière suivante:

* **Start** : un bit à 0 servant à la synchronisation avec le récepteur
* **Données** : entre 5 et 9 bits. Attention, les bits sont envoyés dans l'ordre du bit de poids
faible (LSB Least Significant Bit) au bit de poids fort (MSB ou Most Significant
Bit). De manière générique, les bits de données constituent un symbole. Pour la
valeur 8, les données correspondent en particulier à un octet.
* **Parité** (optionnelle) : Paire ou Impaire.
* **Fin** : nombre de bits à 1 (1, 1.5 ou 2)

La vitesse de l'UART est définie en bps, c'est à dire le nombre de bits transmis en une seconde.
Cette vitesse est paramétrable avec différentes vitesses possibles, par exemple de 4800 bps jusqu'à 921600 bps.

Attention, les deux UARTS en relation doivent impérativement avoir la même
configuration pour pouvoir correctement décoder les trames.

Voici ci-dessous l'exemple d'une trame échangée entre deux UART.

<figure>
    <img src="Images/uart_trame1.png" alt="" height="400"><br />
    <figcaption>Fig. 1 : Exemple de trame entre deux UARTs</figcaption>
</figure>

En étudiant l'état des niveaux de tensions, on peut décomposer la trame comme
suit :

* **0** : bit Start de début de trame
* **10000110** : données inversées (LSB en premier)
* pas de bit de parité (OFF)
* **1** Stop

On convertit les bits de données pour obtenir le symbole final :

* **01100001** en bits
* **0x61** en hexadécimal ou **97** en décimal
* le caractère **a** dans le table ASCII

## 2.2.1b Cas d'usages

* **Interaction avec le MCU** : on peut brancher via les UART un MCU à un
ordinateur afin d'afficher des messages (printf) et d'attendre des messages à
saisir par l'utilisateur (scanf)

```
   UART MCU <-> UART ordinateur <-> terminal
```

* **Module GPS trames NMEA** : les composants de type GPS sont typiquement branchés derrière un UART afin de remonter des trames GPS au MCU. Ces trames GPS sont utiles pour géolocaliser un objet connecté ou pour synchronisation de l'horloge interne de l'objet.

```
   UART GPS <-> UART MCU
```
