# 2.2.0 Introduction

Les micro-contrôleurs actuels proposent plusieurs types de bus de données. Leur
rôle est de relier les entrées/sorties du micro-contrôleur vers les différents
périphériques d'un système embarqué. Généralement, le type de périphérique
impose un bus de données particulier.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez découvrir les différentes technologies de bus de données. Nous allons illustrer leurs usages avec des cas concrets de composants électroniques. Vous saurez ainsi utiliser les bus de données existants en fonction des composants choisis.
</p>
</div>
