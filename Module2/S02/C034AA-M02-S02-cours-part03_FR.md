# 2.2.3. Le bus de données SPI

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.2.3a Fonctionnement](#223a-fonctionnement)
- [2.2.3b Cas d'usages](#223b-cas-dusages)
<!-- TOC END -->

## 2.2.3a Fonctionnement

Le bus *Serial Peripheral Interface* ou **SPI** a été conçu dans les années 1980 par Motorola. La liaison SPI est un bus série utilisé pour la transmission synchrone de données entre un maître et un ou plusieurs esclaves (multipoints). La transmission a lieu en full duplex.

Le bus SPI est composé de deux lignes de données et deux lignes de signal, toutes unidirectionnelles:

* **MOSI** (Master Out Slave In) : cette ligne permet au maître de transmettre des données à l’esclave.
* **MISO** (Master In Slave Out) : cette ligne permet à l'esclave de transmettre des données au maître.
* **SCK** (SPI Serial ClocK) : Signal d’horloge, généré par le maitre, qui synchronise la transmission. La fréquence de ce signal est fixée par le maître et est programmable.
* **SS** (Slave Select) : Ce signal permet de sélectionner (adresser) individuellement un esclave. Il y a autant de lignes SS que d’esclaves sur le bus. Le nombre possible de raccordements SS du maître limitera donc le nombre d’esclaves.

<figure>
    <img src="Images/spi_topology.png" alt="" width="500">
    <figcaption>Fig. 1 : bus série pour les lignes SCLK, MISO, MOSI et ligne directe SS entre le Master et chaque Slave</figcaption>
</figure>

Au niveau du déroulement des échanges de messages:

1. Le Maître active le cadencement de l'horloge
2. Le Maître sélectionne un Esclave par la ligne SS (convertisseur, registre à décalage, mémoire, ..). Chaque esclave n’est actif que lorsqu’il est sélectionné.
3. Les données peuvent alors être échangées dans les deux entre le Maitre et l'Esclave en utilisant les lignes MISO et MOSI.

Tout comme le bus I2C, SPI fonctionne en mode Maître-Esclave. Par contre il ne propose pas de manière standardisée un mécanisme d'acquittement entre le Maitre et l'Esclave. SPI est aussi plus flexible que I2C sur les nombres de bits à transmettre par message et propose un débit plus important jusqu'à 20 Mbits/s.

## 2.2.3b Cas d'usages

* Echanger des données entre le MCU et une puce radio
* Lire et écrire des données dans une Mémoire Flash EEPROM reliée en SPI avec le MCU
