# 2.2.4 Synthèse et éléments de choix

L'UART offre la souplesse d'une communication point à point bi-directionnelle
pouvant être à l'initiative de chacune des extrémités. Cependant le format des
trames est relativement rigide et le débit peut être insuffisant dans certains
cas.

A l'opposé de l'UART, les bus I2C et SPI proposent des échanges de données sur
des débits beaucoup plus importants. Cependant ils imposent un mode de
fonctionnement Maître/Esclave pouvant être bloquant pour initier une
communication. Le protocole I2C spécifie le format des données et sécurise les
échanges grâce à des acquittements. A l'opposé, SPI n'impose aucun format
particulier de message qui est à la discrétion du concepteur de l'application.
SPI procure donc une plus grande souplesse pour le transport des données et un
meilleur débit.

Au niveau du nombre de composants supportés, I2C supporte 128 adresses, réparties
en série de trois lignes. Le Maitre du bus SPI devra posséder autant de broches
nécessaires pour les lignes SS dédiées vers chacun de ses Esclaves.
