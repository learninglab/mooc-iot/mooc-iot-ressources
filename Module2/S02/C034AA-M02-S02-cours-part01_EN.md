﻿# 2.2.1. UART data buses

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.2.1a How it works](#221a-how-it-works)
- [2.2.1b Use cases](#221b-use-cases)
<!-- TOC END -->

## 2.2.1a How it works
*Universal Asynchronous Receiver Transmitters* or **UARTS** are point-to-point
communication buses used to transfer data between 2
MCUs or 1 MCU and an integrated circuit (see example below).

UARTs use two distinct wires - **TX** and **RX** - for uplink and downlink
message flows (**bi-directional**) plus a grounded reference wire
**GND**.

Data is transmitted byte-by-byte. The emitter’s embedded code copies
the byte into the emitting UART, which then transfers it bit-by-bit to the receiver
UART. Once the bits have been captured, and a full byte has been formed,
this is then fed back to the receiver’s embedded code.

The implementation of the protocol uses two levels of TTL tension
in order to code the status of a bit:

* the high logic level or “1”, which corresponds to to 5 V or 3.3 V (VCC) depending on the
voltage used to supply the MCU
* the low logic level or “0”, which corresponds to 0 V (GND)

The default logic level is 1 (no traffic).  UART frames are
broken down into bits as follows:

* **Start**: a bit at 0 used for synchronizing with the receiver
* **Data**: between 5 and 9 bits. Please note, bits are sent in order, beginning with the
Least Significant Bit (LSB) and finishing with the Most Significant Bit
(MSB). Generally speaking, data bits constitute a symbol. For the
value 8, the data corresponds to one byte.
* **Parity** (optional): Even or Odd.
* **End**: number of bits at 1 (1, 1.5 or 2)


The speed of the UART is defined in bps, which is the number of bits transmitted in one second.
This speed can be configured to different possible speeds, from 4,800 bps up to 921,600 bps.

Please note, two UARTs in relation with each other must have the same
configuration if they are to be able to decode frames.

Below is an example of a frame exchanged between two UARTs

<figure>
    <img src="Images/uart_trame1.png" alt="" height="400"><br />
    <figcaption>Fig. 1: An example of a frame between two UARTs</figcaption>
</figure>

By studying the status of the tension levels, the frame can be broken down as
follows:

* **0**: the frame start bit
* **10000110**: inverted data (LSB first)
* no parity bit (OFF)
* **1** Stop

Data bits are converted in order to obtain the final symbol:

* **01100001** in bits
* **0x61** in hexadecimals or **97** in decimals
* the character **a** in the ASCII table

## 2.2.1b Use cases

* **Interaction with the MCU**: MCUs can be connected via UARTs to
computers in order to display messages (printf) and to wait for messages to
be entered by the user (scanf)

```
   MCU UART <-> computer UART <-> terminal
```

* **NMEA frame GPS module**: GPS type components are typically connected behind a UART in order to relay GPS frames to the MCU. These GPS frames are used to geotrack connected objects or to synchronize objects’ internal clocks.

```
   GPS UART <-> MCU UART
```
