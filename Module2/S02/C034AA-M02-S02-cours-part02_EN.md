# 2.2.2. The I2C data bus

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.2.2a How it works](#222a-how-it-works)
- [2.2.2b Use cases](#222b-use-cases)
<!-- TOC END -->

## 2.2.2a How it works

The **I2C** (Inter-Integrated Circuit) bus was developed by Philips in the
1980s in order to link an MCU to the different circuits built-in to
modern televisions. Historically, it is therefore found in a lot of
audio/video circuits.

It's a serial bus that allows electronic components to communicate with each other
via three wires or lines. The protocol implementation uses two signal lines plus one
reference line connected to ground.

* the **SDA** (Serial Data) line: used to transfer data
* the **SCL** (Serial Clock) line: used to time the sending of
messages on the SDA line
* the **GND** line (ground): used as a reference for high and low logical levels on the
SDA and SCL lines

Data exchanges are **bi-directional**, but with a Master-Slave
model
used. Messages are always sent by the Master. The I2C bus
can also have more than one master. Each subscriber (Master or Slave) has a
7-bit address allowing for no more than 128 addresses on the same bus. Each
byte transferred is acknowledged.

The speeds of I2C buses can reach as high as 100 kbits/s (standard) with the first
version of the protocol, and up to 400 kbits/s (fast) and 3.4 Mbits/s (HS) with the
latest versions of the protocol.

SDA and SCL lines use two logical levels: low or “0” and high or “1”:
The rest state mode of the I2C bus corresponds to the high level state for the
SDA and SCL lines.

### SDA and SCL synchronization

Signals between SDA and SCL are **synchronized**. A piece of data from the SDA line is
considered as being valid if the SCL line is high. The SDA line
will maintain the data (0 or 1) for as long as the SCL is high. Once the SCL
has gone back to low, SDA can then change value for the next bit of
the byte to be transferred.

<figure>
    <img src="Images/i2c_chrono_base.png" alt="" width="400">
    <figcaption>Fig. 1: Transmissions of bits 1 and then 0 on the I2C bus</figcaption>
</figure>

### The I2C bus communication process

1. The Master sends a message on the bus
2. Start condition by the Master: switches the SDA line from 1 to 0 while
keeping the SCL line at 1. The rate of the SCL line is controlled by the Master.
3. The Master sends the address of the recipient (7 bits) + read/write
access mode (1 bit)
4. Acknowledgement by the Slave

<figure>
    <img src="Images/i2c_rw.png" alt="" width="500"><br />
    <figcaption>Fig. 2: I2C addressing phase</figcaption>
</figure>

5a. Data transferred, read by the Slave, then acknowledged by the Master

<figure>
    <img src="Images/i2c_lecture.png" alt="" width="500"><br />
    <figcaption>Fig. 3: I2C reading of data sent by the Slave to the Master</figcaption>
</figure>

5b. Data transferred, written by the Master, then acknowledged by the Slave

<figure>
    <img src="Images/i2c_ecriture.png" alt="" width="500"><br />
    <figcaption>Fig. 4: I2C writing of data sent by the Master to the Slave</figcaption>
</figure>

6. Stop condition by the Master: switches the SDA line from 0 to 1 while keeping
the SCL line at 1.

## 2.2.2b Use cases

* Enabling an MCU to interrogate an I2C sensor
* Creating a wired link between two MCUs in order to exchange data. Please note,
only the Master may initiate communication. For example:
  * Two identical MCUs: Arduino UNO <-> Arduino UNO
  * Two different MCUs: Arduino UNO <-> Raspberry Pi
* Reading and writing data in an EEPROM flash memory, linked to the MCU using an I2C bus
