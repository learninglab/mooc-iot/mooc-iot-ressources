
# Module 2. Focus on Hardware Aspects

Objective: At the end of this module you will be able to explain the hardware architecture of a connected device with the energy constraints associated. You will also be able to classify IoT devices according to their role or application.

Hands-on activities (TP) :
TP2 : First IoT-Lab experiment


## Contents of Module 2

### 2.1. IoT Device Hardware Architecture
- 2.1.0. The hardware architecture of connected objects
- 2.1.1. Embedded systems
- 2.1.2. Microcontrollers
- 2.1.3. Sensors and actuators
- 2.1.4. Power supply

### 2.2. Data Buses: UART, I2C, SPI
- 2.2.0. Introduction
- 2.2.1. UART data buses
- 2.2.2. The I2C data bus
- 2.2.3. The SPI data bus
- 2.2.4. Overview and elements of choice

### 2.3. Connected objects: IoT-LAB M3
- 2.3.0. Introduction
- 2.3.1. The connected object IoT-LAB M3
- 2.3.2. The IoT-LAB gateway
- TP2. First IoT-Lab experiment
