
# Module 2. Focus sur les architectures matérielles

Objectif : A la fin de ce module, vous serez capable d'expliquer l'architecture matérielle d'un objet connecté avec les contraintes énergétiques liées. Vous serez également en mesure de classer les objets IoT par rôle ou application.

Activités pratiques (TP) :
TP2 : First IoT-Lab experiment


## Sommaire du Module 2

### 2.1. Architecture matérielle des objets de l'IoT
- 2.1.0. Architecture matérielle d’un objet connecté
- 2.1.1. Systèmes embarqués
- 2.1.2. Micro-contrôleur
- 2.1.3. Capteurs et actionneurs
- 2.1.4. Alimentation électrique

### 2.2. Les bus de données : UART, I2C, SPI
- 2.2.0. Introduction
- 2.2.1. Le bus de données UART
- 2.2.2. Le bus de données I2C
- 2.2.3. Le bus de données SPI
- 2.2.4. Synthèse et éléments de choix

### 2.3. Objets connectés : IoT-LAB M3
- 2.3.0. Introduction
- 2.3.1. L'objet connecté IoT-LAB M3
- 2.3.2. La passerelle IoT-LAB
- TP2. First IoT-Lab experiment
