# 2.1.4. Alimentation électrique

Aux différentes familles d'objets connectés, on peut associer différents niveaux
de consommation énergétique.

* Les **objets contraints** : objets déployés en masse, avec un système embarqué
 optimisé énergétiquement. La consommation en état d'activité est de l'ordre de
 quelques **dizaines de milliampères** et peut descendre jusqu'au **nanoampère**
 en mode veille.

* Les **passerelles** : objets nécessitant d'être toujours allumés pour relayer
 les messages des objets contraints. Les passerelles sont généralement couplées
 à un micro-contrôleur puissant pouvant faire tourner un OS classique (Linux)
 autorisant un traitement en amont de la plate-forme cloud. Cette passerelle est
 également connecté à Internet (Wi-Fi, Ethernet) avec une adresse IP. La
 consommation en état d'activité est de l'ordre de quelques **centaines de milliampères** en permanence.

## 2.1.4a Mode veille

Il est possible d'obtenir une autonomie de plusieurs mois, voire plusieurs années
pour un objet connecté. Les objets connectés qui atteignent plusieurs mois
d'autonomie énergétique sont à 99% en veille.

Afin de préserver au maximum la batterie, les micro-contrôleurs proposent une
gestion avancée de l'énergie. L'idée est de désactiver électriquement un ou
plusieurs ensembles de composants. On parle de mise en veille ou mode "sleep".
En fonction du fabricant ou de la famille de micro-contrôleur, plusieurs niveaux
de mise en veille sont proposés permettant d'économiser plus ou moins d'énergie.
D'une consommation de l'ordre du milliampère en fonctionnement normal, on
arrive à des consommations de l'ordre du nanoampère dans le mode le plus
optimisé. En contre-partie, ces modes avancées nécessitent un temps de réveil plus
long. Dans certains, les données présentes en RAM sont perdues suite à un reset
du micro-contrôleur lors de son réveil (ex: mode Deep Sleep ESP32 de Espressif).

## 2.1.4b Optimisation de l'accès au canal radio

Mise à part les objets connectés équipés d'écrans, le composant qui consomme le
plus d'énergie sur un objet connecté est la **puce radio**. L'émission radio (TX)
est généralement sensiblement plus énergivore qu'une réception radio (RX). Le
concepteur de l'objet connecté peut d'une part limiter les messages d'émissions
au niveau de son application. D'autre part, il convient d'optimiser la réception
radio en mettant périodiquement en veille ce composant. Ces optimisations sont
généralement traitées au niveau de la couche liaison du modèle OSI (couche MAC).

## 2.1.4c Vers des systèmes autonomes

Afin d'atteindre une autonomie énergétique totale, il convient d'adjoindre une
source d'alimentation qui produise plus d'énergie que n'en consomme l'objet
connecté.

Les solutions les plus utilisées sont des modules de recharges couplés à une
batterie (LiPo) stockant l'énergie et une ou plusieurs sources d'énergie
renouvelable produisant de l'électricité (ex: panneau solaire, éolienne). La
surface du panneau solaire doit être suffisante pour atteindre le niveau de
production souhaité et assurer l'autonomie complète du système.
