# 2.1.0. The hardware architecture of connected objects

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>This sequence gives you the opportunity to discover the different components of a connected object: its embedded system, its means of interacting with its environment; and its power supply and energy consumption.
</p>
</div>

## References

  * https://en.wikipedia.org/wiki/Microcontroller
  * https://en.wikipedia.org/wiki/Central_processing_unit

Video presentation introducing the hardware architecture of connected objects.
