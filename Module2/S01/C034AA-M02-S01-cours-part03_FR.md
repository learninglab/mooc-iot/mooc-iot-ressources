
# 2.1.3. Capteurs et actionneurs

Les objets connectés interagissent avec l'environnement physique et embarque de
ce fait un ou plusieurs capteurs ou actionneurs. Au niveau du système
embarqué, les capteurs/actionneurs sont vus comme des périphériques externes au
MCU et reliés par un bus de transmission de données : CAN (convertisseur A/N), numériques, I2C,
SPI, UART, CAN.

## 2.1.3a Capteurs

Un capteur est un élément capable de détecter un phénomène physique (déplacement, présence d’un objet
quantité de lumière, humidité, température).

* Température, humidité
* Air, Gaz
* Accéléromètre, GPS
* Micro
* Courant
* Pression, boutons, potentiometres, joystick
* Présence

## 2.1.3b Actionneurs

Un actionneur est un élément capable de créer un phénomène physique (déplacement d’objet, création de lumière,
création de chaleur, émission de sons). L’actionneur va créer le phénomène physique grâce à une source d’énergie

* LED
* Haut-parleur, vibreur
* Relais électrique
* Moteurs
