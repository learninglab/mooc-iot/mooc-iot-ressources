# Question 2.1.1

Quelles sont la ou les fonctionnalités matérielles qui composent un objet connecté contraint ?

[X] Il s'agit d'un système embarqué.
[ ] Il possède une puissance de traitement proche d'un ordinateur.
[X] Il embarque une batterie.
[X] Il intègre une puce communicante et il est connecté à Internet.
[X] Il embarque des capteurs/actionneurs.

[explanation]
Un objet connecté contrait est en premier lieu un **système embarqué** dont la
puissance de traitement est limitée mais suffisante pour traiter les
**capteurs/actionneurs** qui constituent son application. Le fait d'être sur **batterie**
est une contrainte forte qui nécessite d'utiliser le plus possible des
mécanismes de veille du microcontrôleur, et d'économiser également sur les
échanges de messages via sa **puce communicante**.
[explanation]

#Question 2.1.2

Quels sont les niveaux de consommation électrique possibles d'un objet connecté contraint selon son état ?

[X] Inférieure à 1 mA
[X] Comprise entre 1 mA et 150 mA
[ ] Supérieure à 150 mA

[explanation]
En considérant les deux états distincts en veille et en activité, un objet
connecté contraint consomme moins de 1 mA en veille, et entre 1 mA et 150 mA
en fonctionnement actif. Au delà, on se trouve dans la famille des objets
connectés de types passerelles non contraints nécessitant une source
d'alimentation continue.
[explanation]

#Question 2.1.3

Un objet connecté utilise plutôt un CPU ou un MCU ?

( ) un CPU ?
(X) un MCU ?

[explanation]
Pour faire baisser les coûts, améliorer la consommation d'énergie, minimiser
l'espace sur le système embarqué, un objet connecté utilise plutôt un **MCU** qui
intègre sur une même puce aussi bien un CPU que tous les composants vitaux
nécessaires à son fonctionnement (RAM, ROM, entrées/sorties, bus de
communication, etc).
[explanation]

# Question 2.1.4

Qu'elle sont les mécanismes d'un MCU permettant d'éviter à un objet connecté de
se trouver dans un état instable ?

( ) Les interruptions
(X) Les watchdogs
( ) Les convertisseur analogique/numérique (CAN)
( ) Les bus de données

[explanation]
Le watchdog, s'il n'est pas interrogé avant l'expiration de son compteur, permet d' automatiquement redémarrer le MCU pour répartir dans un état initial stable.
[explanation]
