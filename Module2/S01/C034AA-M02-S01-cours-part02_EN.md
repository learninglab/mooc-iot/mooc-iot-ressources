# 2.1.2. Microcontrollers

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.1.2a Microprocessor architecture](#212a-microprocessor-architecture)
- [2.1.2b From microprocessors to microcontrollers](#212b-from-microprocessors-to-microcontrollers)
- [2.1.2c Timers](#212c-timers)
- [2.1.2d Interrupts](#212d-interrupts)
- [2.1.2e Watchdogs](#212e-watchdogs)
- [2.1.2f Analog-to-digital converters (ADC)](#212f-analog-to-digital-converters-adc)
- [2.1.2g Digital inputs/outputs](#212g-digital-inputsoutputs)
<!-- TOC END -->


## 2.1.2a Microprocessor architecture

The **microprocessor** is the most important component of a microcontroller. These microprocessors are comprised of:

* a (CPU) **processor**, which contains:
  * a control unit used to manage instructions in the process of being executed
  * a clock used to set the tempo of the processor
  * registers, which are small, very quick internal memories used to
store instructions in the process of being executed
  * specialist processing units (arithmetical and logical or UAL, floating
point, etc.)

The CPU interacts with:

* **memories**:
  * read-only memory (ROM) for program storage (firmware)
  * random-access memory (RAM) for storing the program variables in the process of being executed
* **inputs/outputs** (I/O) connected to devices

CPU, memories and I/O are linked by three buses:

* the **address bus**, which is unidirectional and which is controlled by the CPU. This is used to
select the memory box in order to read or write either a piece of data or an instruction.
Its size determines the maximum amount of memory that can be allocated. For
example:

   * 16 bits: 2^16 =         65,536 bytes = 65.5 KB
   * 32 bits: 2^32 =  4,294,967,296 bytes = 4.29 GB

* the bidirectional **data bus**.  This is used to exchange data
or instructions between different elements. Its size will determine the CPU
category (8-bit, 16-bit, 32-bit).

* the **control bus**, which is used to control the type of operation

<figure>
<img src="Images/systeme_microproc_EN.png" width="500"/><br />
    <figcaption>Fig. 1: Architecture of a microprocessor</figcaption>
</figure>

## 2.1.2b From microprocessors to microcontrollers

One single processor is not enough to build an embedded system. Additional
components, such as RAM or ROM, for example, are
essential when it comes to putting together a functional object. In order to simplify the
design of embedded systems, manufacturers of components supply
complete pieces of hardware equipped with all core components needed
to operate at a minimum level. These are known as **microcontrollers** or MCUs.
Practically speaking, all components (CPU, RAM, ROM, etc.) are located on
the same chip, which is miniaturised by the manufacturer.

When it comes to developing embedded systems, there are a number of advantages to using MCUs
instead of multiple separate components:

- saves space on the printed circuit board: no routing between
  components, which require a minimum space between them for welding purposes; MCUs are
  single chips with input/output pins to be welded onto the embedded
  system
- more energy efficient - the components are closer
  together
- saves time on product certification: MCUs and their
  internal components have already passed this stage
- saves on manufacturing costs (components welded one by one)

In order to provide designers of embedded systems with as much flexibility as
possible, MCU manufacturers offer a wide range of products,
catering to all needs (CPU architecture, internal memory size, etc.).

In addition to the CPU and internal memory, MCUs are also generally equipped with
a set of peripheral functions for use in embedded programming, such as
timers, interrupts, watchdogs, analog-to-digital
converters, etc.

## 2.1.2c Timers

**Timers** are directly linked to the cycles or **ticks** of the clock of the MCU
with which they are synchronized. Depending on the type of MCU, timers are
coded either on 8 bits (256 values) or 16 bits (65535 values).  A standard MCU
will easily exceed 1000000 ticks per second (CPU > 1 MHz). In order
to use timers over longer periods, MCUs offer prescalers,
which use a fraction of the clock pulse of an MCU.

## 2.1.2d Interrupts

**Interrupts** managed by MCUs are linked to both internal and external
events. There are many different use cases for interrupts, which are essential
when it comes to smoothly programming embedded systems. Examples include
controlling the on/off state of a button, or the arrival of messages
on radio chips.  

For internal interrupts, internal MCU timers can be programmed
to generate interrupts once they have expired.

For external interrupts, external interrupt registers are linked
to an external pin on the MCU to which a piece of external hardware has been
connected (a sensor, a Real Time Clock (RTC), etc.). This piece of hardware will generate either a high
or a low signal, triggering the interrupt, which is then fed back to the MCU.

With regard to the embedded code, interrupts are linked to processing functions
to be executed. When the MCU detects an interrupt, the main code
in the process of being executed is paused in order to immediately switch to the function linked
to the interrupt. Once the interrupt function is complete, the main
code will return to its execution where it left off.

## 2.1.2e Watchdogs

The purpose of **watchdogs** is to ensure embedded systems remain in a consistent state. More
specifically, watchdogs prevent systems from becoming stuck in
programming loops or stuck waiting for hardware that is slow to respond.
In concrete terms, this is a countdown that the MCU must recharge
on a regular basis; otherwise, if the watchdog expires,
either an interrupt will be triggered on the MCU, or it will reset. The MCU will then
start again from the beginning of the program in a consistent state.

## 2.1.2f Analog-to-digital converters (ADC)

MCUs are also generally equipped with analog-to-digital converters,
or **ADCs**. ADCs receive analog signals,
from potentiometers or photoresistors, for example, which they then convert into
a digital value, which is typically precise to within 8 bits (256)
or 12 bits (4096).

## 2.1.2g Digital inputs/outputs

MCUs are capable of assigning external pins to digital input/output
ports. It is then necessary to specify the operating mode for the digital port
(input or output), in addition to whether or not an internal *pull-up* resistor
will be used to force the pin to its upper state. For example, an LED is
connected to a digital port operating as an output in order to switch the component
on or off.
