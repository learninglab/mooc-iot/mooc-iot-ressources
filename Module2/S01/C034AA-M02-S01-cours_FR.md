# 2.1.0. Architecture matérielle d'un objet connecté

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez découvrir quels sont les différents composants d'un objet connecté : son système électronique embarqué, ses moyens d'interaction avec son environnement ainsi que son alimentation électrique et sa consommation énergétique.
</p>
</div>

Présentation en vidéo de l'architecture matérielle d'un objet connecté : Comment est construit un dispositif IoT ? Quels sont ses composants essentiels ? Comment le rendre communicant ? Comment le faire fonctionner pendant des années ?

## Références

  * https://fr.wikipedia.org/wiki/Microcontrôleur
  * https://fr.wikipedia.org/wiki/Processeur
