
# 2.1.3. Sensors and actuators

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.1.3a Sensors](#213a-sensors)
- [2.1.3b Actuators](#213b-actuators)
<!-- TOC END -->

Connected objects have to be able to interact with their physical environment,
and so are fitted with multiple sensors or actuators. With regard to embedded
systems, sensors/actuators are seen as items of hardware which are external to the
MCU and which are linked via a data transmission bus: ADC, digital, I2C,
SPI, UART, CAN.

## 2.1.3a Sensors

A sensor is an element capable of detecting a physical phenomenon (displacement, presence of an object, amount of light, humidity, temperature).

* Temperature, humidity
* Air, gas
* Accelerometer, GPS
* Microphone
* Current
* Pressure, buttons, potentiometers, joysticks
* Presence

## 2.1.3b Actuators

An actuator is an element capable of creating a physical phenomenon (moving an object, creating light, creation of heat, emission of sounds). The actuator will create the physical phenomenon thanks to an energy source.

* LEDs
* Speakers, buzzers
* Electrical relays
* Motors
