# 2.1.1. Systèmes embarqués

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.1.1a L'objet connecté : un certain type de système embarqué](#211a-lobjet-connecté--un-certain-type-de-système-embarqué)
- [2.1.1b Architecture d'un système embarqué](#211b-architecture-dun-système-embarqué)
<!-- TOC END -->

## 2.1.1a L'objet connecté : un certain type de système embarqué

Un objet connecté n'est qu'une application grand public d'un système embarqué
communicant, présent depuis la fin des années soixante dans de nombreuses
applications industrielles. L'apparition des systèmes embarqués a été rendu
possible grâce à la miniaturisation des transistors, permettant de créer de
nouveaux composants comme les circuits intégrés.

Un circuit intégré ou puce électronique est un composant reproduisant une ou
plusieurs fonctions électroniques plus ou moins complexes. On peut le voir comme
une brique prête à l'emploi, prête à être intégrée dans une application.

On définit un système embarqué comme un système à la fois électronique et
informatique, conçu pour une tâche bien spécifique, fonctionnant de manière
autonome et, au besoin, en temps réel. Les systèmes embarqués sont
optimisés pour une application précise et doivent répondre aux différentes
contraintes suivantes :

- Cout de fabrication des composants
- Puissance de traitement, mémoire
- Consommation énergétique
- Encombrement optimal ("form factor")

Spécifiquement aux objets connectés, on ajoute également au système embarqué
les contraintes de communication suivantes :

- Bande passante pour l'envoi des messages
- Portée radio ou technologie filaire

## 2.1.1b Architecture d'un système embarqué

<figure>
    <img src="Images/C034AA-M02-S01-cours_FR_fig1_FR.png" alt="" height="400"><br >
    <figcaption>Fig. 1 : Aperçu d'un système embarqué</figcaption>
</figure>

On peut décomposer un système embarqué en plusieurs composants matériels et
logiciels (cf figure ci-dessus) :

* Le micro-contrôleur ou **MCU** qui coordonne les entrées-sorties vers les
différentes composants
* La **mémoire ROM** et **RAM** qui stocke le **logiciel** (firmware) de manière
permanente et les variables de manière volatile en cours d'exécution.
* Les **capteurs** et **actionneurs** spécifiques à l'application
* L'**alimentation électrique** qui fournit le courant et régule la tension pour
le MCU et divers composants :
	* Batterie
	* Entrée de courant
	* Module de charge
* Les différents **BUS de communication** locaux pour interfacer le MCU vers les
capteurs, actionneurs, radios, écrans, automates, etc :
	* CAN (convertisseur analogique-numérique) ou ADC en anglais
	* I2C
	* SPI
	* UART
	* MODBUS, CAN (Controller Area Network) surtout pour le milieu industriel et automobile
* Les puces communicantes pour les communications externes
	* Puce **radio**
	* Puce **filaire**
* **JTAG/JLINK** : puce utilisée pour reprogrammer le firmware de l'objet. Souvent
présente sur les cartes de développement, les objets produits en grande série
peuvent faire l'économie de ce type de composant. Il faut alors employer un
programmateur externe au système embarqué ou alors mettre à jour le firmware via
la puce radio en OTA (Over The Air).
