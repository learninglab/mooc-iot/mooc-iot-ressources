# 2.1.2. Micro-contrôleur

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [2.1.2a Architecture des micro-processeurs](#212a-architecture-des-micro-processeurs)
- [2.1.2b Du micro-processeur au micro-contrôleur](#212b-du-micro-processeur-au-micro-contrôleur)
- [2.1.2c Timers](#212c-timers)
- [2.1.2d Interruptions](#212d-interruptions)
- [2.1.2e Watchdog](#212e-watchdog)
- [2.1.2f Convertisseur analogique/numérique (CAN)](#212f-convertisseur-analogiquenumérique-adc)
- [2.1.2g Entrées/Sorties digitales](#212g-entréessorties-digitales)
<!-- TOC END -->

## 2.1.2a Architecture des micro-processeurs

Le **micro-processeur** est la pièce maitresse du micro-contrôleur. Il se compose :

* d'un **processeur** (CPU) qui contient :
  * une unité de contrôle gérant les instructions en cours d'exécution
  * une horloge qui cadence le fonctionnement du processeur
  * des registres qui sont des petites mémoires internes très rapides et servant à
stocker les instructions en cours d'exécution
  * des unités de traitement spécialisées (arithmétiques et logiques ou UAL, virgule
flottante, etc.)

Le micro-processeur interagit avec :

* des **mémoires** :
  * mémoire morte (ROM) pour stocker le programme (firmware)
  * mémoire vive (RAM) pour stocker les variables en cours d'exécution du programme
* des **entrés/sorties** (I/O) vers les périphériques

Le CPU, les mémoires et les I/O périphériques sont reliés par trois bus distincts:

* le **bus d'adresses**, unidirectionnel et piloté par le CPU. Il permet de
sélectionner la case mémoire pour lire ou écrire une donnée ou une instruction.
Sa taille détermine l'espace mémoire maximum que l'on peut allouer. Par
exemple :

   * 16 bits : 2^16 =         65 536 octets = 65,5 ko
   * 32 bits : 2^32 =  4 294 967 296 octets = 4,29 Go

* le **bus de données**, bidirectionnel.  Il permet d'échanger des données ou
instructions entre les différents éléments. Sa taille détermine la catégorie du
CPU (8 bits, 16 bits, 32 bits).

* le **bus de contrôle** qui permet de contrôler le type d'opérations

<figure>
<img src="Images/systeme_microproc_FR.png" width="500" /><br/>
    <figcaption>Fig. 1 : Architecture d'un micro-processeur</figcaption>
</figure>

## 2.1.2b Du micro-processeur au micro-contrôleur

Un processeur seul ne suffit pas à construire un système embarqué. Les
composants supplémentaires tels que par exemple la mémoire RAM, ROM sont
indispensables pour obtenir un objet fonctionnel. Afin de simplifier la
conception d'un système embarqué, les fabricants de composants proposent des
briques matérielles complètes embarquant tous les composants vitaux nécessaires
au fonctionnement minimal. On parle alors de **micro-contrôleur** ou MCU.
Concrètement l'ensemble des composants (CPU, RAM, ROM, ...) se trouvent sur une
même et unique puce qui a été miniaturisée de manière optimale par le fabricant.

Pour réaliser un système embarqué, les avantages à utiliser un MCU par rapport
à de multiples composants séparés sont multiples :

- gain de place du MCU sur le circuit imprimé : pas de routage entre de nombreux
  composants nécessitant un espace minimum entre eux pour les souder, le MCU est
  une unique puce avec des broches d'entrées/sorties à souder sur le système
  embarqué
- gain de consommation d'énergie lié à des chemins plus courts entre les
  composants
- gain de temps pour la certification matérielle du produit : le MCU et ses
  composants internes ont déjà passé cette étape.
- réduction des coûts liés à la fabrication (soudure des composants un par un)

Afin de proposer un maximum de souplesse pour les concepteurs de systèmes
embarqués, les fabricants de MCU proposent de très nombreuses références pour pouvoir
couvrir l'ensemble des besoins (architecture CPU, taille mémoire interne, ...).

En plus du CPU et de la mémoire interne, le MCU embarque généralement un
ensemble de fonctions périphériques utiles pour la programmation embarquée, comme
par exemple des timers, interruptions, watchdog, convertisseurs
analogiques/numériques.

## 2.1.2c Timers

Les **timers** sont directement liés aux cycles ou **ticks** de l'horloge du MCU
avec laquelle ils sont synchronisés. Selon les familles de MCU, il existe des
timers codés sur 8 bits (256 valeurs) ou 16 bits (65535 valeurs). Un MCU
standard dépasse largement 1000000 de ticks par seconde (CPU > 1MHz). Afin
d'utiliser des timers sur une plus longue période, le MCU propose des prescalers
qui utilise un sous-multiple de la fréquence d'horloge du MCU.

## 2.1.2d Interruptions

Les **interruptions** gérées par un MCU sont liées à des événements internes ou
externes. Les cas d'usage des interruptions sont multiples et indispensables
pour programmer facilement un système embarqué. Par exemple, on peut citer la
gestion de l'état on/off d'un bouton poussoir, l'arrivée d'un message sur une
puce radio.  

Dans le cas d'une interruption interne, on peut par exemple programmer un timer
interne du MCU qui une fois arrivé à expiration va générer une interruption.

Dans le cas d'une interruption externe, on associe un registre d'interruption
externe à une broche externe du MCU sur laquelle est branché un périphérique
externe (capteur, Real Time Clock ou RTC). Ce périphérique génère un signal haut
ou bas qui va déclencher l'interruption ensuite remontée au MCU.

Au niveau du code embarqué, l'interruption est liée à une fonction de traitement
à exécuter. Lorsqu'une interruption est détectée par le MCU, le code principal
en cours d'exécution est mis en pause pour immédiatement jouer la fonction liée
à l'interruption. Une fois la fonction d'interruption terminée, le code
principal reprend son exécution là où il s'était mis en pause.

## 2.1.2e Watchdog

Le **watchdog** a pour but de garantir un état cohérent du système embarqué. En
particulier, le watchdog évite que le système reste bloqué dans une boucle du
programme ou dans l'attente d'un périphérique un peu lent à répondre.
Concrètement il s'agit d'un compte à rebours que le MCU doit régulièrement
recharger. Dans le cas contraire, si le watchdog arrive à expiration, une
interruption et/ou un redémarrage (reset) du MCU est enclenché. Le MCU repart
alors depuis le début du programme dans un état cohérent.

## 2.1.2f Convertisseur analogique/numérique (CAN)

Un MCU embarque généralement un convertisseur analogique/numérique (CAN) ou Analog
Digital Converter (ADC) en anglais. A partir d'un signal analogique en entrée, par
exemple un potentiomètre ou une photo-résistance, le CAN va le transformer en une
valeur numérique dont sa précision est typiquement comprise entre 8 bits (256)
et 12 bits (4096).

## 2.1.2g Entrées/Sorties digitales

Un MCU peut assigner des broches externes à des ports d'entrées/sorties
numériques. Il convient alors de préciser le mode de fonctionnement du port digital
(en entrée ou en sortie) ainsi que l'usage ou non d'une résistance interne de
tirage forçant la broche à l'état haut (*pull-up*). Par exemple, une LED est
branchée sur un port digital fonctionnant en sortie pour allumer ou éteindre le
composant.
