# 2.1.4. Power supply

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.1.4a Standby mode](#214a-standby-mode)
- [2.1.4b Radio channel access optimization](#214b-mac-layer-optimisation)
- [2.1.4c Towards autonomous systems](#214c-towards-autonomous-systems)
<!-- TOC END -->

Different groups of connected objects have different levels
of energy consumption.

* **Constrained objects**: mass deployed objects, with energy-optimised
 embedded systems. In active state, consumption is somewhere in the region of
 **40/50 milliamperes**, and can drop as low as **nanoamperes**
 in standby mode.

* **Gateways**: objects that must always be switched on in order to relay
 messages from constrained objects. Gateways are normally coupled
 to powerful microcontrollers capable of running a classic OS (Linux)
 authorising processing ahead of the cloud platform. These gateways are
 also connected to the Internet (Wi-Fi, Ethernet) and have IP addresses. Energy consumption
 in active state is somewhere in the region of a few **hundred milliamperes** on a permanent basis.

## 2.1.4a Standby mode

Some connected objects are capable of operating autonomously for several months,
or even several years. Connected objects that are capable of energy-autonomy
over several months are in standby mode 99% of the time.

In order to extend battery life as much as possible, microcontrollers offer
advanced energy management, which involves electrically deactivating
multiple sets of components. This is referred to as standby or “sleep” mode.
Different manufacturers or types of microcontroller offer different
levels of standby, affecting how much energy is saved.
Energy consumption ranges from milliamperes in normal mode,
to nanoamperes in the most energy-efficient
modes. However, with these advanced modes, objects will take longer
to wake up. In some cases, RAM data can be lost when the microcontroller is
reset while it is waking up (e.g. Espressif ESP32 Deep Sleep mode).

## 2.1.4b Radio channel access optimization

Excluding connected objects with screens, the components that consume the
most energy on connected objects are **radio chips**. Radio transmission (TX)
ordinarily consumes significantly more energy than radio reception (RX). The
designer of the connected object may limit transmission messages
on their application. Alternatively, it is necessary to optimize the
radio reception by periodically putting this component to sleep. These optimizations are
generally treated at the link layer level of the OSI model (MAC layer).

## 2.1.4c Towards autonomous systems

In order to attain full energy autonomy, it is necessary to add a
power supply which produces more energy than is consumed by the connected
object.

The most widely-used solutions involve recharge modules coupled to
batteries (LiPo) storing energy and one or more sources of renewable
energy producing electricity (e.g. solar panels, wind turbines). The
solar panel must have a surface big enough to attain the requisite
level of production for making the system fully autonomous.
