# 2.1.1. Embedded systems

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [2.1.1a Connected objects: a certain type of embedded systems](#211a-connected-objects-a-certain-type-of-embedded-systems)
- [2.1.1b The architecture of embedded systems](#211b-the-architecture-of-embedded-systems)
<!-- TOC END -->


## 2.1.1a Connected objects: a certain type of embedded systems

Connected objects are simply mass market applications of communicating embedded
systems, that have been around since the late 1960s in a range of
industrial applications. The emergence of embedded systems was made
possible thanks to the miniaturisation of transistors, which enabled the creation of
new components, such as integrated circuits.

An integrated circuit or electronic chip is a component that reproduces
multiple electronic functions of varying degrees of complexity. These can be seen as
ready-to-use building blocks, capable of being integrated into applications.

Embedded systems are both electronic and computer
systems, which are designed for a specific task and which operate
autonomously and, where necessary, in real-time. Embedded systems are
optimised for precise applications and must meet the
following constraints:

- components manufacturing cost
- processing power, memory
- energy consumption
- form factor

Specifically with regard to connected objects, we can also add the following communication
constraints to embedded systems:

- bandwidth for sending messages
- the radio range or wired technology

## 2.1.1b The architecture of embedded systems


<figure>
    <img src="Images/C034AA-M02-S01-cours_EN.png" alt="" height="400"><br />
    <figcaption>Fig. 1: Overview of an embedded system</figcaption>
</figure>

Embedded systems can be broken down into individual hardware and software
components (see figure above):

* The microcontroller (**MCU**), which coordinates inputs/outputs for the
different components
* **ROM** and **RAM**, which store **software** (firmware) on a permanent
basis and variables on a volatile basis during execution
* **Sensors** and **actuators** specific to the application
* The **power supply**, which supplies current and controls voltage levels for
the MCU and various other components
	* The battery
	* Current input
	* Charger module
* The various different local **communication buses** used to interface the MCU with the
sensors, actuators, radios, screens, automatic control units, etc.
	* ADC: analogue and digital inputs/outputs
	* I2C
	* SPI
	* UART
	* MODBUS, CAN (particularly in industrial environment, the automobile industry, etc.)
* Communicating chips for external communications
	* **Radio** chips
	* **Wired** chips
* **JTAG/JLINK**: chips used to reprogram objects’ firmware. Often
found on development boards, objects produced in large volumes
can do without this type of component in order to save money. In such cases, an external programmer
must be employed for the embedded system, or the firmware must be updated via
the radio chip using OTA (Over The Air).
