
# Module 6. Sécurisation des objets connectés

Objectif : À la fin de ce module, vous serez capable d'identifier les problèmes de sécurité des objets connectés et les solutions existantes pour les résoudre.

Activités pratiques (TP) :
TP16: Hash
TP17: Encryption
TP18: Signature
TP19: DTLS communication
TP20: Over the Air Firmware update


## Sommaire du Module 6

### 6.1. Aperçu des problèmes de sécurité des objets connectés
- 6.1.0. Introduction
- 6.1.1. IoT et sécurité
- 6.1.2. Types d'attaques
- 6.1.3. En résumé

### 6.2. Cryptographie pour les objets connectés
- 6.2.0. Introduction
- 6.2.1. Primitives crypto pour la sécurité de l’IoT
- 6.2.2. Fonctions de hachage
- TP16 : Hash
- 6.2.3. Schéma de chiffrement
- TP17 : Encryption
- 6.2.4. Codes d’authentification des messages (MAC)
- 6.2.5. Schémas de signature électronique
- TP18 : Signature
- 6.2.6. Autres primitives cryptographiques

### 6.3. Sécurité des réseaux pour les objets connectés
- 6.3.0. Introduction
- 6.3.1. Sécuriser les communications
- 6.3.2. Sécurité des communications locales pour l’IoT
- 6.3.3. Sécurité de la couche de transport pour l’IoT
- TP19 : DTLS Communication
- 6.3.4. Sécurité de la couche d’application pour l’IoT

### 6.4. Mise à jour sécurisée d’un logiciel d’objets connectés (SUIT)
- 6.4.0. Introduction
- 6.4.1. L’importance des mises à jour logicielles
- 6.4.2. Mises à jour du micrologiciel de l’IoT conformes à la norme SUIT
- TP20 : Over the Air Firmware update
