## Synopsis

Catalyzed by the availability of open-source, general-purpose, embedded IoT software and open standards for IoT communication, IoT is being increasingly deployed.
As IoT is rolled out on billions of machines including very heterogeneous microcontroller-based devices, more reports pile up warnings about potential security threats -- and attacks which actually took place.

Cyberwarfare is nothing new: even before the advent of IoT, the Internet had become a battlefield. This cyberwar is in part government-driven (by geopolitics) and in part profit-driven (e.g. cyberpiracy).

In such a context, it is important to grasp what IoT currently offers in terms of functionality *vs* risk trade-off, what attack surface IoT needs to be secured against, and what mechanisms are used to mitigate attacks.

This module briefly overviews these aspects.
