
# Module 6. Securing Connected Objects

Objective: At the end of this module you will be able to identify the security problems of connected objects and the existing solutions to overcome them.

Hands-on activities (TP):
TP16: Hash
TP17: Encryption
TP18: Signature
TP19: DTLS communication
TP20: Over the Air Firmware update


## Contents of Module 6

### 6.1. Overview of Connected Objects Security Problems
- 6.1.0. Introduction
- 6.1.1. IoT and Security
- 6.1.2. Types of Attacks
- 6.1.3. In a nutshell

### 6.2. Cryptography for Connected Objects
- 6.2.0. Introduction
- 6.2.1. Crypto primitives for IoT security
- 6.2.2. Hash Functions
- TP16 : Hash
- 6.2.3. Encryption Schemes
- TP17 : Encryption
- 6.2.4. Message Authentication Codes
- 6.2.5. Digital Signature Schemes
- TP18 : Signature
- 6.2.6. Other Cryptographic Primtives

### 6.3. Network Security for Connected Objects
- 6.3.0. Introduction
- 6.3.1. Securing Communications
- 6.3.2. Local Communication Security for IoT
- 6.3.3. Transport Layer Security for IoT
- TP19 : Communication DTLS
- 6.3.4. Application Layer Security for IoT

### 6.4. Secured Update of a Connected Object Software (SUIT)
- 6.4.0. Introduction
- 6.4.1. The importance of Software Updates in IoT
- 6.4.2. SUIT-compliant IoT Firmware Updates
- TP20 : Over the Air Firmware update
