## Synopsis

L'IoT se déploie actuellement à grande échelle, s'appuyant à la fois sur la disponibilité de logiciels IoT embarqués (polyvalents, open source) et sur des normes ouvertes pour la communication des objets connectés.

Alors que l'IoT est déployé sur des milliards de machines, y compris sur des objets connectés très hétérogènes utilisant des microcontrôleurs, les rapports alarmistes s'accumulent, avertissant des menaces potentielles pour la sécurité, et listant les nombreuses cyberattaques ayant déjà eu lieu.

Le concept de cyberguerre n'est pas nouveau : avant même l'avènement de l'IoT, Internet était déjà devenu un champ de bataille. Cette cyberguerre est en partie menée par des gouvernements (aspects géopolitiques) et en partie motivée par le profit (par exemple: la cyberpiraterie).

Dans un tel contexte, il est important de comprendre :

- ce que l'IoT fourni actuellement en terme de compromis fonctionnalités/risques;
- quelle est la surface d'attaque que l'IoT expose, et qui doit donc être protégée;
- quels mécanismes sont utilisés pour contrer les attaques exploitant l'IoT.

Ce module donne un bref aperçu de ces aspects.
