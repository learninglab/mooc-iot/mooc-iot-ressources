# Question 5.4.1

Why are IoT software updates necessary?

(x) to fix bugs or mitigate new vulnerabilities discovered on IoT devices
(x) to modify, add or remove functionalities on IoT devices
(x) to comply to upcoming standards and regulation 

[explanation]
During the lifetime of an IoT device, both its environment and its usage may evolve, which typically requires its software to co-evolve, and thus to be updated, in order to adapt.
[explanation]

# Question 5.4.2

What does "end-to-end" mean concerning the integrity and authenticity guarantees provided by SUIT for IoT software updates?

( ) software updates are protected against unauthorized modifications, while in transit from the wireless gateway to the IoT device
( ) software updates are protected against unauthorized modifications, while in transit from the update server repository to the IoT device
(x) software updates are protected against unauthorized modifications, while in transit from the legitimate software publisher to the IoT device

[explanation]
SUIT specifies software update metadata, which is signed by the authorized software developer and/or publisher. Signature verification can be performed on the device itself, which provides the guarantee that the software update is legitimate and has not been tampered with, neither while being transported over the network, nor while resting at intermediate storage locations.
[explanation]
