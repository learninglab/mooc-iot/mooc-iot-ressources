# 6.4.0. Introduction

Enabling software updates is necessary to secure IoT devices. Hence, we will now present aspects of securing software updates for microcontroller-based IoT devices.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p> At the end of this sequence, you will then be able to apply secure IoT firmware updates, combining mechanisms for hashing, digital signature, and secure low-power networking which we introduced in the previous sequences of this module.
</p>
</div>

## Sources & References

- H. Tschofenig, E. Baccelli, “[Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security](https://hal.inria.fr/hal-02351892/document),” IEEE Security & Privacy, 2019.
- [SUIT Architecture](https://tools.ietf.org/html/draft-ietf-suit-architecture) Specifications.
- [SUIT Manifest](https://tools.ietf.org/html/draft-ietf-suit-manifest) Specifications.
- K. Zandberg, K. Schleiser, Francisco Acosta, H. Tschofenig, E. Baccelli, “[Secure Firmware Updates for Constrained IoT Devices Using Open Standards: A Reality Check](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8725488),” IEEE Access, 2019.
