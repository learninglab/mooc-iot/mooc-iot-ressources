# 6.4.1. L'importance des mises à jour logicielles

À l'ère d’Internet, le logiciel a donné raison à la maxime « on ne peut pas sécuriser
ce qu'on ne peut pas mettre à jour ». Par exemple, de nombreux périphériques IoT compromis par le [Mirai botnet](https://en.wikipedia.org/wiki/Mirai_(malware)) n’ont toujours pas reçu de correctifs (!) à ce jour, et sont devenus des fardeaux permanents. Et pourtant, [les mises à jour logicielles sont des vecteurs d'attaque elles-mêmes](http://ziyang.eecs.umich.edu/iesr/papers/ronen17may.pdf). Cette dualité fait de la sécurisation des logiciels un défi en général.

Au-delà de la correction des bugs et des failles récemment découverts, les logiciels modernes doivent être mis à jour régulièrement pour suivre l'évolution continue des exigences légales, du chiffrement et des modèles de menace. Enfin, les mises à jour logicielles sont également essentielles pour faciliter le déploiement de fonctionnalités nouvelles (ou améliorées) sur les périphériques IoT lors de leur cycle de vie.

Il est donc primordial d’activer et de sécuriser les mises à jour logicielles de l’IoT.
Nous pouvons distinguer plusieurs types de mises à jour logicielles :

- Modèle 1 : logiciel monolithique, intervenant unique : par exemple, une mise à jour du binaire du micrologiciel ;
- Modèle 2 : modules multiples, intervenant unique : par exemple, mise à jour de la configuration d'exécution ;
- Modèle 3 : modules multiples, intervenants multiples : par exemple, un smartphone proposant plusieurs applications développées et mises à jour par différentes entreprises développant des logiciels.

Jusqu'à présent, dans la pratique, les périphériques IoT sur microcontrôleurs suivent principalement le modèle 1.
Les mises à jour de micrologiciels IoT tirent parti d'une combinaison de mécanismes décrits dans les séquences précédentes de ce module : parmi ceux-ci, le hachage, la signature électronique et la mise en réseau sécurisée à faible consommation.

## Qu'y a-t-il de spécial à propos de l'IoT ?

De toute évidence, les logiciels Internet modernes sont plus proches du modèle 3, qui favorise une innovation plus rapide et une maintenance plus efficace.
Cela a été démontré très récemment par la transition du logiciel du smartphone du modèle 1 au modèle 3 (avant/après Android et iOS).
La complexité croissante due à la cybersécurité, à l'interopérabilité et aux exigences de gestion des périphériques pourrait pousser la maintenance logicielle de l’IoT à faible consommation vers un modèle de plus en plus proche du modèle 3 à l’avenir.
Mais pour l'instant, les mises à jour  « monolithiques » des micrologiciels demeurent la technique de mise à jour dominante pour les périphériques IoT bas de gamme.

Les mises à jour sécurisées du micrologiciel de l’IoT utilisent une combinaison de primitives crypto (hachage, signature électronique) et de protocoles réseau à faible consommation que nous avons introduits dans les séquences précédentes de ce cours.
Par conséquent, les mises à jour sécurisées du micrologiciel de l’IoT sont soumises aux mêmes contraintes et spécificités (voir les séquences précédentes de ce module).

Cependant, les mises à jour micrologicielles impliquent deux aspects importants :

1. La mémoire est encore plus sollicitée, car au lieu de stocker une seule image, il doit stocker plusieurs images et un chargeur d'amorçage. En outre, comme le micrologiciel dans sa totalité ne tient généralement pas dans la mémoire RAM, certaines opérations (comme le hachage) peuvent devoir être effectuées à la volée, car le binaire est reçu et stocké quelque part dans la mémoire flash avant la validation.

2. l'exécution de primitives système de bas niveau cruciales doit être sécurisée (comme l'écriture de la nouvelle image dans la mémoire flash exécutable). Des mécanismes spécifiques sont nécessaires pour cela, intégrés au périphérique, qui diffèrent des mécanismes applicables sur les ordinateurs moins limités - disposant d’une unité de gestion de la mémoire (MMU) par exemple.
