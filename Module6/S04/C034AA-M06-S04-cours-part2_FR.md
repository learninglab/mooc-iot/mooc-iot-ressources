
# 6.4.2. Mises à jour du micrologiciel de l'IoT conformes à la norme SUIT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.4.2a Métadonnées standard pour la mise à jour du micrologiciel de l'IoT](#642a-métadonnées-standard-pour-la-mise-à-jour-du-micrologiciel-de-liot)
- [6.4.2b Génération et validation du manifeste](#642b-génération-et-validation-du-manifeste)
<!-- TOC END -->


L'IETF a récemment formé le groupe de travail Software Updates for Internet of Things (SUIT) pour normaliser une solution de mise à jour sécurisée du micrologiciel de l'IoT.
Le processus de normalisation n'est pas encore terminé, mais les projets de spécifications fournissent déjà des éléments utiles.

SUIT décrit à la fois une architecture générique pour des mises à jour logicielles IoT sécurisées et un format de métadonnées concis qui peut être utilisé conjointement avec le binaire de mise à jour du micrologiciel.

## 6.4.2a Métadonnées standard pour la mise à jour du micrologiciel de l'IoT

Ces métadonnées (appelées *manifeste*) sont contenues dans une structure de données protégée, du moins contre la falsification, de bout en bout, du développeur de logiciels au périphérique lui-même.

Le manifeste contient plusieurs éléments pour indiquer à un
périphérique IoT d'installer uniquement le micrologiciel provenant d'une
source autorisée, n'ayant pas été modifié,
protégé de façon confidentielle (en option), adapté au matériel,
et conforme à diverses autres conditions.

La sécurité de bout en bout (authenticité, intégrité et, si nécessaire, confidentialité) est effectuée à l'aide de COSE (voir la séquence précédente de ce module).
Les protocoles à faible consommation, tels que CoAP (ou LwM2M), peuvent ensuite être utilisés pour transmettre le manifeste et l'image du micrologiciel aux périphériques IoT.

D'ailleurs, les primitives cryptographiques utilisées pour affirmer l'authenticité et l'intégrité du binaire de mise à jour de micrologiciel reçu sont une signature électronique, exécutée sur un hachis du binaire de micrologiciel.

## 6.4.2b Génération et validation du manifeste

Le périphérique est supposé être fourni avec la clé publique du développeur de logiciels autorisé (ou de l'éditeur de logiciels autorisé).
Cette entité peut produire un manifeste indiquant un hachis du binaire du micrologiciel et une signature de ce hachis.
Lors de la réception du manifeste et de la mise à jour du micrologiciel, le périphérique peut vérifier la signature et comparer la valeur de hachage reçue et la valeur de hachage calculée localement. Si ces valeurs correspondent et que la signature est valide, le binaire du micrologiciel reçu est considéré comme légitime (voir Figure 1).

Plusieurs configurations SUIT sont possibles, en utilisant par exemple différents types de signatures électroniques et de fonctions de hachage.
Par exemple, il est possible d'utiliser des signatures ed25519 et la fonction de hachage SHA256.

<figure>
    <img src="Images/SUIT-workflow.png" alt="Les phases d'une mise à jour logicielle suivant la norme SUIT, utilisant des signatures ed25519 et la fonction de hash SHA256." width="800"><br />
    <figcaption>Fig. 1 : Les phases d'une mise à jour logicielle suivant la norme SUIT, utilisant des signatures ed25519 et la fonction de hash SHA256.</figcaption>
</figure>
