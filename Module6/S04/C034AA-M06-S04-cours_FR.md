# 6.4.0. Introduction

La sécurité des objets connectés de l'IoT nécessite des mises à jour du logiciel s'exécutant sur ces machines. Nous allons donc maintenant présenter les spécificités de la sécurisation des mises à jour du micrologiciel s'exécutant sur des microcontrôleurs.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>À la fin de cette séquence, vous serez en mesure d'appliquer des mises à jour sécurisées du micrologiciel IoT, en combinant des mécanismes de hachage, de signature électronique et de protocoles réseau sécurisés à faible consommation énergétique, qui ont été introduits dans les séquences précédentes de ce module.
</p>
</div>

## Sources et références

- H. Tschofenig, E. Baccelli, “Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security,” IEEE Security & Privacy, 2019.
- Spécifications de l'[architecture SUIT](https://tools.ietf.org/html/draft-ietf-suit-architecture)
- Spécifications du [manifeste SUIT](https://tools.ietf.org/html/draft-ietf-suit-manifest)
- K. Zandberg, K. Schleiser, Francisco Acosta, H. Tschofenig, E. Baccelli, “[Secure Firmware Updates for Constrained IoT Devices Using Open Standards: A Reality Check](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8725488),” IEEE Access, 2019.
