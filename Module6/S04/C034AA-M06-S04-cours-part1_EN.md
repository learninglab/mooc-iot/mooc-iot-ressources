
# 6.4.1. The importance of software updates

Software in the Internet age has proven the maxim "you can’t secure what
you can’t update". For instance, many IoT devices compromised by the [Mirai botnet](https://en.wikipedia.org/wiki/Mirai_(malware))  are still (!) unpatched to this day, and have become permanent liabilities. And yet, [software updates are attack vectors themselves](http://ziyang.eecs.umich.edu/iesr/papers/ronen17may.pdf). This duality makes secure software a challenge in general.

Beyond patching newly discovered bugs and vulnerabilities, modern software must be updated regularly to keep up with the continuing evolution of legal requirements, crypto deprecation, and threat models. Last but not least: software updates are also crucial to facilitate deploying new (or improved) functionalities, on IoT devices during their life time.

Hence, it is crucial to enable and secure IoT software updates.
We can distinguish several cases of software updates:

- Model 1: monolithic software, single stakeholder: for example a firmware binary update;
- Model 2: multiple modules, single stakeholder: for example a run-time configuration update;
- Model 3: multiple modules, multiple stakeholders: for example a smartphone with multiple apps developed and maintained/updated by different software companies.

So far, in practice, microcontroller-based IoT devices mostly follow Model 1.
IoT firmware updates take advantage of a combination of mechanisms we described in the previous sequences of this module: in particular, hashing, digital signature, and secure low-power networking.

## What is special about IoT?

Obviously, modern Internet software is closer to Model 3, which fosters quicker innovation and better maintenance.
This was most recently evidenced by smartphone software when it transitionned from Model 1 to Model 3 (before/after Android and iOS).
Increasing complexity due to cybersecurity, interoperability, device management requirements might push low-power IoT software maintenance towards resembling more Model 3 in the future.
But for now, "monolithic" firmware updates is the dominant software update technique for low-end IoT devices.

Secure IoT firmware updates employ a combination of crypto primitives (hashing, digital signature) and low-power networking protocols we introduced in the previous sequences of this course.
Hence, secure IoT firmware updates are subject to the same constraints and specificities (see previous sequences of this module).

However, firmware updates add two important twists:

1. the memory budget is typically even more challenged because, instead of storing a single image, it must store multiple images and a bootloader. Furthermore, as a whole firmware typically does not fit in RAM memory, some operations (e.g. hashing) may have to be performed on-the-fly, as the binary is received and stored somewhere in flash before validation.

2. the execution of crucial low-level systems primitives should be secured (such as writing the new image to executable flash memory). Specific mechanisms are needed for that, on-board the device, which differ from mechanism applicable on less constrained computers -- which have a memory management unit (MMU) for instance.
