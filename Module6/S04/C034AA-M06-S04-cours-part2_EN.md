
# 6.4.2. SUIT-compliant IoT firmware updates

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.4.2a Standard metadata for IoT firmware update](#642a-standard-metadata-for-iot-firmware-update)
- [6.4.2b Manifest generation and validation](#642b-manifest-generation-and-validation)
<!-- TOC END -->

The IETF recently formed the Software Updates for Internet of Things (SUIT) working group to standardize a secure IoT firmware update solution.
The standardization process has not yet finished, but draft specifications already provide useful elements.

SUIT describes both a generic architecture for secure IoT software updates and a concise metadata format which can be used in conjunction with the firmware update binary.

## 6.4.2a Standard metadata for IoT firmware update

This metadata (called a *manifest*) is contained in a data structure that is protected, at least against tampering, end-to-end from the software developer, to the device itself.

The manifest contains several elements to instruct an
IoT device to install only firmware that comes from an
authorized source, has not been modified, is (optionally)
confidentiality protected, is suitable for the hardware, and
meets various other conditions.

The security end-to-end (authenticity, integrity and, if needed confidentiality) is performed using COSE (see previous sequence in this module).
Low-power protocols such as COAP (or LwM2M) can then be used to deliver the manifest and the firmware image to IoT devices.

In particular, the cryptographic primitives used to assert authenticity and integrity of the received firmware update binary is a digital signature, performed on a hash of the firmware binary.

## 6.4.2b Manifest generation and validation

The device is assumed to be preprovisionned with the public key of the authorized software developer (or the authorized software publisher).
This entity can produce a manifest indicating a hash of firmware binary and a signature of this hash.
Upon receiving the manifest and the firmware update, the device can both verify the signature and compare the received hash value and the locally computed hash value. If they match and the signature is valid, the received firmware binary is considered legitimate (see workflow shown Figure 1.)

Several SUIT configurations are possible e.g. using different types of digital signatures and hash functions.
For instance, one possible configuration is using ed25519 digital signatures, and SHA256 for the hash function.

<figure>
    <img src="Images/SUIT-workflow.png" alt="Phases of a SUIT update workflow, using ed25519 digital signatures and SHA256 hash." width="800"><br />
    <figcaption>Fig. 1: Phases of a SUIT update workflow, using ed25519 digital signatures and SHA256 hash.</figcaption>
</figure>
