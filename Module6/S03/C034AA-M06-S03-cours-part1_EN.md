
# 6.3.1. Securing Communications

A network stack is typically modular, consisting in a set of complementary mechanisms. Each mechanism is specified as a protocol, which defines the expected behavior of elements in the network. The reader may recall that protocols coarsely fall into the below categories.

<figure>
    <img src="Images/Layers.png" alt="Categories of protocols and a low-power IoT network stack." width="800"><br />
    <figcaption>Fig. 1: Categories of protocols and a low-power IoT network stack.</figcaption>
</figure>

Bottom-up, in terms of abstraction level:

- *Physical layer protocols* perform the transmission bit per bit over a communication
medium which connects 2 computers;

- *Medium access layer protocols* realize a local network and the transmission of packets over
a link shared by computers;

- *Network layer protocols* realize a global network, by defining packet formats & addresses
usable across heterogeneous local networks;

- *Routing protocols* establish & use paths across the global network;

- *Transport protocols* adapt the rate at which data chunks are sent across  the global network (& verify chunks went through);

- *Content-aware protocols* can work with user-facing names like "www.blabla.com/picture.jpg"

Each protocol provides a service, on which ultimately depend other parts of the system (e.g. higher layer protocols, operating systems or application logic). Disrupting a single protocol can thus disrupt the whole system, potentially. Hence, each category of protocol is prone to specific attacks, which call for specific mitigation. For instance:

- *Physical layer protocols* are prone to passive attacks such as *wiretapping*, whereby an attacker eavesdrops on the communication medium, or to active attacks such as *jamming* whereby an attacker saturates the communication medium, which thus becomes unusable;

- *Medium access protocols* are prone to attacks such as *spoofing*, whereby an attacker attempts masquerading another entity, e.g. faking a legitimate access point. Communications of nodes who attach may be disrupted or eavesdropped);

- *Network layer protocols* are prone to attacks such as a *replay attack*, whereby  valid data transmission is maliciously repeated or delayed. A replay attack can for instance be used to fake the identity of a legitimate entity on the network;

- *Routing protocols* are prone to attacks such as a *sinkhole*, whereby an attacker maliciously attracts all the traffic from neighbor nodes by advertising an unbeatable (fake) routing metric. Traffic can then be disrupted or analyzed;

- *Transport protocols* are prone to attacks such as tricks making them send packets at a rate much higher than sustainable by the receiver or the network, resulting in dropped packets, overflowing buffers and/or network congestion;

- *Content-aware protocols* are prone to attacks such as *Distributed denial-of-service (DDoS) attacks*, whereby several compromised nodes in the network collude to trick instances of the protocol into simultaneously send large, irrelevant content towards a server, which is overwhelmed and becomes unusable;

In a nutshell, communication security in practice must combine several defense mechanisms, working at different levels of the network stack, and protecting against different types of attacks.
Each of these defense mechanisms is necessary, but not sufficient to achieve communication security overall.

In this module we focus on a selection of mechanisms applicable on low-end IoT devices which:

* secure communication for medium access protocols;
* secure communication for transport protocols;
* secure communication for content-aware protocols.


## What is specific to IoT?

As already seen in Module 4, compared to the protocol stacks available on usual types of networks (e.g. TCP/IP
over Ethernet or WiFi), a low-power IoT network stack typically consists in
different protocols (see Fig. 1).
For instance: CoAP is typically used instead of HTTP, hence mechanisms securing CoAP are necessary (instead of mechanisms securing HTTP).
Thus, security mechanisms must be designed and applicable for these specific protocols.

In particular, security mechanisms and protocols for IoT must be able to function with low-cost, low-power devices. Roughly, compared to mechanisms running on other machines on the Internet, this means that IoT security mechanisms and protocols must be much more frugal, and in particular must:

- require 10^6 less (RAM and Flash) memory;
- require 10^6 less (control and user) traffic throughput;
- support much smaller packet size (very efficiently);
- function with much weaker CPU, 10^3 slower or less;

Another IoT specificity which must not be underestimated is a difference concerning the human factor, compared to securing a mobile phone or a PC. For the latter, a convenient human-computer interface (on-board screen/keyboard) and a 1:1 relationship with a human user can be assumed most of the time, and can thus be relied upon e.g. to provide credentials for bootstrapping, or to make common sense decisions such as approving a software update. On the contrary, in the case of low-power IoT devices, not only are typical human-computer interfaces typically absent, but also humans often have a 1:N relationship with IoT devices. These characteristics yield the need for much more automation, which is a challenge, especially at bootstrap phases.

Last but not least, secure storage of secrets (e.g. cryptographic keys) is a challenge on simplistic low-end IoT devices which may not provide traditional hardware mechanisms preventing unauthorized access to sensitive data stored on-board (e.g. credentials).
