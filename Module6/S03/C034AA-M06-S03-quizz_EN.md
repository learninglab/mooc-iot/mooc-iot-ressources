# Question 5.3.1

What guarantees provides IEEE 802.15.4 link layer security ?

( ) protection against sinkhole attacks
( ) confidentiality of communications from the cloud to the IoT device
(x) confidentiality and integrity of communications over a wireless link

[explanation]
Link layer security mechanisms provided by IEEE 802.15.4 only provides guarantees on a single hop over the wireless medium. It provides confidentiality and integrity of data  transferred over a wireless link from A to B (assuming the key is known only to A and B).
[explanation]

# Question 5.3.2

What categories of protocols can be used to guarantee the integrity of data transferred from a remote server (somewhere on the Internet) to an IoT device?

( ) link layer security
(x) transport layer security
(x) application layer security

[explanation]
Both transport layer security and application layer security can protect data during its transfer over the Internet. On the contrary, link layer security can only protect data transmitted over the local network.
[explanation]

# Question 5.3.3

What protocol protects against replay attacks exploiting UDP payload?

(x) DTLS
(x) OSCORE
( ) COSE

[explanation]
Both OSCORE and DTLS each specify a mechanism protecting against replay attacks. COSE does not.
[explanation]