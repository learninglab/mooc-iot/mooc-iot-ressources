# 6.3.3. Sécurité de la couche de transport pour l'IoT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.3.3a DTLS](#633a-dtls)
- [6.3.3b CoAPs, versions de DTLS et TLS](#633b-coaps-versions-de-dtls-et-tls)
<!-- TOC END -->

Une limitation fondamentale de la securité sur la couche MAC (par exemple chiffrer avec AES au sein de la couche IEEE 802.15.4) est que les données sont déchiffrées à chaque intermédiaire dans le réseau, introduisant des vulnérabilités de fait.
Pour sécuriser les communications qui transitent sur le réseau via plusieurs intermédiaires, des mecanismes complémentaires sont nécessaires, comme par exemple la sécurité à la couche transport (DTLS ou TLS) décrite ci-dessous.

Supposez que les noeuds A et B (par exemple, un périphérique IoT et un serveur distant sur Internet) puissent communiquer de bout en bout à l'aide d'un protocole de transport. Dans ce contexte, la sécurité de la couche de transport vise à établir et à maintenir un canal de communication sécurisé entre A et B sur ce protocole de transport.

## 6.3.3a DTLS

DTLS (Sécurité de la couche de transport de datagrammes) est un protocole standardisé par Internet Engineering Task Force (IETF) qui sécurise les communications via UDP.

DTLS garantit l'intégrité, l'authenticité et la confidentialité des données circulant dans le canal sécurisé qu'il établit de A à B, via UDP.

<figure>
    <img src="Images/Layers-Transport-sec.png" alt="Sécurité de la couche de transport au sein d'une pile de protocoles réseau IoT classique." width="800"><br />
    <figcaption>Fig. 3 : Sécurité de la couche de transport au sein d'une pile de protocoles réseau IoT classique.</figcaption>
</figure>

### Handshake DTLS

DTLS s'appuie sur une phase d'amorçage initiale, la couche *handshake*.
Ce dialogue initial établit un contexte de sécurité entre deux terminaux A et B.
Le terminal qui lance le dialogue initial est appelé *client* et l'autre terminal est appelé *serveur*.
Le dialogue initial entraîne l'*authentification* de A et B (à l'aide de clés asymétriques ou pré-partagées) et l'établissement d'un secret principal connu seulement à A et B : une *clé symétrique*.

### Couche d'enregistrement DTLS
Dans une deuxième phase, appelée *couche d'enregistrement* (en anglais: record layer), les paramètres et la clé symétrique établis par la couche de liaison sont utilisés pour chiffrer et déchiffrer les données (c'est-à-dire les  *enregistrements*) envoyées de A à B, et vice-versa, assurant la confidentialité, l'intégrité et l'authenticité.
Cette clé est utilisée jusqu'à ce que le canal soit détruit ou qu'une limite maximale d'enregistrements ait été échangée. La limite maximale dépend de l'algorithme. Ensuite, si A et B doivent à nouveau communiquer en toute sécurité, un nouveau dialogue initial est exécuté.
Notez que les enregistrements DTLS contiennent un numéro de séquence utilisé pour assurer la protection contre les attaques de réexécution.

### Suites de chiffrement

Au sein de ces principes, DTLS permet diverses configurations. DTLS permet notamment l'utilisation d'une variété de primitives crypto différentes : différentes *suites de chiffrement*.
Lors du dialogue initial, avant d'établir le secret principal, le client et le serveur négocient automatiquement les paramètres de la suite de chiffrement à utiliser.
L'exécution DTLS est plus ou moins lourde en calculs en fonction de la suite utilisée. Le calcul le plus lourd se produit lors du dialogue initial, lorsque la clé symétrique partagée est dérivée des clés publiques/privées de A et B.
Cependant, les dialogues initiaux se produisent assez rarement et d'autre part, des suites de chiffrement spécifiques peuvent être utilisées pour diminuer la charge de calcul des périphériques IoT bas de gamme (voir RFC 7925). Par exemple :

- TLS-PSK-WITH-AES-128-CCM est une suite de chiffrement qui peut être utilisée dans DTLS uniquement en cas de cryptographie à clé symétrique (AES 128 en mode CCM). Elle est relativement légère en termes de calcul et de mémoire, mais apporte moins de protection (sujet aux attaques par dictionnaire, et faible confidentialité de transmission).
- TLS-ECDHE-ECDSA-WITH-AES-128-CCM est une autre suite de chiffrement qui peut être utilisée dans DTLS, qui est relativement plus consommatrice de ressources, mais qui fournit des garanties plus fortes contre les attaques par dictionnaire et garantit une confidentialité de transmission plus forte. Cette configuration utilise la cryptographie à courbe elliptique (asymétrique) pendant la liaison pour dériver des clés symétriques utilisées temporairement au niveau de la couche d'enregistrement.


## 6.3.3b CoAPs, versions de DTLS et TLS

Une utilisation commune de DTLS pour l'IoT consiste à sécuriser les communications CoAP sur UDP. Cette construction (CoAP sur DTLS sur UDP) est parfois nommée CoAPs ou bien *coaps* (avec "s"  pour  sécurité).

Actuellement, la version la plus utilisée du protocole DTLS est DTLS 1.2. Toutefois, une nouvelle version du protocole (DTLS 1.3) est en cours de finalisation et devrait bientôt prendre le relais.

Par souci d'exhaustivité, ajoutons également quelques mots sur TLS. Le protocole TLS (Transport Layer Security) est l'équivalent de DTLS conçu pour fonctionner sur des transports fiables et orientés connexion tels que TCP, au lieu de fonctionner sur UDP.

Par rapport au DTLS, la plupart des mécanismes de TLS sont identiques, mais certains mécanismes auxiliaires sont absents. Ainsi, les mécanismes de détection de perte et de duplication des messages sont supprimés (le TCP garantit une livraison des données fiable et ordonnée). TLS 1.3 est la version la plus récente de ce protocole.
