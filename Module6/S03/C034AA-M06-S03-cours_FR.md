# 6.3.0. Introduction

Nous allons maintenant nous concentrer sur une sélection de mécanismes applicables aux petits objets connectés de l’IoT, qui sécurisent les communications aux niveaux de la couche MAC, de la couche transport et de la couche application.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p> À la fin de cette séquence, vous serez en mesure de combiner des protocoles sécurisant les communications IoT, oeuvrant à différents niveaux de la pile réseau, et et visant à empêcher différents types d'attaques.
</p>
</div>

## Sources et références

- H. Tschofenig, E. Baccelli, “[Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security](https://hal.inria.fr/hal-02351892/document),” IEEE Security & Privacy, 2019.
- IEEE Standard for Low-Rate Wireless Networks, [IEEE Std 802.15.4-2015](https://ieeexplore.ieee.org/document/7460875)
- [RFC 6347](https://tools.ietf.org/html/rfc6347): DTLS1.2
- [RFC 8446](https://tools.ietf.org/html/rfc8446): TLS1.3
- [RFC 7925](https://tools.ietf.org/html/rfc7925): TLS and DTLS profiles for IoT
- [RFC 8152](https://tools.ietf.org/html/rfc8152): COSE
- [RFC 8613](https://tools.ietf.org/html/rfc8613): OSCORE
