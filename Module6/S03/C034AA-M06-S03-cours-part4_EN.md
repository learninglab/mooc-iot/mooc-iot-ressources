
# 6.3.4. Application layer security for IoT

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.3.4a COSE](#634a-cose)
- [6.3.4b OSCORE](#634b-oscore)
<!-- TOC END -->

While security using CoAPs works well in a number of IoT use cases, it is not efficient for some IoT traffic patterns, and if a proxy intermediates, DTLS stops at the proxy and end-to-end security breaks. To cater for such use cases, application layer security can be used instead of transport layer security, as described next and shown Fig. 4.


Assume nodes A and B (e.g. an IoT device and a remote server on the Internet) can communicate end-to-end using a content-aware (application layer) protocol. In this context, transport layer security aims to establish and maintain a secure communication channel between A and B over this application layer protocol.

In the following we briefly overview two mechanisms used to secure communications over the application protocol CoAP.

<figure>
    <img src="Images/Layers-Application-sec.png" alt="Application layer security within a typical IoT network protocol stack." width="800"><br />
    <figcaption>Fig. 4: Application layer security within a typical IoT network protocol stack.</figcaption>
</figure>


## 6.3.4a COSE

Some traffic patterns in IoT incur infrequent, asynchronous communication,
and ”one-shot” payloads, e.g. software updates over the network. To secure such communication end-to-end, TLS/DTLS can be inefficient because of the handshake overhead. The IETF thus standardized a different security mechanism, COSE, which can be used over CoAP, and which we briefly cover in this section.

A recent trend in IoT is to use the Concise Binary Object Representation (CBOR) encoding and serialization format.
For reference, compared with the JavaScript Object Notation
(JSON) format, CBOR was designed with much smaller
code and message size in mind. COSE offers several
security services, including digital signatures, counter signatures, Message Authentication Code (MAC), encryption as well as
rudimentary key distribution methods using CBOR for serialization.

COSE can be seen as a set of building blocks
that applications use in the way they find useful (keeping
an eye on code size). A device implementing a firmware update solution may, for example, rely on asymmetric crypto
and would, therefore, implement the signature verification
capability offered by COSE and none of the other security
services (e.g. no symmetric key crypto).

### Securing IoT Data at Rest
An interesting characteristic of COSE is that it can be used to protect either synchronous data transmissions over the network, or asynchronous data transmissions, whereby transmitted data temporarily rests somewhere on the path towards its destination(s). An example of synchronous transmission is an IoT device sending its current sensor values to a dashboard application visualizing real-time data. An example of asynchronous transmission is the publication of a software update, which is first uploaded to a (untrusted) repository, and eventually downloaded by IoT devices, individually, when they are ready.


## 6.3.4b OSCORE

When CoAP is deployed with CoAP proxies and application layer gateways TLS/DTLS cannot provide end-to-end security because a classical DTLS exchange terminates at the gateway. To secure CoAP messages the IETF defines another communication security solution called Object
Security for Constrained RESTful Environments (OSCORE).

OSCORE defines a CoAP option and a mechanism reusing COSE to protect CoAP messages, providing end-to-end encryption, integrity, replay protection, and binding of response to request (thus countering some related attacks).

### Specific CoAP Option
An OSCORE packet is essentially a CoAP packet with an OSCORE Option, and an OSCORE Ciphertext as payload.

The OSCORE option is an OSCORE-specific header extending the CoAP packet header, which contains information such as a (sender) sequence number, and an ID which enables the receiver to figure out the security context (i.e. which pre-shared key to use) to decrypt the packet.


### COSE Encryption

OSCORE plaintext consists of the CoAP payload, CoAP options and CoAP method code which, when encrypted with a COSE-defined encryption procedure, results in OSCORE ciphertext. The encrypted CoAP options (i.e. the inner options) are used to communicate with the OSCORE-aware endpoint, and are opaque to the intermediary proxies.

Note that other options (so-called outer options) are not encrypted and are thus not protected.
The reason for this is that CoAP proxies must be able to inspect part of the message (including outer options).

Also note that OSCORE does not offer key management itself,
it has to rely on a separate key management mechanism.
For each end-point with whom secure communication is required,
OSCORE thus assumes that a shared secret (a symmetric key) is pre-established.

As such, OSCORE requires only symmetric cryptography operation.
The default encryption scheme is AES 128 bit in CCM mode.
