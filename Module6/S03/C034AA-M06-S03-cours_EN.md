# 6.3.0. Introduction

We are now going to focus on a selection of mechanisms applicable on low-end IoT devices which secure communication for medium access protocols, for transport protocols and for content-aware protocols.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>At the end of this sequence, you will then be able to combine protocols securing IoT communication, working at different levels of the network stack, preventing different types of attacks.
</p>
</div>

## Sources & References

- H. Tschofenig, E. Baccelli, “[Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security](https://hal.inria.fr/hal-02351892/document),” IEEE Security & Privacy, 2019.
- IEEE Standard for Low-Rate Wireless Networks, [IEEE Std 802.15.4-2015](https://ieeexplore.ieee.org/document/7460875)
- [RFC 6347](https://tools.ietf.org/html/rfc6347): DTLS1.2
- [RFC 8446](https://tools.ietf.org/html/rfc8446): TLS1.3
- [RFC 7925](https://tools.ietf.org/html/rfc7925): TLS and DTLS profiles for IoT
- [RFC 8152](https://tools.ietf.org/html/rfc8152): COSE
- [RFC 8613](https://tools.ietf.org/html/rfc8613): OSCORE
