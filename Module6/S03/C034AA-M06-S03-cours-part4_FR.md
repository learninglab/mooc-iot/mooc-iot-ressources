# 6.3.4. Sécurité de la couche d’application pour l'IoT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.3.4a COSE](#634a-cose)
- [6.3.4b OSCORE](#634b-oscore)
<!-- TOC END -->

La sécurité avec CoAPs fonctionne de manière efficace pour une partie des cas d'usages IoT. Cependant, pour d'autres cas d'usages présentants certaines charactéristiques de flux de données, ou bien impliquants un proxy, la sécurité de CoAPs devient inefficace, voire inopérante. Pour ces cas d'usages IoT, en remplacement de la sécurité de la couche transport (DTLS) utilisé dans CoAPs, des protocoles de sécurité peuvent être employés à la couche application comme décrit ci-dessous, et montrés Fig. 4.

Supposez que les nœuds A et B (par exemple, un périphérique IoT et un serveur distant sur Internet) puissent communiquer de bout en bout à l'aide d'un protocole compatible avec le contenu (couche d'application). Dans ce contexte, la sécurité de la couche de transport vise à établir et à maintenir un canal de communication sécurisé entre A et B sur ce protocole de couche d'application.

Dans ce qui suit, nous allons brièvement présenter deux mécanismes utilisés pour sécuriser les communications sur le protocole d'application CoAP.

<figure>
    <img src="Images/Layers-Application-sec.png" alt="Sécurité de la couche d'application au sein d'une pile de protocoles réseau IoT commune." width="800"><br />
    <figcaption>Fig. 4 : Sécurité de la couche d'application au sein d'une pile de protocoles réseau IoT commune.</figcaption>
</figure>

## 6.3.4a COSE

Certains modèles de trafic dans l'IoT entraînent des communications asynchrones rares
et des charges utiles « ponctuelles », comme les mises à jour logicielles sur le réseau. Pour sécuriser une telle communication
de bout en bout, TLS/DTLS n'est pas efficace dû aux coûts des handshakes. L'IETF a donc normalisé un mécanisme de sécurité différent, COSE, qui peut être utilisé par dessus CoAP, et que nous couvrons brièvement dans cette section.

Une tendance récente de l'IoT consiste à utiliser le format de codage et de sérialisation "Concise Binary Object Representation" (CBOR).
Pour information, en comparaison au format "JavaScript Object Notation"
(JSON), CBOR a été conçu avec un code
et une taille de message beaucoup plus restreints. COSE offre plusieurs
services de sécurité, y compris les signatures électroniques, les signatures de compteur, les codes d'authentification des messages (MAC), le chiffrement
ainsi que des méthodes de distribution de clés rudimentaires utilisant CBOR pour la sérialisation.

COSE peut être considéré comme un ensemble de blocs de construction
que les applications utilisent selon leurs besoins
(selon la taille du code). Un périphérique qui met en œuvre une solution de mise à jour du micrologiciel peut, par exemple, compter sur une crypto asymétrique
en utilisant la capacité de vérification de la signature
offerte par COSE, sans avoir besoin
des autres services de sécurité (par exemple, pas de crypto de clé symétrique).

### Sécurisation des données IoT au repos
Une caractéristique intéressante de COSE est qu'il peut être utilisé pour protéger soit les transmissions de données synchrones sur le réseau, soit les transmissions de données asynchrones, où les données transmises sont stockées temporairement quelque part sur le chemin vers leur(s) destination(s). Exemple de transmission synchrone : un périphérique IoT envoie ses valeurs de capteur actuelles à une application de tableau de bord pour visualiser les données en temps réel. Exemple de transmission asynchrone : la publication d'une mise à jour logicielle, qui est d'abord téléchargée dans un référentiel (non approuvé), avant d’être téléchargée par les périphériques IoT, individuellement, lorsqu'ils sont prêts.


## 6.3.4b OSCORE

Lorsque CoAP est déployé avec les proxys CoAP et les passerelles de couche d’application, TLS/DTLS ne peut pas assurer une sécurité de bout en bout car un échange DTLS classique prend fin sur la passerelle. Pour sécuriser les messages CoAP, l'IETF définit une autre solution de sécurité de communication appelée Object Security for Constrained RESTful Environments (OSCORE).

OSCORE définit une option CoAP et un mécanisme qui réutilise COSE pour protéger les messages CoAP, fournissant un chiffrement, une intégrité, une protection de relecture et une liaison réponse-demande de bout en bout (donc capable de prévenir certaines attaques liées).

### Option CoAP Spécifique
Un paquet OSCORE est en fait un paquet CoAP avec une Option OSCORE et une suite de chiffrement OSCORE faisant office de charge utile.

L'option OSCORE est un en-tête spécifique à OSCORE qui étend l'en-tête de paquet CoAP, contenant des informations telles qu'un numéro de séquence (expéditeur) et un ID qui permet au récepteur de déterminer le contexte de sécurité (c'est-à-dire la clé pré-partagée à utiliser) pour déchiffrer le paquet.


### Chiffrement COSE

Le texte en clair d'OSCORE comprend la charge utile CoAP, les options et le code de méthode COAP ; lorsqu'il est chiffré avec une procédure de chiffrement définie par COSE, il produit une suite de chiffrement OSCORE. Les options CoAP chiffrées (c'est-à-dire les options internes) sont utilisées pour communiquer avec le point d'extrémité OSCORE et demeurent opaques aux proxys intermédiaires.

Notez que les autres options (appelées options externes) ne sont pas chiffrées et ne sont donc pas protégées.
La raison en est que les proxys CoAP doivent pouvoir inspecter une partie du message (y compris les options externes).

Notez également qu’OSCORE n'offre pas la gestion des clés en soit,
il doit s'appuyer sur un mécanisme de gestion des clés distinct.
Pour chaque terminal avec lequel une communication sécurisée est requise,
OSCORE suppose donc qu'un secret partagé (une clé symétrique) est préétabli.

Ainsi, OSCORE ne nécessite qu'une opération de cryptographie symétrique.
Le schéma de chiffrement par défaut est AES 128 bits en mode CCM.
