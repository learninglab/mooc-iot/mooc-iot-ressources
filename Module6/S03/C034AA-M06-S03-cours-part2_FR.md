# 6.3.2. Sécurité des communications locales pour l'IoT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.3.2a Sécurité IEEE 802.15.4](#632a-sécurité-ieee-802154)
- [6.3.2b Frais généraux des mécanismes de sécurité](#632b-frais-généraux-des-mécanismes-de-sécurité)
<!-- TOC END -->

Supposez que les noeuds A et B (par exemple, un périphérique IoT et un point d'accès sans fil) peuvent communiquer avec un protocole MAC/PHY sur un support partagé - un réseau local. Dans ce contexte, la sécurité de la couche d'accès moyen vise à protéger ces communications contre les attaques telles que les écoutes, l'usurpation ou la réexécution.

Les mécanismes de sécurité sont spécifiés différemment en fonction des protocoles utilisés au niveau des couches MAC et des couches physiques. Dans ce qui suit, nous examinons plus spécifiquement les mécanismes de sécurité pour les radios IEEE 802.15.4.

<figure>
    <img src="Images/Layers-MAC-sec.png" alt="Sécurité de la couche MAC au sein d'une pile de protocoles réseau IoT classique." width="800"><br />
    <figcaption>Fig. 2 : Sécurité de la couche MAC au sein d'une pile de protocoles réseau IoT classique.</figcaption>
</figure>

## 6.3.2a Sécurité IEEE 802.15.4

La sécurité IEEE 802.15.4 est basée sur le chiffrement symétrique avec AES.
La distribution, l'établissement et la maintenance des clés sont hors du champ d'application de la norme IEEE 802.15.4.

En général, les clés sont pré-fournies et le chiffrement/déchiffrement AES est effectué dans le matériel (généralement beaucoup plus efficace que dans le logiciel).

Les charges utiles (messages en clair) sont divisées en blocs de taille fixe.
Chaque bloc est chiffré/déchiffré via AES.
Différentes configurations sont possibles, qui spécifient les "modes" de sécurité détaillant la manière dont AES doit être utilisé dans le détail (voir ci-dessous).

De plus, les paquets envoyés par radio doivent porter des informations de contrôle supplémentaires (en-tête et suffixe) comme décrit dans les sections suivantes.

### Sous-couche de sécurité

Le chiffrement/déchiffrement est effectué par une sous-couche de sécurité, qui est insérée dans la pile, entre la couche physique et le traitement normal des paquets IEEE 802.15.4.

Outre les opérations cryptographiques, la sous-couche de sécurité étend également les paquets sortant en ajoutant un en-tête supplémentaire et un suffixe, dédiés à la sécurité.

Les paquets entrants sont validés par la sous-couche de sécurité à l'aide de l'en-tête et du suffixe supplémentaires.
S'il est validé par la sous-couche de sécurité, le paquet déchiffré est supprimé des champs de sécurité supplémentaires et transmis au traitement de paquets entrants IEEE 802.15.4 normal.


### Modes de chiffrement
La primitive de chiffrement de base utilisée dans IEEE 802.15.4 est généralement AES 128. Cependant, cette primitive est utilisée différemment selon le mode de fonctionnement indiqué dans l'en-tête de sécurité.

**mode compteur (CTR) --**
Dans ce mode, pour chaque bloc, les valeurs successives d'un compteur pré-convenu sont utilisées pour dériver cryptographiquement un masque de bits spécifique à ce bloc.

Plus précisément, la valeur du compteur est liée à un nom occasionnel (un nombre pseudo-aléatoire non secret utilisé une fois). Ce résultat est d'abord chiffré avec la clé symétrique (AES 128), puis XORé avec le bloc (texte en clair), afin d'obtenir un cryptogramme.

Le récepteur peut déchiffrer et récupérer le texte en clair parce qu'il connaît la clé, le nom occasionnel et la valeur de compteur actuelle, soit tout ce qui est nécessaire à l'élaboration du masque de bits.

**mode de chiffrement de bloc chaîné (CBC) --**
Dans ce mode, le texte en clair de chaque bloc est d'abord XORé avec le texte de chiffrement du bloc *précédent* , dont le résultat est ensuite chiffré avec la clé symétrique (AES 128).

Le cas spécial du premier bloc est XORé avec un vecteur d'initialisation (IV), soit un nombre pseudo-aléatoire non secret.

Le récepteur peut déchiffrer avec la clé, en supposant qu'il conserve temporairement en mémoire le texte chiffré du ou des bloc(s) qui précède(nt) immédiatement.

**mode CTR avec CBC (CCM) --**
Dans ce mode, CTR comme CBC sont utilisés. Pour chaque bloc, CBC est d'abord calculé sur le message pour obtenir une balise ; le message et la balise sont ensuite chiffrés à l'aide de CTR.


### En-tête et suffixe de paquet auxiliaire

La sous-couche de sécurité gère une table en mémoire qui correspond aux clés crypto avec les identificateurs de noeuds (indiquant la clé à utiliser, pour communiquer avec qui, ainsi que la valeur actuelle du compteur et le dernier VI utilisé pour chaque clé).

En outre, la sous-couche de sécurité gère l'ajout, le traitement et la validation d'un en-tête supplémentaire et/ou d'un suffixe dans les paquets IEEE 802.15.4, dédiés au contrôle de sécurité.

Plusieurs configurations sont possibles. La configuration utilisée est indiquée dans l'en-tête de sécurité. Les configurations incluent :

- AES-CTR-128 : dans cette configuration, la charge utile est chiffrée à l'aide du mode de chiffrement CTR avec AES 128.
- AES-CBC-MAC-128 : dans cette configuration, un code d'authentification de message (MAC) est suffixé à la charge utile. Ce MAC est calculé à l'aide du mode de chiffrement CBC avec AES 128 (et tronqué à 4, 8 ou 16 octets, selon la configuration). Cette configuration garantit l'authentification et l'intégrité des messages, mais pas la confidentialité de la charge utile.
- AES-CCM-128 : dans cette configuration, la charge utile et le MAC sont chiffrés à l'aide de CTR (le MAC est calculé à l'aide de CBC). Cette configuration garantit l'authentification et l'intégrité des messages ainsi que la confidentialité de la charge utile.

La sous-couche de sécurité valide les paquets entrants en fonction de la configuration et de la clé cryptographique actuellement utilisée par l'émetteur du paquet. Si les vérifications réussissent, l'en-tête de sécurité et le suffixe sont supprimés et le paquet est transmis pour le traitement normal de trame IEEE 802.15.4. Sinon, le paquet est abandonné.

## 6.3.2b Frais généraux des mécanismes de sécurité

Notez qu'*il n'y a rien de gratuit*.
Bien que les mécanismes ci-dessus fournissent des garanties en termes de confidentialité, d'intégrité et d'authenticité des données transmises par radio, ces mécanismes de sécurité sont fournis au prix d'une longueur de trame accrue, d'une utilisation plus intense du processeur et d'une empreinte mémoire plus importante. Tout ceci a un impact négatif sur les performances en matière de latence, de débit et d'utilisation de la mémoire.

Notez également que la sécurité est *facultative* dans la norme.
