# 6.3.1. Sécuriser les communications

Une pile réseau est généralement modulaire, composée d'un ensemble de mécanismes complémentaires. Chaque mécanisme est spécifié comme protocole, qui définit le comportement attendu des éléments du réseau. Le lecteur se souviendra peut-être que les protocoles relèvent grossièrement des catégories ci-dessous.

<figure>
    <img src="Images/Layers.png" alt="Catégories de protocoles et pile réseau IoT basse consommation." width="800"><br />
    <figcaption>Fig. 1 : Catégories de protocoles et pile réseau IoT basse consommation.</figcaption>
</figure>

Du bas vers le haut, en termes de niveau d'abstraction :

- les *protocoles de couche physique* effectuent la transmission bit par bit via un moyen
 de communication qui connecte 2 ordinateurs ;

- les *protocoles de couche d'accès au support physique* constituent un réseau local et transmettent des paquets
via un lien partagé par les ordinateurs ;

- les *protocoles de couche réseau* produisent un réseau global, en définissant les formats et les adresses des paquets
utilisables sur des réseaux locaux hétérogénes ;

- les *protocoles de routage* établissent et utilisent des chemins sur le réseau global ;

- les *protocoles de transport* adaptent la vitesse à laquelle les fragments de données sont envoyés sur le réseau global (et vérifient les fragments passés) ;

- les *protocoles sensibles au contenu* peuvent fonctionner avec des noms d'utilisateurs, par exemple "www.blabla.com/picture.jpg".

Chaque protocole fournit un service qui dépend en fin de compte d'autres parties du système (par exemple, protocoles de couche supérieure, systèmes d'exploitation ou logique d'application). La perturbation d'un protocole unique peut potentiellement perturber l'ensemble du système. Par conséquent, chaque catégorie de protocole est sujette à des attaques spécifiques, qui exigent des mesures d'atténuation spécifiques. Par exemple :

- les *protocoles de couche physique* sont sujets à des attaques passives telles que le *branchement clandestin*, par lesquelles un attaquant surveille le moyen de communication, ou à des attaques actives telles que le *brouillage* par lesquelles un attaquant sature le support de communication, qui devient ainsi inutilisable ;

- les *protocoles d'accès moyens* sont sujets à des attaques telles que l'*usurpation*, par lesquelles un attaquant tente de masquer une autre entité, par exemple en simulant un point d'accès légitime. Les communications des noeuds qui se connectent peuvent être perturbées ou écoutées ;

- les *protocoles de couche réseau* sont sujets à des attaques *par réexécution*, où la transmission de données valides est répétée ou retardée de façon malveillante. Une attaque par réexécution peut par exemple être utilisée pour contrefaire l'identité d'une entité légitime sur le réseau ;

- les *protocoles de routage* sont sujets à des attaques de type *entonnoir*, où un attaquant attire de manière malveillante tout le trafic des noeuds voisins en annonçant une métrique de routage imbattable (fausse). Le trafic peut alors être interrompu ou analysé ;

- les *protocoles de transport* peuvent subir des attaques qui les forcent à envoyer des paquets à un taux beaucoup plus élevé que ce qui est accepté par le récepteur ou le réseau, ce qui entraîne des pertes de paquets, des tampons surchargés et/ou une congestion du réseau ;

- les *protocoles sensibles au contenu* peuvent subir des attaques DDoS ou *déni de service réparti*, par lesquelles plusieurs noeuds compromis du réseau s'allient pour tromper les instances du protocole pour les forcer à envoyer simultanément un contenu important et non pertinent vers un serveur, qui est ainsi submergé et devient inutilisable ;

En bref, la sécurité des communications doit dans la pratique combiner plusieurs mécanismes de défense, travailler à différents niveaux de la pile de réseau, et protéger contre différents types d'attaques.
Chacun de ces mécanismes de défense est nécessaire, mais pas suffisant pour assurer la sécurité de la communication dans son ensemble.

Dans ce module, nous nous concentrons sur une sélection de mécanismes applicables aux périphériques de l'IoT bas de gamme qui :

* sécurisent la communication pour les protocoles d'accés moyen ;
* sécurisent la communication pour les protocoles de transport ;
* sécurisent la communication pour les protocoles compatibles avec le contenu.


## Qu'est-ce qui est spécifique à l'IoT ?

Comme on l'a déjà vu dans le module 4, par rapport aux piles de protocoles que l'on trouve sur les types de réseaux classiques (par exemple TCP/IP
via l'Ethernet ou la WiFi), une pile réseau IoT de faible consommation se compose généralement de
différents protocoles (voir Fig. 1).
Par exemple : COAP est généralement utilisé au lieu de HTTP, de sorte que les mécanismes de sécurisation du COAP sont nécessaires (au lieu de mécanismes de sécurisation du protocole HTTP).
Ainsi, les mécanismes de sécurité doivent être conçus et applicables à ces protocoles spécifiques.

D'autant plus que les mécanismes et protocoles de sécurité destinés à l'IoT doivent pouvoir fonctionner avec des appareils basiques, consommant peu. Pour simplifier, par rapport aux mécanismes à l'oeuvre sur d'autres machines sur Internet, cela signifie que les mécanismes et protocoles de sécurité IoT doivent être beaucoup plus économes, et doivent notamment :

- nécessiter 10^6 fois moins de mémoire (RAM et Flash) ;
- nécessiter un débit effectif 10^6 fois moindre (contrôle et utilisateur) ;
- prendre en charge une taille de paquet beaucoup plus petite (de façon très efficiente) ;
- fonctionner avec un CPU beaucoup plus faible, 10^3 fois plus lent ou moins ;

Une autre spécificité de l'IoT qui ne doit pas être sous-estimée est la différence du facteur humain, par rapport à la sécurisation d'un téléphone mobile ou d'un PC. Pour ces derniers, une interface homme-ordinateur simple (écran/clavier) et une relation 1:1 avec un utilisateur humain suffit la plupart du temps, et peut ainsi être sollicitée par exemple pour fournir des informations d'identification pour le démarrage, ou pour prendre des décisions évidentes, comme l'approbation d'une mise à jour logicielle. Au contraire, dans le cas des périphériques de l'IoT à faible consommation, non seulement les interfaces homme-ordinateur typiques sont généralement absentes, mais les humains ont aussi une relation 1:N avec ces périphériques. Ces caractéristiques prouvent que l'automatisation est nécessaire, ce qui constitue un défi, en particulier lors des phases d'amorçage.

Enfin, le stockage sécurisé des secrets (clés cryptographiques, par exemple) est un problème pour les périphériques de l'IoT bas de gamme basiques qui peuvent ne pas fournir de mécanismes matériels traditionnels empêchant l'accès non autorisé aux données sensibles stockées (par exemple, les informations d'identification).
