
# 6.3.3. Transport layer security for IoT

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.3.3a DTLS](#633a-dtls)
- [6.3.3b CoAPs, DTLS versions and TLS](#633b-coaps-dtls-versions-and-tls)
<!-- TOC END -->

One limitation of security at the link layer (e.g. encrypting with AES on IEEE 802.15.4) is that communications are decrypted at each intermediary on the path through the network.
This caracteristic introduces vulnerabilities.
Complementary security mechanisms must be used to guarantee security over a communication channel crossing several network intermediaries, e.g. transport layer security as described below.

Assume nodes A and B (e.g. an IoT device and a remote server on the Internet) can communicate end-to-end using a transport protocol. In this context, transport layer security aims to establish and maintain a secure communication channel between A and B over this transport protocol.

## 6.3.3a DTLS

DTLS (Datagram Transport Layer Security) is a protocol  standardized by the Internet Engineering Task Force (IETF) which secures communications over UDP.

DTLS guarantees the integrity, authenticity and confidentiality of the data flowing through the secure channel it establishes from A to B, over UDP.

<figure>
    <img src="Images/Layers-Transport-sec.png" alt="Transport layer security within a typical IoT network protocol stack." width="800"><br />
    <figcaption>Fig. 3: Transport layer security within a typical IoT network protocol stack.</figcaption>
</figure>


### DTLS Handshake

DTLS relies on an initial bootstrap phase called the *handshake* layer.
The handshake sets up a security context between two endpoints A and B.
The end point initiating the handshake is called the *client*, while the other end point is called the *server*.
The handshake results in the *authentication* of A and B (using asymmetric or pre-shared keys) and the establishment of a master secret known only to A and B: a *symmetric key*.

### DTLS Record Layer
In a second phase, called the *record layer*, the parameters and the symmetric key established by the handshake layer are used to encrypt and decrypt data (i.e. so-called *records*) sent from A to B, and vice-versa, ensuring confidentiality, integrity and authenticity.
This key is used until the channel gets torn down or a maximum limit of records have been exchanged. The maximum limit depends on the algorithm. Beyond that point, should A and B need to communicate securely again, a new handshake is performed.
Note that in particular, DTLS records contain a sequence number which is used to provide protection against replay attacks.

### Ciphersuites

Within these principles, DTLS allows various configurations. Among others DTLS allows the use of a variety of different crypto primitives: different *ciphersuites*.
During the handshake, prior to establishing the master secret, client and server automatically negotiate the cipher suite parameters to be employed.
DTLS operation is more or less computationally-heavy depending on the ciphersuite used. The heaviest computation happens in the handshake, when the shared symmetric key is derived from A and B's public/private keys.
However, on the one hand handshakes happen rather infrequently, and on the other hand specific ciphersuites can be used to decrease computation load on low-end IoT devices (see RFC 7925). For instance:

- TLS-PSK-WITH-AES-128-CCM is a ciphersuite which can be used in DTLS which is only based on symmetric key cryptography (AES 128 in CCM mode). It is relatively lightweight in terms of computation and memory requirements, at the price of less protection (prone to dictionary attacks, and weak forward secrecy).
- TLS-ECDHE-ECDSA-WITH-AES-128-CCM is another ciphersuite which can be used in DTLS, which is relatively more resource-consuming, but which provides stronger guarantees against dictionary attacks and forward secrecy. This configuration uses elliptic curve (asymmetric) cryptography during the handshake to derive symmetric keys used temporarily at the record layer.


## 6.3.3b CoAPs, DTLS versions and TLS

A typical use of DTLS for IoT is to secure CoAP communications over UDP. This construct (CoAP over DTLS over UDP) is sometimes named CoAPs or the coaps scheme, (whereby the "s" is for secure).

Currently, the most widely used version of the DTLS protocol is DTLS 1.2. However, a new version of the protocol (DTLS 1.3) is being finalized, and is expected to take over soon.

For sake of completeness, let's also add a few words about TLS. The Transport Layer Security (TLS) protocol is the equivalent of DTLS designed to operate over reliable, connection-oriented transports such as TCP, instead of operating over UDP.

Compared to DTLS, most of the mechanisms of TLS are the same, but some auxiliary mechanisms are absent, e.g. mechanisms detecting message loss and duplication are removed (as TCP guarantees reliable and in-order delivery of data). TLS 1.3 is the most recent version of this protocol.
