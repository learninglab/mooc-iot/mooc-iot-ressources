
# 6.3.2. Local Communication Security for IoT

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.3.2a IEEE 802.15.4 Security](#632a-ieee-802154-security)
- [6.3.2b Security Mechanisms Overhead](#632b-security-mechanisms-overhead)
<!-- TOC END -->

Assume nodes A and B (e.g. an IoT device and a wireless access point) can communicate with a MAC/PHY protocol over a shared medium -- a local network. In this context, Medium Access layer security aims to protect such communications against attacks such as eavesdropping, spoofing or replay.

Security mechanisms are specified differently depending on the actual protocols used at the MAC and physical layers. In the following we look more specifically into security mechanisms for IEEE 802.15.4 radios.

<figure>
    <img src="Images/Layers-MAC-sec.png" alt="MAC layer security within a typical IoT network protocol stack." width="800"><br />
    <figcaption>Fig. 2: MAC layer security within a typical IoT network protocol stack.</figcaption>
</figure>


## 6.3.2a IEEE 802.15.4 Security

IEEE 802.15.4 security is based on symmetric cryptography with AES.
Key distribution, establishment and maintenance is outside of the scope of the IEEE 802.15.4 standard.

Typically keys are pre-provisionned, and AES encryption/decryption is performed in hardware (generally much more efficient than in software).

Payloads (clear text messages) are broken into blocks of fixed size.
Each block is encrypted/decrypted using AES.
Different configurations are possible, which specify security "modes" detailing how AES is used exactly (see below).

Furthermore, packets sent over the radio must carry additional control information (header and suffix) as described in the following.

### Security sublayer

Encryption/decryption is performed by a security sublayer, which is inserted in the stack, between the physical layer and the normal IEEE 802.15.4 packet processing.

Additionally to performing cryptographic operations, the security sublayer also extends outgoing packets with a supplementary header and a suffix dedicated to security.

Incoming packets are validated by the security sublayer using the supplementary header and suffix.
If validated by the security sublayer, the decrypted packet is stripped from the supplementary security fields and passed on to normal IEEE 802.15.4 incoming packet processing.


### Encryption modes
The basic encryption primitive used in IEEE 802.15.4 is typically AES 128. However, this primitive is used slightly differently depending on the mode of operation indicated in the security header.

**Counter (CTR) mode --**
In this mode, for each block, the successive values of a pre-agreed counter are used to cryptographically derive a bit mask that is specific to this block.

More precisely, the value of the counter is concatenated with a nonce (a non-secret pseudo-random number used once). This result is first encrypted with the (AES 128) symmetric key and then XORed with the (clear text) block, the result of which is the cipher text.

The receiver can decrypt and recover the clear text because it knows the key, the nonce and the current counter value, which is all that is needed to construct the bit mask.

**Chained Block Cipher (CBC) mode --**
In this mode, the clear text of each block is first XORed with the cipher text of the *previous* block, the result of which is then encrypted with the (AES 128) symmetric key.

The special case of the first block is XORed with a so-called Initialization Vector (IV), a non-secret pseudo-random number.

The receiver can decrypt with the key, assuming it temporarily keeps in memory the ciphertext of the immediately preceding block(s).

**CTR with CBC (CCM) mode --**
In this mode both CTR and CBC are used. For each block, CBC is first computed on the message to obtain a tag; the message and the tag are then encrypted using CTR.


### Auxiliary Packet Header and Suffix

The security sublayer manages a table in memory which matches crypto keys with nodes identifiers (indicating which key to use, to communicate with whom, as well as current counter value and last IV used for each key).

Furthermore, the security sublayer manages appending, processing and validation of an additional header, and/or a suffix in IEEE 802.15.4 packets, dedicated to security control.

Several configurations are possible. The configuration used is indicated in the security header. Configurations include:

- AES-CTR-128: in this configuration, the payload is encrypted using the CTR encryption mode with AES 128.
- AES-CBC-MAC-128: in this configuration a Message Authentication Code (MAC) is suffixed to the payload. This MAC is computed using the CBC encryption mode with AES 128 (and truncated at 4, 8 or 16 bytes, depending on the configuration). This configuration guarantees message authentication and integrity, but not confidentiality of the payload.
- AES-CCM-128: in this configuration, the payload and the MAC are encrypted using CTR (the MAC is computed using CBC). This configuration guarantees message authentication and integrity as well as payload confidentiality.

The security sublayer validates incoming packets according to the configuration and cryptographic key currently in use for the originator of the packet. If checks pass, security header and suffix are stripped, and the packet is passed on for normal IEEE 802.15.4 frame processing. Else it is dropped.

## 6.3.2b Security Mechanisms Overhead

Note that *there is no free lunch*.
While the above mechanisms provide guarantees in terms of confidentiality, integrity and authenticity of the data transmitted over the radio, these security mechanisms come at the price of increased frame length, CPU usage and memory footprint, which negatively impact performance regarding latency, throughput and memory usage.

Also note that security is *optional* in the standard.
