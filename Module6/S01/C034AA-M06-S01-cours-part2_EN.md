# 6.1.2. Types of attacks

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.1.2a Network attacks](#612a-network-attacks)
- [6.1.2b Software attacks](#612b-software-attacks)
- [6.1.2c Hardware attacks](#612c-hardware-attacks)
- [6.1.2d Cyberphysical attacks?](#612d-cyberphysical-attacks)
<!-- TOC END -->


To secure IoT devices, one must consider attacks of diverse natures. Let's start by distinguishing the main types of attacks.

<figure>
    <img src="Images/C034AA-M05-S01.png" alt="Different types of attacks on IoT devices." width="800"><br />
    <figcaption>Fig. 1: Different types of attacks on IoT devices.</figcaption>
</figure>


## 6.1.2a Network attacks

Network attacks aim to exploit the network as attack surface. During such an attack, a malicious intermediary inserts itself on the communication path and may:

* disrupt communication,
* analyse traffic sent over the network, or
* trick other entities on the network into amplifying another larger attack.

For instance, a *sinkhole* attack tricks elements into sending network traffic through a rogue node, which can dispose of the traffic altogether. A *man-in-the-middle* attack tricks end-points of a communication with a stealth intermediary, e.g. to maliciously bypass some access control checks.

This type of attack is cheap for attackers - who can easily set up tentative attacks, remotely, on a massive number of devices.

Specific network protocols must be used to defend against these attacks, while fitting both low-power communication constraints and resource constraints on microcontrollers.

## 6.1.2b Software attacks

Software attacks aim to exploit vulnerabilities in the embedded firmware. During such an attack, malicious logic input is fed to the software running on the IoT device which can lead to:

* The IoT device leaking data which should remain private, or
* An attacker covertly taking control of the IoT device.

For instance, *malware* is hostile software which is stealthily installed on the device, and which performs malicious operations. A *buffer overflow* exploits a software vulnerability (i.e. missing memory bound checks) to read or modify memory which should *not* be accessible normally.

These types of attacks are more cumbersome to put in place for attackers, but can bring higher value. For example, it can result in a botnet of compromised IoT devices.

Additional mechanisms must be used to defend against such attacks, while still fitting microcontrollers’ resource constraints.
For example, such mechanisms can protect reading, writing or executing some part of the memory. These are either themselves implemented in software, or are provided by specific hardware functionalities (e.g. a "trusted" execution environment such as [ARM TrustZone](https://www.arm.com/why-arm/technologies/trustzone-for-cortex-m)).

As an example, before deploying and running some software, formal verification of this software can guarantee the absence of some vulnerabilities. After the software is deployed and running, if other vulnerabilities are discovered (which happens often down the line) secure updates over the network can patch the software to harden it, incrementally. Nevertheless, software updates themselves can become an attack vector, if a legitimate update is somehow laced with malicious logic.


## 6.1.2c Hardware attacks

Hardware attacks aim on the other hand to exploit vulnerabilities based on physical interaction with the IoT device.

For instance, a fine-grained analysis of power consumption during cryptographic operations may leak information that should remain private (for instance digits of a secret cryptographic key). This type of attack is called a *side-channel attack*.

Or, a malicious use of strong magnetic field around the device may cause this device to skip some critical checks. This type of attack is called a *fault injection* attack.

These types of attacks are very cumbersome as they require both physical access to a targeted device and, often, quite specific tooling.

Again, mitigating such attacks may require both modifying software (e.g. guarantee constant time execution of some cryptographic primitive) and modifying hardware (e.g. hardening the device).

## 6.1.2d Cyberphysical attacks?

On top of network, software and hardware attacks, new types of attacks might exploit cyberphysical characteristics of the system.

A concrete example of such an attack is a *chain reaction* such as the one causing the electric power-grid disruption, using a smart heater botnet, which we described earlier in this section.

Another example is based on the principle of exploiting *extended functionality* on some elements in the system. The hacked smart lighting system we described earlier (weaponized to cause epilepsy) falls in this category.

In fact, such attacks are not easy to classify yet. Currently, research is only beginning to study them.
