# 6.1.0. Brève introduction à la sécurité de l’IoT

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Au terme de cette séquence, vous saurez identifier les différents types d’attaques visant les objets connectés, et comprendre les défis afférents.
</p>
</div>

## Sources et références

- H. Tschofenig, E. Baccelli, “[Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security](https://hal.inria.fr/hal-02351892/document),” IEEE Security & Privacy, 2019.

- S. Soltan et al. "[BlackIoT: IoT Botnet of high wattage devices can disrupt the power grid](https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-soltan.pdf)." USENIX Security 18, 2018.

- E. Ronen, A. Shamir, "[Extended functionality attacks on IoT devices: The case of smart lights](https://ieeexplore.ieee.org/abstract/document/7467343)." IEEE EuroS&P, 2016.


Vidéo de présentation des défis liés à la sécurité de l’IoT : par rapport à la sécurisation d’Internet, quels sont les risques spécifiques IoT? Quels sont les types d’attaques potentielles? Comment sécuriser les objets connectés ? Quelles sont les contraintes pour sécuriser une application IoT?
