# 6.1.2. Types d'attaques

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.1.2a Attaques réseau](#612a-attaques-réseau)
- [6.1.2b Attaques logicielles](#612b-attaques-logicielles)
- [6.1.2c Attaques matérielles](#612c-attaques-matérielles)
- [6.1.2d Attaques cyberphysiques ?](#612d-attaques-cyberphysiques)
<!-- TOC END -->

Pour sécuriser les objets connectés, il est d'abord nécessaire de cataloguer les différent types d'attaques possibles. De quelle(s) nature(s) sont ces attaques potentielles?

<figure>
    <img src="Images/C034AA-M05-S01.png" alt="Types d'attaques visant les objets connectés." width="800"><br />
    <figcaption>Fig. 1 : Types d'attaques visant les objets connectés.</figcaption>
</figure>


## 6.1.2a Attaques réseau

Comme leur nom l'indique, les attaques réseau exploitent le réseau comme surface d'attaque. Leur principe est qu'un intermédiaire malveillant s'insère quelque part sur le chemin pris par la communication et ce faisant :

* perturbe la communication, ou
* analyse le trafic envoyé sur le réseau, ou
* leurre d'autres entités sur le réseau pour amplifier (à leur insu) une autre attaque de plus grande envergure.

Une attaque *sinkhole*, par exemple, induit en erreur les éléments du réseau pout faire passer le trafic via un nœud malveillant, qui peut couper ce trafic. Un autre exemple est l'attaque de type *man-in-the-middle*, qui perturbe la communication entre deux terminaux en insérant (à leur insu) un acteur intermédiaire, permettant de contourner de manière malveillante certains contrôles d'accès.

Les attaques réseau sont peu contraignantes à mettre en oeuvre pour les attaquants : ils peuvent donc facilement mettre en place des tentatives d'attaques, à distance et en masse, sur un grand nombre de périphériques.

Des protocoles réseau spécifiques doivent être utilisés pour se défendre contre ces attaques, tout en respectant à la fois les contraintes de (très) basse consommation énergétique pour les communications et les contraintes de ressources propres aux microcontrôleurs.

## 6.1.2b Attaques logicielles

Les attaques logicielles exploitent des vulnérabilités présentes dans les micrologiciels embarqués sur les objets connectés. Lors d'une telle attaque, du code malveillant est injecté dans le logiciel de l'objet et peut avoir les conséquences suivantes :

* l'objet divulgue des informations confidentielles, ou
* l'attaquant prend (secrètement) le contrôle de l'objet.

Par exemple, un logiciel hostile de type *malware*, installé furtivement sur l'appareil, peut lancer des opérations malveillantes. Un *buffer overflow* exploite une vulnérabilité logicielle (une absence de vérification de certaines zones mémoire utilisées) pour accéder à des zones mémoire qui ne devraient pas être accessibles.

En général, les attaques logicielles sont plus compliquées à mettre en place que les attaques réseau, mais peuvent avoir un impact plus important. Par exemple, un *botnet* peut exploiter ce type d'attaque pour compromettre tout un ensemble d'objets et en prenant le contrôle.

Des mécanismes complémentaires doivent être utilisés pour se défendre ce type d'attaques. Cependant ces mécanismes doivent respecter des contraintes strictes en ressources propres aux microcontrôleurs.
Ces mécanismes peuvent par exemple viser à protéger les accès en lecture ou en écriture de certaines zones mémoire, ou encore l'exécution d'une partie du code. Ils sont soit directement implémentés dans le logiciel, soit fournis par des fonctionnalités matérielles spécifiques (par exemple, un environnement de type « trusted execution » tel que [ARM TrustZone](https://www.arm.com/why-arm/technologies/trustzone-for-cortex-m)).

Avant de déployer du logiciel sur des objets connectés, il également possible d'utiliser des méthodes de vérification formelle sur le code, pour garantir l'absence de certaines vulnérabilités. Une fois le logiciel déployé, si d'autres vulnérabilités sont détectées (ce qui se produit souvent au cours de la vie d'un objet connecté), des mises à jour sécurisées déployées par le réseau peuvent corriger le logiciel au fur et à mesure. Néanmoins, les mises à jour logicielles elles-mêmes peuvent devenir un vecteur d'attaque, si une mise à jour approuvée par une entité légitime est détournée à des fins frauduleuses.


## 6.1.2c Attaques matérielles

Les attaques matérielles exploitent des vulnérabilités utilisant une interaction physique avec les objets.

Par exemple, une analyse détaillée de la consommation d'énergie pendant les opérations cryptographiques peut indirectement révéler des informations confidentielles (par exemple: tout ou partie d'une clef cryptographique privée). Ce type d'attaque est une attaque dite *side-channel*.

Un autre exemple est l'utilisation malveillante d'un champ magnétique puissant qui, dirigé vers l'appareil peut le forcer à ignorer certaines instructions de contrôle critiques. Ce type d'attaque est une attaque dite *fault injection*.

Les attaques matérielles sont très complexes à mettre en place, puisqu'elles nécessitent à la fois un accès physique à l'appareil ciblé et, souvent, un outillage très pointu.

Pour atténuer les risques liés à ce type d'attaques, il peut être nécessaire soit de modifier le logiciel (par exemple, pour garantir l'exécution en temps constant de certaines primitives cryptographiques) soit de modifier le matériel (par exemple, renforcer l'électronique de l'objet).

## 6.1.2d Attaques cyberphysiques ?

En plus des attaques réseau, logicielles et matérielles, de nouveaux types d'attaques peuvent exploiter les caractéristiques cyberphysiques du système.

Un exemple concret d'une telle attaque est la *réaction en chaîne* que nous avons décrit plus haut dans cette section: le botnet de climatiseurs qui pourraient synchroniser leur demande maximale d'electricité pour provoquer la rupture du réseau électrique.

Citons aussi l'exploitation malveillante d'une *fonctionnalité étendue* de certains éléments du système: le système d'éclairage intelligent piraté que nous avons décrit plus tôt (instrumentalisé pour provoquer l'épilepsie) relève de cette catégorie.

Pour le moment, de telles attaques ne sont pas faciles à classer, et la recherche dans ce domaine n’en est qu’au stade préliminaire.
