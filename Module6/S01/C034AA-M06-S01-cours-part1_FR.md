#  6.1.1. IoT et sécurité

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.1.1a IoT : De nouveaux risques](#611a-iot--de-nouveaux-risques)
- [6.1.1b Sécurité informatique *vs* sécurité de l’IoT](#611b-sécurité-informatique-vs-sécurité-de-liot)
<!-- TOC END -->

L'IoT se déploie actuellement à grande échelle, s'appuyant à la fois sur la disponibilité de logiciels IoT embarqués (polyvalents, open source) et sur des normes ouvertes pour la communication des objets connectés.

Alors que l'IoT est déployé sur des milliards de machines, y compris sur des objets connectés très hétérogènes utilisant des microcontrôleurs, les rapports alarmistes s'accumulent, avertissant des menaces potentielles pour la sécurité, et listant les nombreuses cyberattaques ayant déjà eu lieu.

Le concept de cyberguerre n'est pas nouveau : bien avant l'avènement de l'IoT, Internet était déjà un champ de bataille. Cette cyberguerre est en partie menée par des gouvernements (aspects géopolitiques) et en partie motivée par le profit (par exemple: la cyberpiraterie). Parallèlement, la surveillance de masse impacte la vie privée des utilisateurs.

Dans ce contexte, malgré les nombreuses failles de sécurité et les atteintes récurrentes à la vie privée, Internet continue d'être utilisé massivement. Pourquoi?
La raison est que les gens considèrent qu'Internet offre, au total, un compromis  fonctionnalités/risques raisonnable.

Il est important de réaliser qu'en général, il est peu pratique (voire impossible) d'éliminer les risques entièrement. C'est pourquoi les mécanismes de sécurité visent plutôt à rendre les risques *négligeables*, compte tenu d'un ensemble d'hypothèses.

Qu'y a-t-il donc de nouveau avec l'IoT ?

## 6.1.1a IoT : De nouveaux risques

Les fonctionnalités apportées par l'IoT ont tendance à s'améliorer plus rapidement que la sécurité, qui devient ainsi le principal facteur limitant.
Comparés à la sécurisation du reste d'Internet,
les limitations intrinsèques des objets connectés contraints modifient considérablement les risques et les contraintes.

Les fonctionnalités de l'IoT ont tendance à s'améliorer plus rapidement que sa sécurité. Cette dernière devient de ce fait le principal goulet d'étranglement.
Par rapport à la sécurisation du reste de l'Internet,
les limitations en resources intrinsèques des petits objets connectés et leurs cas d'applications font que la sécurité de l'IoT doit considérer des risques et des contraintes radicalement différents.

Un objet connecté piraté peut causer des dommages physiques directs et avoir de graves conséquences sur la sécurité.
Par exemple, un objet piraté tel qu'un stimulateur cardiaque ou une pompe à insuline pourrait provoquer une mort rapide de l'utilisateur. Un système d'éclairage piraté pourrait être détourné pour provoquer une crise d’épilepsie.
Par conséquent, **le niveau de risque acceptable est modifié**, si on le compare aux risques associés au piratage d'un compte de messagerie électronique, par exemple.

Avec des objets connectés présents partout, tout le temps, des fuites d'informations beaucoup plus personnelles sont possibles, et ce potentiellement en temps réel. De ce fait, **la portée des atteintes à la vie privée est modifiée**.

En combinant de manière malveillante les fonctionnalités de l'IoT et l'interdépendance accrue entre réseaux, des réactions en chaîne catastrophiques pourraient être déclenchées. Ainsi, **de nouveaux types d'attaques peuvent être envisagés**.
Par exemple, un botnet de climatiseurs intelligents pourrait perturber un réseau électrique à l'échelle nationale, en synchronisant de manière malveillante leur demande maximale d'électricité et causer une panne du réseau électrique à grande échelle.

## 6.1.1b Sécurité informatique *vs* sécurité de l’IoT

En général, comparées à la sécurité informatique traditionnelle, les solutions de sécurité pour l'IoT doivent être **beaucoup moins gourmandes en ressources** pour s'adapter aux limites de mémoires et de CPU des microcontrôleurs, **mais sans perdre en vitesse d'exécution, et sans compromis sur la sécurité**, ce qui constitue un vrai défi.

L'exemple typique de ce défi est la cryptographie. Selon l'algorithme et sa mise en œuvre, les microcontrôleurs souffrent d’un temps de calcul excessivement long, ou d’une empreinte mémoire beaucoup trop importante.
