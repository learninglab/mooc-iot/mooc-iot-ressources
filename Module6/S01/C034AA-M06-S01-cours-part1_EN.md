#  6.1.1. IoT and security

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.1.1a IoT: New risks](#611a-iot-new-risks)
- [6.1.1b IT security *vs* IoT security](#611b-it-security-vs-iot-security)
<!-- TOC END -->


As IoT is rolled out on billions of machines including heterogeneous microcontroller-based devices, reports pile up, warning about potential security threats - and cyberattacks which actually took place.

Cyberattacks are nothing new: even before the advent of IoT, the Internet had become a battlefield, in part driven by governments  (geopolitics) and in part driven by profit  (cyberpiracy). In parallel, the emergence of surveillance capitalism also threatens end-users' privacy.

In this context, despite recurrent security and privacy breaches, the Internet continues to be used - because people consider that it still offers a reasonable tradeoff regarding functionality *vs* risks.

It is important to realise that it is generally impractical (if not impossible) to eliminate risk entirely. What security mechanisms aim for instead, is making risks *negligible*, given a set of assumptions.

So now, what is new with IoT?

## 6.1.1a IoT: New risks

IoT functionalities tend to improve faster than security, which becomes the main bottleneck.
Compared to securing the rest of the Internet,
aspects of low-end IoT security drastically modify both risks and constraints.

A hacked IoT system can cause direct physical harm, and may have severe safety implications.
For instance, a hacked actuator such as a pacemaker or an insulin pump could kill, for example. A hacked smart lighting system could be weaponized to cause epilepsy.
Hence, **the level of acceptable risk is changed**, compared to risks with a compromised email account, for instance.

With connected sensors everywhere, all the time, potential leaks of much finer-grained information are possible, potentially in real-time. Thus, **the scope of privacy breaches is changed**.

By maliciously combining IoT functionalities and increased network interdependence, catastrophic chain reactions could be triggered, and thus **entirely new types of attacks may be possible**.
For instance, a relatively small botnet of smart heaters and coolers could disrupt a nation-wide powergrid, by maliciously synchronizing their maximum electricity demand.

## 6.1.1b IT security *vs* IoT security

Generally, compared to traditional IT security, solutions for the Internet of Things must be **significantly smaller** in order to fit microcontrollers’ memory and CPU limitations, **but without compromises on execution times, or on security**, which is a challenge.

A typical example of this challenge is cryptography. Depending on the algorithm and on its implementation, a common issue on microcontrollers is outrageously long computation time, or a memory footprint that is much too large.
