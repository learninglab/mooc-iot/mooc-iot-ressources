# 6.1.3. In a nutshell

In a nutshell, IoT security in practice must combine several defense mechanisms, working at different levels of the system, and protecting against different types of attacks.
Each of these defense mechanisms is necessary, but not sufficient to achieve security overall.

In this module we focus mostly on mechanisms applicable on low-end IoT devices to:

* defend against network attacks, and
* defend against some software attacks (based on remote malware installation).
