# 6.1.3. En résumé

En pratique, la sécurité de l'IoT doit combiner plusieurs mécanismes de défense, oeuvrant à différents niveaux du système, pour contrer différents types d'attaques.
Chacun de ces mécanismes de défense est nécessaire, mais pas suffisant pour assurer la sécurité globalement.

Dans ce module, nous nous concentrons sur les mécanismes applicables aux petits objets connectés de l'IoT, visant principalement à :

* contrer les attaques réseau, et
* contrer certaines attaques logicielles (basées sur l'installation à distance de programmes malveillants).
