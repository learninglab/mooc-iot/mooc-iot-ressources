# 6.1.0. A Short Introduction to IoT Security

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>At the end of this sequence, you will be able to identifiy the different types of attacks targeting IoT devices, and the challenges these attacks incur.
</p>
</div>

## Sources and references

- H. Tschofenig, E. Baccelli, “[Cyber-Physical Security for the Masses: Survey of the IP Protocol Suite for IoT Security](https://hal.inria.fr/hal-02351892/document),” IEEE Security & Privacy, 2019.

- S. Soltan et al. "[BlackIoT: IoT Botnet of high wattage devices can disrupt the power grid](https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-soltan.pdf)." USENIX Security 18, 2018.

- E. Ronen, A. Shamir, "[Extended functionality attacks on IoT devices: The case of smart lights](https://ieeexplore.ieee.org/abstract/document/7467343)." IEEE EuroS&P, 2016.


Video presenting IoT security challenges: compared to securing the Internet, what are the risks with IoT? What are the types of potential attacks? How to secure IoT devices? What are the constraints of IoT security?
