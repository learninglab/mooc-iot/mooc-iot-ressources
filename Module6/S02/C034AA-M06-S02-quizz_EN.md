# Question 5.2.1

Among the below cryptographic primitives, which ones can be used to authenticate data?

( ) SHA3-256
(x) edDSA with ed25519
(x) HMAC

[explanation]
SHA3-256 is a hashing function, it does not provide authentication guarantees. edDSA provides authentication with *digital signatures* generated and verified using asymmetric cryptography. HMAC provides authentication using *tags* generated and verified by combining symmetric cryptography and a hashing function.
[explanation]

# Question 5.2.2

Among the below cryptographic primitives, which one ensures data confidentiality?

(x) AES128
( ) SHA256
( ) ECDSA with P256

[explanation]
SHA256 is a hashing function, and ECDSA is a digital signature. Neither provide confidentiality. On the other hand, AES128 is an encryption scheme, which guarantees data confidentiality.
[explanation]

# Question 5.3.3

Among the below cryptographic primitives, which one does not rely on a pre-established secret known by the sender and the receiver?

( ) HMAC
( ) AES128
(x) Diffie-Hellman

[explanation]
HMAC and AES128 are both based on symmetric cryptography, which assumes a pre-shared secret. Diffie-Hellman, on the other hand, is a key establishment technique which uses asymmetric cryptography, which does not rely on a pre-shared secret.
[explanation]
