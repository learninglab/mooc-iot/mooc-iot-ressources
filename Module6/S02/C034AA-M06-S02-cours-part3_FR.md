# 6.2.3. Schémas de chiffrement

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.2.3a Qu'est-ce qu'un schéma de chiffrement ?](#623a-quest-ce-quun-schéma-de-chiffrement-)
- [6.2.3b Comment/où le chiffrement est-il utilisé ?](#623b-commentoù-le-chiffrement-est-il-utilisé-)
- [6.2.3c chiffrements symétriques](#623c-chiffrements-symétriques)
- [6.2.3d Chiffrement asymétrique et échange de clés Diffie-Hellman](#623d-chiffrement-asymétrique-et-échange-de-clés-diffie-hellman)
<!-- TOC END -->

## 6.2.3a Qu'est-ce qu'un schéma de chiffrement ?

Un schéma de chiffrement numérique est un ensemble d'algorithmes fournissant un *chiffrement*, c'est-à-dire une technique permettant à l'auteur des données numériques (message en clair) d'encoder ces données à l'aide d'une ou de plusieurs clés. Les données codées, que nous appelons _ciphertext_, ne révèlent rien sur le texte en clair (sauf sa longueur) à moins qu'il ne soit déchiffré avec la clé. Seules les parties autorisées (qui possèdent les clés requises) peuvent déchiffrer, c'est-à-dire récupérer le texte en clair à partir du texte chiffré.

Un bon schéma de chiffrement garantit qu'il est pratiquement impossible pour les parties non autorisées (qui n'ont pas les clés) de déchiffrer le texte chiffré.
Un schéma de chiffrement encore plus efficace fournit le même niveau d'assurance ou un niveau supérieur, tout en étant moins lourd en termes de mémoire et/ou en besoins de calcul.

## 6.2.3b Comment/où le chiffrement est-il utilisé ?

Par exemple, le chiffrement est utile pour protéger la confidentialité des communications sur le réseau.
Autre exemple, dans le contexte des mises à jour sécurisées du micrologiciel de l'IoT, le binaire de l'image du micrologiciel peut être récupéré par un attaquant à des fins d'analyse de vulnérabilité et d'ingénierie inverse. Pour se protéger contre de telles attaques, l'image du micrologiciel peut être chiffrée avant d'être transférée vers le périphérique de l'IoT ciblé par la mise à jour, qui peut déchiffrer et récupérer l'image binaire du micrologiciel. Dans ce cas, le périphérique de l'IoT doit être (d'une manière ou d'une autre) fourni avec les clés nécessaires pour déchiffrer.


## 6.2.3c chiffrements symétriques

Les chiffrements symétriques sont basés sur le principe que l'entité qui crypte un message et l'entité qui décrypte ce message utilisent la même clé.

La norme AES (Advanced Encryption Standard) est probablement le chiffrement symétrique le plus populaire à ce jour.
AES a été normalisé par NIST et définit une famille de chiffrements qui fonctionnent sur une taille fixe de blocs de 128 bits et une taille de clé de 128, 192 ou 256 bits.
Par exemple, AES-128 est la variante utilisant une taille de clé de 128 bits. Plus la clé est longue, plus les garanties de sécurité sont fortes.
AES utilise un algorithme de chiffrement basé sur une combinaison complexe de permutations et de substitutions qui est souvent intégrée (et optimisée) dans le matériel, même sur les microcontrôleurs bas de gamme. Les mises en oeuvre matérielles réduisent généralement les budgets de mémoire et d'énergie, par rapport aux mises en oeuvre logicielles de cette primitive cryptographique. Pour plus de détails, le lecteur peut se reporter à [FIPS 197](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf).

Sur les périphériques qui ne fournissent pas AES au niveau matériel, le chiffrement doit être effectué au niveau logiciel. Une alternative à AES qui est plus rapide au niveau logiciel est ChaCha20, un autre chiffrement symétrique conçu par D. J. Bernstein et standardisé par l'IETF.
Pour plus de détails, le lecteur peut se reporter à [RFC 8439](https://tools.ietf.org/html/rfc8439).

Un problème crucial avec les chiffrements symétriques est qu'ils supposent que le chiffreur et le déchiffreur partagent un secret (la clé secrète).
L'établissement d'un secret partagé (c'est-à-dire la distribution de clés sur un réseau non sécurisé) nécessite un mécanisme supplémentaire distinct : le chiffrement asymétrique.

## 6.2.3d Chiffrement asymétrique et échange de clés Diffie-Hellman

Le chiffrement asymétrique est basé sur le principe que chaque entité est associée à une paire de clés : une clé publique et une clé privée.
La clé publique est ouvertement distribuée (connaissance publique), alors que la clé privée doit être connue uniquement au propriétaire de cette paire de clés.
En supposant ce qui précède :

- n'importe qui peut chiffrer un message à l'aide de la clé publique d'une entité (appelons-le A), mais ce message ne peut être déchiffré que par A, à l'aide de sa clé privée.
- en utilisant sa clé privée, l'entité A peut produire une signature qui peut être authentifiée par n'importe qui (en utilisant la clé publique de A).

Par rapport aux chiffrements symétriques, les chiffrements asymétriques sont généralement plus gourmands en calculs, mais fournissent des propriétés d'authentification et permettent des mécanismes de création de clés qui ne sont pas possibles avec les chiffrements symétriques seuls.

L'échange de clés Diffie-Hellman (DH) est une méthode utilisant la cryptographie à clé publique pour établir un secret (une clé symétrique) partagé entre deux parties communiquant sur un canal non sécurisé.

Un problème crucial avec les chiffrements asymétriques est qu'il faut avoir la certitude que la clé publique du récepteur est effectivement légitime (c'est-à-dire qu'elle appartient à l'entité qui le prétend).
Pour remédier à ce problème, une infrastructure complémentaire complexe est utilisée en pratique, mettant en oeuvre une chaîne de confiance et de certificats basés sur des signatures numériques : l'infrastructure de clés publiques (ICP).
Cependant, dans l'IoT contraint, l'utilisation de l'ICP est jusqu'à présent évitée car les certificats représentent un coût supplémentaire trop important. Au lieu de cela, les clés publiques des points finaux sont préprogrammées dans les firmwares des objets connectés. Cela permet à ces objets de communiquer avec ces points finaux durant leur existence (ce qui constitue aussi une contrainte).
