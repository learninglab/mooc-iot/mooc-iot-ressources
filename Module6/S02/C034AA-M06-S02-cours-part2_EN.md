# 6.2.2. Hash functions

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.2.2a What is a hash function?](#622a-what-is-a-hash-function)
- [6.2.2b How/where is hashing used?](#622b-howwhere-is-hashing-used)
- [6.2.2c SHA-3](#622c-sha-3)
- [6.2.2d SHA-2](#622d-sha-2)
- [6.2.2e Other hashing primitives](#622e-other-hashing-primitives)
<!-- TOC END -->

## 6.2.2a What is a hash function?
A hash function is a function that maps data of arbitrary size to (smaller) fixed-size values, for example: 64 bytes. Such a value is called a *digest* or a *hash*.

A good cryptogaphic hash function should have a negligible risk of *collision*, i.e. that different input produce the same output.
A better hash function provides the same or smaller risk, and requires less memory and/or less computation.

In a security context, hash functions are often used to provide a *one-way function*: deterministic and easy to compute for any input, but hard (computationally infeasible) to invert given the digest of arbitrary input. An important characteristic of hashing is that, the slightest change in the message to-be-hashed (for instance modifying the value of a single bit) typically results in a massive change in the resulting digest.

## 6.2.2b How/where is hashing used?

Since digests are *small*, they are nice to handle in a context where memory is scarce. Since digests are also of *fixed size*, handling a digest is simpler than handling the actual data, which allows implementation optimizations (e.g. avoids the complexity of handling larger, variable size input).

For instance, hashes are used to detect when data changed. In the context of secure IoT firmware updates, a hash function is used to produce a digest of the (binary) firmware image, and is sent together with the image. The receiver recomputes the digest locally (on the received firmware binary) and checks it against the received digest. If they do not match, an issue (corruption or tampering) is detected.
Another example is how hashing data together with a shared secret key is used to create a message authenticator tag (see section [6.2.4. on MAC](https://fun-mooc.fr/courses/course-v1:inria+41020+session03/jump_to_id/952c21dad9aa4d618ac1c69a2b6a2a64)).
Hashing is also used to to derive cryptographic keys from entropy sources.

## 6.2.2c SHA-3

SHA-3 (Secure Hash Algorithm 3) is the latest family of hash functions standardized by NIST (USA's National Institute of Standards and Technology).
SHA-3 is based on a cryptographic primitive called Keccak, a so-called *sponge construction*.

Sponge functions compute hashes in two phases: (1) the absorption phase, and (2) the squeezing phase.

In the absorption phase, the data is split in blocks of fixed size _r_.
For instance, for SHA3-256 the blocks are of size _r_=1088 bits.
Bit scrambling is performed on the current block extended with internal state of size _c_. A larger value of _c_ increases security. For instance, for SHA3-256, _c_=512 bits.

Scrambling is performed on the _r_+_c_ bits based on a series of bit permutations noted _f_ composed only of simple operations (XOR, AND and NOT operations).
The result is then XORed with the next block, and bit scrambling is performed again on the output. And so on until all blocks have been absorbed in the state (see Figure 1).

At this stage, the squeezing phase starts. This phase resembles the absorption phase, except rounds of bit scrambling (the same permutations) are performed directly one after the other. Each round produces a fixed-sized bit output: the first _r_ bits are the result of this round of squeezing. The number of rounds can be chosen at will. Thus, Keccak is an extendable-output function (XOF) i.e. a cryptographic function producing arbitrarily long output (which can be used not only for hashing, but also for other purposes).

The hash is defined as the first output of the squeeze phase, truncated after the desired digest length. For instance, for SHA3-256, the output of the XOF is truncated after 256 bits.

<figure>
    <img src="Images/Sponge.png" alt="Sponge function used for SHA-3 hashing. (Source: G. Van Assche, RIOT Summit 2017)." width="800"><br />
    <figcaption>Fig. 1 : Sponge function used for SHA-3 hashing. (Source: G. Van Assche, RIOT Summit 2017).</figcaption>
</figure>

## 6.2.2d SHA-2

SHA-2 is another family of hash functions previously standardized by NIST.
SHA-2 is completely different from SHA-3 in its internal design: it is built using the Merkle--Damgard construction, and not a sponge function.  It is not an XOF: it provides fixed-sized digests.

The most popular SHA-2 algorithm is without a doubt SHA256, which processes data in 512-bit blocks and produces a message digest consisting of 256 bits.

## 6.2.2e Other hashing primitives

There are many other hashing primitives.
Some of them are deprecated in cryptographic contexts, because they have been shown to incur a non-negligible risk of collision.
This is for example the case of MD5 message digest algorithm, which is considered unsafe in a cryptographic context nowadays.
