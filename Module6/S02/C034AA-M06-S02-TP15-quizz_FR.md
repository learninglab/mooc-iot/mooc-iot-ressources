# Question 1

En utilisant la command shell `encrypt`, donner le resultat du chiffrement du
message "I am encrypted" ?

= DC0F10B85B44CB8F7DAEF6E55571

# Question 2

En utilisant la command shell `decrypt`, donner le message chiffré dans la
chaine de caractère C14718A65B48D6CC5D9EC9C51139F74745755AFA3F6EE68A ?

= This is RIOT!, encrypted
