# Question 1

Donner le résultat du hash de "This is RIOT!" en utilisant l'algorithme SHA256.

= 7808CE5C27FBA9BDDCD5CB076B423E22961D6BCC5FF2CA62722C0D6DFD521926

# Question 2

Donner le résultat du hash de "This is RIOT!" en utilisant l'algorithme SHA3-256.

= B4A39232E5A1DAE41917C429EF1CAD90DC08714BFAC12C4EB5D3583262031B49
