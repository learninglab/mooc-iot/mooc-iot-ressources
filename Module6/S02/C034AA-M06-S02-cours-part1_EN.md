
# 6.2.1. Crypto primitives for IoT security

Cybersecurity currently relies on various combinations of mechanisms at work in hardware, and at different levels of the software and network stack.

Though diverse, all these mechanisms build upon on a set of common crypto *primitives*, i.e. specialized low-level operations on digital data. These primitives are used to provide basic guarantees on digital data, including:

- *Authenticity*: guarantee on the origin of the data;
- *Integrity*: guarantee that original data has not been tampered with;
- *Confidentiality*: guarantee that data is intelligible only for intended recipients.

Based on these, more elaborate guarantees can then be provided at higher level by the system, such as authorization, privacy...

IoT network and system security currently relies on various combinations of the following crypto primitives:

- Hash functions;
- Digital signature schemes;
- [Message authentication codes](https://en.wikipedia.org/wiki/Message_authentication_code) (MACs)
- Authenticated encryption;
- Key exchange schemes.

In the following part of these sequence, we will briefly look into each of the above categories, and
give an overview of the typical solutions used in IoT on microcontrollers.

## What is special about IoT?

Typically, cryptographic operations require rather intensive computation. On microcontrollers, computing power is (very) limited compared to microprocessors available on other types of machines (e.g. desktop, laptop, smartphone, tablet or even single-board computers such as a RaspberryPi).

In general, the challenge on low-end IoT is thus to achieve fast enough execution time with much smaller memory requirements, and without compromising on the primitive's security strength.
In particular, for the most computation-intensive primitives (e.g. public key cryptography) this challenge is exacerbated.

One approach to overcome this challenge is to rely on very efficient hardware implementation of some primitives (e.g. a specialized crypto co-processor). However, if additional hardware modules are needed, the cost of devices increases. Furthermore, crypto valid today may not be valid tomorrow (because flaws are discovered, or because average attackers' computation power increases). Therefore, crypto often relies on logic implemented in software, and not only on hardware.
