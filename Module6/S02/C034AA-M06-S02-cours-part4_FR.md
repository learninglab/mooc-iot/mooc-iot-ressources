# 6.2.4. Codes d'authentification des messages (MAC)

Un code d'authentification de message (Message Authentication Code ou MAC en anglais, parfois aussi appelé *balise*)
est une chaîne de bits courte et de taille fixe, générée à l'aide d'une clé secrète (symétrique), qui peut être utilisée pour authentifier un message de longueur arbitraire.
Les vérificateurs (qui doivent posséder la clé secrète) peuvent vérifier la balise pour valider l'origine et l'intégrité du message.

Par exemple, HMAC (Hash-based Message Authentication Code) est une catégorie de MAC qui réalise deux cycles de hachage sur la clé secrète (symétrique) et traite le message morceau par morceau pour produire une balise. Par exemple, HMAC-SHA256 produit des balises utilisant SHA256 pour le hachage. Il est possible de baser HMAC sur d'autres fonctions de hachage. La taille de sortie (et la puissance cryptographique) est la même que celle de la fonction de hachage sous-jacente.
