# 6.2.1. Primitives cryptographiques pour la sécurité de l'IoT

La cybersécurité repose actuellement sur diverses combinaisons de mécanismes présents dans le matériel et à différents niveaux dans les logiciels et les piles réseau.

Malgré leur diversité, tous ces mécanismes s'appuient sur un ensemble de primitives cryptographiques communes, qui sont des opérations mathématiques de bas niveau appliquées sur les données numériques. Ces primitives permettent de fournir des garanties de base sur les données numériques, notamment :

- *l'authenticité* : garantie sur l'origine des données ;
- *l'intégrité* : garantie que les données originales n'ont pas été falsifiées ;
- *la confidentialité* : garantie que les données sont intelligibles uniquement par les destinataires autorisés.

Sur cette base, des garanties plus élaborées peuvent alors être fournies à un niveau supérieur par le système, comme l'autorisation, la confidentialité...

La sécurité du réseau et d'un système IoT repose actuellement sur diverses combinaisons des primitives cryptographiques suivantes :

- fonctions de hachage et [codes d'authentification de messages](https://fr.wikipedia.org/wiki/Code_d%27authentification_de_message) (Message Authentication Code ou MAC en anglais) ;
- systèmes de signature numérique ;
- chiffrement authentifié ;
- systèmes d'échange de clés.

Dans cette séquence, nous examinerons brièvement chacunes de ces primitives cryptographiques, et nous
donnerons un aperçu des solutions classiques utilisées sur les microcontrôleurs.

## Qu'y-a-t'il de spécial avec l'IoT ?

En général, les opérations cryptographiques nécessitent un calcul assez intensif. Sur les microcontrôleurs, la puissance de calcul est (très) limitée par rapport aux microprocesseurs disponibles sur d'autres types de machines (par exemple, ordinateur de bureau, ordinateur portable, smartphone, tablette ou même ordinateurs monocarte tels qu'un RaspberryPi).

En général, le défi de l'IoT sur microcontrôleur consiste à atteindre un temps d'exécution suffisament faible avec des exigences de mémoire beaucoup plus petites, sans pour autant compromettre l'efficacité sécuritaire de la primitive.
C'est d'autant plus vrai pour les primitives les plus gourmandes en calculs (par exemple, chiffrement à clé publique), pour lesquelles la difficulté est amplifiée.

Une approche permettant de surmonter ce défi consiste à utiliser du matériel implémentant directement certaines primitives (par exemple, un coprocesseur cryptographique spécialisé). Cependant, si des modules matériels supplémentaires sont nécessaires, le coût du appareil va augmenter. En outre, une cryptographie considérée comme valide aujourd'hui peut ne plus l'être demain (parce que des défauts sont découverts, ou parce que la puissance de calcul moyenne des attaquants augmente). Par conséquent, la cryptographie repose souvent sur la logique mise en oeuvre dans les logiciels, et pas seulement sur le matériel.
