
# 6.2.6. Other Cryptographic Primitives

Quantum computing is expected to emerge in the (near?) future.
The advent of such computing would provide potential attackers with novel and significantly powerful means -- powerful enough to break many cryptographic solutions considered safe nowadays.
A challenge which looms next for cryptography, is thus efficient quantum-resistant cryptography.
By that we mean: cryptographic primitives using traditional (non-quantum) computing, which can resist attackers which have access to quantum computing power.

A particularly tough challenge in this domain is designing quantum-resistant public key cryptography for IoT.
A [competition launched by NIST](https://csrc.nist.gov/Projects/post-quantum-cryptography/Post-Quantum-Cryptography-Standardization) is ongoing, aiming to identify candidate cryptographic primitives which can address this challenge.
This competition is on-going (it's a long process, lasting years).
Selected primitives having proven their worth would subsequently be standardized.
The implementation of a number of candidates primitives able to run on microcontrollers are being gathered by the [PQM4](https://github.com/mupq/pqm4) project (work in progress).
