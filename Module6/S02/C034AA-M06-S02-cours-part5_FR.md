# 6.2.5. Schémas de signature électronique

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.2.5a Qu'est-ce qu'un système de signature électronique ?](#625a-quest-ce-quun-système-de-signature-électronique)
- [6.2.5b Comment/où les signatures électroniques sont-elles utilisées ?](#625b-commentoù-les-signatures-électroniques-sont-elles-utilisées)
- [6.2.5c chiffrement par courbes elliptiques (ECC)](#625c-chiffrement-par-courbes-elliptiques-ecc)
- [6.2.5d ECDSA couplé à P-256](#625d-ecdsa-couplé-à-p-256)
- [6.2.5e EdDSA couplé à ed25519](#625e-eddsa-couplé-à-ed25519)
<!-- TOC END -->

## 6.2.5a Qu'est-ce qu'un système de signature électronique ?
Un schéma de signature électronique est en général un ensemble d'algorithmes qui permet :

 1. La *génération de clé* : l'algorithme produit une clé privée (aléatoire) et sa clé publique correspondante.
 2. La *génération de signatures* : selon un message électronique et une clé privée, l'algorithme produit une signature (électronique).
 3. La *vérification de la signature* : en fonction du message, de la clé publique et de la signature, l'algorithme confirme ou invalide l'authenticité du message.

Un bon schéma de signature offre au destinataire des données numériques une assurance raisonnablement élevée que, si la signature est vérifiée, ces données ont effectivement été émises par l'entité à laquelle la clé publique est associée.
Un très bon schéma de signature offre le même niveau d'assurance ou un niveau supérieur, en utilisant des clés plus petites et moins de calcul pour générer/vérifier des signatures.

## 6.2.5b Comment/où les signatures électroniques sont-elles utilisées ?

Par exemple, dans le contexte des mises à jour sécurisées du micrologiciel de l’IoT, une signature électronique est utilisée pour authentifier l'origine de la mise à jour de l'image du micrologiciel (validation de la légitimité) et pour assurer son intégrité (s'assurer qu'elle n'a pas été falsifiée).

Toutefois, les données qui doivent être signées dépassent fréquemment la taille maximale des schémas de signature. Dans de tels cas, les données sont d'abord hachées, ce qui produit un bref résumé sur lequel la génération/vérification de signatures est effectuée (au lieu des données elles-mêmes).

La génération et la vérification de signatures sont généralement des tâches nécessitant un calcul intensif qui prennent beaucoup de temps sur les microcontrôleurs - beaucoup plus de temps que le hachage par exemple.

## 6.2.5c chiffrement par courbes elliptiques (ECC)

Le chiffrement par courbes elliptiques (ECC), est une technique exploitant les propriétés des équations définissant les courbes elliptiques et la théorie des groupes.
Le principe premier est de fournir une *fonction unidirectionnelle* en multipliant un point sur la courbe elliptique par un nombre, ce qui produira un autre point sur la courbe. Cependant, il est difficile de calculer le nombre utilisé, même si l’on connaît le point d'origine et le résultat.
Par exemple, ECDH (Elliptic Curve Diffie-Hellman) est une variante de DH qui utilise la théorie des courbes elliptiques pour atteindre cet objectif.

Par rapport à d'autres techniques de chiffrement asymétrique basées sur d’autres principes (tels que RSA, Rivest–Shamir–Adleman, répondant à la difficulté de factoriser le produit de grands nombres premiers), ECC peut fournir une sécurité équivalente avec des clés beaucoup plus petites, diminuant les ressources informatiques et énergétiques requises.

Pour ces raisons, ECC est largement utilisé dans l'IoT sur microcontrôleurs.
Différentes courbes elliptiques peuvent être utilisées, qui définissent différentes normes ECC.

## 6.2.5d ECDSA couplé à P-256

L’algorithme de signature électronique par courbe elliptique (ECDSA) est un algorithme de signature électronique qui utilise le chiffrement par courbe elliptique.

l’ECDSA couplé à P-256 génère et vérifie les signatures électroniques à l'aide de la courbe elliptique nommée courbe *P-256*, normalisée par NIST.
Pour plus de détails, le lecteur peut se reporter à [FIPS 186-3](https://csrc.nist.gov/csrc/media/publications/fips/186/3/archive/2009-06-25/documents/fips_186-3.pdf).

## 6.2.5e EdDSA couplé à ed25519

L'algorithme de signature électronique de la courbe Edwards (EdDSA) est une variante du schéma de signature de Schnorr. Entre autres différences, il utilise une courbe elliptique différente : la courbe appelée ed25519.

Les spécifications de EdDSA utilisées avec ed25519 ont été normalisées par l'IETF.
Pour plus de détails, le lecteur peut se reporter à [RFC 8032](https://tools.ietf.org/html/rfc8032).
