# 6.2.2. Fonctions de hachage

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [6.2.2a Qu'est-ce qu'une fonction de hachage ?](#622a-quest-ce-quune-fonction-de-hachage-)
- [6.2.2b Comment/où le hachage est-il utilisé ?](#622b-commentoù-le-hachage-est-il-utilisé-)
- [6.2.2c SHA-3](#622c-sha-3)
- [6.2.2d SHA-2](#622d-sha-2)
- [6.2.2e Autres primitives de hachage](#622e-autres-primitives-de-hachage)
<!-- TOC END -->


## 6.2.2a Qu'est-ce qu'une fonction de hachage ?
Une fonction de hachage est une fonction qui mappe les données de taille arbitraire à des valeurs de taille fixe (plus petites), par exemple : 64 octets. Une telle valeur est appelée *condensé* ou *hachis*.

Une bonne fonction de hachage cryptographique doit comporter un risque négligeable de *collision*, correspondant au cas où différentes entrées produisent la même empreinte.
Une meilleure fonction de hachage offre le même risque ou un risque moindre et nécessite moins de mémoire et/ou moins de calcul.

Dans un contexte de sécurité, les fonctions de hachage sont souvent utilisées pour fournir une fonction *unidirectionnelle* : déterministe et facile à calculer pour n'importe quelle entrée, mais difficile (informatiquement infaisable) à inverser compte tenu du condensé de l'entrée arbitraire. Une caractéristique importante des fonctions de hachage est que le plus petit changement opéré sur le message à hacher (par exemple, changer la valeur d'un seul bit) résulte dans un condensé complètement différent.

## 6.2.2b Comment/où le hachage est-il utilisé ?

Étant donné que les condensés sont *petits*, ils sont simples à manipuler dans un contexte où la mémoire est restreinte. Comme ils sont aussi de *taille fixe*, la gestion d'un condensé est plus aisée que la manipulation des données réelles, ce qui permet des optimisations de mise en oeuvre (par exemple, évite la complexité de la gestion des entrées de taille variable ou plus grandes).

Par exemple, le hachage est utilisé pour détecter toute modification sur les données d'origine. Dans le contexte des mises à jour sécurisées d'un microprogramme, une fonction de hachage est utilisée pour produire un condensé de l'image (binaire) du microprogramme, et est envoyée avec l'image. Le destinataire recalcule le condensé localement (sur le binaire du microprogramme reçu) et le compare au condensé reçu. S'ils ne correspondent pas, un problème (corruption ou altération) est détecté.
Autre exemple : le hachage de données associées à une clé secrète partagée est utilisé pour créer une balise d'authentification de message (voir section [6.2.4. sur MAC](https://fun-mooc.fr/courses/course-v1:inria+41020+session03/jump_to_id/952c21dad9aa4d618ac1c69a2b6a2a64)).
Le hachage est également utilisé pour dériver des clés cryptographiques à partir de sources d'entropie.

## 6.2.2c SHA-3

SHA-3 (Secure Hash Algorithm 3) est la dernière famille de fonctions de hachage normalisées par le NIST (National Institute of Standards and Technology des États-Unis).
SHA-3 est basé sur une primitive cryptographique appelée Keccak, une construction dite *éponge*.

Les fonctions éponge calculent les hachages en deux phases : (1) la phase d'absorption et (2) la phase d'essorage.

Dans la phase d'absorption, les données sont divisées en blocs de taille fixe.
Par exemple, pour SHA3-256, les blocs sont de taille  _r_=1088 bits.
Le brouillage de bits est effectué sur le bloc courant étendu avec un état interne de taille _c_. Une plus grande valeur pour _c_ augmente la sécurité. Par exemple, pour SHA3-256, on a _c_=512 bits.

Le brouillage de bits est effectué sur les _r_+_c_ bits en utilisant une serie de permutations des bits notée _f_ composée d'opération simples (XOR, AND et NOT).
Le résultat est alors XORé avec le bloc suivant, et un brouillage de bits est de nouveau exécuté à la sortie. Et ainsi de suite jusqu'à ce que tous les blocs aient été absorbés (voir Figure 1).

A ce stade, la phase d'essorage commence. Cette phase ressemble à la phase d'absorption, sauf que les cycles de brouillage de bits (utilisant les mêmes permutations) s'enchainent directement. Chaque cycle produit une sortie de taille fixe: les _r_ premiers bits sont le résultat de ce cycle d'essorage. Le nombre de cycles peut être choisi à volonté. Ainsi, Keccak est une fonction de sortie extensible (XOF), c'est-à-dire une fonction cryptographique générant une production arbitrairement longue (qui peut être utilisée non seulement pour le hachage, mais aussi à d'autres fins).

Le hachage est défini comme le résultat de la première phase d'essorage, tronqué selon la longueur souhaitée du condensé. Par exemple, pour SHA3-256, la sortie du XOF est tronquée à 256 bits.

<figure>
    <img src="Images/Sponge.png" alt="Fonction éponge utilisée pour le hachage avec SHA-3. (Source: G. Van Assche, RIOT Summit 2017)" width="800"><br />
    <figcaption>.Fig. 1 : Fonction éponge utilisée pour le hachage avec SHA-3. (Source: G. Van Assche, RIOT Summit 2017).</figcaption>
</figure>

## 6.2.2d SHA-2

SHA-2 est une autre famille de fonctions de hachage précédemment normalisées par NIST.
SHA-2 est complètement différent de SHA-3 dans sa conception interne : il est construit selon le protocole Merkle-Damgard, et n'est pas une fonction éponge. Ce n'est pas un XOF : il fournit des condensés de taille fixe.
L'algorithme SHA-2 le plus populaire est sans aucun doute SHA256, qui traite les données par blocs de 512 bits et produit un condensé de messages de 256 bits.

## 6.2.2e Autres primitives de hachage

Il existe beaucoup d'autres primitives de hachage.
Certaines ne sont pas (ou ne sont plus) utilisées dans des contextes cryptographiques, car il est démontré qu'elles présentent un risque non négligeable de collision.
C'est par exemple le cas de MD5, une primitive de hachage utilisée autrefois dans des contextes cryptographiques, mais qui n'est plus considérée comme sûre de nos jours, et n'est donc plus utilisée dans ce type de contexte.
