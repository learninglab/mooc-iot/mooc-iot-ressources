
# 6.2.4. Message Authentication Codes (MAC)

A Message Authentication Code (MAC, sometime named *tag*)
is a short, fixed-sized bit string, generated using a secret (symmetric) key, which can be used to authenticate a message of arbitrary length.
Verifiers (who must possess the secret key) can check the tag to validate the origin and the integrity of the message.

For instance, Hash-based Message Authentication Code (HMAC) is a category of MAC which uses two rounds of hashing on the secret (symmetric) key and the message piece by piece to produce a tag. For instance, HMAC-SHA256 produces tags using SHA256 for hashing. Basing HMAC on other hash functions is possible. The size of the output  (and the cryptographic strength) is the same as that of the underlying hash function.
