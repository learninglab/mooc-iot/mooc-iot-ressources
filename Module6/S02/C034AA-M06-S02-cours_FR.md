# 6.2.0. Introduction

Nous allons maintenant examiner brièvement plusieurs primitives crypto et vous donner un aperçu des solutions typiques appliquées à l'IoT sur les microcontrôleurs.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>À la fin de cette séquence, vous serez alors en mesure de combiner des mécanismes de cryptographie applicables sur les microcontrôleurs, fournissant des garanties de base sur la sécurité des données IoT.
</p>
</div>

## Sources et références

- [Spécifications SHA-2](https://csrc.nist.gov/csrc/media/publications/fips/180/2/archive/2002-08-01/documents/fips180-2.pdf)
- [Spécifications SHA-3](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
- [ECDSA avec spécification P-256](https://csrc.nist.gov/csrc/media/publications/fips/186/3/archive/2009-06-25/documents/fips_186-3.pdf)
- [RFC 8032](https://tools.ietf.org/html/rfc8032) Algorithme de signature numérique Courbe d'Edwards (EdDSA)
- [RFC 8439](https://tools.ietf.org/html/rfc8439) ChaCha 20 et Poly1305 pour les protocoles IETF
- [FIPS 197](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf) La norme de chiffrement avancé
