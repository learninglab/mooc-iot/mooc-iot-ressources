# 6.2.5. Digital Signature Schemes

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.2.5a What is a digital signature scheme?](#625a-what-is-a-digital-signature-scheme)
- [6.2.5b How/where are digital signatures used?](#625b-howwhere-are-digital-signatures-used)
- [6.2.5c Elliptic Curve Cryptography (ECC)](#625c-elliptic-curve-cryptography-ecc)
- [6.2.5d ECDSA with P-256](#625d-ecdsa-with-p-256)
- [6.2.5e EdDSA with ed25519](#625e-eddsa-with-ed25519)
<!-- TOC END -->

## 6.2.5a What is a digital signature scheme?
A digital signature scheme is in general a set of algorithms which enables:

 1. *Key generation*: the algorithm outputs a (random) private key and its corresponding public key.
 2. *Signature generation*: given a digital message and a private key, the algorithm produces a (digital) signature.
 3. *Signature verification*: given the message, the public key and the signature, the algorithm either confirms or invalidates the authenticity of the message.

A good signature scheme offers the recipient of digital data a reasonably high assurance that, if the signature verifies, this data was indeed originated by the entity to which the public key is associated.
A better signature scheme provides the same or higher level of assurance, using smaller keys, and less computation to generate/verify signatures.

## 6.2.5b How/where are digital signatures used?

For example, in the context of secure IoT firmware updates, a digital signature is used to authenticate the origin of the firmware image update (validating legitimacy), and to ensure its integrity (ensuring it has not been tampered with).

Frequently, however, data which must be signed exceeds the maximum input size of signature schemes. In such cases the data is first hashed, which produces a short digest over which signature generation/verification is performed (instead of over the data itself).

Signature generation and verification are typically computation-intensive tasks which take significant time on microcontrollers -- typically much longer than hashing for instance.

## 6.2.5c Elliptic Curve Cryptography (ECC)

Elliptic Curve Cryptography (ECC), is a technique exploiting the properties of equations defining elliptic curves and group theory.
The main principle is to provide a *one-way function* by multiplying a point on the elliptic curve by a number, which will produce another point on the curve, but it is computationally difficult to find what number was used, even if you know the original point and the result.
For instance, ECDH (Elliptic curve Diffie-Hellman) is a variant of DH which uses elliptic curve theory to achieve this goal.

Compared to alternative asymmetric cryptography techniques based on different principles (such as RSA, Rivest–Shamir–Adleman, based on the difficulty of factoring the produce of large primes), ECC can provide equivalent security with much smaller keys, diminishing required computing and energy resources.

For these reasons, ECC is widely used in microcontroller-based IoT.
Different elliptic curves may be used, which define different ECC standards.

## 6.2.5d ECDSA with P-256

Elliptic curve Digital Signature Algorithm (ECDSA) is a digital signature algorithm using elliptic curve cryptography.

ECDSA with P-256 generates and verifies digital signatures using the elliptic curve named the *P-256 curve*, standardized by NIST.
For more details, the reader can refer to [FIPS 186-3](https://csrc.nist.gov/csrc/media/publications/fips/186/3/archive/2009-06-25/documents/fips_186-3.pdf).

## 6.2.5e EdDSA with ed25519

The Edwards curve Digital Signature Algorithm (EdDSA) is a variant of the Schnorr signature scheme. Among other differences, it uses a different elliptic curve: the so-called ed25519 curve.

The specifications of EdDSA used with ed25519 have been standardized by the IETF.
For more details, the reader can refer to [RFC 8032](https://tools.ietf.org/html/rfc8032).
