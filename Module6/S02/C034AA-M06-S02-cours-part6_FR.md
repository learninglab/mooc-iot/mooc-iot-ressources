# 6.2.6. Autres primitives cryptographiques

Dans un avenir de moins en moins lointain, l'ordinateur quantique sera une réalité.
L'informatique quantique mettrait à disposition des attaquants potentiels une puissance de calcul radicalement nouvelle et puissante -- assez puissante, en particulier, pour rendre vulnérables de nombreuses primitives cryptographiques considérées comme sûres de nos jours.

Le prochain défi qui se profile donc pour la cryptographie est d'aboutir à un chiffrement efficient sur le plan quantique.
Nous entendons par là des primitives cryptographiques utilisant le calcul traditionnel (non quantique), capables de résister aux attaquants qui peuvent avoir accès à une puissance de calcul quantique.

Un défi particulièrement difficile dans ce domaine consiste à concevoir une cryptographie à clé publique résistante aux attaques quantiques pour l'IoT.
Un [concours lancé par NIST](https://csrc.nist.gov/Projects/post-quantum-cryptography/Post-Quantum-Cryptography-Standardization) est en cours, visant à identifier les primitives cryptographiques candidates qui peuvent relever ce défi.
Ce concours n'est pas terminé (c'est un long processus, étalé sur des années).
Les primitives sélectionnées ayant prouvé leur valeur seraient ensuite normalisées.
La mise en oeuvre d'un certain nombre de primitives candidates capables de s'exécuter sur les microcontrôleurs est en cours de recensement par le projet [PQM4](https://github.com/mupq/pqm4) (travaux en cours).
