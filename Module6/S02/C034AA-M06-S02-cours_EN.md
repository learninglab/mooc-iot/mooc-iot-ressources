# 6.2.0. Introduction

We are now going to briefly look into several crypto primitives and give you an overview of the typical solutions used in IoT on microcontrollers.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>At the end of this sequence, you will be able to combine a variety of cryptographic mechanisms, applicable on-board microcontroller-based IoT devices.
</p>
</div>

## Sources and References

- [SHA-2 Specifications](https://csrc.nist.gov/csrc/media/publications/fips/180/2/archive/2002-08-01/documents/fips180-2.pdf)
- [SHA-3 Specifications](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
- [ECDSA with P-256 Specification](https://csrc.nist.gov/csrc/media/publications/fips/186/3/archive/2009-06-25/documents/fips_186-3.pdf)
- [RFC 8032](https://tools.ietf.org/html/rfc8032) Edwards-Curve Digital Signature Algorithm (EdDSA)
- [RFC 8439](https://tools.ietf.org/html/rfc8439) ChaCha20 and Poly1305 for IETF Protocols
- [FIPS 197](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf) The Advanced Encryption Standard
