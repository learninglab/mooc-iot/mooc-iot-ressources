
# 6.2.3. Encryption schemes

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [6.2.3a What is an encryption scheme?](#623a-what-is-an-encryption-scheme)
- [6.2.3b How/where is encryption used?](#623b-howwhere-is-encryption-used)
- [6.2.3c Symmetric ciphers](#623c-symmetric-ciphers)
- [6.2.3d Asymmetric Cryptography & Diffie-Hellman Key Exchange](#623d-asymmetric-cryptography--diffie-hellman-key-exchange)
<!-- TOC END -->

## 6.2.3a What is an encryption scheme?

A digital encryption scheme is a set of algorithms providing *a cipher*, i.e. a technique enabling the originator of digital data (referred to as plaintext) to encode this data using one or more keys. The encoded data, which we call _ciphertext_, reveals nothing about the plaintext (except for its length) unless it is decrypted with the key. Only authorized parties (who possess the required keys) can decrypt, i.e. recover the plaintext from the ciphertext.

A good encryption scheme ensures that it is practically impossible for unauthorized parties (who do not have the keys) to decrypt the ciphertext.
A better encryption scheme provides the same or higher level of assurance, with less overhead in terms of memory and/or computation requirements.

## 6.2.3b How/where is encryption used?

For example, encryption is useful to protect the confidentiality of communciations over the network.
As another example, in the context of secure IoT firmware updates, the firmware image binary could be captured by an attacker for vulnerability analysis and adversarial reverse engineering. To protect against such attacks, the firmware image can be encrypted before it is transferred to the IoT device targeted by the update, which can decrypt and recover the firmware image binary. In this case, the IoT device should be (somehow) provided with the necessary keys to decrypt.


## 6.2.3c Symmetric ciphers

Symmetric ciphers are based on the principle that the entity which encrypts a message and the entity that decrypts this message use the same key.

The Advanced Encryption Standard (AES) is probably the most popular symmetric cipher, to date.
AES was standardised by NIST, and defines a family of ciphers which work on a fixed block size of 128 bits, and a key size of 128, 192, or 256 bits.
For instance, AES-128 is the variant using a key size of 128 bits. The longer the key, the stronger the security guarantees.
AES uses an encryption algorithm based on a complex combination of permutations and substitutions which is often implemented (and optimized) in hardware, even on low-end microcontrollers. Hardware implementations typically reduce both memory and energy budgets, compared to software-only implementations of this crypto primitive. For more details, the reader can refer to [FIPS 197](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf).

On devices that do not provide AES in hardware, encryption must be done in software. An alternative to AES which is faster in software is ChaCha20, another symmetric cipher designed by D. J. Bernstein and standardized by the IETF.
For more details, the reader can refer to [RFC 8439](https://tools.ietf.org/html/rfc8439).

One crucial issue with symmetric ciphers however, is that it assumes the encrypter and decrypter share a secret (the secret key).
The establishment of a shared secret (i.e. key distribution over an insecure network) requires a separate, additional mechanism: asymmetric cryptography.

## 6.2.3d Asymmetric Cryptography & Diffie-Hellman Key Exchange

Asymmetric cryptography is based on the principle that each entity is associated with a pair of keys: a public key and a private key.
The public key is openly distributed (it is public knowledge), while the private key must be known only to the owner of this key pair.
Assuming the above:

- anyone can encrypt a message using an entity's (let's call it A) public key, but this encrypted message can only be decrypted by A, using its private key.
- using its private key, entity A can produce a signature which can be authentified by anyone (using A's public key).

Compared to symmetric ciphers, asymmetric ciphers typically are more computation-intensive, but provide authentication properties and enable key establishment mechanisms that are not possible with symmetric ciphers alone.

In particular, Diffie-Hellman (DH) key exchange is a method using public key cryptography to establish a secret (a symmetric key) shared between two parties communicating over a (still) insecure channel.

One crucial problem with asymmetric ciphers, is being reasonably certain that the public key of the receiver is indeed legitimate (i.e. is owned by the entity which claims so).
To alleviate this issue, a complex complementary infrastructure is used in practice, implementing a chain of trust and certificates based on digital signatures: the so-called Public Key Infrastructure (PKI).
In low-end IoT however, using PKI is so far being avoided because certificates represent a significant additional overhead. Instead, IoT devices are pre-provisionned with the public keys of end-points they will communicate with over their lifetime (which is a limitation).
