# 3.4.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence you will be given an overview of the important APIs for
interacting with hardware.
By the end of this sequence, you will be able to write RIOT-based IoT applications applicable to a wide range of situations.
</p>
</div>
