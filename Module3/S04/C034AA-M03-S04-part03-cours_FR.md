# 3.4.3. Interagir avec les GPIO
[gpio]: #interagir-avec-les-GPIO

L'API du module **periph_gpio** offre une interface unifiée pour interagir avec les
broches d'entrée/sortie du micro-contrôleur. Les fonctions de l'API peuvent être
utilisées après avoir inclus l'entête **periph/gpio.h**.

Cette API est documentée [ici](http://doc.riot-os.org/group__drivers__periph__gpio.html).

Les GPIO sont généralement regroupées par _port_ sur un microcontrôleur et l'API
utilise la macro `GPIO_PIN(<port>, <pin>)` pour obtenir l'adresse mémoire du
périphérique dans le microcontrôleur.
La valeur retournée par cette macro dépend donc de l'architecture du
microcontrôleur.

Pour commencer à utiliser une broche GPIO, il faut d'abord l'initialiser avec le
bon mode en utilisant la fonction `gpio_init()`.
Tous les modes usuels (_INPUT_, _INPUT_ avec pull-down, _INPUT_ avec pull-up,
_OUTPUT_, etc) de manipulation des GPIOS peuvent être utilisés, à condition
qu'ils soient supportés par la cible matérielle.

```c
gpio_init(GPIO_PIN(0, 5), GPIO_OUT);
```

Ensuite les fonctions `gpio_set` et `gpio_clear` permettent de passer la GPIO
respectivement à l'état haut et à l'état bas:

```c
gpio_clear(GPIO_PIN(0, 5));
gpio_set(GPIO_PIN(0, 5));
```

La gestion des interruptions par les GPIO est désactivée par défaut mais peut
être ajoutée avec le module **periph_gpio_irq**. Cette fonctionnalité sert à
optimiser la taille du code lorsque l'utilisation des interruptions GPIO n'est
pas nécessaire.
L'initialisation d'une GPIO avec interruption s'effectue avec la fonction
`gpio_init_int()`.

Voici un exemple simple:

```c
static void gpio_cb(void *arg)
{
    (void) arg;
    /\* manage interrupt \*/
}

int main()
{
    gpio_init_int(GPIO_PIN(PA, 0), GPIO_IN, GPIO_RISING, gpio_cb, NULL);
}
```
