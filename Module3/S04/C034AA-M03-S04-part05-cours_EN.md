# 3.4.5. TP7: the RIOT application in action

This video shows the RIOT application developed during the programming exercise in TP7. The sensor values are read from an M3 board in the FIT IoT-LAB testbed.
