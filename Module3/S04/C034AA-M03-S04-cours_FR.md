# 3.4.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous ferez un tour d'horizon des APIs importantes pour
interagir avec le matériel.
A la fin de cette séquence, vous serez donc en mesure d'écrire des applications IoT basées sur RIOT et applicables dans un grand nombre de situations.
</p>
</div>
