# 3.4.1. The hardware abstraction layer

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.4.1a. A reminder of the concept](#341a-a-reminder-of-the-concept)
- [3.4.1b. Board level abstraction](#341b-board-level-abstraction)
- [3.4.1c. CPU level abstraction](#341c-cpu-level-abstraction)
- [3.4.1d. The APIs for CPU hardware devices](#341d-the-apis-for-cpu-hardware-devices)
<!-- TOC END -->

## 3.4.1a. A reminder of the concept

As we saw previously, the concept of hardware abstraction in RIOT involves 4 blocks (see figure 1):
- **CPUs**
- **boards**
- the APIs for hardware peripherals
- high-level **drivers**

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>Fig. 1: General structure of RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

A target device for a RIOT application is seen as
a microcontroller mounted on a board, which will expose some of the
pins of the microcontroller, potentially connecting up
external hardware devices such as sensors, actuators or radios.

As a result, you must define what the hardware target will be during the compilation process
(e.g. iotlab-m3, samr21-xpro, etc.). The build system will then select which parts of the code to compile (in the directories _boards_, _cpu_, _drivers_), as well as the toolchain to use.

Hardware abstraction makes it possible to compile a RIOT application for
different hardware targets without changing the source code. The result of the build for each target can be found in a different sub-directory of the application directory:

```sh
<application-dir>/bin/<board-name>
```

## 3.4.1b. Board level abstraction

Board level abstraction can be found in the _boards_ directory of the RIOT's
source code.

In this directory, apart from _common_, each sub-directory corresponds to a
board supported by the operating system.
The directory _common_ contains parts shared by a number of boards - hardware configurations, tools, etc. - thus avoiding having too much
code duplication for certain very similar boards.

The directory names specific to each board correspond to the name used
to inform the build system of the hardware target to use when
generating a piece of firmware.
Put simply, these names correspond to the value assigned to the variable
`BOARD` during the call made to _make_.
In terms of the build system, each directory in _board_ determines
its own _module_ - specifying `BOARD=<board name>` will load the
module corresponding to the board in its RIOT application.
To find out which toolchain was used to produce the firmware (i.e. which
compiler, linker, which CPU architecture), each board support must
define the CPU model, group and architecture. This is done in the file
`Makefile.features`.
This `Makefile` file also lists the features (including the hardware
drivers) available for this board.
The configurations for the internal clocks and hardware are defined in
the header file `periph_conf.h`. This choice is due to the fact that the majority of
hardware devices, such as UARTs, I2Cs or SPIs, are dependent on how they are
connected to the board (which pin the RX is connected to for UARTs, etc.)
The configuration structures are specific to the architecture of the CPU used
on the board, but they can be used uniformly in
shared APIs: it is not the type of the structure which is used,
but rather its index number in the list of configured hardware devices. We will return to
this concept later in this section.

To summarize, the support for a board will define the macros for using
LEDs and buttons, and potentially for specifying the different pin
configurations for sensors or radio technologies.
The name of these macros will be uniform across all boards, ensuring they remain
portable.
All these macros are defined in the header file `board.h`.

## 3.4.1c. CPU level abstraction

CPU level abstraction can be found in the _cpu_ directory of RIOT's source
code.

For each type of CPU, a hierarchical approach is used for hardware abstraction:


* at the highest level, a distinction is made between different **architectures**, e.g.
  ARM, AVR, MIPS, RISC-V, etc. This level is where the initialization
  sequences for each architecture can be found: entry point
  executed after a system reboot, interrupt management, etc. Generally
  speaking, these functions are specific to individual architectures.

* A distinction is then made between the specializations for each **group** in a
  given architecture. The ARM architecture, for example, is where the
  STM32 (STMicroelectronics), SAM (Microchip) and Kinetis (NXP) groups can be found. The way
  in which integrated circuits are structured varies considerably between manufacturers,
  meaning specific implementation/configuration is required.

* Each manufacturer also defines **types**, such as _stm32l0_ and _stm32f7_ for
  STM32 or _sam0_ and _sam3_ for SAM.

* Lastly, at the lowest and most specific hierarchical level, there is the
  microcontroller **model**, such as _stm32I072cz_ or _samd21g18a_. It is this
  model that will be entered in the board configuration. The associated hierarchical dependencies
  (_type_ -> _group_ -> _architecture_) are then resolved by the build system.

This hierarchical approach cuts back on code duplication, maximizing reuse and
simplifying long-term maintenance.

<figure style="text-align:center">
    <img src="Images/riot-cpus.png" alt="" style="width:400px;"/>
    <figcaption>Fig. 2: CPU models organization</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

## 3.4.1d. The APIs for CPU peripherals

The purpose of common APIs for each CPU's internal peripherals/features is to
provide a shared interface above as many different architectures/groups/types
as possible.
What this means is that the same code can be executed regardless of the underlying
hardware, thus ensuring that applications remain portable.

In order to achieve this portability, RIOT defines shared APIs in
header files located in `drivers/include/periph`. Concrete
implementations, dependent on each CPU, can be found in a sub-directory `periph` of
each CPU.
For a given hardware target, the corresponding implementation will be selected
by the build system, but the functions signature will not change.

A key point to note in RIOT is that concrete implementations are written
_from scratch_, using manufacturer libraries as little as possible. This requires
a lot more work from the community, but it also tends to enable
more efficient implementations, with less code duplication, as little
memory as possible used up and more readable code. This also ensures
vendor independence: the only file needed is the header file where MCU register
addresses, bit fields, interrupts and the macros specific to the different
CPU models are described.

To add support for an internal CPU peripheral/feature, i.e. to
compile the corresponding module in its application, developers must
modify the content of the **FEATURES_REQUIRED** variable in the application's
Makefile.

Here are a few modules (or _features_) provided by these APIs:

* The **periph_timer** module can be used to operate the CPU's internal counters.
  In order to do so, you must
  1. go to the application's Makefile and load this module in the list of
    required functions.

    ```mk
    FEATURES_REQUIRED += periph_timer
    ```

  2. Include the header `periph/timer.h` in its `.c` file:

    ```c
    #include "periph/timer.h"
    ```

  You must also ensure that the board provides the support for this
  hardware peripheral (which is normally the case, otherwise the board would be practically
  unusable). This can be verified in the file
  `boards/<target>/Makefile.features` which must contain the line:

  ```mk
  FEATURES_PROVIDED += periph_timer
  ```

* The module **periph_i2c** provides the support for I2C hardware devices. In a way that is
  similar to `periph_timer`, the module `periph_i2c` has to be added to the
  list of required features (ensuring that the board provides the
  support), before then including the header `periph/i2c.h` in its `.c` file.
  The I2C support in RIOT currently only supports `master` mode, meaning
  it is not yet possible to implement a module behaving like an I2C
  `slave` (such as a sensor, for example).

* On the same design, you will have the modules **periph_uart**, **periph_spi**,
  **periph_pwm**, **periph_adc**, **perhip_rtc** (Real-Time Clock),
  **periph_rtt** (Real-Time Ticker).

The full list of all these functions is described in the RIOT online documentation
at http://doc.riot-os.org/group__drivers__periph.html.

The next two sections take a closer look at how APIs are used to
control timers and GPIO, but if you would like to find out more about
the use of these hardware APIs or to find out how to use the other
features (RTC, UART, SPI, I2C, ADC, DAC, EEPROM), the RIOT source code
contains examples of applications in the _tests_
directory.
The names of the applications testing these APIs will start with
`periph_<hardware device name>`. Even if they are only tests, these are very good
use examples.
