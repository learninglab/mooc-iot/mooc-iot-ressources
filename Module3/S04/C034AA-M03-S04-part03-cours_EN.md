# 3.4.3. Interacting with GPIOs
[gpio]: #interacting-with-GPIOs

The API for the **periph_gpio** module provides a unified interface for controlling a
microcontroller's input/output pins. The API's functions can be
used after having included the **periph/gpio.h** header.

This API is documented [here](http://doc.riot-os.org/group__drivers__periph__gpio.html).

GPIOs are generally grouped together by _port_ on a microcontroller, with the API
using the macro `GPIO_PIN(<port>, <pin>)` in order to obtain the memory address for the
device in the microcontroller.
As a result, the value returned by this macro will depend on the architecture of the
microcontroller.

In order to start using a GPIO pin, you must first initialize it with the
right mode by calling the function `gpio_init()`.
Usual modes (_INPUT_, _INPUT_ with pull-down, _INPUT_ with pull-up,
_OUTPUT_, etc.) for using GPIOS are available, provided they are supported by
the underlying hardware target.

```c
gpio_init(GPIO_PIN(0, 5), GPIO_OUT);
```

The functions `gpio_set` and `gpio_clear` can be used to switch the GPIO
from high state to low state respectively:

```c
gpio_clear(GPIO_PIN(0, 5));
gpio_set(GPIO_PIN(0, 5));
```

The management of GPIO interrupts is deactivated by default, but can
be added using the module **periph_gpio_irq**. The purpose of this feature is to
optimize the size of the code in cases where the use of GPIO interrupts is
not necessary.
GPIOs with interrupts are initialized using the function `gpio_init_int()`.

Here is a simple example:

```c
static void gpio_cb(void *arg)
{
    (void) arg;
    /* manage interrupt */
}

int main()
{
    gpio_init_int(GPIO_PIN(PA, 0), GPIO_IN, GPIO_RISING, gpio_cb, NULL);
}
```
