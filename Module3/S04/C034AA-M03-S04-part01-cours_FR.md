# 3.4.1. La couche d'abstraction matérielle

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.4.1a. Rappel du concept](#341a-rappel-du-concept)
- [3.4.1b. L'abstraction niveau carte](#341b-labstraction-niveau-carte)
- [3.4.1c. L'abstraction niveau CPU](#341c-labstraction-niveau-cpu)
- [3.4.1d. Les APIs des périphériques du CPU](#341d-les-apis-des-périphériques-du-cpu)
<!-- TOC END -->

## 3.4.1a. Rappel du concept

Comme nous l'avons vu précédemment, le concept d'abstraction matérielle dans
RIOT repose sur 4 blocs (voir figure 1):
- les **CPUs**,
- les **boards**,
- les APIs des
périphériques
- et les **drivers** de haut niveau.

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>Fig. 1 : Architecture d'ensemble de RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

Sur le principe général, un objet cible pour une application RIOT est vu comme
un microcontrôleur monté sur une carte, celle-ci exposant certaines des
broches du microcontrôleur, en y connectant éventuellement directement des
périphériques externes comme des capteurs, des actionneurs ou des radios.

Il faut donc définir, lors de la compilation, quelle sera la cible matérielle
(par exemple iotlab-m3, samr21-xpro, etc). Ensuite, le système de compilation se charge de choisir les parties du code à compiler (dans les dossiers _boards_, _cpu_, _drivers_) et les outils de compilation à utiliser.

L'abstraction matérielle permet de compiler une application RIOT vers
différentes cibles matérielles sans en changer le code source. Le résultat de la compilation pour chaque cible se trouve dans un sous-dossier différent dans le dossier de l'application:

```sh
<application-dir>/bin/<board-name>
```

## 3.4.1b. L'abstraction niveau carte

L'abstraction au niveau carte se trouve dans le dossier _boards_ du code
source de RIOT.

Dans ce dossier, mis à part _common_, chaque sous dossier correspond à une
carte supportée par le système d'exploitation.
Le dossier _common_ contient des parties communes à plusieurs cartes - configurations de périphériques, outils, etc. -  et évite ainsi d'avoir trop de
duplication de code pour certaines cartes très similaires.

Les noms de dossiers spécifiques à chaque carte correspondent au nom utilisé
pour indiquer au système de compilation la cible matérielle à utiliser lors de
la génération d'un firmware.
Pour simplifier, ces noms correspondent à la valeur affectée à la variable
_BOARD_ lors de l'appel à _make_.
Du point de vue du système de compilation, chaque dossier dans _board_ définit
son propre _module_ et spécifier `BOARD=<nom de board>` revient à charger le
module correspondant à la carte dans son application RIOT.
Pour savoir quelle chaîne d'outils utilisée pour produire le firmware (i.e. quel
compilateur, linker, quelle architecture CPU), chaque support de carte doit
définir la famille et le modèle de CPU. Cela se fait dans le fichier
`Makefile.features`.
Ce fichier `Makefile` liste également les fonctionnalités (i.e. les pilotes de
périphériques en particulier) disponibles pour cette carte.
Les configurations des horloges internes et des périphériques sont définis dans
le fichier d'entête `periph_conf.h`. Ce choix est dû au fait que la plupart des
périphériques, comme l'UART, l'I2C, le SPI, dépendent de la façon dont ils sont
branchés sur la carte (sur quelle broche est connecté RX pour l'UART, etc.).
Les structures de configuration sont spécifiques à l'architecture du CPU utilisé
sur la carte mais elles permettent d'être utilisées de façon uniforme dans les
APIs communes : en effet, ce n'est pas le type de la structure qui est utilisé
mais son indice dans la liste des périphériques configurés. Nous reviendrons sur
cet aspect plus loin dans cette partie.

Pour terminer, le support d'une carte définit des macros pour manipuler des
LEDs, des boutons, éventuellement pour spécifier des configurations de broches
différentes pour des capteurs ou radio.
Le nom de ces macros est uniforme sur toutes les cartes, ce qui garantit leur
portabilité.
Toutes ces macros sont définies dans le fichier d'entête `board.h`.

## 3.4.1c. L'abstraction niveau CPU

L'abstraction au niveau CPU se trouve dans le dossier _cpus_ du code source de
RIOT.

Pour chaque type de CPU, l'abstraction matérielle suit une approche hiérarchique
:

* au plus haut niveau, on distingue l'**architecture**, comme par exemple les
  architectures de type ARM, AVR, MIPS, RISC-V, etc. A ce niveau se trouvent les
  séquences d'initialisation relative à chaque architecture : point d'entrée
  exécuté après le redémarrage système, gestion des interruptions, etc. Ces
  fonctions sont en général spécifiques à une architecture.

* Ensuite on distingue les spécialisations pour chaque **famille** dans une
  architecture donnée. Par exemple dans l'architecture ARM, on retrouve les
  familles STM32 (STMicroelectronics), SAM (Microchip), Kinetis (NXP). En effet,
  en fonction du fabricant, l'organisation du silicium varie fortement et il
  faut donc une implémentation/configuration spécifique.

* Puis chaque fabricant définit des **types** comme _stm32l0_, _stm32f7_ pour
  STM32 ou encore _sam0_, _sam3_ pour SAM.

* Enfin, au niveau le plus bas et le plus spécifique de la hiérarchie, on a l'indication du
  **modèle** de microcontrôleur comme _stm32l072cz_ ou _samd21g18a_. C'est le
  modèle qui est renseigné dans la configuration de la carte. La résolution des
  dépendances hiérarchiques associées (_type_ -> _famille_ -> _architecture_)
  est ensuite realisée par le système de compilation.

Cette approche hiérarchique permet de minimiser la duplication du code et donc
de maximiser sa réutilisation ainsi que de simplifier sa maintenance sur
le long terme.

<figure style="text-align:center">
    <img src="Images/riot-cpus.png" alt="" style="width:400px;"/>
    <figcaption>Fig. 2 : Organisation des CPUs</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

## 3.4.1d. Les APIs des périphériques du CPU

L'objectif des APIs des périphériques/fonctionnalités internes des CPUs est de
fournir une interface commune au-dessus d'un maximum d'architectures/familles/types
différents.
De cette manière, le même code peut s'exécuter quel que soit le matériel
sous-jacent. Cela garantit donc la portabilité des applications.

Pour arriver à cette portabilité, RIOT définit des API communes dans des
fichiers d'entête se trouvant dans `drivers/include/periph`. Les implémentations
concrètes, dépendantes des CPU, se trouvent dans un sous-dossier `periph` du
code des CPUs.
Pour une cible matérielle donnée, l'implémentation correspondante sera choisie
par le système de compilation mais la définition des fonctions ne change pas.

Un point important à noter dans RIOT : les implémentations concrètes sont écrites
_from scratch_, en utilisant au minimum les bibliothèques des fabricants. Cela demande
beaucoup plus de travail à la communauté, mais cela permet aussi une meilleure
efficacité de l'implémentation en général : moins de duplication de code, une
taille mémoire réduite au maximum, un code plus lisible. Cela garantit également
une indépendance vis-à-vis des fabricants : le seul fichier nécessaire est le
fichier d'entête CMSIS définissant la liste des interruptions, les adresses
mémoires, les macros spécifiques aux modèles de CPUs.

Pour ajouter le support d'une fonctionnalité interne d'un CPU, c'est-à-dire pour
compiler le module correspondant, dans son application, le dévelopeur doit
modifier le contenu de la variable **FEATURES_REQUIRED** dans le Makefile de
l'application.

Voici quelques modules (ou _features_) fournis par ces APIs:

* Le module **periph_timer** permet de faire fonctionner les compteurs internes du CPU.
  Pour cela, il faut :
  1. dans le `Makefile` de l'application, charger ce module dans la liste des
    fonctionnalités requises :

    ```mk
    FEATURES_REQUIRED += periph_timer
    ```

  2. dans son fichier `.c`, inclure l'entête `periph/timer.h`:

    ```c
    #include "periph/timer.h"
    ```

  Il faut également s'assurer que la carte fournit le support pour ce
  périphérique (ce qui est le cas en général, sinon la carte serait quasiment
  inutilisable...). Cela se vérifie dans le fichier
  `boards/<cible>/Makefile.features` qui doit contenir la ligne :

  ```mk
  FEATURES_PROVIDED += periph_timer
  ```

* Le module **periph_i2c** fournit le support pour les périphériques I2C. De manière
  similaire à `periph_timer`, il faut ajouter le module `periph_i2c` dans la
  liste des fonctionnalités requises (en s'assurant que la carte fournit le
  support) et ensuite inclure l'entête `periph/i2c.h` dans son ficher `.c`.
  Le support I2C dans RIOT ne supporte actuellement que le mode `master`, il n'est
  donc pas possible d'implémenter un module qui se comporterait comme un `slave`
  I2C (comme un capteur par exemple).

* Sur le même modèle, on aura les modules **periph_uart**, **periph_spi**,
  **periph_pwm**, **periph_adc**, **periph_rtc** (Real-Time Clock),
  **periph_rtt** (Real-Time Ticker).

La liste complète de ces fonctionnalités est disponible dans la documentation en ligne de
RIOT à l'adresse http://doc.riot-os.org/group__drivers__periph.html.

Les 2 parties suivantes présentent en détails l'utilisation des API pour
manipuler les timers et les GPIOS mais si vous souhaitez aller plus loin dans
l'utilisation de ces APIs matérielles ou découvrir comment utiliser les autres
fonctionnalités (RTC, UART, SPI, I2C, ADC, DAC, EEPROM), vous trouverez dans
le code source de RIOT des exemples d'applications placés dans le dossier
_tests_.
Le nom des applications testant ces APIs commence par `periph_<nom du
péripherique>`. Et même si elles servent de test, ce sont de très bons exemples
d'utilisation !
