# 3.4.4. Les pilotes de haut niveau

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.4.4a Concept](#344a-concept)
- [3.4.4b Initialisation](#344b-initialisation)
<!-- TOC END -->

## 3.4.4a Concept

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>Architecture d'ensemble de RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_


Les pilotes de périphériques permettent d'utiliser toutes sortes de capteurs,
d'actionneurs, de radios branchés au microcontrôleur sur ses GPIOs ou sur ses
bus de données comme l'UART, l'I2C ou le SPI.

Pour garantir leur portabilité sur toute les architectures supportées par RIOT,
ces pilotes de haut niveau s'appuient sur les APIs uniformisées vues
précédemment pour interagir avec le microcontrôleur.

Le fait que tous ces pilotes soient directement implémentés dans le code source
de RIOT facilite le développement d'application et garantit leur maintenance sur
le long terme par la communauté.

Le principe utilisé par RIOT pour implémenter ces pilotes de périphériques offre
également la possibilité d'utiliser plusieurs périphériques du même type en même
temps sur la même application : par exemple, grâce à ce principe, il est
possible d'avoir plusieurs capteurs sur le même bus I2C, puisqu'ils utilisent
des adresses différentes. Le pilote saura gérer cela car, en mémoire, il
utilise un descripteur contenant l'état de chaque périphérique pendant
l'exécution d'une application.
Cette caractéristique permet de mieux gérer les problèmes de concurrence (accès
depuis plusieurs _thread_ au même périphérique) ou de facilement utiliser ces
périphériques dans des _thread_/contextes séparés (puisque chaque _thread_
possède sa propre _stack_ mémoire).

Tous ces pilotes de haut niveau sont implémentés dans le dossier `drivers` de
RIOT et à chaque nom de dossier correspond un module qui pourra être importé
dans le `Makefile` d'une application pour compiler le pilote correspondant.

Il reste ensuite à inclure le fichier d'entête définissant l'interface (`#include <driver name>.h`).

Comme pour les périphériques internes du CPU, il existe une application de test dans le dossier _tests_ pour chaque pilote. Ces applications s'appellent toutes
`driver_<driver name>` et encore une fois, même si elles servent de test, ce sont de très bons exemples d'utilisation de ces pilotes !

## 3.4.4b Initialisation

Un aspect important à considérer pour utiliser correctement les pilotes de haut niveau
est leur procédure d'initialisation. Chaque pilote dans RIOT définit une
fonction d'initialisation de type `<driver name>_init()` prenant 2 paramètres :
* le premier paramètre est le pointeur vers le descripteur du pilote qui
  contiendra l'état du périphérique pendant l'exécution de l'application.
  Généralement, le type est nommé `<driver name>_t`.
* le second paramètre est le pointeur vers la structure contenant les paramètres
  d'initialisation du pilote pour ce périphérique.
  Chaque implémentation de pilote fournit une configuration d'initialisation
  avec des paramètres par défaut (généralement ils sont adaptés aux cartes les
  plus répandues). Ces paramètres sont définis dans le fichier d'entête
  `drivers/<driver name>/include/<driver name>_params.h`.
  Comme les paramètres par défaut utilisent des macros, ils sont facilement
  surchargeables soit depuis le code de l'application, soit depuis un support de
  carte. Dans ce second cas, les macros sont définies dans le fichier `board.h`
  du support de la carte.
  Ce mécanisme mêlant fichiers d'entête et macros permet de spécialiser la
  configuration initiale du driver en fonction des besoins de son applicaction
  et/ou de la configuration de la carte.

Une séquence typique d'initialisation d'un pilote s'effectuera donc comme dans
l'exemple suivant:

```c
#include "driver_name.h"
#include "driver_name_params.h"

static driver_name_t dev;

int main()
{
    [...]
    driver_name_init(&dev, &driver_name_params[0]);
    [...]
}
```

Le design des drivers dans RIOT est très bien documenté
[ici](http://doc.riot-os.org/driver-guide.html).
