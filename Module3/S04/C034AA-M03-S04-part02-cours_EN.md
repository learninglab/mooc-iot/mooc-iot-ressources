# 3.4.2. Timers
[timers]: #timers

Uniformly managing timers on heterogeneous platforms
is generally a complex problem. Indeed, the majority of CPUs provide several
internal timers capable of timing in parallel, at different speeds and in
different conditions (some are able to run while the CPU is asleep, while others can't).

That said, timers are among the most important features, in that
they provide a time base and allow events (such as interrupts)
to be generated at a specific time or at different time intervals.

In a microcontroller, a timer can't count up to infinity because
it has a maximum number of cycles pre-determined in accordance with the manufacturer's specifications.
This number of cycles will depend on the _length_ of the timer, which may be 8, 16
or 32 bits. Once the timer reaches its maximum value (referred to
as an _overflow_), it will start again from zero.

RIOT's API for managing timer devices makes it possible to transparently use
timers on a wide variety of architectures. The following
tasks can be performed:
* starting and stopping timers using the functions
_timer\_start()_/_timer\_stop()_,
* reading the current value using
_timer\_read()_,
* configuring a _callback_ after a
specific delay using the functions _timer\_set()_ or _timer\_set_absolute()_.

This API is documented [here](http://doc.riot-os.org/group__drivers__periph__timer.html).

This API is quite useful, but it has its limitations, and is not capable of really getting the most out of the
multi-task functions of an operating system.

The system module _ztimer_ was developed to address these
limitations. The concept behind this module is to provide a multiplexing mechanism on top of a
hardware timer: one single API that can be used to manage multiple delays
from one hardware timer.
The _ztimer_ module enables delays that are precise to the microsecond.
The _ztimer_ module offers timings with a precision of the order of
microseconds for high frequency timers, milliseconds or seconds for low power
timers.
The _ztimer_ module is composed of 3 sub-modules, each corresponding to a timing
accuracy:
- _ztimer\_usec_: uses an underlying timer `ZTIMER_USEC` with microsecond accuracy
- _ztimer\_msec_: uses an underlying timer `ZTIMER_MSEC` with millisecond accuracy
- _ztimer\_sec_: uses an underlying timer `ZTIMER_SEC` with second accuracy

The _ztimer_ module is fully portable because it is implemented on top of the
hardware abstraction layer.

The module's API is extremely easy to use. Here are a few examples of
the most important uses of this module's functions:

* retrieving the current system time in microseconds:
  ```c
  uint32_t now = ztimer_now(ZTIMER_USEC);
  ```

* retrieving the current system time in milliseconds :
  ```c
  uint32_t now = ztimer_now(ZTIMER_MSEC);
  ```

* blocking the execution of a _thread_ for a delay of _sec_ seconds:

  ```c
  ztimer_sleep(ZTIMER_SEC, sec);
  ```
  or
  ```c
  ztimer_sleep(ZTIMER_MSEC, sec * MS_PER_SEC);
  ```
  or even
  ```c
  ztimer_sleep(ZTIMER_USEC, sec * US_PER_SEC);
  ```

  During this time, the system can switch to another _thread_ in order
  to carry out other tasks.

* blocking the execution of a _thread_ for a delay of _microsec_ microseconds:

  ```c
  ztimer_sleep(ZTIMER_USEC, microsec);
  ```


* calling a _callback_ function after a delay of _offset\_in\_ms_ milliseconds.

  - Using an `ztimer_t` type variable to execute a _callback_
    function at a certain time:

  ```c
  static void cb(void)
  {
      /* code executed in callback */
  }
  [...]
  ztimer_t timer;
  timer.callback = cb;

  ztimer_set(ZTIMER_MSEC, &timer, offset_in_ms);
  ```
