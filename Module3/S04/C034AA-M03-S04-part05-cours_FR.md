# 3.4.5. TP7: l'application RIOT en action

Cette video montre l'application RIOT développée dans l'exercise de programmation du TP7. Les données des capteurs sont lues sur une carte M3 de la plateforme d'experimentation FIT IoT-LAB.
