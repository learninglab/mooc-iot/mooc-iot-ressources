# 3.4.2. Les compteurs
[timers]: #les-compteurs

La gestion uniforme des compteurs sur des plateformes hétérogènes est en
général un problème complexe. En effet, la plupart des CPUs fournissent plusieurs
compteurs internes pouvant compter en parallèle, à des vitesses différentes et dans
des conditions différentes (certains peuvent continuer à fonctionner pendant que
le CPU dort, alors que d'autres non).

Cela dit, les compteurs sont l'une des fonctionnalités les plus importantes car
ils permettent de fournir une base de temps et de générer des évènements (comme
des interruptions) à un instant donné ou à différents intervalles de temps.

Dans un microcontrôleur, un compteur ne peut pas compter jusqu'à l'infini car il
a un nombre de cycles maximal prédéfini par les spécifications du constructeur.
Ce nombre de cycles dépend de la _longueur_ du compteur qui peut être de 8, 16
ou 32 bits suivant les cas. Quand le compteur atteint sa valeur maximale (on dit
qu'il _overflow_), il repart ensuite à zéro.

L'API de gestion des périphériques de type compteurs de RIOT permet de manipuler
les compteurs de façon portable sur un grand nombre d'architectures. Les actions
possibles sont :
*  le démarrage et l'arrêt des compteurs avec les fonctions
_timer\_start()_/_timer\_stop()_,
* la lecture de la valeur courante avec
_timer\_read()_,
* ou encore la configuration d'une _callback_ appelée après un
certain délai via les fonctions _timer\_set()_ ou _timer\_set_absolute()_.

Cette API est documentée [ici](http://doc.riot-os.org/group__drivers__periph__timer.html).

Cette API est assez utile mais s'avère assez limitée pour tirer pleinement parti
des fonctions multi-tâches d'un système d'exploitation. Une autre limitation de
ces compteurs est liée à la consommmation d'énergie : en effet, ils s'appuient
généralement sur des compteurs hardware à haute fréquence qui sont désactivés
dans les modes de fonctionnement basses consommation.

C'est pour remédier à ces limitations que le module système _ztimer_ a été
développé. L'idée de ce module est de proposer un mécanisme de multiplexage des
compteurs matériels : une seule API permettant de gérer plusieurs temporisations
à partir de compteurs matériels de différent types (haute fréquence ou basse
consommation).
Le module _ztimer_ offre des temporisations ayant une précision de l'ordre de la
microseconde pour les compteurs haute fréquence, de la milliseconds ou de la
seconde pour les compteurs basse consommation.
Le module _ztimer_ est composé de 3 sous modules, chacun correspondant à une
précision donnée:
- _ztimer\_usec_: créé un timer `ZTIMER_USEC` avec une précision de l'ordre de la microseconde
- _ztimer\_msec_: créé un timer `ZTIMER_MSEC` avec une précision de l'ordre de la milliseconde
- _ztimer\_sec_: créé un timer `ZTIMER_SEC` avec une précision de l'ordre de la seconde

Le module _ztimer_ est totalement portable car il est implémenté au dessus de la
couche d'abstraction matérielle.

L'API du module est très simple à utiliser. Voici quelques exemples
d'utilisation des fonctions de ce module parmi les plus importantes pour :

* obtenir le temps système en microsecondes :
  ```c
  uint32_t now = ztimer_now(ZTIMER_USEC);
  ```

* obtenir le temps système en millisecondes :
  ```c
  uint32_t now = ztimer_now(ZTIMER_MSEC);
  ```

* bloquer l'exécution du code pendant un délai de _sec_ secondes :

  ```c
  ztimer_sleep(ZTIMER_SEC, sec);
  ```
  ou
  ```c
  ztimer_sleep(ZTIMER_MSEC, sec * MS_PER_SEC);
  ```
  ou encore
  ```c
  ztimer_sleep(ZTIMER_USEC, sec * US_PER_SEC);
  ```

  Pendant ce temps, le système peut passer la main à un autre _thread_ pour
  effectuer d'autres tâches.

* bloquer l'exécution du code pendant un délai de _microsec_ microsecondes :

  ```c
  ztimer_sleep(ZTIMER_USEC, microsec);
  ```


* appeler une fonction de _callback_ après un délai de _offset\_in\_ms_ millisecondes.

  - Utiliser une variable de type `ztimer_t` pour exécuter une fonction de
    _callback_ à un instant donné :

  ```c
  static void cb(void)
  {
      /\* code executed in callback \*/
  }
  [...]
  ztimer_t timer;
  timer.callback = cb;

  ztimer_set(ZTIMER_MSEC, &timer, offset_in_ms);
  ```
