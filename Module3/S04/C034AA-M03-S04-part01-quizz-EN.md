# Question 3.4.1

Refer to the [ztimer module online documentation](https://doc.riot-os.org/group__sys__ztimer.html) of RIOT and
select the ztimer calls that can be used to block a thread execution during a 5 seconds period (multiple choices possible):

( ) `ztimer_now(ZTIMER_USEC)`
(x) `ztimer_sleep(ZTIMER_SEC, 5)`
(x) `ztimer_sleep(ZTIMER_MSEC, 5000)`
( ) `ztimer_sleep(ZTIMER_USEC, 5000)`
( ) `ztimer_sleep(ZTIMER_SEC, 5000)`

[explanation]
`ztimer_now(ZTIMER_USEC)` returns the current system time in microseconds, since it's using the underlying microseconds clock.
`ztimer_sleep(ZTIMER_SEC, 5000)` blocks during 5000 seconds.
`ztimer_sleep(ZTIMER_USEC, 5000)` blocks during 5000 microseconds (or 5 milliseconds) but definitely not during 5 seconds.
`ztimer_sleep(ZTIMER_SEC, 5)` blocks during 5 seconds, so right answer.
`ztimer_sleep(ZTIMER_MSEC, 5000)` blocks during 5000 milliseconds, e.g. 5 seconds, so right answer.
[explanation]

# Question 3.4.2

Refer to [the online GPIO API documentation](https://doc.riot-os.org/group__drivers__periph__gpio.html) of RIOT and select
the GPIO usual configuration modes (multiple choices possible):

(x) `GPIO_IN`
(x) `GPIO_IN_PD`
( ) `GPIO_IN_OUT`
(x) `GPIO_IN_PU`
( ) `GPIO_IN_OD`
(x) `GPIO_OUT`
(x) `GPIO_OD`
(x) `GPIO_OD_PU`

[explanation]
All modes are correct except `GPIO_IN_OUT` et `GPIO_IN_OD` which don't exist.
[This page](https://embeddedartistry.com/blog/2018/06/04/demystifying-microcontroller-gpio-settings/) explains very well
the behaviour of the different modes.
[explanation]
