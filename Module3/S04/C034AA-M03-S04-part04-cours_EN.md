# 3.4.4. High-level drivers

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.4.4a Concept](#344a-concept)
- [3.4.4b Initialization](#344b-initialization)
<!-- TOC END -->

## 3.4.4a Concept

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>General structure of RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

Device drivers are used to operate a wide variety of sensors,
actuators and radios connected to microcontrollers on GPIOs or on
data buses such as UARTs, I2Cs or SPIs.

In order to ensure they are compatible with all architectures supported by RIOT,
these high-level drivers rely on the standardized APIs we came across
earlier for interacting with microcontrollers.

The fact that all these drivers are implemented directly in RIOT's
source code makes developing the application easier and ensures they will be maintained
long-term by the community.

The way in which these device drivers are implemented by RIOT
makes it possible to use more than one device of the same type at the same
time in the same application: it is possible, for example,
to have multiple sensors on the same I2C bus, owing to the fact that they use
different addresses. The driver will know how to handle this, because in its memory
it uses a descriptor containing the status of each device.
This descriptor is kept in memory during the whole application life-time which is kept for the duration of the application's life time.
This characteristic also provides a better way of dealing with concurrency issues (multiple
_threads_ trying to access the same device), while making it easy to use these
devices in separate _threads_/contexts (remember that each _thread_
has its own memory _stack_).

All these high-level drivers are implemented in the `drivers` directory in
RIOT, with each driver directory name corresponding to a module that can be imported
into an application's `Makefile` in order to compile the corresponding driver.

All that has to be done is to include the header file defining the interface (`#include <driver name>.h`).

As is the case for internal CPU peripherals, there is a test application in the _tests_ directory for each driver. These applications are all called
`driver_<driver name>` and, once again, even if they are for test purposes, provide very useful examples of how these drivers are used.

## 3.4.4b Initialization

In order to use high-level drivers correctly, an important aspect to consider
is their initialization procedure. Each driver in RIOT defines
an initialization function (e.g. `<driver name>_init()`) taking two parameters:
* the first parameter is the pointer towards the driver descriptor, which
  will contain the status of the device while the application is being executed.
  The type is usually called `<driver name>_t`.
* the second parameter is the pointer towards the structure containing the driver's
  initialization parameters for this device.
  Each driver implementation will supply an initialization configuration
  with default parameters (normally adapted for the most widely-used
  boards). These parameters are defined in the header file
  `drivers/<driver name>/include/<driver name>_params.h`.
  Given that the default parameters use macros, these can easily be
  overwritten, either from the application code, or from a support board. In this second case, macros will be defined in the file `board.h`
  of the support board.
  This mechanism, which mixes together header files and macros, is used to specialize
  the driver's initial configuration to suit the needs of its application
  and/or the configuration of the board.

A typical initialization sequence for a driver will be performed in
the following example:

```c
#include "driver_name.h"
#include "driver_name_params.h"

static driver_name_t dev;

int main()
{
    [...]
    driver_name_init(&dev, &driver_name_params[0]);
    [...]
}
```

Device drivers design in RIOT is very well documented
[here](http://doc.riot-os.org/driver-guide.html).
