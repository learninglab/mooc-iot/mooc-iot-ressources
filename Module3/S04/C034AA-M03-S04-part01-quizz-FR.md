# Question 3.4.1

En se référant à [la documentation en ligne du module ztimer](https://doc.riot-os.org/group__sys__ztimer.html) de RIOT,
quelles sont les fonctions qui permettent de bloquer un thread pendant 5 secondes ?

( ) `ztimer_now(ZTIMER_USEC)`
(x) `ztimer_sleep(ZTIMER_SEC, 5)`
(x) `ztimer_sleep(ZTIMER_MSEC, 5000)`
( ) `ztimer_sleep(ZTIMER_USEC, 5000)`
( ) `ztimer_sleep(ZTIMER_SEC, 5000)`

[explanation]
`ztimer_now(ZTIMER_USEC)` renvoie the temps système courant en microsecondes, puisque l'horloge sous-jacente a une précision de l'ordre de la microseconde.
`ztimer_sleep(ZTIMER_SEC, 5000)` bloque pendant 5000 secondes.
`ztimer_sleep(ZTIMER_USEC, 5000)` bloque pendant 5000 microsecondes (ou 5 millisecondes) et donc pas pendant 5 secondes.
`ztimer_sleep(ZTIMER_SEC, 5)` bloque pendant 5 secondes, c'est une bonne réponse.
`ztimer_sleep(ZTIMER_MSEC, 5000)` bloque pendant 5000 millisecondes, i.e. 5 secondes, c'est une bonne réponse.
[/explanation]

# Question 3.4.2

D'après [la documentation en ligne de l'API GPIO](https://doc.riot-os.org/group__drivers__periph__gpio.html) de RIOT,
quels sont les modes usuels de configuration des GPIOs ?

(x) `GPIO_IN`
(x) `GPIO_IN_PD`
( ) `GPIO_IN_OUT`
(x) `GPIO_IN_PU`
( ) `GPIO_IN_OD`
(x) `GPIO_OUT`
(x) `GPIO_OD`
(x) `GPIO_OD_PU`

[explanation]
Tous les modes sont valides sauf `GPIO_IN_OUT` et `GPIO_IN_OD` qui n'existent pas.
Voir [cette page](https://embeddedartistry.com/blog/2018/06/04/demystifying-microcontroller-gpio-settings/) qui
expliquent bien les différences entre les différents modes.
[explanation]
