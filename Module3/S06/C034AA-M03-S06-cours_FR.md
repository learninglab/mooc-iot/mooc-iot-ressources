# 3.6.0. Allez plus loin avec RIOT

Ce module d'initiation à RIOT est terminé. Vous êtes maintenant
capables d'écrire des applications très simples, par exemple pour lire des capteurs, ou
des applications plus compliquées utilisant plusieurs _threads_.
Les modules suivants vous permettront de découvrir les fonctions de RIOT pour
d'une part rendre vos applications communicantes et d'autre part les sécuriser.

Si vous souhaitez utiliser RIOT sur d'autres plateformes que celles fournies
par IoT-LAB, RIOT supporte plus de 200 cartes actuellement, qui couvrent la
plupart des cas d'utilisation. La liste des cartes supportées est disponible dans
la [documentation en ligne](https://doc.riot-os.org/group__boards.html).

La communauté RIOT propose également des tutoriels en ligne pour découvrir RIOT:
- le [tutoriel RIOT](https://github.com/RIOT-OS/Tutorials) est axé sur les
  protocoles IPv6 et aussi sur les ICN (Information Centric Networks),
- le [RIOT course](https://github.com/RIOT-OS/riot-course), dont sont tirés une
  partie du cours et certains exercices de ce module sur RIOT, va un peu plus
  loin sur les _thread_ et sur les protocoles de communication basés IPv6.

Enfin, [Inria Academy](https://www.inria.fr/fr/inria-academy) propose une 
[formation sur RIOT](https://www.inria-academy.fr/formation/riot-le-systeme-dexploitation-adapte-a-linternet-des-objets/)
destinée aux entreprises pour aller vraiment plus loin ; elle est dispensée par
certains core developpeurs du projet.
