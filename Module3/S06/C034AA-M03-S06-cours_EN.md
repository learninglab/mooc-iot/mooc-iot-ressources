# 3.6.0. Going further with RIOT

This introductory module to RIOT is completed. You are now
able to write very simple applications to read sensors, as well as more
complicated ones that are using multiple _threads_.
The next modules will allow you to discover RIOT's functions to
make sure your applications can communicate (4 & 5) and are secure (6).

If you want to use RIOT on other platforms than the ones provided by IoT-LAB,
RIOT currently supports more than 200 boards, which cover
most of the use cases. The supported boards are listed in
the [online documentation](https://doc.riot-os.org/group__boards.html).

The RIOT community also offers online tutorials to learn RIOT:
- the [RIOT tutorial](https://github.com/RIOT-OS/Tutorials) is focused on the
  IPv6 protocols (CoAP, RPL) and on ICN (Information Centric Networks),
- the [RIOT course](https://github.com/RIOT-OS/riot-course) from which some parts
  of this course and some of its exercises are taken, goes a bit further on _threads_ and
  and on communication protocols based on IPv6.

Finally, [Inria Academy](https://www.inria.fr/fr/inria-academy) offers a 
[RIOT training](https://www.inria-academy.fr/formation/riot-le-systeme-dexploitation-adapte-a-linternet-des-objets/)
intended for companies to go really further and is given by some core developers
of the RIOT project.
