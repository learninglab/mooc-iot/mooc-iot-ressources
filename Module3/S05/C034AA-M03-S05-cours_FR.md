# 3.5.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez écrire une application IoT basée sur RIOT et applicable dans un grand nombre de situations. Cette mise en pratique prendra la forme d'<strong>une activité évaluée par les pairs</strong>. Elle compte pour 5% dans la note finale pour obtenir l'attestation de réussite avec succès de ce Mooc.<br/></br>
Le TP7 peut toutefois être réalisé sans participation à cette évaluation par les pairs. A vous de décider en fonction de votre <a href="https://www.fun-mooc.fr/courses/course-v1:inria+41020+session03/courseware/2bd6fa6815204f73ad5f2f46ed005fb9/8a18495c188140f2ace45a256fa6fc0f/" target="blank">test d'auto-positionnement</a>.<br/></br>
<strong>Dates clés :</strong><br/>
<strong>(1) du 19 avril au 24 mai : Dépôt </strong>de votre travail<br/>
<strong>(2) du 25 avril au 7 juin : Correction et notation</strong> de 3 travaux de vos pairs<br/>
</p>
</div>
