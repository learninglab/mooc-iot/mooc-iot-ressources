# 3.5.3. RIOT application on FIT IoT-LAB

This video shows an example of how a RIOT application works. It displays the measures performed by the sensors of the M3 board on the FIT IoT-LAB platform. It is provided as an example only; the instructions given in the video do not apply to the practical exercises in this session.

**NB**: This video was recorded outside of the Jupyterlab environment, that's why the manipulations are done on a computer and not via Jupyterlab.
