# 3.5.3. Application RIOT sur FIT IoT-LAB

Cette vidéo montre un exemple de fonctionnement d'une application RIOT. Il s'agit d'afficher les mesures effectuées par les capteurs de la carte M3 de la plateforme FIT IoT-LAB. Elle est présentée à titre d'exemple uniquement ; les consignes données dans la vidéo ne s'appliquent pas dans le cadre des exercices pratiques de cette session.

**NB** : Cette vidéo a été tournée en dehors de l’environnement Jupyterlab, c’est pourquoi les manipulations se font sur un ordinateur et non via Jupyterlab.
