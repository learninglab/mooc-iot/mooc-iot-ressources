# 3.5.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence you will write a RIOT-based IoT application that can be applied in a wide range of situations.<br/>
This practice will take the form of a <strong>peer-reviewed activity</strong>. This activity counts for 5% towards the grade for successful completion of this Mooc.<br/></br>
However, you can follow and complete TP7 "Use sensors on the IoT-LAB M3 board" even if you do not wish to participate to the peer-reviewed activity. It is up to you to decide on the basis of your <a href="https://www.fun-mooc.fr/courses/course-v1:inria+41020+session03/courseware/2bd6fa6815204f73ad5f2f46ed005fb9/8a18495c188140f2ace45a256fa6fc0f/" target="blank">self-positioning test</a>.</br></br>
<strong>Key dates:</strong><br/>
<strong>(1) from April 19th to May 24th: Submission </strong>of your work<br/>
<strong>(2) from April 25th to June 7nd: Correction and grading</strong> of 3 peer-reviewed papers<br/>
</p>
</div>
