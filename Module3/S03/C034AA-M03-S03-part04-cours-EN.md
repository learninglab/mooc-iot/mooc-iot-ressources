# 3.3.4. Power management

The power management mechanisms in RIOT rely on the capacity most
microcontrollers have to switch to extremely low-power
operating modes.
This involves switching the microcontroller over to one of these modes
automatically once all tasks are either terminated or blocked, awaiting
an external event.

However, as we saw earlier, RIOT applications begin by default with 2
_threads_: _main_ and _idle_. Furthermore, a RIOT application will switch over to the
_idle_ _thread_ once all tasks are either terminated or blocked.

_Note_: with ARM Cortex-M microcontrollers, there's no _idle thread_, so in this case, the scheduler is
also in charge of activating the power management mode when necessary.

<figure style="text-align:center">
    <img src="Images/riot-application.png" alt="" style="width:300px"/>
    <figcaption>Fig. 1: Structure of a RIOT application, &copy; A. Abadie</figcaption>
</figure>

In RIOT, it is therefore the _idle_ _thread_ (or the scheduler) which is responsible for switching the
microcontroller over to the operating mode with the lowest possible power
consumption. This is referred to as putting the microcontroller to sleep.

RIOT defines a maximum of 4 low-power modes, from 3 to
0, Depending on the architecture or group of microcontrollers, in
reality, there can be fewer levels.

For example, with ST Microelectronics microcontrollers, RIOT defines 2 low-power
modes, while for NXP Kinetis, there is only one.

In all cases, the operating mode with the lowest power consumption
will be `0`.

Switching over to the mode with the lowest possible power consumption is done
using a _cascade_ algorithm:

1. Beginning with the number of modes defined for a given architecture (e.g.
  2 for STM32), RIOT will check to see if the highest mode is unblocked.
2. If this mode is blocked, it will remain in the current mode (idle).
3. If the mode is unblocked, it will check to see if a lower mode is unblocked, and
   so on.

This method ensures that the system automatically selects the mode with the lowest
power consumption.

By default, none of the low-power modes will be unblocked.

Unblocking the low-power modes to use is left to the needs of the developer of
the application - depending on the mode, some
features will not be available (such as RAM retention, for
example), or certain devices will be deactivated.
The selection is made by settings the `PM_BLOCKER_INITIAL` constant during the
compilation process, using the variable `CFLAGS`.
The `PM_BLOCKER_INITIAL` constant must contain a 32bit long value (e.g a 4 bytes
long value), in hexadecimal format.
Each byte corresponds to the initial state of one of the 4 power modes: `0x<mode 3><mode 2><mode 1><mode 0>`.
For each mode, a value of `0x00` means the mode is unblocked and a value of `0x01` means the mode is blocked.

To unblock all modes but the mode 0 by default, for example, all you need to do
is to add the following to an application's Makefile:

```mk
CFLAGS += '-DPM_BLOCKER_INITIAL=0x00000001'
```

It is also possible to block or unblock a mode while an application is being
executed using the `pm_block` and `pm_unblock` functions. These functions
are defined in the header `pm_layered.h`.

Power management functions are briefly documented
[here](http://doc.riot-os.org/group__sys__pm__layered.html).
