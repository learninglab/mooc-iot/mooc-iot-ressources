# 3.3.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Au cours des différentes parties de cette séquence, vous irez plus loin dans la connaissance de RIOT: vous comprendrez le fonctionnement interne du noyau interne de RIOT et la création d'une application multi-tâche.
</p>
</div>
