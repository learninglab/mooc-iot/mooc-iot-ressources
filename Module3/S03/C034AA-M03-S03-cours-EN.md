# 3.3. The RIOT kernel


## 3.3.0. Introduction
[introduction]: #introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>This sequence will expand your knowledge of RIOT - by the end, you
will understand how RIOT’s internal kernel works and how to create
a multi-task application.
</p>
</div>
