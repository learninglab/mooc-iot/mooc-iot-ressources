# 3.3.3. Utilisation avancée des threads

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.3.3a. Gérer la concurrence entre threads](#333a-gérer-la-concurrence-entre-threads)
- [3.3.3b. Communication entre threads](#333b-communication-entre-threads)
<!-- TOC END -->

## 3.3.3a. Gérer la concurrence entre threads

Comme nous l'avons vu dans le paragraphe précédent, les _threads_ dans RIOT
disposent de leur propre espace mémoire. Cependant, il peut quand même arriver
que les _threads_ partagent des variables, par exemple via la variable
contextuelle `arg` de la fonction `thread_create()`, ou accèdent à des variables
globales de l'application ou même du système.

Pour éviter les problèmes d'accès concurrents liés à ces cas de figure, le noyau
RIOT fournit des mécanismes de synchronisation entre _thread_ :

1. Les **mutex** permettent de gérer les problèmes d'exclusion mutuelle tels
   que le blocage d'accès à une variable par le _thread_ actif. Aucun autre
   _thread_ ne pourra alors accéder à cette variable tant qu'elle n'est pas
   relâchée. L'API pour manipuler les _mutex_ est définie dans le fichier
   `mutex.h`. Son utilisation est très simple :

   ```c
   mutex_t lock;
   mutex_lock(&lock);
   /* portion de code protégée par le mutex */
   mutex_unlock(&lock);
   ```

2. Les sémaphores permettent de gérer des problèmes de synchronisation plus
   complexes que les _mutex_ comme le problème du "rendez-vous"  ou le problème des
   "producteurs-consommateur". ( [_The Little Book of Semaphores_](http://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf), Allen B. Downey)
   L'API pour utiliser des sémaphores dans RIOT est définie dans le fichier
   `semaphores.h`.

Pour plus de détails, vous pouvez consulter la [documentation en ligne des mutex](http://doc.riot-os.org/group__core__sync__mutex.html).

## 3.3.3b. Communication entre threads

Les _threads_ dans RIOT peuvent également communiquer entre eux, à l'intérieur
d'une même application. C'est ce que l'on appelle généralement la communication
inter-processus, ou IPC (_Inter-Process Communication_).

Les messages IPC peuvent être échangés soit entre 2 _threads_, soit entre le
contexte d'interruption via la sous-routine d'interruption (ISR) et un _thread_.

L'IPC peut avoir 2 modes :

* **synchrone**: dans ce mode, le _thread_ qui a envoyé un message est bloqué
  tant que le _thread_ destinataire n'a pas reçu le message,
* **asynchrone**: dans ce mode, l'envoi du message n'est pas bloquant. Cela est
  possible grâce à l'utilisation d'une queue de messages dans laquelle est posté
  le message. Le message sera traité lorsque le destinataire sera prêt.
  **L'envoi d'un message depuis une sous-routine d'interruption est toujours asynchrone.**
  Dans ce cas, il faudra toujours penser à initialiser une file de messages dans
  le _thread_ destinataire.


L'API de communication inter-processus (IPC) est définie dans l'entête `msg.h` (qui se trouve dans `core`).
Cette API est documentée dans la [documentation en ligne](http://doc.riot-os.org/group__core__msg.html).
Le type d'une variable contenant un message est `msg_t` et possède 2 attributs :

* `type` qui permet d'identifier le type de message et donc de le traiter
  différemment en fonction,
* `content` - qui est une _union_ - peut contenir soit une valeur entière non
  signée sur 32 bits (de type `unit32_t`) soit l'addresse d'une variable (i.e un
  pointeur).

Le code ci-dessous montre comment définir et remplir un message :

```c
msg_t msg;
msg.type = MSG_TYPE;
msg.content.value = 42; /* content can be a value */
msg.content.ptr = address; /* or content can be a pointer */
```

La macro `MSG_TYPE` serait dans ce cas définie globalement dans un fichier
d'en-tête :

```c
#define MSG_TYPE  (1234)
```

L'envoi de message peut se faire à l'aide de plusieurs fonctions, chacune ayant
un fonctionnement différent :

* Un envoi bloquant (synchrone) vers un _thread_ ayant pour numéro _pid_ se
  fait avec l'appel suivant:

  ```c
  msg_send(&msg, pid);
  ```

  **Remarque:** si cet appel se fait depuis une sous-routine d'interruption,
  l'envoi est non bloquant (asynchrone).

* Un envoi non bloquant se fait avec l'appel :

  ```c
  msg_try_send(&msg, pid);
  ```

* Il est aussi possible d'envoyer un message et de bloquer le _thread_ émetteur
  en attente d'une réponse. Dans ce cas, il faut utiliser l'appel :

  ```c
  msg_send_receive(&msg, &msg_reply, pid);
  ```

  Le paramètre `msg_reply` contient alors le message de réponse du _thread_
  destinataire de l'envoi initial.

  L'envoi de la réponse depuis le destinataire se fait avec l'appel :

  ```c
  msg_reply(&msg, &msg_reply);
  ```

Pour recevoir un message depuis un _thread_, le noyau RIOT met à disposition 2
fonctions :

* un appel à `msg_receive` bloque le _thread_ courant en attente d'un message :

  ```c
  msg_receive(&msg);
  ```

* un appel à `msg_try_receive` vérifie si un message a été reçu et retourne `1`
  dans ce cas:

  ```c
  int ret = msg_try_receive(&msg);
  if (ret > 0) {
      /* message received */
  }
  else {
      /* no message received */
  }
  ```

L'utilisation typique des fonctions de réception est une boucle d'attente de
messages venant d'un autre _thread_ ou d'une sous-routine d'interruption et
s'exécutant dans un _thread_ :

```c
static void *thread_handler(void *arg)
{
    /* endless loop */
    while (1) {
        msg_t msg;
        msg_receive(&msg);
        printf("Message received: %s\n", (char *)msg.content.ptr);
    }
    return NULL;
}
```

Dans le cas où les messages sont envoyés depuis le contexte d'une interruption,
l'envoi de message est asynchrone et il faut donc initialiser une file de
messages au début de la fonction du _thread_ :

```c
static void *thread_handler(void *arg)
{
    msg_t msg_queue[8];
    msg_init_queue(msg_queue, 8);

    while (1) {
        /* Wait for messages and process them */
    }

    return NULL;
}
```
