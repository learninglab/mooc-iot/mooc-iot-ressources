# 3.3.2. Ordonnancement et threads
[thread-scheduler]: #ordonnancement-et-threads

L'ordonnancement des _threads_ dans le noyau de RIOT est de type préemptif avec
priorités. C'est un ordonnancement dit statique où :

* le _thread_ non bloqué et non terminé, ayant la plus haute priorité, est actif, i.e. il
  s'exécute sur le CPU,
* tout _thread_ en cours d'exécution peut être interrompu par un autre
  _thread_ de priorité plus élévée,
* en cas d'inactivité, i.e lorsque tous les _threads_ sont bloqués ou terminés,
  le système bascule automatiquement sur un _thread_ particulier, le _thread_
  d'attente, dit _idle_,
* une interruption peut préempter n'importe quel _thread_ à n'importe quel
  moment et exécuter la sous-routine d'interruption (_ISR_).

L'ordonnancement utilise une politique _tick-less_ c'est-à-dire que la sélection
du _thread_ actif ne se fait pas par vérifications périodiques de l'état des _threads_
mais par évènements (typiquement des interruptions internes).
Le temps de basculement entre _threads_ est constant quel que soit le nombre de
_threads_ donc la complexité algorithmique de l'ordonnanceur RIOT est _O(1)_.

Le noyau RIOT supporte 16 niveaux de priorité différents dont les valeurs vont
de 0 à 15. Plus la valeur d'une priorité est faible, plus la priorité est
élevée.
Les _threads_ par défaut d'une application, _idle_ et _main_ ont respectivement
les priorités 15 et 7. Le _thread_ _idle_ se voit donc attribuer la priorité la
plus faible possible : c'est donc pour cela qu'il n'obtient l'accès au CPU que
lorsque tous les autres _threads_ sont bloqués ou terminés.

Un autre aspect important des _threads_ dans RIOT est qu'ils disposent de leur
propre espace mémoire (i.e. leur propre _stack_, voir https://en.wikipedia.org/wiki/Stack-based_memory_allocation).
Cette propriété est très importante pour assurer un certain cloisonnement entre
les différents contextes d'exécution des _threads_ :
un _thread_ ne peut pas accéder directement (et donc modifier) une variable
locale d'un autre _thread_.

Concrètement, la manipulation des _threads_ dans RIOT s'effectue grâce à une API
très simple :

* Tout d'abord, dans le code, un _thread_ se présente sous la forme d'une
  fonction ayant la signature suivante:

  ```c
  void *thread_handler(void *arg);
  ```

  Le paramètre `arg` peut être utilisé pour partager du contexte entre le
  _thread_ créateur et le _thread_ créé.
* Ensuite, un _thread_ est créé à l'aide de la fonction `thread_create()`,
  définie dans l'entête `thread.h`:

  ```c
  kernel_pid_t pid;
  pid = thread_create(stack,  /* stack array pointer */
                      sizeof(stack), /* stack size */
                      THREAD_PRIORITY_MAIN - 1, /* thread priority*/
                      flag, /* thread configuration flag, usually*/
                      thread_handler, /* thread handler function */
                      NULL, /* argument of thread_handler function*/
                      "thread name");
  ```

  Dans l'exemple, ci-dessus, le _thread_ est initialisé avec l'espace mémoire
  disponible dans le tableau `stack`, avec une priorité de `6` (donc plus
  prioritaire que _main_), le pointeur vers la fonction à exécuter, pas de
  contexte et un nom.
  L'espace mémoire du _thread_ est déclaré globalement sous la forme d'un
  tableau statique:

  ```c
  static char stack[THREAD_STACKSIZE_MAIN];
  ```

L'API de gestion des _threads_ propose également la fonction `thread_getpid()`
qui renvoie le numéro du _thread_ courant.

Pour plus de détails, vous pouvez consulter la documentation en ligne des
[threads de RIOT](http://doc.riot-os.org/group__core__thread.html).
