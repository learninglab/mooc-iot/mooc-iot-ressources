# 3.3.1. Le noyau RIOT et son initialisation

Le noyau de RIOT contient toutes les fonctionnalités de base d'un système
d'exploitation :

* l'ordonnanceur
* la gestion du multi-tâches (ou _multi-thread_)
* la synchronisation entre _threads_
* la gestion des interruptions

Tout le code qui implémente les fonctionnalités du noyau se trouve dans le dossier
`core`. Ce code est indépendant du matériel : le principe est d'avoir un noyau
fonctionnant de la même manière, sur tous les types d'architectures supportés.

L'initialisation du noyau intervient donc après les initialisations essentielles
du matériel. La figure 1 présente la séquence d'initialisation complète d'une
application RIOT.

<figure style="text-align:center">
    <img src="Images/riot-boot.png" alt="" style="width:600px;"/><br/>
    <figcaption>Figure 1 : Séquence d'initialisation d'une application RIOT, &copy; A. Abadie</figcaption>
</figure>

Cette séquence d'initialisation intervient pendant l'exécution du point d'entrée
du CPU et est donc divisée en 2 phases :

1. L'initialisation du matériel dans la fonction `board_init()`. Cette
   fonction est généralement implémentée, pour chaque carte, dans le fichier
   `boards/<board name>/board.c`.
   La fonction `board_init()` se charge d'abord d'initialiser le cpu par un
   appel à `cpu_init()`: cette dernière initialise le cpu, ses horloges
   internes, les priorités entre interruptions. Ensuite, s'ils sont nécessaires
   pour l'application, les périphériques (UART, RTC, etc) sont initialisés.
2. Une fois le matériel prêt, le noyau peut donc être initialisé à son tour.
   L'implémentation de l'initialisation du noyau se trouve dans le fichier
   `core/kernel_init.c` et elle consiste simplement à créer 2 _threads_,
   _idle_ et _main_, et à exécuter le _thread_ _main_ qui sera donc le
   point de départ de l'application puisqu'il lance la fonction principale
   `main()` de l'application.

L'application elle-même ou des modules chargés dans l'application peuvent
également démarrer des _threads_. Pendant l'éxecution de l'application,
l'ordonnanceur du noyau se charge de basculer d'un _thread_ à l'autre en
fonction de son état et de sa priorité.
