# 3.3.4. La gestion d'énergie
[pm]: #La-gestion-dénergie

Les mécanismes de gestion d'énergie dans RIOT s'appuient sur la capacité des
microcontrôleurs de basculer dans des modes de fonctionnement à très basse
consommation.
Dans RIOT, le principe est de basculer automatiquement le microcontrôleur dans l'un de ces
modes lorsque toutes les tâches sont terminées ou bloquées en attente d'un
évènement extérieur.

Or nous avons vu plus tôt qu'une application RIOT démarrait par défaut avec 2
_threads_ : _main_ et _idle_. De plus, une application RIOT bascule dans le
_thread_ _idle_ lorsque toutes les tâches sont terminées ou bloquées.

_Note_: dans le cas des microcontrôleurs ARM Cortex-M, il n'y a plus de _thread idle_, c'est
donc le scheduler de RIOT qui se charge d'activer le mode de gestion d'énergie.

<figure style="text-align:center">
    <img src="Images/riot-application.png" alt="" style="width:300px"/>
    <figcaption>Fig. 1 : Structure d'une application RIOT, &copy; A. Abadie</figcaption>
</figure>

Dans RIOT, c'est donc le _thread_ _idle_ (ou le scheduler) qui a la charge de basculer le
microcontrôleur dans le mode de fonctionnement de plus basse consommation
possible. On parle dans ce cas d'endormir le microcontrôleur.

RIOT définit au maximum 4 niveaux de fonctionnement basse consommation, allant de 3 à
0. Cela dit, en fonction des architectures ou familles de microcontrôleurs, en
pratique il peut y en avoir moins.

Par exemple, pour les microcontrôleurs ST Microelectronics, RIOT définit 2 modes
de basse consommation alors que pour les Kinetis de NXP, il n'y en a qu'un seul.

Dans tous les cas, le mode de fonctionnement avec la plus faible consommation
est `0`.

Le basculement vers le mode de plus faible consommation possible se fait en
utilisant un algorithme de type cascade :

1. En partant du nombre de modes définis pour une architecture donnée (par
  exemple 2 pour STM32), RIOT regarde si le mode le plus élevé est débloqué.
2. Si ce mode est bloqué, il reste dans le mode courant (idle).
3. Si le mode est débloqué, il regarde si un mode inférieur est débloqué et ainsi
   de suite.

Cette méthode garantit que le système sélectionnera le mode de plus basse
consommation automatiquement.

Par défaut, aucun des modes de fonctionnement basse consommation n'est débloqué.

Le déblocage des modes de basse consommation à utiliser est laissé au choix
du développeur de l'application car en fonction du mode, certaines
fonctionnalités ne seront pas disponibles (comme la rétention de la RAM par
exemple) ou certains périphériques seront désactivés.
La sélection se fait en définissant la constante `PM_BLOCKER_INITIAL` lors de la
compilation, via la variable `CFLAGS`.
La constante `PM_BLOCKER_INITIAL` doit contenir une valeur sur 32bits exprimée
en représentation hexadécimal. Chacun des 4 octets de cette valeur correspond à
l'état d'un des 4 modes de consommation: `0x<mode 3><mode 2><mode 1><mode 0>`
Pour chaque mode, si la valeur est `0x00`, le mode est débloqué et si la valeur
est `0x01`, le mode est bloqué.

Par exemple, pour débloquer tous les modes sauf le mode 0 par défaut, il
suffira d'ajouter au Makefile d'une application :

```mk
CFLAGS += '-DPM_BLOCKER_INITIAL=0x00000001'
```

Il est également possible de bloquer/débloquer manuellement un mode pendant
l'exécution d'une application à l'aide des fonctions `pm_block` et
`pm_unblock`. Ces fonctions sont définies dans l'entête `pm_layered.h`.

Les fonctions de gestion de l'énergie sont brièvement documentées
[ici](http://doc.riot-os.org/group__sys__pm__layered.html).
