# 3.3.3. Advanced use of threads

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.3.3a. Managing threads concurrency](#333a-managing-threads-concurrency)
- [3.3.3b. Communication between threads](#333b-communication-between-threads)
<!-- TOC END -->

## 3.3.3a. Managing threads concurrency

As we saw in the previous section, _threads_ in RIOT
have their own memory stack. It can happen, however,
that _threads_ share variables, via the context variable `arg`
of the `thread_create()` function, for example, or that they access global
variables from the application or even from the system itself.

To avoid simultaneous access issues related to these use cases, the RIOT
kernel provides two mechanisms to synchronize _threads_:

1. **Mutex** can be used to handle mutual exclusion problems such
   as access to variables being locked by the active _thread_. No other
   _thread_ will be able to access this variable until it is
   released. The _mutex_ API is defined in the file `mutex.h` and it is very
   easy to use:

   ```c
   mutex_t lock;
   mutex_lock(&lock);
   /* portion of code protected by the mutex */
   mutex_unlock(&lock);
   ```

2. Semaphores can be used to manage more complex synchronization problems than is possible with
   _mutex_, such as "meeting" problems, for example, or "producer-consumer"
   problems. ( [_The Little Book of Semaphores_](http://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf), Allen B. Downey)
   The API for using semaphores in RIOT is defined in the file
   `semaphores.h`.

Pour more détails, please read the [mutex online documentation](http://doc.riot-os.org/group__core__sync__mutex.html).

## 3.3.3b. Communication between threads

_Threads_ in RIOT are also able to communicate with each other within
the same application. This is generally referred to as inter-process
communication or IPC.

IPC messages can either be exchanged between 2 _threads_, or between an
interrupt context (via the interrupt subroutine, ISR) and a _thread_.

IPC can have 2 modes:

* **synchronous**: in this mode, the _thread_ that sent the message will be locked
  until the recipient _thread_ has received the message.
* **asynchronous**: in this mode, there is no blockage when messages are sent. This is
  possible through the use of a message queue in which the message
  is posted. The message will be processed once the recipient is ready.
  **Messages are always sent asynchronously from interrupt subroutines.**
  In such cases, you must make sure you initialize a message queue in
  the receiving _thread_.


The API for the IPC is defined in the header `msg.h` (which is found in `core`).
Please also read the [Messaging/IPC API online documentation](http://doc.riot-os.org/group__core__msg.html).
The type for a variable containing a message is `msg_t` and has 2 attributes:

* `type`, which can be used to identify the type of message and to process it
  differently according to the type of message
* `content` - which is a _union_ - may contain either a full 32 bits unsigned
  integer (`unit32_t`) or the address of a variable (i.e. a
  pointer).

The code below shows how to define and fill in a message:

```c
msg_t msg;
msg.type = MSG_TYPE;
msg.content.value = 42; /* content can be a value */
msg.content.ptr = address; /* or content can be a pointer */
```

In this case, the macro `MSG_TYPE` would be defined globally in a header
file:

```c
#define MSG_TYPE  (1234)
```

Messages can be sent using several functions, each with a different behaviour:

* A blocking delivery (synchronous) for a _thread_ with a given _pid_ number can be
  made using the following call:

  ```c
  msg_send(&msg, pid);
  ```

  **Comment:** if this call is made from an interrupt subroutine,
  then delivery will be non-blocking (asynchronous).

* A non-blocking delivery can be made using the call:

  ```c
  msg_try_send(&msg, pid);
  ```

* It is also possible to send a message and to block the sending _thread_
  until a response is received. In such cases, you must use the call:

  ```c
  msg_send_receive(&msg, &msg_reply, pid);
  ```

  The parameter `msg_reply` will contain the reply from the _thread_ to which
  the initial delivery was sent.

  A response can be sent from the recipient using the call:

  ```c
  msg_reply(&msg, &msg_reply);
  ```

The RIOT kernel has 2 functions for receiving messages from _threads_:

* a call to `msg_receive` will block the current _thread_ until a message is received:

  ```c
  msg_receive(&msg);
  ```

* a call to `msg_try_receive` will verify whether or not a message has been received, and, if so
  will return `1`.

  ```c
  int ret = msg_try_receive(&msg);
  if (ret > 0) {
      /* message received */
  }
  else {
      /* no message received */
  }
  ```

Receiving functions are typically used in an endless loop to wait for
messages from another _thread_ or from an interrupt subroutine:

```c
static void *thread_handler(void *arg)
{
    /* endless loop */
    while (1) {
        msg_t msg;
        msg_receive(&msg);
        printf("Message received: %s\n", (char *)msg.content.ptr);
    }
    return NULL;
}
```

When messages are being sent from an interrupt context,
these messages will be sent asynchronously, meaning you must initialize a message
queue at the start of the _thread_.

```c
static void *thread_handler(void *arg)
{
    msg_t msg_queue[8];
    msg_init_queue(msg_queue, 8);

    while (1) {
        /* Wait for messages and process them */
    }

    return NULL;
}
```
