# Question 3.3.3

Quels mécanismes de RIOT permettent la synchronisation entre threads:

( ) les GPIOs
(x) les _mutex_
(x) les _sémaphores_
( ) les pointeurs
(x) les _messages IPC_
( ) les interruptions

[explanation]
RIOT fournit 3 mecanismes permettant de synchroniser des _threads_: les mutex,
les sémaphores et les messages IPC bloquants.
[explanation]

# Question 3.3.4

Quelle fonction de l'API de communication inter-processus faut-il utiliser pour
bloquer un _thread_ en attente d'un message ?

( ) `msg_try_send`
(x) `msg_receive`
( ) `msg_try_receive`

[explanation]
`msg_try_send` sert à envoyer de façon non bloquante un message à un autre
_thread_. `msg_try_receive` essaie de recevoir un message s'il y en a un mais ne
bloque pas. `msg_receive` bloque le thread jusqu'à ce message soit reçu.
[explanation]
