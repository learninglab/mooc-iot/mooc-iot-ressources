# Question 3.3.1

Combien y a-t-il de niveaux de priorité différents dans le noyau de RIOT ?
( ) 1
( ) 7
(x) 16
[explanation]
Il y a 16 niveaux de priorité différents dans RIOT.
[explanation]

# Question 3.3.2

En considérant 2 _threads_ `thread_1` et `thread_2` ayant respectivement les
niveaux de priorité 7 et 10, lequel sera prioritaire ?

(x) `thread_1`
( ) `thread_2`

[explanation]
C'est `thread_1` qui sera prioritaire car plus le niveau est faible plus la priorité est haute !
[explanation]
