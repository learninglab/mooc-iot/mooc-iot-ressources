# 3.3.1. The kernel and how it is used

RIOT's kernel contains all core features of an operating system:

* the scheduler
* multi-task (or _multithreaded_) management
* synchronization between _threads_
* interrupt management

All the source code implementing the kernel's features can be found in the `core`
directory. This code is independent of the hardware: the concept is to have a kernel
that operates in the same way on all types of supported architectures.

What this means is that the kernel is initialized after the other hardware essential initializations have been
carried out. Figure 1 outlines the full initialization sequence for a
RIOT application.

<figure style="text-align:center">
    <img src="Images/riot-boot.png" alt="" style="width:600px;"/><br/>
    <figcaption>Figure 1: The initialization sequence for a RIOT, &copy; A. Abadie</figcaption>
</figure>

This initialization sequence takes place during the execution of the entry point
for the CPU, and is therefore divided into 2 phases:

1. The hardware initialization in the `board_init()` function. This
   function is generally implemented, for each board, in the file
   `boards/<board name>/board.c`.
   The `board_init()` function first initializes the CPU by making a
   call to `cpu_init()`: this will initialize the CPU, its internal
   clocks and priorities between interrupts. Next, if they are required
   for the application, hardware peripherals (UARTs, RTCs, etc.) are also initialized.
2. Once the hardware is ready, the kernel itself can be initialized.
   The implementation of the kernel initialization can be found in the file
   `core/kernel_init.c`, and involves simply creating 2 _threads_,
   _idle_ and _main_, and to execute the _main_ _thread_, which will be the
   starting point for the application, by running the `main()` function.

The application itself, and even the modules loaded in the application, can
also create _threads_. While the application is being executed,
the kernel scheduler will switch between _threads_
depending on their priority levels.
