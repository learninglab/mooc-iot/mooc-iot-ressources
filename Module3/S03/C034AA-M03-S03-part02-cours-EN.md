# 3.3.2. Scheduling and Threads
[thread-scheduling]: #thread-scheduling

_Threads_ are scheduled in the RIOT kernel preemptively, using
a priority system. This is what is called `static` scheduling, where:

* the unblocked and non terminated _thread_ with the highest priority is active, i.e. it will be
  executed on the CPU.
* any _thread_ in the process of being executed can be interrupted by another
  _thread_ with higher priority
* in case of inactivity, i.e. when all _threads_ are either blocked or terminated,
  the system will automatically switch to a specific _thread_, the so-called
  _idle_ _thread_
* an interrupt can preempt any _thread_ at any
  time to execute the interrupt subroutine (_ISR_).

Scheduling uses a _tick-less_ policy, i.e. active
_threads_ are not selected through periodic checks on the status of _threads_
but through incidents (typically internal interrupts).
The time taken to switch between _threads_ will remain constant, irrespective of the number
of _threads_, meaning the algorithmic complexity of the RIOTs scheduler is _O(1)_.

The RIOT kernel supports 16 different priority levels, with values ranging
from 0 to 15. The lower the priority value, the higher the priority.
The default _threads_ for an application, _idle_ and _main_, have the priorities
15 and 7 respectively. The _idle_ _thread_ is allocated the lowest
priority possible, meaning it will only be granted access to the CPU
once all other _threads_ are either blocked or terminated.

Another important aspect of _threads_ in RIOT is that they have their
own memory stack (see https://en.wikipedia.org/wiki/Stack-based_memory_allocation).
This is a very important property in that it ensures
a degree of isolation between the different execution contexts for the _threads_:
a _thread_ may not directly access (and, therefore, modify) a local
variable of another _thread_.

_Threads_ in RIOT are used through an extremely simple API:

* To begin with, in the code, a _thread_ is presented in the form of a
  function with the following signature:

  ```c
  static void *thread_handler(void *arg);
  ```

  The parameter `arg` may be used to share the context between the
  creator _thread_ and the created _thread_.
* Next, a _thread_ is created using the `thread_create()` function,
  defined in the header `thread.h`:

  ```c
  kernel_pid_t pid;
  pid = thread_create(stack,  /* stack array pointer */
                      sizeof(stack), /* stack size */
                      THREAD_PRIORITY_MAIN - 1, /* thread priority*/
                      flag, /* thread configuration flag, usually*/
                      thread_handler, /* thread handler function */
                      NULL, /* argument of thread_handler function*/
                      "thread name");
  ```

  In the above example, the thread is initialized with the memory space
  available in the `stack` array, with a priority of 6 (making it higher
  priority than _main_), the pointer towards the function to be executed, no
  context and a name.
  The memory stack of the _thread_ is declared globally in the form of a
  static array:

  ```c
  static char stack[THREAD_STACKSIZE_MAIN];
  ```

The _thread_ management API also provides the `thread_getpid()` function,
which returns the process identifier of the current _thread_.

For more details, please read the [RIOT threads documentation](http://doc.riot-os.org/group__core__thread.html).
