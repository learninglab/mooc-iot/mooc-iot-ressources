# 3.1.0. Software solutions for programming connected objects


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>During this sequence, you will discover what is involved in programming a connected object and the software solutions used to write IoT applications.
</p>
</div>

References:

- https://hal.inria.fr/hal-01245551/document

- https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8308310&tag=1

- https://www.osrtos.com/
