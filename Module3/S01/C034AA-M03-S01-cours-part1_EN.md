# 3.1.1. Programming a connected object

A microprogram (*firmware*) is a program for a microcontroller which contains the application code, the *bootloader* code, the code needed to interface with the hardware, the code for the *kernel*, and the code for the libraries used. If we were to make an analogy with computers, installing a microprogram is like wiping a hard-drive, flashing the BIOS and reinstalling both the operating system and the application.

Generally speaking, microcontrollers are no longer programmed in assembly language but in a higher level language (C, C++, etc.). This source code is compiled in machine language, and must then be sent to the microcontroller (*flashed*) by a programmer (e.g. OpenOCD for the ARM and MIPS architectures and AVRDUDE for AVR Microchip (formerly Atmel) microcontrollers).

<figure style="text-align:center">
    <img src="Images/firmware-workflow_EN.png" width="500px"/><br/>
    <figcaption>Fig. 1: The source code is compiled and then flashed</figcaption>
</figure>

In order to write a microprogram, you will need the code for the application, the code for managing communications and the code for the input/output devices used (displays, motors, sensors, etc.). You will also need to adopt a strategy for managing memory, optimise energy consumption, guarantee its security and support a wide range of hardware targets.

As you will understand, the wide variety of connected objects, material constraints, the need for interoperability and security are all reasons for using an operating system as opposed to starting from scratch.
