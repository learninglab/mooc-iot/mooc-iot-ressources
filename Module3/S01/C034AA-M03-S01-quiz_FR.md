# Question 3.1.1


Que contient un microprogramme ?
[X] Le code d'amorçage
[X] Le code des pilotes des différents périphériques
[X] Le code des différentes librairies utilisées
[X] Le code noyau
[X] Le code de l'application
[X] Le code spécifique à la plateforme
[explanation]
Tout ce code est nécessaire à l'exécution d'une application sur un microcontrôleur.
[explanation]

#Question 3.1.2

Quelle est l'empreinte minimale en RAM que l'on peut attendre d'un OS ?
(x) moins de 5KO
( ) entre 5KO et  10KO
( ) entre 10KO et 20KO
( ) plus de 20KO
[explanation]
Des OS comme FreeRTOS ou RIOT peuvent avoir des empreintes inférieures à 5KO en RAM.
[explanation]

#Question 3.1.3

Quelle est l'empreinte minimale en ROM que l'on peut attendre d'un OS ?
( ) moins de 2KO
(x) entre 2KO et 20KO
( ) entre 10KO et  20KO
( ) plus de 20 KO
[explanation]
FreeRTOS, ARM Mbed ou RIOT peuvent prendre moins de 20KO en ROM.
[explanation]

# Question 3.1.4

Quelles licences donnent la liberté d'utiliser le logiciel et le devoir de faire bénéficier la communauté des versions modifiées ?
( ) MIT
( ) Apache
(x) GPL
[explanation]
Seule GPL est une licence dite "copyleft" qui donne la liberté d'utiliser le logiciel et le devoir de faire bénéficier la communauté des versions modifiées.
[explanation]






