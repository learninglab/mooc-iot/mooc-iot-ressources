# 3.1.2. Qu'est-ce qu'un système d'exploitation (OS) et comment le choisir ?

## Sommaire

<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.1.2a Principales caractéristiques des systèmes d'exploitation](#312a-principales-caractéristiques-des-systèmes-dexploitation)
- [3.1.2b Points-clés de conception](#312b-points-clés-de-conception)
<!-- TOC END -->

 On verra dans cette partie quelles sont les caractéristiques attendues d'un système d'exploitation et les différents points-clés de conception qui aideront à choisir le plus adéquat.

## 3.1.2a Principales caractéristiques des systèmes d'exploitation

### Gestion de la mémoire
La place prise par l'OS en mémoire vive (RAM) et en stockage (ROM) est un facteur important vu les limitations des objets connectés que l'on veut adresser.

### Support matériel
Le parc des objets connectés est très hétérogène. Supporter les différents types de microcontrôleurs, les différents périphériques et autres capteurs peut rapidement devenir un casse-tête. Ainsi la capacité de l'OS à fournir une abstraction matérielle est un enjeu important.

### Connectivité réseau
Avec les contraintes de la communication sans-fil (e.g. interférences radio) et la gestion de la consommation l'OS doit proposer différentes piles de protocoles réseaux
en fonction des usages mais aussi être en capacité de facilement en intégrer de nouvelles.

### Efficacité énergétique
Certaines applications nécessitent que l'objet puisse fonctionner plusieurs années sur batterie sans être rechargé. L'OS doit donc à la fois fournir aux développeurs des moyens de diminuer la consommation de leurs applications mais également être lui-même le moins énergivore possible.

### Capacités temps réel
Un OS est dit temps réel (Real-Time Operating System ou RTOS) s'il garantit un traitement des entrées en un temps fixé. Cette caractéristique est nécessaire pour des systèmes embarqués avec des contraintes de temps de réponse.

### Sécurité
Protéger les données et se protéger des attaques venant de l'extérieur est un enjeu majeur pour les objets connectés. L'OS doit donc fournir les mécanismes nécessaires pour développer des applications sûres (librairies de cryptographie et protocoles de sécurité) et pour qu'elles le restent dans le temps (par exemple avec la possibilité de mise à jour à distance).

## 3.1.2b Points-clés de conception

###  Noyau
Le noyau du système d'exploitation (kernel) assure le chargement et l'exécution des processus et fournit les mécanismes d'abstraction du matériel. Il existe différentes approches:

* l'approche microkernel (μ-kernel) plus robuste et plus flexible
* l'approche monolithique moins complexe et plus efficace
* les approches hybrides qui se situent entre les deux approches précédentes

### Ordonnanceur
L'ordonnanceur va affecter les performances énergétiques, les capacités temps réel et la façon de programmer. Un OS peut proposer plusieurs types d'ordonnanceurs mais un seul est utilisé à l'exécution:

* un ordonnanceur préemptif assigne du temps CPU à chaque tâche
* un ordonnanceur coopératif donne quant à lui la responsabilité à chaque tâche de "rendre la main"

### Allocation mémoire
La mémoire des objets connectés étant limitée, sa gestion est très importante:

* l'allocation statique requiert de connaître à l'avance la quantité de mémoire nécessaire
* l'allocation dynamique fragmente la mémoire et nécessite de gérer les scénarios où il n'y a plus de mémoire disponible

### Gestion des tampons réseaux
La mémoire destinée à la gestion des paquets dans la pile de protocoles réseau peut être:

* soit allouée par chaque couche (ce qui consomme plus de mémoire)
* soit passée en référence (ce qui est plus complexe à gérer)

### Modèle de programmation
L'OS peut être :

* soit *multithread* auquel cas chaque tâche a son propre contexte
* soit orienté événement : chaque tâche est alors déclenchée par un événement externe comme une interruption

### Langages de programmation
Les langages propriétaires peuvent être plus adaptés au développement pour les objets connectés et donc être plus sûrs et performants tandis que les langages standards (comme le C ou le C++) facilitent la portabilité et permettent d'utiliser des outils de débogage standards.

### Fonctionnalités
Les fonctionnalités des OS sont fournies d'une part par le noyau (ordonnanceur, mécanismes de synchronisation,...) et par différentes librairies (réseau, sécurité, ...).

### Tests
Comme pour tous les logiciels, les tests jouent un rôle crucial pour garantir la stabilité de l'OS.

### Certification
Pour des applications critiques, les certifications peuvent être obligatoires. Elles sont toutefois coûteuses à mettre en œuvre car elles doivent être réalisées par un organisme tiers et tous les projets ne peuvent donc pas faire certifier leurs versions.

### Documentation
Une documentation complète, à jour et facilement compréhensible est d'autant plus importante pour un OS qu'elle est le moyen de base pour écrire l'application.

### Maturité du code
La maturité d'un code est difficile à estimer. Elle dépend, entre autres, de l'âge du projet, du nombre de contributeurs et d'utilisateurs.

### Licence
On peut regrouper les licences en trois catégories:

* Les licences payantes qui limitent l'accès au code source et donc au nombre de contributeurs.
* Les licences dites *copyleft* (comme GPL) qui donnent l'obligation au développeur de faire bénéficier la communauté des versions modifiées. Ces licences sont généralement moins acceptées par les industriels.
* Les licences permissives (BSD, MIT, Apache...) qui donnent accès au code source et laissent aux utilisateurs plus de liberté, ce qui peut amener à une fragmentation de la communauté.

### Support
La communauté des OS *open source* fournit un support avec une qualité de service du meilleur possible (best effort) via les forums, les listes de diffusion ou les outils de suivi de bogues. Certains OS comme ARM Mbed proposent un support commercial qui assure un délai de réponse maximum (ex: 2 jours ouvrables) aux questions ou aux corrections de bogues.
