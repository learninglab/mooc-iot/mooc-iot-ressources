
# 3.1.4. Higher level solutions

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.1.4a C++](#314a-c)
- [3.1.4b Rust](#314b-rust)
- [3.1.4c Java](#314c-java)
- [3.1.4d Interpreted languages](#314d-interpreted-languages)
<!-- TOC END -->

Although it is necessary to develop in C with many operating systems, there are other solutions, which we will take a look at here.

## 3.1.4a C++

C++ has endured a bad reputation of late, but the majority of functions have no impact on either size or speed. As is the case with C, it is necessary to understand what happens at machine code level in order to use C++ effectively within an embedded system.

## 3.1.4b Rust

Rust, a language developed by Mozilla, is also used to develop microprograms. It was designed to be concurrent and safe, and is interoperable with code written in C. The Rust compiler supports the following architectures: ARM, MIPS and RISC-V.

## 3.1.4c Java

Some Java virtual machines can be used to execute Java bytecode on microcontrollers.

## 3.1.4d Interpreted languages

C is more difficult to use than interpreted languages such as Python or Javascript. It is for this reason that there are interpreters for IoT-adapted versions of these languages (Micropython and Jerryscript). As is often the case, there is no ideal solution, and gains in terms of productivity and accessibility may be offset by losses in terms of performance and memory.  


There are many operating systems and languages used for the development of connected objects. We will take a closer look at the RIOT operating system in the next sequence, both because it is supported by the FIT/IoT-LAB platform and because authors of this MOOC are contributors of this project.
