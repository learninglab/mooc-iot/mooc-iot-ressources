# 3.1.2. What is an operating system (OS) and how to choose the right one?

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.1.2a The main characteristics of operating systems](#312a-the-main-characteristics-of-operating-systems)
- [3.1.2b Key design points](#312b-key-design-points)
<!-- TOC END -->

This section outlines the characteristics you would expect to find in an operating system and the key design points that will help you choose the right one.

## 3.1.2a The main characteristics of operating systems

### Memory management
The amount of space taken up by the OS in terms of random-access memory (RAM) and storage (ROM) is an important factor, given the limitations of the connected objects that we will be dealing with.

### Hardware support
Connected objects come in all shapes and sizes. Finding a way of supporting the different types of microcontrollers, the different hardware devices and other sensors can quickly become overwhelming, making the OS’s capacity to provide hardware abstraction an important consideration.

### Network connectivity
Given the constraints of wireless communication (e.g. radio interference) and managing power consumption, operating systems must be capable of offering different network protocol stacks
for different uses, in addition to being able to easily integrate new ones.

### Efficient power use
Some applications require an object to be able to run off a battery for multiple years without being recharged. The OS must therefore provide developers with ways of limiting the power consumption of their applications, while also using as little power as possible.

### Real-time capabilities
An OS is said to be ‘real-time’ (a Real-Time Operating System or RTOS) if it is capable of processing inputs in a fixed time. This characteristic is needed for embedded systems with constraints in terms of response time.

### Security
Protecting data and guarding against external attacks is a major challenge facing connected objects. The OS must therefore provide the mechanisms needed to develop secure applications (cryptography libraries and security protocols) and to ensure that they remain secure in the long run (e.g. using remote updates).

## 3.1.2b Key design points

### Kernel
The kernel of the operating system is responsible for loading and executing processes, in addition to providing the hardware abstraction mechanisms. There are a number of different approaches:

* the microkernel approach (μ-kernel), which is more robust and more flexible
* the monolithic approach, which is less complex and more efficient
* hybrid approaches, which sit somewhere between the two previous approaches

### Schedulers
Schedulers have an impact on power consumption, real-time capabilities and programming methods. Operating systems can offer several types of schedulers, but only one is used for execution:

* preemptive schedulers assign CPU time to each task
* cooperative schedulers give each task the responsibility of “returning control”

### Memory allocation
Given the limited memory of connected objects, how this memory is managed is extremely important:

* static allocation requires knowing the quantity of memory needed in advance
* dynamic allocation fragments memory, resulting in scenarios where there is no more memory available

### Managing network buffers
The memory for managing packets in the network protocol stack may be:

* allocated per layer (consuming more power)
* changed to reference (more complex)

### Programming models
Operating systems can either be:

* *multithreaded*, where each task has its own context
* event-driven: each task is triggered by an external event, such as an interrupt

### Programming languages
Proprietary languages may be better suited to developing connected objects, making them more secure and efficient; while standard languages (such as C or C++) facilitate portability, enabling standard debugging tools to be used.

### Functions
OS functions are provided by both the kernel (the scheduler, synchronization mechanisms, etc.) and by different libraries (network, security, etc.).

### Tests
As is the case with all software, tests play a crucial role when it comes to ensuring the stability of the OS.

### Certification
For some critical applications, certification may be required. This is costly to implement, however, given that it must be performed by a third-party body; as a result, not all projects are able to certify their versions.

### Documentation
Full, up-to-date and easily understandable documentation is essential for an OS in that this is the basic method for writing an application.

### Code maturity
Code maturity is difficult to estimate. It depends on the age of the project, the number of contributors and the number of users, among other factors.

### Licensing
Licenses can be divided into three categories:

* Proprietary licenses, which limit access to the source code and, therefore, to the number of contributors.
* *Copyleft* licenses (such as GPL), which force developers to share modified versions with the rest of the community. These licenses are generally less popular among manufacturers.
* Permissive licenses (BSD, MIT, Apache, etc.) grant access to the source code, giving users more freedom, but this can result in the community becoming fragmented.

### Support
The *open source* OS community provides support with the best possible quality of service (best effort) via forums, mailing lists and bug tracking tools. Some operating systems, such as ARM Mbed, offer commercial support, ensuring maximum response delay to questions (eg. two working days) or for bug correction.
