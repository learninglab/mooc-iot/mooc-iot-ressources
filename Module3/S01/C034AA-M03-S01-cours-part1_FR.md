# 3.1.1. Principe de programmation d'un objet connecté

Un microprogramme (*firmware*), aussi appelé micrologiciel, est un programme pour microcontrôleur qui contient à la fois du code applicatif mais également du code d'amorçage (*bootloader*), du code nécessaire pour faire l'interface avec le matériel, du code du noyau (*kernel*), du code des librairies utilisées... Pour faire une analogie avec les ordinateurs, installer un microprogramme c'est comme effacer tout le disque dur, flasher le BIOS et réinstaller à la fois le système d'exploitation et l'application.

On ne programme généralement plus les microcontrôleurs en assembleur mais dans un langage de plus haut niveau (C, C++, ...). Ce code source est compilé en langage machine et doit être ensuite envoyé au microcontrôleur (*flashé*) par un programmateur (e.g. OpenOCD pour les architectures ARM et MIPS et AVRDUDE pour les microcontrôleurs AVR de Microchip (anciennement Atmel)).

<figure style="text-align:center">
    <img src="Images/firmware-workflow_FR.png" width="500px"/><br/>
    <figcaption>Fig. 1 : Le code source est compilé puis flashé</figcaption>
</figure>

Pour écrire un microprogramme, il va donc falloir du code pour l'application, du code pour gérer les communications, du code pour les périphériques d'entrée/sorties utilisés (afficheurs, moteurs, capteurs, ...) mais aussi adopter une stratégie pour gérer la mémoire, optimiser la consommation électrique et veiller à la sécurité, supporter un grand nombre de cibles matérielles.

On le comprend, le caractère hétérogène du parc des objets connectés, les contraintes matérielles, le besoin d'interopérabilité, la sécurité sont autant de raisons qui conduisent à utiliser un système d'exploitation (en anglais Operating System ou OS) plutôt que de partir d'une feuille blanche.
