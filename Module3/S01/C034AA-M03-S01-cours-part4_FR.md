# 3.1.4. Les solutions de plus haut niveau

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.1.4a C++](#314a-c)
- [3.1.4b Rust](#314b-rust)
- [3.1.4c Java](#314c-java)
- [3.1.4d Langages interprétés](#314d-langages-interprétés)
<!-- TOC END -->


Si une grande partie des OS nécessite de développer en C, il existe d'autres solutions que nous présentons ici.

## 3.1.4a C++

Le C++ a longtemps eu mauvaise réputation mais la plupart des fonctionnalités n'ont d'impact ni sur la taille, ni sur la vitesse. Comme pour le C, il est nécessaire de comprendre ce qu'il se passe au niveau du code machine pour utiliser efficacement le C++ dans un système embarqué.

## 3.1.4b Rust

Le langage Rust, développé par Mozilla, est également utilisé pour développer des microprogrammes. Il a été développé pour être sûr et concurrent et il est interopérable avec du code écrit en C. Le compilateur Rust supporte les architectures ARM, MIPS et RISC-V.

## 3.1.4c Java

Certaines machines virtuelles Java permettent d'exécuter du bytecode Java sur des microcontrôleurs.

## 3.1.4d Langages interprétés

L'utilisation du langage C est plus difficile que des langages interprétés comme Python ou Javascript. C'est pourquoi on trouve des interpréteurs pour des versions adaptées à l'IoT de ces langages (Micropython et Jerryscript). Comme souvent, il n'y a pas de solution idéale et ce qu'on gagne en productivité et en accessibilité peut être perdu en performance et mémoire.  


Les langages et OS permettant le développement d'objets connectés sont donc nombreux. Nous allons dans la prochaine séquence porter notre attention plus particulièrement sur le système d'exploitation RIOT, d'une part car il est supporté par la plateforme FIT/IoT-LAB, et d'autre part car des auteurs de ce MOOC contribuent à son développement.
