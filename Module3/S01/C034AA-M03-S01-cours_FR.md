# 3.1.0. Introduction


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Au cours des différentes parties de cette séquence, vous allez découvrir les principes de programmation d'un objet connecté et les solutions logicielles pour écrire une application IoT.
</p>
</div>

Présentation en vidéo des spécificités de programmation d'une application pour objets connectés compte-tenu de leurs contraintes de mémoire, de performance, de sécurité et de consommation d'énergie.

## Références:

* https://hal.inria.fr/hal-01245551/document
* https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8308310&tag=1
* https://www.osrtos.com/
