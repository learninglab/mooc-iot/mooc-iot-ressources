# 3.1.3. The main existing operating systems

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.1.3a A history of operating systems used in the IoT](#313a-a-history-of-operating-systems-used-in-the-iot)
- [3.1.3b RIOT](#313b-riot)
- [3.1.3c Zephyr](#313c-zephyr)
- [3.1.3d Contiki-NG](#313d-contiki-ng)
- [3.1.3e ARM Mbed](#313e-arm-mbed)
- [3.1.3f FreeRTOS](#313f-freertos)
<!-- TOC END -->


## 3.1.3a A history of operating systems used in the IoT

<figure style="text-align:center">
    <img src="Images/os-timeline.png" width="500"/><br/>
    <figcaption>Fig. 1: Start dates of the main IoT OS.</figcaption>
</figure>

## 3.1.3b RIOT

<img src="Images/logo-riot.png" width="200px"/><br/>

- License: LGPL v2.1
- Supported architectures: AVR, MSP430, ESP8266, ESP32, MIPS, RISC-V, ARM7, ARM Cortex-M, ...
- Main contributors:  Freie Universität Berlin, Inria, Hamburg University of Applied Sciences
- Main characteristics:
  - RTOS
  - Networks: Bluetooth LE, WPAN, LPWAN, Wi-Fi/Ethernet, UWB, ...
  - Minimum memory footprint: ~ 1.5kB RAM, ~ 5kB ROM (1)
- First version: 2013
- Number of contributors over the last 12 months: 40 (2)  

## 3.1.3c Zephyr

<img src="Images/logo-zephyr.png" width="200px"/><br/>

- License: Apache 2.0
- Supported architectures: Cortex-M, Intel x86, ARC, Nios II, Tensilica Xtensa, RISC-V, ...
- Main contributors: Linux Foundation Project (Intel, NXP, Oticon, Nordic Semiconductor, ST, Linaro)
- Main characteristics:
  - RTOS
  - Networks: Bluetooth LE, WPAN, Wi-Fi/Ethernet, ...
  - Minimum memory footprint: "fitting in devices with at least 16k RAM" (3)
- First version: 2015
- Number of contributors over the last 12 months: 70 (2)


## 3.1.3d Contiki-NG

<img src="Images/logo-contiki.png" width="200px"/><br/>

- License: BSD 3
- Supported architectures: ARM, AVR, x86, MSP430, PIC32, ...
- Main contributors: Atmel, Cisco, ETH, Redwire LLC, SAP, Thingsquare
- Main characteristics:
  - Protothreads
  - Networks: Bluetooth LE, WPAN, WiFi/Ethernet, ...
  - Minimum memory footprint: 10 KB of RAM, 100 KB of ROM (4)
- First version: 2003
- Number of contributors over the last 12 months: 10 (2)  

## 3.1.3e ARM Mbed

<img src="Images/logo-arm-mbed.png" width="200px"/><br/>

- License: Apache 2.0
- Supported architectures: ARM Cortex-M
- Main contributors: ARM
- Main characteristics:
  - RTOS
  - Networks: Bluetooth LE, WPAN, LPWAN, WiFi/Ethernet, ...
  - Minimum memory: 8 KB of RAM, 14 KB of ROM (5)
- First version: 2009
- Number of contributors over the last 12 months: 25 (2)  


## 3.1.3f FreeRTOS


<img src="Images/logo-freertos.jpg" width="200px"/><br/>

- License: MIT
- Supported architectures: AVR, MSP430, ARM Cortex-M, ARM7, x86, RISC-V, ...
- Main contributors: Amazon
- Main characteristics:
  - RTOS
  - Networks: Bluetooth LE, WPAN, Wi-Fi/Ethernet, ...
  - Minimum memory: 4 KB of RAM, 9 KB of ROM (6)
- First version: 2002
- Number of contributors over the last 12 months: 41 (2)

1: http://riot-os.org/
2: https://www.openhub.net, http://github.com
3: https://docs.zephyrproject.org/latest/introduction/index.html
4: https://github.com/contiki-ng/contiki-ng/wiki
5: https://os.mbed.com/blog/entry/Optimizing-memory-usage-in-mbed-OS-52/
6: https://www.freertos.org/index.html
