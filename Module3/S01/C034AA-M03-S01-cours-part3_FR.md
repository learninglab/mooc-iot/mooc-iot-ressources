# 3.1.3. Les Principaux OS existants

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.1.3a Historique des OS pour l'IoT](#313a-historique-des-os-pour-liot)
- [3.1.3b RIOT](#313b-riot)
- [3.1.3c Zephyr](#313c-zephyr)
- [3.1.3d Contiki-NG](#313d-contiki-ng)
- [3.1.3e ARM Mbed](#313e-arm-mbed)
- [3.1.3f FreeRTOS](#313f-freertos)
<!-- TOC END -->

## 3.1.3a Historique des OS pour l'IoT

<figure style="text-align:center">
    <img src="Images/os-timeline.png" width="500"/><br/>
    <figcaption>Fig. 1 : Dates de début des principaux OS pour l'IoT</figcaption>
</figure>

## 3.1.3b RIOT

<img src="Images/logo-riot.png" width="200px"/><br/>

- Licence: LGPL v2.1
- Architectures supportées: AVR, MSP430, ESP8266, ESP32, MIPS, RISC-V, ARM7, ARM Cortex-M, ...
- Principaux contributeurs:  Freie Universität Berlin, Inria, Hamburg University of Applied Sciences
- Principales caractéristiques:
  - RTOS
  - Réseaux: Bluetooth LE, WPAN, LPWAN, Wi-Fi/Ethernet, UWB, ...
  - Empreinte mémoire: ~ 1.5ko RAM, ~ 5ko ROM (1)
- Première version: 2013
- Nombre de contributeurs les 12 derniers mois: 40 (2)  

## 3.1.3c Zephyr

<img src="Images/logo-zephyr.png" width="200px"/><br/>

- Licence: Apache 2.0
- Architectures supportées: Cortex-M, Intel x86, ARC, Nios II, Tensilica Xtensa, RISC-V, ...
- Principaux contributeurs: Linux Foundation Project (Intel, NXP, Oticon, Nordic Semiconductor, ST, Linaro)
- Principales caractéristiques:
  - RTOS
  - Réseaux: Bluetooth LE, WPAN, WiFi/Ethernet, ...
  - Empreinte mémoire: "pour des objets d'au moins 16ko de RAM" (3)
- Première version: 2015
- Nombre de contributeurs les 12 derniers mois: 70 (2)  


## 3.1.3d Contiki-NG

<img src="Images/logo-contiki.png" width="200px"/><br/>

- Licence: BSD 3
- Architectures supportées: ARM, AVR, x86, MSP430, PIC32, ...
- Principaux contributeurs: Atmel, Cisco, ETH, Redwire LLC, SAP, Thingsquare
- Principales caractéristiques:
  - Protothreads
  - Réseaux: Bluetooth LE, WPAN, WiFi/Ethernet, ...
  - Empreinte mémoire: 10 ko RAM, 100 ko ROM (4)
- Première version: 2003
- Nombre de contributeurs les 12 derniers mois: 10 (2)

## 3.1.3e ARM Mbed

<img src="Images/logo-arm-mbed.png" width="200px"/><br/>

- Licence: Apache 2.0
- Architectures supportées: ARM Cortex-M
- Principaux contributeurs: ARM
- Principales caractéristiques:
  - RTOS
  - Réseaux: Bluetooth LE, WPAN, LPWAN, WiFi/Ethernet, ...
  - Empreinte mémoire: 8ko RAM, 14ko ROM (5)
- Première version: 2009
- Nombre de contributeurs les 12 derniers mois: 25 (2)


## 3.1.3f FreeRTOS

<img src="Images/logo-freertos.jpg" width="200px"/><br/>

- Licence: MIT
- Architectures supportées: AVR, MSP430, ARM Cortex-M, ARM7, x86, RISC-V, ...
- Principaux contributeurs: Amazon
- Principales caractéristiques:
  - RTOS
  - Réseaux: Bluetooth LE, WPAN, WiFi/Ethernet, ...
  - Empreinte mémoire: 4ko RAM, 9ko ROM (6)
- Première version: 2002
- Nombre de contributeurs les 12 derniers mois: 41 (2)

1: http://riot-os.org/
2: https://www.openhub.net, http://github.com
3: https://docs.zephyrproject.org/latest/introduction/index.html
4: https://github.com/contiki-ng/contiki-ng/wiki
5: https://os.mbed.com/blog/entry/Optimizing-memory-usage-in-mbed-OS-52/
6: https://www.freertos.org/index.html
