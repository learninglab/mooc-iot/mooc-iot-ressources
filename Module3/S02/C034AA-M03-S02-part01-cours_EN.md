# 3.2.1. RIOT characteristics

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.2.1a System characteristics](#321a-system-characteristics)
- [3.2.1b The hardware abstraction layer](#321b-the-hardware-abstraction-layer)
- [3.2.1c A modular system](#321c-a-modular-system)
- [3.2.1d Network protocol stacks](#321d-network-protocol-stacks)
<!-- TOC END -->

## 3.2.1a System characteristics

RIOT has all the characteristics you would expect from an operating system designed
to operate on a microcontroller: it is real-time and requires very little
memory space.
Generally speaking, microcontrollers are highly constrained in terms of
memory (kilobytes of RAM and ROM) and are much
slower than modern microprocessors: they operate at frequencies in MHz, while
microprocessors operate at frequencies in GHz.
Another key characteristic of microcontrollers is their ability to
“go to sleep”, enabling them to minimize their power consumption: this is reduced to just a few
microamperes in the most extreme cases. An object operating on a microcontroller
is thus able to run off a battery for several months, or even several
years. As we will see later on, RIOT provides an original mechanism for
managing the sleep state of microcontrollers and thus minimizing their
power consumption.

In order to get the most out of microcontrollers, the RIOT operating system is based around a so-called **micro-kernel**
architecture.
This micro-kernel contains only the building blocks needed in order for the system
to function:

* **A multi-task system**, which makes it possible to have more than one task, also called execution context, on the same microcontroller. Execution contexts are totally independent of each other: each manages its own memory space. These tasks/execution contexts are commonly known as **threads**.

* **A real-time scheduler**, which is responsible for switching between
  execution contexts depending on their status. This scheduler is referred to as
  _tickless_, owing to the fact that the rescheduling takes place following a
  hardware interrupt and not via busy waiting (e.g. busy loops).

* **A mechanism for communication between tasks** used to exchange information
  in the form of messages between different execution contexts.

* **Synchronization mechanisms** for dealing with concurrency in cases where multiple threads are attempting to access shared resources for writing/reading, such as memory variables or the microcontroller's hardware. Two such synchronization mechanisms are mutex and semaphore.

In its most basic configuration, RIOT only requires 2.8 KB of RAM and 3.2 KB of ROM.

The structure of a RIOT application is closely related to the thread concept. In its most simple version, of at least two threads:

* The **main thread** in which the main function of the application's program is executed.

* The **idle thread**, an execution context with the lowest priority that the system will switch to in situations where all the other threads are either blocked or exited. This thread is also responsible for managing power consumption modes. If all the other threads have finished their tasks, this means that the system no longer has anything to do and can therefore switch to sleep mode.

_Note_: starting from [version 2020.07](https://github.com/RIOT-OS/RIOT/releases/tag/2020.07), the thread _idle_ is optional on ARM Cortex-M architectures. This saves the RAM used by this thread. In case the _idle_ thread is not used, the scheduler directly take in the charge management of power modes.

The mechanism for communication between threads is also useful when it comes to effectively
dealing with external interrupts, without any risk of losing an intermediary
state. Given that interrupts can happen at any
time, it is vital to be able to guarantee the status of the system once this interrupt
has been handled.
The efficient way in which interrupts are handled in RIOT involves a message being sent to the thread responsible for processing the interrupt from the latter's context. This enables the interrupt to be handled within a secure context (that of the thread receiving the message), without any loss of status in the system.

This is also known as _soft_ real-time: the system will ensure that
all external events (interrupts) are processed, but cannot guarantee
that they will be handled immediately.

<figure>
    <img src="Images/riot-application.png" alt="" style="width:300px"/>
    <figcaption>Fig. 1: Structure of a RIOT application, &copy; A. Abadie</figcaption>
</figure>

## 3.2.1b The hardware abstraction layer

A hardware abstraction layer is added to this real-time micro-kernel, enabling RIOT to execute on a wide range of hardware targets.

**This hardware abstraction layer is divided into 4 levels**:

1. **The cpu level**, the lowest level block as it touches the core of the object. This functional block is where certain elements common to all platforms are defined, enabling applications to know which functions are supplied by a microcontroller (random number generators, writing on flash, EEPROM, etc.).

2. **The board level**, which is where certain variables (or macros) common to all platforms are defined. Some boards, for example, feature access to certain serial buses (SPI, I2C) with their configuration, while others don't. A specific microcontroller is defined for each board.

3. **The periph level**: RIOT defines generic APIs for the main types of hardware available to microcontrollers, i.e. UART, SPI, I2C, PWM, etc. These are generic APIs which can be used to write code that is compatible with different hardware platforms without the need for modification.

4. **The driver level**, which is where all radio drivers, sensor drivers and actuator drivers can be found. This is the layer which depends on higher level hardware since it in itself is heavily reliant on the _hardware_ layer when it comes to accessing different platforms generically.

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>Fig. 2: General structure of RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

When compiling a RIOT application, you must always specify the target for which the firmware is to be produced. This gives the following mapping: one application &#x21d2; one board &#x21d2; one microcontroller model.

There is now an extensive range of hardware support for RIOT
thanks to this hardware abstraction layer: the system is capable of operating on a number of hardware
architectures, from 8-bit to 32-bit, and on ARM, AVR, MIPS, RISC-V and Xtensa.
What this means is that the system can be used on boards produced by the main manufacturers: Microchip, NXP, STMicroelectronics, Nordic, TI, Espressif, etc.

<figure style="text-align:center">
    <img src="Images/riot-boards.png" alt="" style="width:700px"/>
</figure>

## 3.2.1c A modular system

As we saw previously, RIOT is based on a micro-kernel architecture, to which developers add the modules needed for their application.
This modular mechanism means you **only need to include what is actually necessary** for an application, thus limiting the microcontroller's memory requirement (in kB).

RIOT's system provides a whole ecosystem of modules that can be used to develop applications quickly and easily, without having to reinvent the wheel.

**These modules are split into multiple categories**:

* **System libraries**: these modules are fully independent of the hardware, supplying features that can be used to develop an application. The **shell** module, for example, provides an interface that makes it easy to implement a command interpreter via the standard input-output (stdio), generally using the board's serial link (UART). There are also cryptography modules, and a highly-efficient (in terms of memory) formatting module for string.
The **ztimer** module provides advanced features for adding delays or actions (_callbacks_) at different moments during the execution of the application.

* **Actuator and sensor drivers**: these modules are generally
  based on the HAL (Hardware Abstraction Layer), meaning they can be used on any type of
  microcontroller. They are highly practical, making it easy to add
  sensor reading, motor control or an external display to an
  application, without having to write this code yourself. This category also includes
  drivers for external storage media, such as SD
  cards or communication interface drivers (radio, CAN, etc.).

<figure style="text-align:center">
    <img src="Images/riot-ucglib.jpg" alt="" style="width:200px"/>
    <figcaption>Fig. 3: Graphical user interface using the Ucglib package</figcaption>
</figure>

* **Network protocol** modules are actually a sub-category of system libraries, supplying implementations for protocols currently used in the IoT: CoAP, MQTT-SN, etc. We will return to this in more detail in module 4, which deals exclusively with “Network communication”.

* **External packages** are modules used to import source codes external to RIOT into an application. For maintenance reasons of the code base, it is not possible to copy as-is the whole code from another project. However, it is always useful to be able to integrate it easily into its application.
  All kinds of projects can be found in external RIOT packages:
  embedded interpreters for Javascript or LUA, file systems such as
  LittleFS or fatfs, complete networking stacks, such as Iwip or OpenThread, but also
  cryptography libraries, graphic libraries and even
  machine learning libraries (uTensor, TensorFlow-Lite).

<figure style="text-align:center">
    <img src="Images/packages.png" alt="" style="width:400px"/>
    <figcaption>Fig. 4: Comparison of required lines of code changes when porting an external package to RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT:an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

The RIOT build system is also responsible for resolving dependencies between modules in order to ensure the consistency of the generated application: for example, to load a driver that requires the I2C bus and the `ztimer` system, all you have to do is to load the module corresponding to this driver and the build system will automatically load the modules required for `I2C` and `ztimer`.

## 3.2.1d Network protocol stacks

RIOT supports the main network protocols currently used for the Internet of Things through what are known as **network protocol stacks**. They're called stacks because they implement the different layers of the OSI (**O**pen **S**ystems **I**nterconnection) model defined in order to make items of IT equipment communicate.

Some of these network stacks are grouped together in a set designed for
IP-oriented (Internet Protocol) communication. This set implements
protocols that are compatible with the current internet, making it possible for
objects to communicate with other items of equipment over the internet (such as
computers, smartphones, etc.).
These IP-oriented network stacks can be used with wired communication technology,
like Ethernet, or radio, such as Wi-Fi, 802.15.4 or BLE.
We will take a more detailed look at these protocols in module 4 “Network communication”.

The network stacks supplied by RIOT are either supplied natively, like **GNRC**
(`GeNeRiC`), or as external packages, like **IwIP**, or **OpenThread**
(an open-source implementation of the **Thread** specifications).

<img src="Images/openthread-logo.png" alt="" style="width:200px"/>

Lastly, **other network stacks** are also available:

* For the **CAN** (Controller Area Network) protocol, which is widely used in the automotive industry. This stack is implemented natively in RIOT.

* For support for **_Bluetooth Low Energy_** (BLE). This stack is provided
  as a package from the [NimBLE](https://github.com/apache/mynewt-nimble) project.
  <img src="Images/ble.png" alt="" style="width:180px"/>

* For support for **LoRaWAN** networks. This stack is provided
  as a package from the [Loramac-node](https://github.com/Lora-net/LoRaMac-node) project.
  <img src="Images/lorawan.png" alt="" style="width:200px"/>
