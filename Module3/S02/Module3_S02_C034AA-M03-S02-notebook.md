# Débuter avec RIOT

## Sommaire

* 3.3.0 [Introduction](#Introduction)
* 3.3.1 [Premiers pas avec RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session01)
* 3.3.2 [Ma première application RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session01)

## 3.3.0. Introduction

TODO: Présentation en vidéo du branchement d'une carte sur son ordinateur ou du
fonctionnement d'une carte dans IoT-LAB. Zoom sur un éditeur de texte avec du
code dedans et lancement d'une compilation.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i> 
<p>Dans cette séquence vous allez apprendre les bases pour commencer à écrire et compiler une application RIOT.
A la fin de cette séquence, vous serez en mesure d'ecrire une application interactive (un shell) pour lire des données sur un capteur.
</p>
</div>
