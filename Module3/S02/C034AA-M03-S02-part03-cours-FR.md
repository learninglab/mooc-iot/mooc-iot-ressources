# 3.2.3. Le système de compilation

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.2.3a Introduction](#323a-introduction)
- [3.2.3b Principe](#323b-principe)
- [3.2.3c Variables et cibles pour le développement](#323c-variables-et-cibles-pour-le-développement)
- [3.2.3d Autres cibles et variables utiles](#323d-autres-cibles-et-variables-utiles)
<!-- TOC END -->

## 3.2.3a Introduction

Le système de compilation de RIOT correspond à l'ensemble des fichiers
permettant de passer du code source d'une application écrite en C (ou C++) au
code binaire (le firmware) qui sera écrit sur la mémoire flash du
microcontrôleur.
Ce système de compilation contient aussi les règles pour lancer la procédure
d'écriture sur la mémoire flash, ouvrir un terminal série pour lire la sortie
standard de l'application, lancer un déboggeur et plein d'autres choses
utiles.

Le système de compilation de RIOT s'appuie sur l'outil `make` pour effectuer
toutes ces actions.
`make` est un outil largement répandu pour compiler du code source en langage
machine directement exécutable mais il peut aussi être utilisé pour d'autres
types de tâches.
Ces tâches sont communément appelées _cibles_ (ou _target_).
Pour effectuer toutes les opérations nécessaires aux différentes tâches realisées par le système de
compilation de RIOT (compiler, flasher, ouvrir un terminal, etc.) `make` a besoin de _recettes_.
Les recettes `make` sont écrites dans les fichiers `Makefile` qui se trouvent
un peu partout dans le code source de RIOT.
La connaissance de la syntaxe utilisée pour écrire un `Makefile` n'est pas
nécessaire pour aller plus loin dans ce Mooc.
En effet, le système de compilation de RIOT se charge de la plupart des choses
complexes et, comme nous le verrons plus loin, seules certaines variables
doivent être renseignées.

##  3.2.3b Principe

Pour générer un firmware à partir du code d'une application, il faut lancer
`make` en lui donnant en paramètre le `Makefile` qui se trouve dans le dossier
de cette application.
Par défaut, `make` cherche dans le dossier courant un fichier `Makefile` donc
la compilation peut se faire de 2 façons :

* en se plaçant dans le dossier de l'application et en invoquant `make`
  directement :
  ```
  $ cd <application_dir>
  $ make
  ```
* en utilisant l'option `-C` de `make` pour spécifier le dossier de
  l'application :
  ```
  $ make -C <application_dir>
  ```
  Cette méthode est utile si on compile des applications qui se trouvent dans
  différents dossiers car il n'est alors pas nécessaire de changer de dossier à chaque
  fois.

**C'est toujours le fichier Makefile de l'application qui doit être vu par `make`.**

## 3.2.3c Variables et cibles pour le développement

La structure du `Makefile` d'une application est en général divisée en 3 parties:

1. Définition de variables générales
2. Ajout des modules, fonctionnalités, paquetages nécessaires à l'application
3. Inclusion des règles définies par le système de compilation

Dans la première partie, une variable est toujours définie : _RIOTBASE_. Cette
variable correspond au chemin vers le dossier où se trouve le code source de
RIOT. Elle servira ensuite à inclure les règles génériques (le fameux fichier
`Makefile.include`) en fin de `Makefile`, dans l'étape 3.

D'autres variables, comme `BOARD` et `APPLICATION` sont aussi définies dans
cette première partie :

* `BOARD` contient le nom de la cible matérielle pour laquelle sera compilée notre
  application. L'utilisation de l'operateur `?=` au lieu de `=` permet de
  surcharger cette variable depuis la ligne de commande
* `APPLICATION` contient le nom de l'application et est utilisée par exemple
  comme nom pour les fichiers de firmware générés (ces fichiers ont l'extension
  elf, bin ou hex)

Dans la seconde partie du `Makefile` sont ajoutés les modules nécessaires à
l'application. Le nom d'un module correspond en général au nom du dossier
dans lequel il se trouve dans le code de RIOT.
En fonction du type de module à charger, 3 variables sont utilisées :

* `USEMODULE` contient la liste des modules systèmes chargés dans l'application.
* `FEATURES_REQUIRED` contient la liste des fonctionnalités du CPU nécessaires à
  l'application. Par exemple, pour les bus UART et SPI, on chargera les modules
  `periph_uart` et `periph_spi`.
* `USE_PKG` contient la liste des paquetages à charger dans
  l'application. Les noms de paquetages possibles correspondants aux noms des
  sous-dossiers du dossier `pkg`.

Ces trois variables contiennent des listes donc on utilisera l'opérateur
`+=` pour les modifier.

Enfin le `Makefile` d'une application se ponctue par l'inclusion du fichier
`Makefile.include` qui se trouve à la racine du code de RIOT.

Voici un exemple du fichier Makefile d'une application:

```mk
# Give a name to the application
APPLICATION = example_application

# If not already specified, use native as default target board
BOARD ?= native

# List the modules, features, packages needed by this application.
USEMODULE += ztimer
USEMODULE += ztimer_msec

# Specify the path the RIOT code base
RIOTBASE ?= $(CURDIR)/../../RIOT

# Final step: include the Makefile.include file, which contains the build
# system logic and definitions of make targets
include $(RIOTBASE)/Makefile.include
```

Le fichier `Makefile.include` contient le code permettant de gérer les outils
nécessaires en fonction de la cible (`BOARD`) choisie, des modules chargés, des
paquetages.
C'est aussi à partir de ce fichier que sont définies les cibles correspondant
aux différentes tâches à réaliser :

* _all_ s'occupera de compiler le code de l'application et donc d'appeler le
  bon compilateur en fonction de l'architecture du CPU de la cible matérielle
  choisie. Appeler `make` sans cible revient à appeler la cible _all_.
* _flash_ appelle l'outil de programmation de la carte pour écrire sur la
  mémoire flash le firmware produit par la cible _all_. Parmi ces outils de
  programmation généralement utilisés on rencontre OpenOCD, JLink, Avrdude,
  Edbg, etc
  Ils dépendent de l'interface de programmation de la carte ou d'un éventuel
  programmeur externe branché sur la carte (JLink, ST-Link) .
* _term_ appelle l'outil de connexion au port série de la carte. Cette cible
  permet d'afficher sur un ordinateur les messages écrits sur le port série de
  la carte.

Avec `make`, il est aussi possible d'invoquer plusieurs tâches en une commande,
`make` se chargeant de les appeler dans le bon ordre (si le `Makefile` est bien
écrit !). C'est ce qui permet par exemple de compiler, de flasher et de se
connecter au port série de la carte en une commande :
```
$ make -C <application_dir> flash term
```

## 3.2.3d Autres cibles et variables utiles

En plus des cibles de base vues précédemment, le système de compilation fournit
d'autres cibles qui peuvent s'avérer très utiles :

* **info-build** retourne les informations utiles au sujet de la compilation
  d'une application : liste des cartes supportées, cpu, liste des modules
  chargés, etc).

* **flash-only** est une alternative à _flash_ pour ne pas recompiler avant de
  flasher(puisque _flash_ dépend de _all_ et pas _flash-only_).

* **list-ttys** renvoie la liste des ports séries disponibles sur le PC hôte
  ainsi que les informations sur les cartes connectées (numéro de série). Cette
  cible est utile lorsque l'on travaille avec plusieurs cartes branchées en même
  temps sur machine locale).

* **debug** permet de débogger une application s'exécutant directement sur le
  hardware à l'aide de GDB (GNU Debugger). Par exemple, pour une carte se programmant avec
  l'outil OpenOCD, cette cible :
  1. Lance un serveur GDB localement. Ce serveur exécutera les commandes de GDB
     directement sur la carte.
  2. Lance le client GDB, se connecte au serveur GDB et charge l'application.
     Il est alors possible d'utiliser les commandes de déboggage GDB pour
     contrôler l'exécution de son application (point d'arrêt, valeur de
     variable, affichage de la position dans le code C ou assembleur, etc.)

Pour obtenir la liste complète des cibles `make` disponibles, il suffit de taper
`make` puis d'utiliser la touche `<tab>`. L'autocomplétion du shell affichera
alors cette liste.


D'autres variables utiles peuvent aussi être utilisées dans le `Makefile` d'une
application ou passées en ligne de commande. La liste complète de ces variables
est documentée dans le fichier `makefiles/vars.inc.mk`.

Parmi ces variables, on utilise souvent :

* **CFLAGS** pour paramétrer plus finement la compilation de son application.
  Avec cette variable il est possible de passer des valeurs de macros de
  préprocesseur pour activer ou désactiver la compilation de certaines parties du
  code. Par exemple :
  * l'option `-DLOG_LEVEL=4` change le niveau de _logging_ dans l'application à _verbose_
    qui affichera alors plus de messages. Attention cependant, la taille du
    firmware augmentera également.
  * l'option `-DDEBUG_ASSERT_VERBOSE` permet d'afficher plus de messages de debug en cas
    de `FAILED ASSERTION` (i.e un appel à la fonction _assert_ avec une
    assertion fausse) : le message contiendra le nom du fichier et la ligne où
    s'est produit l'erreur, ce qui rend sa résolution plus facile.
    Utilisée dans le Makefile, la variable _CFLAGS_ doit être modifiée en
    utilisant l'opérateur `+=`:

    ```mk
    CFLAGS += -DDEBUG_ASSERT_VERBOSE -DLOG_LEVEL=4
    ```

* **DEVELHELP** pour activer des vérifications diverses comme la compilation en
  mode debug, les assertions, l'affichage du nom des threads, etc.

* **PORT** pour spécifier un autre port série que celui par défaut lorsque
  plusieurs cartes sont branchées en même temps. Cette variable est utilisée par
  la cible _term_ de `make` pour ouvrir le terminal série.
  Par exemple, si le port série par défaut est `/dev/ttyACM0` mais que
  _list-ttys_ donne `/dev/ttyACM1` pour le port série de la carte visible qui
  nous intéresse, on pourra passer `PORT=/dev/ttyACM1` pour connecter le
  terminal sur le bon port.

* **IOTLAB_NODE** pour utiliser de manière transparente des cartes hébergées sur
  la plateforme IoT-LAB.
  Cette variable peut contenir soit la valeur `auto-ssh` pour choisir automatiquement
  une carte correspondant à la valeur de `BOARD` soit directement le nom complet
  d'une carte sur la plateforme, par exemple `m3-42.grenoble.iot-lab.info`:

  ```
  $ make BOARD=iotlab-m3 IOTLAB_NODE=auto-ssh -C examples/hello-world flash term
  ```

  La commande précédente compilera l'application `examples/hello-world` pour la
  carte `iotlab-m3`, puis flashera le firmware généré sur une carte iotlab-m3 au
  hasard (parmi celle d'une expérience en cours !) et enfin ouvrira un terminal
  sur le port série de la carte.
  Cela revient à travailler comme si la carte était branchée directement sur
  l'ordinateur !
