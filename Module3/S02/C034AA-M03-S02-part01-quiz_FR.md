# Question 3.2.1


RIOT est système d'exploitation:
[X] permettant de piloter des microcontrôleurs
[ ] permettant de piloter des microprocesseurs
[ ] de type **noyau monolithique** (comme Linux)
[X] de type **micro-kernel**
[X] _multi-thread_
[X] temps-réel

[explanation]
RIOT est système d'exploitation temps-réel et multi-thread de type micro-kernel.
Il ne permet de piloter que des microcontrôleurs.
[explanation]

# Question 3.2.2

Quelle est la licence de RIOT ?
( ) MIT
( ) Apache
(x) LGPL
() GPL
[explanation]
RIOT est distribué sous license LGPL et plus précisément LGPLv2.1.
[explanation]

# Question 3.2.3

Dans sa configuration minimale, une application utilisant RIOT...
[ ] ...nécessite plus de 3kB de RAM.
[X] ...nécessite moins de 3kB de RAM.
[ ] ...est composée d'un _thread_ principal.
[X] ...est composée d'un _thread_ principal et d'un _thread_ d'attente.

# Question 3.2.4

Dans la liste suivante, quels blocs composent la couche d'abstraction matérielle de RIOT ?
[ ] Le code du noyau (_kernel_)
[X] Le code lié à un CPU
[X] Le code lié à une carte
[ ] Le code contenant les bibliothèques système
[ ] Le code contenant des paquetages externes
[X] Le code des pilotes de périphérique

# Question 3.2.5

Quelles piles protocolaires orientées IP sont disponibles dans RIOT ?
[ ] CAN
[X] GNRC
[ ] LoRaWAN
[X] lwIP
[X] OpenThread
