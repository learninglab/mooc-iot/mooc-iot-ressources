# 3.2.2. Organisation du code source

Le code source de RIOT est disponible librement [sur GitHub](https://github.com/RIOT-OS/RIOT).

Comme nous l'avons vu précédemment dans cette séquence et comme l'illustre la figure
ci-dessous, l'architecture de RIOT s'articule autour de plusieurs briques essentielles:

- sa **couche d'abstraction matérielle** où se trouve tout le code dépendant du
  matériel et fournissant les interfaces de programmation communes,
- son **noyau** qui gère les différents contextes d'exécution, la communication
  entre les tâches, les priorités, etc.
- des **bibliothèques systèmes** (`ztimer`, etc.), des **paquetages**, etc.


<figure style="text-align:center">
    <img src="Images/riot-apis.png" alt="" style="width:500px;"/><br/>
    <figcaption>Fig. 1 : Interaction entre les APIs dans RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT:an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

D'autres briques offrent des niveaux d'abstraction supérieurs pour intégrer
facilement des fonctionnalités plus complexes :

- l'API `netdev` sert de pont entre des pilotes de communications hétérogènes
  (radio, Ethernet, etc) et les piles protocolaires (comme GNRC, LwIP ou OpenThread),
- l'API `SAUL` (Sensor Actuator Uber Layer) fournit une interface commune
  permettant de lire les données de capteurs similaires (température, pression,
  etc.).


Le **code source** est organisé comme suit :

* **boards:** contient le code spécifique au support des cartes. Le code de
  chaque carte supportée se trouve dans son propre dossier. Pour chaque carte, on définit ainsi le modèle du CPU utilisé, la configuration des horloges internes, les configurations par défaut des périphériques (UART, SPI, etc).
  On retrouve également la configuration par défaut du port série (par exemple si `/dev/ttyACM0` est créé lorsqu'on branche la carte par USB sur un ordinateur), la configuration relative aux outils de programmation (OpenOCD, AVRdude, etc).

* **core:** contient le code relatif au noyau, à la gestion des _thread_, la
  communication inter-processus.

* **cpu:**  contient le code spécifique au support des microcontrôleurs (fichier
  d'entête des fabricants, définitions, pilotes de periphériques internes).
  C'est aussi là que pour chaque architecture est implémentée la séquence
  d'initialisation du CPU (ou point d'entrée). Par exemple, cette fonction
  s'appelle _reset\_handler\_default_ pour l'architecture ARM CortexM.

* **dist:** contient le code des outils de gestion du projet RIOT. Ces outils
  sont généralement des scripts et sont utilisés par exemple par le système
  d'intégration continue de RIOT pour vérifier la qualité du code ou plus
  directement pour permettre au développeur de programmer une carte facilement.

* **doc:** contient les fichiers statiques de documentation Doxygen qui permet
  de générer la documentation de RIOT automatiquement à partir du code source.

* **drivers:** contient les pilotes haut-niveau pour les modules externes
  (capteurs, actionneurs, radios). C'est aussi dans ce dossier que se trouvent les
  définitions des API pour les périphériques internes au CPU (les
  implémentations concrètes se trouvant dans le dossier CPU).

* **examples:** contient plusieurs exemples d'applications.

* **makefiles:** contient une partie du code du système de compilation de RIOT.

* **pkg:** contient tous les paquetages externes compatibles avec RIOT. Chaque
  paquetage externe se trouve dans son propre dossier.

* **sys:** contient le code de toutes les bibliothèques systèmes, de la pile
  réseau GNRC (dans `sys/net`).

* **tests:** contient le code des tests unitaires et des applications de test.
  En général, à chaque fonctionnalité/pilote/paquetage est associée une
  application de test donc ces applications peuvent aussi servir d'exemple (et
  il est aussi recommandé de regarder là pour avoir des exemples d'utilisation).
