# 3.2.3. The build system

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [3.2.3a. Introduction](#323a-introduction)
- [3.2.3b. How it works](#323b-how-it-works)
- [3.2.3c. Variables and targets for development](#323c-variables-and-targets-for-development)
<!-- TOC END -->

## 3.2.3a. Introduction

RIOT's build system corresponds to all files
that are used to produce, from an application's source code written in C (or C++), the
binary code (the firmware) that will be written on the microcontroller's flash
memory.
This build system also contains the rules for launching the write procedure
on the flash memory, for opening a serial terminal to read the application's
standard output, for launching a debugger and a whole host of other useful
things.

RIOT's build system relies on the `make` tool for performing
all these tasks.
`make` is a widely-used tool for compiling source code in a binary that can run directly
on a machine, but it can also be used for other types of tasks.
All these tasks are commonly known as _targets_.
In order to carry out all the operations needed for the different tasks performed by RIOT's
build system (compiling, flashing, opening a terminal, etc.), `make` needs _recipes_.
`make` recipes are written in `Makefile` files, which can be found
pretty much everywhere in RIOT's source code.
Note that you won't need to know the `Makefiles` syntax to continue in this MOOC.
RIOT's build system handles all the complex tasks for you. As we will see
later, only certain variables have to be set.

##  3.2.3b. How it works

To generate a firmware from the source code of an application, you must launch
`make`, giving it as a parameter the `Makefile` located in the directory
for this application.
By default, `make` will search through the current directory for a `Makefile`, so
the compilation can be made in one of two ways:

* by going to the application directory and selecting `make`
  directly:
  ```
  $ cd <application_dir>
  $ make
  ```
* by using the `-C` option of `make` to specify the path to the application
  directory where the application `Makefile is located`:
  ```
  $ make -C <application_dir>
  ```
  This method is useful if you are compiling applications located in
  different directories, as it is not necessary to change the current directory each
  time.

## 3.2.3c. Variables and targets for development

An application's `Makefile` is normally divided into 3 parts:

1. The definition of general variables
2. The addition of the modules, features, and packages necessary for the application
3. The inclusion of the rules defined by the build system

In the first section, one variable is always defined: `RIOTBASE`. This
variable corresponds to the path where RIOT's source code base directory is
located. This path will be used to include the generic rules (the famous `Makefile.include`
file) at the end of the `Makefile`, in step 3.

Other variables, such as `BOARD` or `APPLICATION` are also defined in
this first part:

* `BOARD` contains the name of the hardware target for which our application will be
  built. Use of the operator `?=` instead of `=` makes it possible to
  overwrite this variable from the command line
* `APPLICATION` contains the name of the application and is used, for example,
  as the name for the generated firmware files (these files have the
  elf, bin or hex extension)

The modules required for the application are added in the second part of the
`Makefile`. The name of a module will generally correspond to the name of the file
containing RIOT's code.
Depending on the type of module to be loaded, 3 different variables are used:

* `USEMODULE` contains a list of the system or driver modules loaded in the application.
* `FEATURES_REQUIRED` contains a list of the CPU features required for
  the application. For example, for UART and SPI buses, the modules
  `periph_uart` and `periph_spi` are loaded.
* `USE_PKG` contains a list of packages to load in
  the application. The names of the possible packages correspond to the names of the
  sub-directories in the `pkg` directory.

These three variables contain lists, and so the operator `+=` must be used to modify them.

Lastly, an application's `Makefile` is punctuated by the inclusion of the main `Makefile.include`
file, which is located at the root of RIOT's code base directory.

Here is an example of application `Makefile`:

```mk
# Give a name to the application
APPLICATION = example_application

# If not already specified, use native as default target board
BOARD ?= native

# List the modules, features, packages needed by this application.
USEMODULE += ztimer
USEMODULE += ztimer_msec

# Specify the path the RIOT code base
RIOTBASE ?= $(CURDIR)/../../RIOT

# Final step: include the Makefile.include file, which contains the build
# system logic and definitions of make targets
include $(RIOTBASE)/Makefile.include
```

The `Makefile.include` file contains all logic used to manage the tools required for the
selected target (in `BOARD`), the loaded modules and the packages.
This is also the file used to define the targets corresponding
to the different tasks to be carried out:

* `all` will compile the application code and call the
  appropriate compiler according to the architecture of the CPU for the selected hardware
  target. Calling `make` without a `make` target will result in the target `all` being called.
* `flash` will call the flashing tool corresponding to the board in order to write
  the produced firmware on the flash memory. Some of the more
  widely-used flashing tools available in RIOT include OpenOCD, JLink, Avrdude and Edbg.
  These depend on the board's programming interface, or a potential
  external programmer connected to the board (JLink, ST-Link).
* `term` calls the tool for connecting to the board's serial port. This target
  can be used to display on a computer the messages written on the board`s serial port.

With `make`, it is also possible to invoke several tasks into one command:
`make` will ensure that they are called in the right order (provided the `Makefile` has been
written properly). This can be used to compile, flash and
connect to the board's serial port using just one command:
```
$ make -C <application_dir> flash term
```

## 3.2.3d. Other useful variables and targets

In addition to the basic targets seen before, the build system provides
other targets, which can prove very useful.

* **info-build** returns useful information about the build of
  an application: a list of supported boards, CPU, a list of the modules
  loaded, etc.)

* **flash-only** is an alternative to `flash`, sparing you from having to recompile before
  flashing (because `flash` depends on `all` but not `flash-only`).

* **list-ttys** returns a list of all available serial ports on the host computer,
  plus information on the plugged-in boards (serial number). This
  target is useful when you're working with more than one board plugged-in at the same
  time on the local host).

* **debug** lets you debug an application directly on the
  hardware using GDB (GNU Debugger). For example, with OpenOCD, this target:
  1. Will launch a GDB server locally on the computer. This server will execute the GDB commands
     directly on the board via a debugging protocol.
  2. Will launch the GDB client and connect it to the GDB server.
     It is then possible to use the GDB debugging commands in order to
     control the execution of its application (load it, add stop point, check variable
     value, display the current position in the C code or assembler, etc.)

To obtain a full list of all available `make` targets, simply enter
`make` before pressing the `<tab>` button. Autocompletion in the shell will then
display this list.


Other useful variables can also be used in an application's
`Makefile` or turned into command lines. A full list of these variables
can be found in the file `makefiles/vars.inc.mk`.

One common variable is:

* **CFLAGS**, which is used to produce a finer compilation configuration of its application.
  This variable makes it possible to bypass the values of some preprocessor's
  macros in order to activate or deactivate the compilation of certain parts of the
  code. For example:
  * the option `-DLOG_LEVEL=4` changes the _logging_ level in the application to _verbose_,
    meaning it will display more messages. Please note, however, that the size of the
    firmware will also increase when debug log level is enabled.
  * the option `-DDEBUG_ASSERT_VERBOSE` lets you display more debug messages in the event of
    a `FAILED ASSERTION` (i.e. a call to the _assert_ function with a
    false assertion): the message will contain the name of the file and the line where
    the error occurred, making the problem easier to fix.
    Used in the Makefile, the variable _CFLAGS_ must be modified
    using the operator `+=`:

    ```mk
    CFLAGS += -DDEBUG_ASSERT_VERBOSE -DLOG_LEVEL=4
    ```

* **DEVELHELP** to activate various verifications, such as enabling debug mode,
  assertions, displaying thread names, etc.

* **PORT** to specify a different serial port from the default serial port when
  multiple boards are plugged at the same time. This variable is used by
  the `make` target _term_ in order to open the serial terminal.
  If the default serial port is `/dev/ttyACM0`, for example, but
  _List-ttys_ gives `/dev/ttyACM1` for the serial port of the visible board we
  are interested in, we can set `PORT=/dev/ttyACM1` on the command line or in
  the application's `Makefile` in order to connect the terminal to the right port.

* **IOTLAB_NODE** to transparently use the boards hosted on
  the IoT-LAB testbed.
  This variable may contain either the value `auto-ssh` for automatically choosing
  a board corresponding to the `BOARD` value, or the fully qualified domain name
  of a board on the platform, e.g. `m3-42.grenoble.iot-lab.info`:


  ```
  $ make BOARD=iotlab-m3 IOTLAB_NODE=auto-ssh -C examples/hello-world flash term
  ```

  The previous command will compile the application `examples/hello-world` for the
  board `iotlab-m3`, before flashing the firmware generated on a random iotlab-m3
  board (one from the experiment in progress) and finally opening a terminal
  on the board's serial port.
  This is working the same as if the board was plugged directly into the computer.
