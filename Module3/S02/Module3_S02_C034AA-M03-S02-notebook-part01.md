# Premiers pas avec RIOT

## Sommaire

* [Structure d'une application][structure]
* [Compilation d'une application][compilation]
* [Lancement d'une application][lancement]

## Structure d'une application
[structure]: #Structure-dune-application

Ecrire une application RIOT minimale consiste à écrire deux fichiers :

* un **fichier `Makefile`** pour renseigner un certain nombre de variables et inclure les directives de compilation de l'application. Ce fichier `Makefile` doit au minimum renseigner les variables :

  * `APPLICATION` pour définir le nom de l'application. Ce nom sera aussi utilisé comme nom de fichier du firmware généré.

  * `BOARD` pour définir la cible pour laquelle sera généré le firmware.

  * `RIOTBASE` contient le chemin vers le dossier où se trouve le code source de RIOT.

  Voici un exemple de `Makefile` minimal :

  ```mk
  APPLICATION = example

  BOARD ?= native

  RIOTBASE ?= $(CURDIR)/../../../RIOT

  include $(RIOTBASE)/Makefile.include
  ```

* un **fichier `main.c`** contenant le code source de l'application à proprement parler, écrit en language C.
  Dans ce fichier, est implémentée, a minima, la fonction `main` qui sert de point d'entrée à l'application RIOT.

  Voici par exemple le contenu d'un fichier `main.c`. Notez que la fonction `main` affiche une chaîne de caractères en utilisant les fonctions C standard :

  ```c
  #include <stdio.h>

  int main(void)
  {
      puts("Welcome to RIOT!");
      return 0;
  }
  ```

## Compilation d'une application
[compilation]: #Compilation-dune-application

Le système de construction d'application de RIOT est basé sur l'outil `make`, outil souvent utilisé pour exécuter des étapes de compilation de code source complexes.
`make` utilise des fichiers `Makefile` qui décrivent les différentes étapes pour générer un exécutable binaire.
Pour plus d'informations sur l'outil `make` et la syntaxe d'un `Makefile`, référez-vous à la documentation disponible sur Internet : [Wikipedia](https://fr.wikipedia.org/wiki/Make) ou mieux [la documentation officielle](https://www.gnu.org/software/make/manual/make.html).

Comme nous l'avons vu précédemment, chaque application RIOT contient un fichier `Makefile` qui décrit l'application elle-même ainsi que ses dépendances.
C'est ce fichier qui est passé à `make` pour produire un firmware. Il faut donc lancer `make`, soit depuis le dossier de l'application, soit en utilisant le paramètre `-C <dossier de l'application>` pour faire pointer `make` vers le dossier de l'application :

* Compilation depuis de le dossier de l'application :
```sh
$ cd <application directory>
$ make
```

* Compilation depuis n'importe quel dossier, en spécifiant le dossier de l'application avec le paramètre `-C`:
```sh
$ make -C <application directory>
```

Pour compiler l'application, il est nécessaire de spécifier le chemin vers le dossier où se trouve le code source de RIOT. Cela permet à `make` de trouver les directives et les cibles définies dans le système de compilation de RIOT.

Ce chemin est fourni avec la variable `RIOTBASE`. Un cas d'utilisation de cette variable serait de compiler une même application en utilisant des versions différentes de RIOT, présentes dans des dossiers différents.

-----

### Exercice 1

Voir question 3.3.1 du quizz.

-----

Dans l'exercice précédent, la carte cible par défaut pour laquelle le firmware a été généré était `native`. Il s'agit d'un mode de RIOT qui permet de lancer le système comme un exécutable sur la machine de compilation.
Pour spécifier une autre cible hardware, il faut paramétrer la variable `BOARD` avec un nom de carte supportée par l'OS. Par exemple: iotlab-m3, samr21-xpro,etc.
La liste des cartes supportées correspond aux noms des sous-dossiers présents dans le dossier `boards` du code source de RIOT.

Par exemple, pour compiler une application pour une carte `iotlab-m3`, comme celles que l'on retrouve dans la plateforme FIT IoT-LAB, il suffit de lancer la commande suivante:

```sh
$ make BOARD=iotlab-m3 -C <application directory>
```

Le choix de la carte indique au système de compilation de RIOT la suite d'outils à utiliser pour générer un firmware compatible avec la carte cible. Il indique également les outils configurés pour écrire le firmware sur la carte, les interfaces d'entrée/sortie standard à utiliser (port de communication série sur
la machine hôte), etc.

-----

### Exercice 2

Voir question 3.3.2 du quizz

-----

## Lancement d'une application
[lancement]: #Lancement-dune-application

Le lancement d'une application dépend du type de carte cible : 

* Pour la cible `native` il s'agira de lancer un simple exécutable sur le système hôte. 
* Par contre, pour une cible matérielle telle qu'une carte iotlab-m3 ou samr21-xpro, il s'agira d'écrire le firmware compilé sur la mémoire flash de la carte (la ROM) et, éventuellement, de se connecter à son interface d'entrée/sortie standard pour lire les messages affichés par l'application.
L'interface d'entrée/sortie utilise généralement un port série (UART) de la carte et est visible sur le système hôte comme un port de communication virtuel (COMx sous Windows ou /dev/ttyXXX sous Linux).

Pour lancer et afficher les messages affichés par une application, le système de compilation de RIOT fournit 2 cibles pour `make`: `flash` et `term`.

Le principe est d'ajouter ces cibles à la fin de la commande de compilation de l'application:

```sh
make BOARD=<my board> -C <application directory> flash
```

Les deux cibles peuvent être combinées :

```sh
make BOARD=<my board> -C <application directory> flash term
```

* la **cible `flash`** lance la compilation pour la cible sélectionnée et écrit le binaire produit sur la cible :

  * dans le cas d'une cible `native`, le résultat de la compilation correspond directement au binaire produit et est donc directement exécutable.

  * dans le cas d'une cible hardware, il faut écrire le binaire généré sur la mémoire flash du micro-contrôleur. Pour cela, le système de compilation de RIOT s'appuie sur des outils comme OpenOCD, JLink, avrdude, etc. pour charger le firmware sur le micro-contrôleur. Tous les paramètres sont gérés de façon transparente en fonction de la carte cible.
    Par exemple, une carte iotlab-m3 est programmée par un appel à OpenOCD, une carte samr21-xpro, par un appel à EDBG, un autre outil de programmation. Une fois que le firmware est écrit sur la mémoire flash du microcontrôleur, il est directement lancé par celui-ci.

* la **cible `term`** dépend également de la cible :

  * pour la cible `native`, l'appel à `term` lance l'application directement sur le système hôte ;
  * pour une cible matérielle, cette cible lance un client série qui se connecte au port série virtuel configuré pour la carte en question. Par défaut, dans RIOT, il s'agit d'un petit script python avec des fonctionnalités avancées telles que l'horodatage des messages ou encore la gestion d'un historique des entrées envoyées par un utilisateur.

------

### Exercice 3

Voir question 3.3.3 du quizz

------
