# Question 3.2.7

Among the following commands, which allow you to compile a RIOT application
located in the `/tmp/riot-app` directory without changing the directory and
without specifying a specific target.

[X] make -C /tmp/riot-app
[ ] make -C /tmp/riot-app term
[ ] make -C /tmp/riot-app debug
[x] make -C /tmp/riot-app all

[explanation]
`all` is the make target for compiling an application. If no target is provided,
then `all` is used by default. The `term` and `debug` targets are used to
launch a terminal on the standard output and to launch a debug session (GDB
server + GDB client) respectively. On the other hand, these two targets do not
depend on the `all` target and therefore when used alone, the application is
not compiled.
[explanation]

# Question 3.2.8

In the application specified in the previous question, which targets should be
given to `make' to compile, flash and open a terminal?

[X] flash term
[x] term flash
[ ] all term
[ ] flash debug
[ ] flash-only term
[x] all flash term

[explanation]
The `flash` target depends on `all` so when it is called, the build is also
started. `flash-only` does not depend on `all`, so no build. The order of
targets is not important with `make`: `term flash` and `flash term` are
equivalent.
[explanation]
