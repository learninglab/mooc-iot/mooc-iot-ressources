# Présentation de RIOT

## Qu'est ce que RIOT ?

[RIOT](https://riot-os.org) est système d'exploitation de type logiciel libre développé par une large communauté internationale de développeurs. Son code source est disponible sur la plateforme de développement [GitHub](https://github.com/RIOT-OS/RIOT).

Ce système d'exploitation est de type temps-réel et fonctionne uniquement sur microcontrôleurs. Il s'appuie sur une architecture micro-noyau et la flexibilité de cette approche rend ce système moins complexe, plus robuste et nécessite très peu de ressources mémoire.

RIOT dispose de capacités multi-tâches dites également "multi-thread" : il est capable de gérer plusieurs contextes d'exécution concurrents dans la même application. Nous y reviendrons.

RIOT suit la tendance actuelle des systèmes d'exploitation pour objets connectés. En plus de son noyau, il intègre toutes les bibliothèques nécessaires pour mettre au point son objet : pilotes de capteurs, radio et piles protocolaires pour rendre un objet communiquant.

Enfin, RIOT est très facile à utiliser et à réutiliser :

* Il s'appuie sur des standards de programmation pour l'Internet des Objets, i.e langage C standard (C99), des outils standard (GDB, OpenOCD, Make), etc. 
* Son interface de programmation, indépendante du matériel (ou HAL pour Hardware Abstraction Layer) permet à une application basée sur RIOT d'être facilement portée sur une base matérielle différente.

Nous reviendrons en détails sur tous ces points dans la suite de ce module.

## Une brève histoire du projet

RIOT a été fondé en 2013 à la suite d'une collaboration de recherche entre l'Inria, l'Université libre de Berlin (FU Berlin) et l'Université de Hambourg (HAW). RIOT est en fait une évolution d'un noyau temps-réel qui existait déjà, FireKernel.

Depuis 2013, la communauté a continué à évoluer et aujourd'hui les développeurs de RIOT viennent d'horizon très divers : académiques, industriels, start-up mais aussi développeurs indépendants ou encore hobbyistes.

<img src="Images/riot-activity.png" alt="" style="width:350px"/>

Le projet RIOT est par ailleurs membre de l'initiative EdgeXFoundry au sein de la Fondation Linux.

Chaque année, plusieurs évènements sont organisés, en particulier le RIOT Summit où se retrouvent les principaux acteurs du projet. Ce sont des occasions pour présenter des réalisations avec RIOT, discuter des orientations futures et partager des idées.

<img src="Images/riot-summit.png" alt="" style="width:220px;"/>&nbsp;
<img src="Images/summit-2018.jpg" alt="" style="width:220px;"/>

## Ecosystème et philosophie de RIOT

La communauté RIOT s'inspire des pratiques utilisées pour le développement du système d'exploitation/noyau Linux.

### Sa licence est libre

Tout d'abord, comme Linux, c'est un logiciel libre. Il est distribué sous licence [LGPL v2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html), une licence qui permet d'utiliser RIOT comme bibliothèque d'une application commerciale, sans contraintes fortes.
La licence LGPLv2.1 garantit aussi que le code de RIOT restera libre dans le temps et, juridiquement, que la version de RIOT utilisée dans un objet commercialisé est également open source.

### Le manifeste RIOT

En tant que système d'exploitation, le projet RIOT suit 3 grands principes :

1. **Rester indépendant** des différentes technologies et des fabricants de matériel.
   Un système d'exploitation doit pouvoir fonctionner sur un maximum de plateformes matérielles et fournir du support aux technologies existantes en IoT, quelles qu'elles soient.

2. **Prendre les décisions et choisir les orientations du projet de façon démocratique par la communauté**, dans l'intérêt du plus grand nombre et non pas pour satisfaire ceux d'un unique grand groupe industriel.
   Ces décisions doivent rester les plus objectives et pragmatiques possibles.

3. **Garantir la maintenabilité du projet à long terme** en évitant la duplication de code, en choisissant les concepts les plus simples, en respectant la modularité et la portabilité du code.

### L'utilisation des standards

L'utilisation de standards est un objectif important de la communauté RIOT car ils garantissent l'interopérabilité du système à plusieurs niveaux :

* **Au niveau programmation**, l'utilisation du C-ANSI rend l'application portable entre plusieurs architectures voire systèmes. Par exemple, une même application peut être compilée pour une cible hardware type microcontrôleur avec RIOT ou comme un simple programme sur un système Linux.

* **Au niveau des outils**, l'utilisation du débogueur GDB est courante sous Linux et grâce à OpenOCD (on-chip debogueur), il est aussi possible de l'utiliser sur une cible hardware.

* **Au niveau des procédures de développement**, la communauté s'efforce d'utiliser les bonnes pratiques : revues de code, intégration continue, y compris sur cible hardware, vérifications statiques sur le code ([CppCheck](http://cppcheck.sourceforge.net/), [Coccinelle](http://coccinelle.lip6.fr/)).
Ces pratiques minimisent le risque d'introduction de bogues ou de régressions dans le code de RIOT et augmentent le niveau de fiabilité de l'OS. Chaque nouvelle contribution (Pull-Request sur GitHub) est ainsi intégralement vérifiée (par exemple par regénération pour toutes les cibles supportées par le système) automatiquement par plusieurs systèmes d'intégration continue ([Murdock](https://ci.riot-os.org), [Travis CI](https://travis-ci.org/RIOT-OS/RIOT/pull_requests)). Dans certains cas, les applications sont même testées directement sur les cibles matérielles.
  <img src="Images/murdock.png" alt="" style="width:400px"/>

* **Au niveau des interfaces de programmation (API)**, RIOT essaie de suivre au mieux la norme [POSIX](https://fr.wikipedia.org/wiki/POSIX) et donc de rendre ses interfaces compatibles avec ce qui existe actuellement dans le monde des systèmes d'exploitation de type UNIX (Linux, etc).

* **Au niveau des protocoles de communication**, certains membres de la communauté RIOT sont très actifs à l'IETF (Internet Engineering Task Force), un organisme de standardisation des protocoles utilisés pour Internet.

### L'organisation du projet

#### Des versions publiées régulièrement

Tous les 3 mois, une nouvelle version de RIOT est publiée. Les noms des versions de RIOT utilisent le schéma `<année>.<mois>`. Par exemple: `2019.01`, `2019.04`, etc.
Pour chaque version, des vérifications fonctionnelles très poussées sont réalisées par les mainteneurs pour s'assurer du bon fonctionnement du système. Ces vérifications concernent des scénarios IoT complexes où plusieurs dispositifs communiquent entre eux, ces scénarios sont difficiles à réaliser de manière totalement automatique.

#### Une bonne documentation

De manière générale, un projet bien documenté permet aux débutants de rapidement
démarrer. La communauté RIOT s'efforce de maintenir à jour et d'assurer une
bonne qualité de sa documentation.
La documentation de RIOT est accessible en ligne à l'addresse https://doc.riot-os.org et permet d'apprendre rapidement comment écrire une application RIOT. Cette documentation décrit également les APIs de tous les modules proposés par le système:

* interfaces du noyau : threads, messages, etc.,

* interfaces des modules systèmes,

* interfaces des pilotes : capteurs, actionneurs, radios, etc., 

* interfaces des paquetages externes,

* description des cartes supportées.

Nous reviendrons en détails sur la description de ces interfaces et sur la façon de les utiliser dans les séquences ultérieures de ce MOOC.
