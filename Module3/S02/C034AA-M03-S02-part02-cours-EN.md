# 3.2.2. Source code structure

The RIOT source code is freely available [on GitHub](https://github.com/RIOT-OS/RIOT).

As we saw before in this sequence and as is shown by the figure
below, RIOT's architecture is based around a number of essential components:

 - its **hardware abstraction layer**, which is where you will find all the code that depends on the
  hardware and provides the shared programming interfaces
- its **kernel**, which manages the different execution contexts, communication
  between tasks, priorities, etc.
- **system libraries** (`ztimer`, etc.), **packages**, etc.

<figure style="text-align:center">
    <img src="Images/riot-apis.png" alt="" style="width:500px;"/><br/>
    <figcaption>Fig. 1: Interaction between APIs in RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT:an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

Other components provide higher abstraction levels, making it easier
to integrate more complex functions:

- the `netdev` API acts as a bridge between heterogeneous communications drivers
  (radio, Ethernet, etc.) and protocol drivers (such as GNRC, LwIP or OpenThread)
- the `SAUL` (Sensor Actuator Uber Layer) API provides a common interface
  enabling data from similar sensors to be read (temperature, pressure,
  etc.).


The **source code** is organized as follows:

* **boards:** contains the code specific to the support for the boards. The code for
  each supported board can be found in its own directory. For each board, the CPU model used, the way in which internal clocks are configured and the default configurations for devices (UARTs, SPIs, etc.) are all defined.
  There is also the default configuration for the serial port (e.g. if `/dev/ttyACM0` is created when the board is plugged into a computer via one of its USB ports) and the configuration relating to the programming tools (OpenOCD, AVRdude, etc.)

* **core:** contains the code relating to the kernel, to _thread_ management and
  to inter-process communication.

* **cpu:**  contains the code specific to the support for the microcontrollers (vendor
  header file, definitions, internal device drivers, etc.).
  This is also where the CPU initialization sequence (or entry point)
  is implemented for each architecture. This function, for example,
  is called _reset\_handler\_default_ for the ARM CortexM architecture.

* **dist:** contains the code for RIOT helper tools. These tools
  are normally scripts and are used, for example, by RIOT's
  continuous integration system in order to verify the quality of the code or, more
  directly, to make it easier for developers to flash boards.

* **doc:** contains Doxygen static documentation files, enabling
  RIOT documentation to be generated automatically from the source code.

* **drivers:** contains the higher-level drivers for external modules
  (sensors, actuators, radios). This directory is also where the API definitions
  for the internal CPU devices can be found
  (concrete implementations are located in the CPU directory).

* **examples:** contains several example applications.

* **makefiles:** contains part of the code for RIOT's build system.

* **pkg:** contains all external packages adapted to RIOT. Each
  external package is located in its own directory.

* **sys:** contains the system libraries code and the in-house GNRC
  network stack (in `sys/net`).

* **tests:** contains the code for the unit tests and test applications.
  Generally speaking, each function/driver/package is associated with a
  test application, which means that these applications can be used as good examples
  (it is also recommended to look there for some usage examples).
