# 3.2.0. Discovering RIOT


A video presentation of the RIOT project and the characteristics of this operating
system for microcontrollers.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>This sequence will introduce you to the [RIOT](https://riot-os.org) operating system, which is widely-used for programming microcontroller-based connected objects. We will present a general overview of this project before taking a look at its main characteristics.
</p>
</div>
