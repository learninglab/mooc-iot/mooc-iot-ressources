# 3.2.0. Introduction à RIOT

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez découvrir le système d'exploitation [RIOT](https://riot-os.org), largement utilisé pour programmer des objets connectés à base de microcontrôleurs. Nous ferons une présentation générale de ce projet puis nous détaillerons ses caractéristiques principales.
</p>
</div>

Présentation en vidéo du projet RIOT et des caractéristiques de ce système
d'exploitation pour microcontrôleurs.
