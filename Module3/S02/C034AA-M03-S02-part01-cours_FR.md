# 3.2.1. Caractéristiques de RIOT

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [3.2.1a Caractéristiques système](#321a-caractéristiques-système)
- [3.2.1b Couche d'abstraction matérielle](#321b-couche-dabstraction-matérielle)
- [3.2.1c Un système modulaire](#321c-un-système-modulaire)
- [3.2.1d Les piles protocolaires réseaux](#321d-les-piles-protocolaires-réseaux)
<!-- TOC END -->

## 3.2.1a Caractéristiques système

RIOT possède les caractéristiques attendues d'un système d'exploitation pour
microcontrôleur: il est temps-réel et consomme très peu d'espace mémoire.
En effet, en règle générale, les microcontrôleurs sont très contraints en
mémoire (on parle de kilo octets de RAM et de ROM) et fonctionnent beaucoup
moins rapidement qu'un microprocesseur moderne : on parle de MHz quand la
vitesse d'un microprocesseur est de l'ordre du GHz.
Une autre caractéristique importante des microcontrolleurs est leur capacité à
"s'endormir" pour minimiser leur consommation d'énergie: on parle de quelques
micro ampères dans les cas extrèmes. Un objet fonctionnant sur microcontrôleur
peut donc fonctionner sur batterie pendant plusieurs mois, voire plusieurs
années. Comme nous le verrons plus tard, RIOT propose un mécanisme original pour
gérer l'état d'endormissement du microcontrôleur et ainsi minimiser sa
consommation d'énergie.

Pour pouvoir exploiter au mieux les caractéristiques d'un microcontrôleur, le
système d'exploitation RIOT s'articule autour d'une architecture de type
**micro-kernel**.
Ce micro-kernel ne contient que les briques essentielles au fonctionnement du
système :

* **Une gestion multi-tâches** qui permet d'avoir plusieurs contextes d'exécution concurrents sur un même microcontrôleur. Les contextes d'exécution sont totalement indépendants les uns des autres : chacun gère son propre espace mémoire. Ces contextes d'exécution/tâches sont communément appelés **thread**.

* **Un ordonnanceur temps-réel** qui s'occupe de basculer d'un contexte
  d'exécution à un autre en fonction de leur état. Cet ordonnanceur est dit
  _tickless_ dans le sens où tout réordonnancement s'effectue à la suite d'une
  interruption matérielle et non par attente active (i.e avec une boucle d'attente).

* **Un mécanisme de communication entre tâches** pour échanger des informations,
  sous la forme de messages, entre différents contextes d'exécution.

* **Des mécanismes de synchronisation** pour gérer la concurrence lorsque plusieurs threads accèdent en écriture/lecture à des ressources partagées, comme des variables en mémoire ou des périphériques du microcontrôleur. Parmi ces mécanismes de synchronisation, on retrouve les classiques mutex et les sémaphores.

Dans sa configuration minimale, RIOT ne nécessite que 2.8kB de RAM et 3.2kB de ROM.

La structure d'une application RIOT est intimement liée au concept de _thread_ puisqu'elle se compose, dans sa version la plus simple, d'au moins deux _threads_:

* Le **thread principal**, ou **thread _main_** dans lequel s'exécute la fonction `main` du programme de l'application.

* Le **thread d'attente**, ou **thread _idle_**, qui est un contexte d'exécution ayant la plus faible priorité et dans lequel le système bascule lorsque tous les autres threads sont bloqués ou terminés. C'est aussi ce thread qui a la charge de la gestion des modes de consommation. En effet, si tous les autres threads ont terminé leur travail, cela veut dire que le système n'a plus rien à faire et qu'il peut donc se mettre en veille.

_Note_: depuis la [version 2020.07](https://github.com/RIOT-OS/RIOT/releases/tag/2020.07), le _thread idle_ est optionel sur les architectures ARM Cortex-M. Cela permet d'économiser l'espace mémoire (RAM) utilisé par ce thread. Dans le cas où le _thread_ idle n'est pas utilisé,
c'est le scheduler qui prend directement en charge la gestion des modes de consommation.

Le mécanisme de communication entre _thread_ est aussi utile pour gérer des
interruptions externes efficacement et sans risque de perte d'un état
intermédiaire. En effet, une interruption pouvant arriver à n'importe quel
moment, il faut pouvoir garantir l'état du système une fois cette interruption
traitée.
Le principe de la gestion efficace des interruptions dans RIOT est donc d’envoyer un message au thread en charge du traitement de l'interruption depuis le contexte de cette-dernière. De cette manière, la gestion de l'interruption peut se faire
dans un contexte sûr (celui du _thread_ recevant le message), sans perte d'état
pour le système.

On parle aussi dans ce cas de temps-réel _mou_ : le système garantit le
traitement de tout évènement extérieur (une interruption) mais ne garantit pas
qu'il sera traité immédiatement.

<figure style="text-align:center">
    <img src="Images/riot-application.png" alt="" style="width:300px"/>
    <figcaption>Fig. 1 : Structure d'une application RIOT, &copy; A. Abadie</figcaption>
</figure>

## 3.2.1b Couche d'abstraction matérielle

Autour de ce micro-kernel temps-réel vient ensuite se greffer une couche d'abstraction matérielle qui va permettre à RIOT de s'exécuter sur une grande variété de cibles matérielles.

**Cette couche d'abstraction matérielle se divise en 4 niveaux** :

1. **Le niveau cpu**, c'est le bloc de plus bas niveau car il touche au coeur de l'objet. C'est dans ce bloc fonctionnel que sont définis certains éléments communs à toutes les plateformes et qui vont permettre aux applications de savoir quelles fonctionnalités sont fournies par un microcontrôleur (générateur de nombre aléatoire, écriture sur la flash, EEPROM, etc.).

2. **Le niveau de la carte** où sont définies certaines variables (ou macros) communes à toutes les plateformes. Par exemple, certaines cartes exposent un accès à certains bus série (SPI, I2C) avec leur configuration et d'autres non. Pour chaque carte est défini un microcontrôleur.

3. **Le niveau périphériques** : RIOT définit des API génériques pour les principaux types de périphériques dont disposent les microcontrôleurs, i.e UART, SPI, I2C, PWM, etc. Ce sont ces API génériques qui permettent d'écrire du code adapté à des plateformes matérielles différentes sans modification.

4. **Le niveau des pilotes** où sont regroupés tous les pilotes radio, les pilotes de capteurs et d'actionneurs. C'est la couche dépendante du hardware de plus haut niveau puisqu'elle s'appuie elle-même largement sur la couche _periph_ pour accéder de manière générique aux différentes plateformes.

<figure style="text-align:center">
    <img src="Images/riot-architecture.png" alt="" style="width:400px;"/><br/>
    <figcaption>Fig. 2 : Architecture générale de RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT: an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

Lorsqu'on compile une application RIOT, il faut toujours spécifier la cible pour laquelle on va produire un firmware. Cela donne la correspondance suivante : une application &#x21d2; une carte &#x21d2; un modèle de microcontrôleur.

Grâce à cette couche d'abstraction matérielle, le support matériel de RIOT est
aujourd'hui particulièrement étoffé : le système peut fonctionner sur plusieurs architectures
matérielles, du 8bit au 32 bit, sur ARM, AVR, MIPS, RISC-V, Xtensa.
Le système est donc utilisable sur des cartes vendues par les principaux fabricants: Microchip, NXP, STMicroelectronics, Nordic, TI, Espressif, etc.

<figure style="text-align:center">
    <img src="Images/riot-boards.png" alt="" style="width:700px"/>
</figure>

## 3.2.1c Un système modulaire

Comme on l'a vu précédemment, RIOT utilise une architecture micro-kernel autour de laquelle le développeur ajoute les modules nécessaires à son application.
Ce mécanisme modulaire permet de n'**utiliser que ce qui est réellement nécessaire** à une application et donc de limiter sa taille (en kB) sur la mémoire du microcontrôleur.

Le système RIOT fournit tout un écosystème de modules permettant de developper des applications rapidement et efficacement sans avoir à réinventer la roue.

**Ces modules sont regroupés en plusieurs catégories** :

* Les **bibliothèques système** : ces modules sont totalement indépendants du matériel et fournissent des fonctionnalités utiles pour développer une application. Par exemple, le module **shell** fournit une interface pour implémenter facilement un interpréteur de commandes via l'entrée-sortie standard (stdio), qui utilise généralement le lien série (UART) de la carte. On trouve des modules de cryptographie, un module de formatage efficace (en mémoire) des chaînes de caractères.
Le module **ztimer** offre des fonctionnalités avancées pour ajouter des délais ou programmer des actions (_callbacks_) à différents moments de l'exécution de l'application.

* Les **pilotes de capteurs et d'actionneurs** : ces modules reposent
  généralement sur la HAL (Hardware Abstraction Layer) pour pouvoir être utilisés sur tout type de
  microcontrôleur. Ils sont très pratiques pour ajouter facilement dans une
  application la lecture d'un capteur, contrôler un moteur ou un afficheur
  externe sans avoir à écrire soi-même ce code. On trouve également dans cette
  catégorie des pilotes pour des supports de stockage externe, comme des cartes
  SD ou encore des pilotes d'interface de communication (radio, CAN, etc).

<figure style="text-align:center">
    <img src="Images/riot-ucglib.jpg" alt="" style="width:200px"/>
    <figcaption>Fig. 3 : Interface utilisateur utilisant le paquetage Ucglib</figcaption>
</figure>

* Les modules de **protocoles réseaux** sont en fait une sous-catégorie des bibliothèques systèmes et fournissent des implémentations pour les protocoles couramment utilisés en IoT : CoAP, MQTT-SN, etc. Nous y reviendrons en détails dans le module 4 dédié à la "Communication réseau".

* Les **paquetages externes** sont des modules permettant d'importer des codes sources externes à RIOT dans une application. En effet, pour des raisons de maintenance de la base de code, il n'est pas possible d'ajouter tel quel du code provenant d'un autre projet. Par contre, il est toujours utile de pouvoir l'intégrer facilement dans son application.
  Dans les paquetages externes de RIOT, on retrouve des projets de tous types:
  interpréteurs embarqués pour Javascript ou LUA, systèmes de fichiers comme
  LittleFS ou fatfs, pile réseau complète comme lwip ou OpenThread, mais aussi
  des bibliothèques de cryptographie, des librairies graphiques voire même des
  bibliothèques de machine learning (uTensor, TensorFlow-Lite).

<figure style="text-align:center">
    <img src="Images/packages.png" alt="" style="width:400px"/>
    <figcaption>Fig. 4 : Comparaison des changements nécessaires en terme de lignes de code pour porter un paquet externe sur RIOT</figcaption>
</figure>

_[Source:](https://www.researchgate.net/publication/323743215_RIOT_an_Open_Source_Operating_System_for_Low-end_Embedded_Devices_in_the_IoT) E. Baccelli et al. (2018) "RIOT:an Open Source Operating System for Low-end Embedded Devices in the IoT", IEEE Internet of Things Journal. PP. 1-1. 10.1109/JIOT.2018.2815038_

Le système de compilation de RIOT s'occupe également de gérer les dépendances entre modules pour s'assurer de la cohérence de l'application qui sera générée : par exemple, pour charger un pilote utilisant le bus I2C et le système `ztimer`, il suffit simplement de charger le module correspondant à ce pilote et le système de compilation s'occupe de charger automatiquement les modules pour `I2C` et pour `ztimer`.

## 3.2.1d Les piles protocolaires réseaux

RIOT supporte la plupart des protocoles réseaux couramment utilisés pour l'Internet de Objets grâce à ce qu'on appelle des **piles protocolaires réseaux**. On parle de piles parce qu'elles implémentent les différentes couches du modèle OSI (**O**pen **S**ystems **I**nterconnection) défini pour faire communiquer des équipements informatiques.

Parmi ces piles réseaux, certaines sont regroupées dans une famille destinée à
la communication orientée IP (Internet Protocol). Cette famille implémente des
protocoles compatibles avec l'Internet actuel et en pratique permettent de faire
communiquer des objets avec d'autres équipements sur Internet (comme des
ordinateurs, des smartphones; etc.).
Ces piles réseaux IP reposent sur des technologies de communication filaire
comme Ethernet ou radio comme le WiFi, 802.15.4 ou BLE.
Ces protocoles seront abordés en détails dans le module 4 sur la "Communication réseau".

Les piles réseaux fournies par RIOT le sont soit nativement, comme **GNRC**
(pour `GeNeRiC`), soit sous forme de paquetage externe, comme **OpenThread**
(une implémentation open-source des spécifications de **Thread**) ou **lwIP**.

<img src="Images/openthread-logo.png" alt="" style="width:200px"/>


Enfin, d'**autres piles réseaux** sont également disponibles :

* Pour le protocole **CAN** (Controller Area Network), largement utilisé dans l'industrie automobile.
Cette pile est impléméntée nativement dans RIOT.

* Pour le support du **_Bluetooth Low Energy_** (BLE).
Cette pile est fournie sous forme d'un paquetage du projet [NimBLE](https://github.com/apache/mynewt-nimble).

  <img src="Images/ble.png" alt="" style="width:180px"/>

* Pour le support des réseaux **LoRaWAN**.
Cette pile est fournie sous forme d'un paquetage du projet [Loramac-node](https://github.com/Lora-net/LoRaMac-node).

  <img src="Images/lorawan.png" alt="" style="width:200px"/>
