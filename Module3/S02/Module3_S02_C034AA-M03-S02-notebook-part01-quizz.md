# Question 3.3.1

* Compiler votre première application RIOT
  (TODO: détailler avec plus de directives)

* Vérifier le type d'application avec la commande `file`

# Question 3.3.2

* Compiler votre première application RIOT pour une carte iotlab-m3
  (TODO: détailler avec plus de directives)

  ```sh
  $ cd ~/mooc-iot/example
  $ make
  Building application "example" for "native" with MCU "native".

  "make" -C /home/user/RIOT/boards/native
  "make" -C /home/user/RIOT/boards/native/drivers
  "make" -C /home/user/RIOT/core
  "make" -C /home/user/RIOT/cpu/native
  "make" -C /home/user/RIOT/cpu/native/periph
  "make" -C /home/user/RIOT/cpu/native/vfs
  "make" -C /home/user/RIOT/drivers
  "make" -C /home/user/RIOT/drivers/periph_common
  "make" -C /home/user/RIOT/sys
  "make" -C /home/user/RIOT/sys/auto_init
   text   data  bss    dec    hex   filename
   20206  568   47652  68426  10b4a /home/user/mooc-iot/example/bin/example.elf
 ```

* Vérifier le type d'application avec la commande `file`

# Question 3.3.3

- Lancer une application `native` sur la machine hôte.
- Lancer une expérience dans IoT-LAB sur un m3, compiler une application pour cette cible, la flasher, et accéder à son port série à distance.
- Comprendre les problématiques de timing entre moment du lancement de
  l'application sur la carte et la connexion à son port série.
