# Question 3.2.7

Parmi les commandes suivantes, lesquelles permettent de compiler une
application RIOT se trouvant dans le dossier `/tmp/riot-app` sans changer de
dossier et sans spécifier de cible spécifique.

[X] make -C /tmp/riot-app
[ ] make -C /tmp/riot-app term
[ ] make -C /tmp/riot-app debug
[x] make -C /tmp/riot-app all

[explanation]
`all` est la cible make pour compiler une application. Si aucune cible n'est donnée, alors `all` est utilisée par défaut.
Les cibles `term` et `debug` permettent de lancer respectivement un terminal sur la sortie standard et de lancer une
session de debug (serveur GDB + client GDB). Par contre, ces deux cibles ne dépendent pas de la cible `all` et donc lorsqu'elles
sont utilisées seules, l'application n'est pas compilée.
[explanation]

# Question 3.2.8

Dans l'application spécifiée dans la question précédente, quelles cibles faut-il passer à `make` pour
compiler, écrire sur la flash et ouvrir un terminal ?

[X] flash term
[x] term flash
[ ] all term
[ ] flash debug
[ ] flash-only term
[x] all flash term

[explanation]
La cible `flash` dépend de `all` et lorsqu'elle est appelée, la compilation est aussi
lancée. `flash-only` ne dépend pas de `all`, donc pas de compilation.
L'ordre des cibles n'est pas important avec `make`: `term flash` et `flash term` sont équivalents.
[explanation]
