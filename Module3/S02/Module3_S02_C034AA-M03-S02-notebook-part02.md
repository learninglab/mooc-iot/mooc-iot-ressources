# Débuter avec RIOT

## Sommaire

* [Etendre son application][extension]
* [Interaction avec le matériel][hardware]

## Etendre son application
[extension]: #Etendre-son-application

Pour étendre une application RIOT, par exemple pour y ajouter des fonctions de lecture d'un capteur, un pilote radio ou d'autres bibliothèques systèmes, il faut tout d'abord les ajouter explicitement dans la configuration de l'application.

Cela se fait avec **3 nouvelles variables** :

TODO: expliquer en deux mots l'utilité des module et paquets cités en exemple

* **USEMODULE** contient la liste des modules systèmes ou des drivers nécessaires à l'application.
  Quelques exemples: `ztimer`, `fmt`, `shell`, `ps`, `hts221`, etc.

* **USEPKG** contient la liste des paquetages externes.
  Quelques exemples: `lwip`, `openthread`, `semtech-loramac`, etc.

* **FEATURES_REQUIRED** contient les fonctionnalités matérielles requises pour la cible en question. Dans RIOT, ces fonctionnalités correspondent aux pilotes de périphériques disponibles dans le microcontrôleur mais aussi à des fonctionnalités de plus haut niveau comme `riotboot`, le système chargeur de démarrage de RIOT.
  Par exemple, un pilote utilisant un bus I2C nécessitera la fonctionnalité `periph_i2c`.
  D'autres exemples de fonctionnalités du microcontrôleur: `periph_gpio`, `periph_uart`, `periph_spi`, `periph_i2c`, etc.

L'ajout des modules se fait directement dans le Makefile de l'application :

```mk
USEMODULE += ztimer shell

USEPKG += semtech-loramac

FEATURES_REQUIRED += periph_gpio
```
Les modules peuvent aussi être directement ajoutés à partir de la ligne de commande de compilation:
```sh
$ USEMODULE=ztimer make BOARD=bl-072z-lrwan1
```

------

### Exercice 4

Voir question 3.3.4 du quizz

------

## Interaction avec le matériel
[hardware]: #Interaction-avec-le-matériel

L'interaction avec le matériel peut s'effectuer à 3 niveaux:

* au niveau de la carte, en utilisant des macros du préprocésseur définies, par
exemple, pour contrôler des LEDs ou des boutons. Pour cela, il suffit simplement
  d'inclure le fichier `board.h` dans le code de son application. Cela peut se
  faire au début du fichier `main.c` par exemple:
  ```c
  #include "board.h"
  ```

  _Exemple :_ les macros utilisées pour les LEDs utilisent toutes des noms de la
  forme `LED<id>_<action>`:
  * `LED0_ON` allume la première LED
  * `LED0_TOGGLE` bascule la première LED

* au niveau du CPU, en utilisant les interfaces de programmation définies pour
  les périphériques disponibles (GPIO, UART, I2C, etc).
  Ces interfaces sont chargées en incluant les fonctionnalités correspondantes
  dans le `Makefile` de l'application:
  ```mk
  # Pour l'api des GPIO:
  FEATURES_REQUIRED += periph_gpio
  # Pour l'api de l'UART:
  FEATURES_REQUIRED += periph_uart
  ```
  Ensuite, il suffit d'inclure le fichier d'entête dans son code (au début du
  fichier `main.c` par exemple):
  ```c
  #include "periph_gpio.h"
  #include "periph_uart.h"
  ```
  Ce niveau de l'abstraction matérielle de RIOT est très important car il
  fournit une interface (_API_) commune pour toutes les architectures supportées
  par RIOT.

* au niveau des drivers de modules externes, en utilisant les API des pilotes.
  Pour cela, il faudra identifier le nom du _module_ correspondant à charger
  dans son `Makefile` et inclure le fichier d'entête dans son code:
  ```mk
  # Dans son Makefile, on charge le module du pilote pour le capteur bmp180
  USEMODULE += bmp180
  ```
  ```c
  #include "bmp180.h"
  ```
  Tous les pilotes sont regroupés dans le dossiers `drivers` et on y trouve des
  pilotes pour des capteurs (température, accéléromètre), actionneurs (écrans,
  moteurs), radio (BLE, LoRa, 802.15.4, etc).

------

### Exercice 5

Voir question 3.3.5 du quizz

------
