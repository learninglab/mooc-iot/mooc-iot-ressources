# Question 3.2.6

Quels dossiers de RIOT contiennent du code dépendant du matériel ?
[X] boards
[X] cpu
[ ] sys
[ ] core
[X] drivers

[explanation]
Seuls les dossiers `cpu`, `boards` et `drivers` contiennent du code dépendant du
matériel.
[explanation]
