# Module 3. Focus on Embedded Softwares

Objective: At the end of this module you will be able to apply the specific programming principles for a connected object. You will also be able to describe the characteristics of the RIOT operating system.

Hands-on activities (TP):
TP3: First RIOT application
TP4: Extending a RIOT application
TP5: Managing threads with RIOT
TP6: Use timers
TP7: Use sensors on the IoT-LAB M3 board

## Contents of Module 3

### 3.1. Software Solutions For Writing an Application
- 3.1.0. Software solutions for programming connected objects
- 3.1.1. Programming a connected object
- 3.1.2. What is an operating system (OS) and how to choose the right one?
- 3.1.3. The main existing operating systems
- 3.1.4. Higher level solutions

### 3.2. Discovering RIOT
- 3.2.0. Introduction
- 3.2.1. RIOT characteristics
- 3.2.2. Source code structure
- 3.2.3. The build system
- TP3.   First RIOT application
- TP4.   Extending a RIOT application

### 3.3. RIOT Architecture
- 3.3.0. Introduction
- 3.3.1. The kernel and how it is used
- 3.3.2. Scheduling and Threads
- TP5.   Managing threads with RIOT
- 3.3.3. Advanced use of threads
- 3.3.4. Power management

### 3.4. Hardware APIs
- 3.4.0. Introduction
- 3.4.1. The hardware abstraction layer
- 3.4.2. Timers
- TP6.   Use timers
- 3.4.3. Interacting with GPIOs
- 3.4.4. High-level drivers

### 3.5. Peer evaluation of your IoT application
- 3.5.0 Introduction
- 3.5.1. Presentation of "Peer Evaluation" _(not included)_
- 3.5.2. Scoring scale _(not included)_
- TP7.   Use sensors on the IoT-LAB M3 board
- 3.5.3. RIOT application on FIT IoT-LAB
- 3.5.4. Submit your activity and evaluate your peers _(not included)_
