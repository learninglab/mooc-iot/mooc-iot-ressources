
# Module 3. Focus sur les architectures logicielles

Objectif : À la fin de ce module, vous serez capable d'appliquer les principes spécifiques de programmation d'un objet connecté et de décrire les caractéristiques du système d'exploitation RIOT.

Activités pratiques (TP) :
TP3: First RIOT application
TP4: Extending a RIOT application
TP5: Managing threads with RIOT
TP6: Use timers
TP7: Use sensors on the IoT-LAB M3 board


## Sommaire du Module 3

### 3.1. Solutions logicielles pour programmer une application
- 3.1.0. Les solutions logicielles pour programmer un objet connecté
- 3.1.1. Principe de programmation d'un objet connecté
- 3.1.2. Qu'est-ce qu'un OS et comment le choisir ?
- 3.1.3. Les Principaux OS existants
- 3.1.4. Les solutions de plus haut niveau

### 3.2. Découverte du système d'exploitaiton RIOT
- 3.2.0. Introduction
- 3.2.1. Caractéristiques de RIOT
- 3.2.2. Organisation du code source
- 3.2.3. Le système de compilation
- TP3.   First RIOT application
- TP4.   Extending a RIOT application

### 3.3. Architecture RIOT
- 3.3.0. Introduction
- 3.3.1. Le noyau et son initialisation
- 3.3.2. Ordonnancement et threads
- TP5.   Managing threads with RIOT
- 3.3.3. Utilisation des threads
- 3.3.4. La gestion d’énergie

### 3.4. APIs hardware
- 3.4.0. Introduction
- 3.4.1. La couche d’abstraction matérielle
- 3.4.2. Les compteurs
- TP6.   Use timers
- 3.4.3. Interagir avec les GPIO
- 3.4.4. Les pilotes de haut niveau

### 3.5. Evaluation par les pairs de votre application IoT
- 3.5.0 Introduction
- 3.5.1. Présentation de "Peer Evaluation" _(non inclus)_
- 3.5.2. Barème d'évaluation _(non inclus)_
- TP7.   Use sensors on the IoT-LAB M3 board
- 3.5.3. Application RIOT sur FIT IoT-LAB
- 3.5.4. Soumettre votre activité et évaluer vos pairs _(non inclus)_
