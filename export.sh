#!/bin/bash

# Reset and create export directories
rm -rf export/ && mkdir -p  export/html export/html/en export/html/fr;

# Find all course markdown files
for file in $(find Module* -type f -name '*.md' ! -name '*intégral*' ! -name '*quiz*' ! -name '*old_content*' ! -name '*notebook*' ! -name '*storyboard*' | sort -z); do
    # Set export filepath based on language suffix
    if [[ $file == *"FR"* ]];then
      html_file=export/html/fr/$(basename ${file%.*}.html)
    else
      html_file=export/html/en/$(basename ${file%.*}.html)
    fi

    # Convert markdown to html
    pandoc --quiet -s -f gfm+gfm_auto_identifiers -t html --css "../../../style.css" -o $html_file $file && echo "Exported $html_file" || echo "ERROR $file";

    # Remove title/toc link and fix urls
    sed -i 's|<li><a href="#sommaire">Sommaire</a></li>||g' $html_file;
    sed -i 's|<li><a href="#content">Content</a></li>||g' $html_file;
    sed -i "s|<p>TITLE:\(.*\)<br|<b>TITLE:\1</b><br|g" $html_file;
    sed -i "s|Date\(.*\)<br|<i>Date\1</i><br|g" $html_file;
    sed -i "s|<img src=\"\(\S*\)\"|<img src=\"../../../$(dirname $file)/\1\"|g" $html_file;
    sed -i "s|href=\"\(Docs\/\S*\)\"|href=\"../../../$(dirname $file)/\1\"|g" $html_file;
done

# Group html files together
(cd export/html/fr && pandoc --quiet -s -t html -V title:"" --css "../../../style.css" -o cours-complet-FR.html *.html && echo "Exported html_file");
(cd export/html/en && pandoc --quiet -s -t html -V title:"" --css "../../../style.css" -o cours-complet-EN.html *.html && echo "Exported html_file");

# Convert html files to pdf
WKHTML_FR_OPTS=(--pdf-engine-opt="--header-center" --pdf-engine-opt="L'Internet des Objets sur microcontrôleurs par la pratique" --pdf-engine-opt="--footer-left" --pdf-engine-opt="Session 2: Février - Mars, 2021")
WKHTML_EN_OPTS=(--pdf-engine-opt="--header-center" --pdf-engine-opt="Internet of Things with Microcontrollers: a hands-on course" --pdf-engine-opt="--footer-left" --pdf-engine-opt="Session 2: February 1 - March 31, 2021")
WKHTML_HEADER_OPTS=(--pdf-engine-opt="--header-spacing" --pdf-engine-opt="10" --pdf-engine-opt="--header-font-size" --pdf-engine-opt="10")
WKHTML_FOOTER_OPTS=(--pdf-engine-opt="--footer-right" --pdf-engine-opt="[page] / [topage]" --pdf-engine-opt="--footer-spacing" --pdf-engine-opt="10" --pdf-engine-opt="--footer-font-size" --pdf-engine-opt="10")
WKHTML_OPTS=(--pdf-engine-opt="--enable-local-file-access" "${WKHTML_HEADER_OPTS[@]}" "${WKHTML_FOOTER_OPTS[@]}")
(cd export/html/fr && pandoc --quiet -t html -V title:"" --css "../../../style.css" "${WKHTML_OPTS[@]}" "${WKHTML_FR_OPTS[@]}" -o ../../cours-complet-FR.pdf cours-complet-FR.html && echo "Exported pdf_file");
(cd export/html/en && pandoc --quiet -t html -V title:"" --css "../../../style.css" "${WKHTML_OPTS[@]}" "${WKHTML_EN_OPTS[@]}" -o ../../cours-complet-EN.pdf cours-complet-EN.html && echo "Exported pdf_file");
