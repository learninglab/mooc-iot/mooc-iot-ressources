<div class="cover">
<figure>
    <img src="C034AA-image.png"/>
</figure>

<div class="title">
<h1>L'Internet des Objets <br/>sur microcontrôleurs <br/>par la pratique</h1>

<p><b>Session 2: Février - Mars, 2021</b></p>
</div>
<hr/>
<div class="authors">
Alexandre Abadie, <em>Research engineer, Inria, Saclay</em><br/>
Emmanuel Baccelli, <em>Professor, Freie Universität, Berlin </em><br/>
Antoine Gallais, <em>Professor, U. Polytechnique Hauts-de-France </em><br/>
Olivier Gladin, <em>Research engineer, Inria, Saclay</em><br/>
Nathalie Mitton, <em>Researcher, Inria, Lille</em><br/>
Frédéric Saint-Marcel, <em>Research engineer, Inria, Grenoble</em><br/>
Guillaume Schreiner, <em>Research engineer, CNRS, Strasbourg</em><br/>
Julien Vandaële, <em>Research engineer, Inria, Lille</em><br/><br/>
</div>

<div class="bottom">
<em>Cours sur FUN-MOOC produit par Inria Learning Lab.<br/>
Le contenu de ce document est sous Licence CC-BY-NC et extrait de :<br/> <a href="https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/about">https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/about</a></em>
</div>
</div>
