<div class="cover">

<figure>
    <img src="C034AA-image.png"/>
</figure>

<div class="title">
<h1>Internet of Things with Microcontrollers: <br/>a hands-on course</h1>
<p><b>Session 2: February 1 - March 31, 2021</b></p>
</div>

<hr/>

<div class="authors">
Alexandre Abadie, <em>Research engineer, Inria, Saclay</em><br/>
Emmanuel Baccelli, <em>Professor, Freie Universität, Berlin </em><br/>
Antoine Gallais, <em>Professor, U. Polytechnique Hauts-de-France </em><br/>
Olivier Gladin, <em>Research engineer, Inria, Saclay</em><br/>
Nathalie Mitton, <em>Researcher, Inria, Lille</em><br/>
Frédéric Saint-Marcel, <em>Research engineer, Inria, Grenoble</em><br/>
Guillaume Schreiner, <em>Research engineer, CNRS, Strasbourg</em><br/>
Julien Vandaële, <em>Research engineer, Inria, Lille</em><br/><br/>
</div>

<div class="bottom">
<em>Course on FUN-MOOC produced by Inria Learning Lab.<br/>
The contents of this document are provided under Licence CC-BY-NC and extracted from:<br/> <a href="https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/about">https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/about</a></em>
</div>
</div>
