# Se synchroniser pour économiser

Écouter et analyser avant de pouvoir parler, cela coûte cher en énergie. Les objets étant contraints, il faut trouver une solution pour s'économiser.

Pour cela, un mode incluant l'envoi de balises (appelées _Beacons_) a été ajouté au protocole CSMA/CA. Son objectif est de synchroniser les dispositifs avec le coordinateur. Tous les dispositifs deviennent ainsi indépendants, y compris le coordinateur. Ce dernier organise l'accès au canal et le transfert des données en utilisant une structure appelée _Superframe_, qui se répétera en ayant toujours la même taille, et qui se compose d'une période Active et d'une période Inactive (optionnelle).

![The _Superframe_ Structure](Images/superframe.png)

La période Active est subdivisée en 16 _Slots_. Le premier est dédié à la transmission du _Beacon_. Le restant forme 2 périodes qui diffèrent par leur méthode d'accès au médium :
1. une première avec mise en concurrence, la CAP (_Contention Access Period_);
2. une seconde sans mise concurrence, la CFP (_Contention Free Period_).

__Le _Beacon___ contient les spécifications de la _Superframe_ courante, notamment la description et la durée en nombre de _Slots_ des différentes périodes citées précédemment.

__Durant la CAP,__ le réseau fonctionne en mode CSMA/CA classique, chaque dispositif attendra que le médium soit libre pour transmettre, chaque transmission étant alignée sur un début de _Slot_. La transmission sera elle limitée par la taille restante de la CAP. Le dispositif émetteur devra attendre la prochaine _Superframe_ s'il n'a pas pu émettre pendant la CAP. Il en sera de même si le nombre de _Slots_ nécessaires à la transmission était supérieur au nombre de _Slots_ restants.

__Durant la CFP,__ le réseau fonctionne en mode TDMA (_Time Division Multiple Access_). Cette technique de contrôle d'accès au médium utilise une division temporelle de la bande passante et une répartition du temps disponible entre les dispositifs voulant émettre. Le médium va donc être alloué à tour de rôle aux différents émetteurs. Le coordinateur va spécifier dans le _Beacon_ le découpage de la CFP en plusieurs _Slots_ de temps garantis pour un émetteur (les _GTS - Guaranteed Time Slot_).

Enfin, une __période d'inactivité__ radio peut être définie. Elle permet à minima de mettre les composants radio en veille, laissant le temps aux couches supérieures de traiter les données. Dans le cas d'une application collectant des données à faible fréquence, elle permet de mettre le dispositif en veille profonde et d'économiser l'énergie entre deux relevés.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-bookmark"></i> 
<p><b>Le déroulement d'une _Superframe_ peut se résumer ainsi :</b>   
1. Le coordinateur envoie un _Beacon_     
2. Les dispositifs souhaitant émettre vont se me mettre en concurrence durant la CAP, en adoptant le mode CSMA/CA. Ils peuvent émettre des données, mais aussi demander l'allocation d'un GTS dans une _Superframe_ à venir.     
3. Les dispositifs concernés par un GTS, alloué par le coordinateur et communiqué dans le _Beacon_, auront le canal libéré pour émettre ou recevoir des données.     
4. Les dispositifs observent une période d'inactivité.</p>
</div>



En terme d'économie d'énergie, le coordinateur sera actif durant la période Active de la _Superframe_. Les autres dispositifs seront uniquement actifs durant la CAP, s'ils ont quelque chose à transmettre, et/ou durant un GTS, s'ils ont un intervalle assigné.
