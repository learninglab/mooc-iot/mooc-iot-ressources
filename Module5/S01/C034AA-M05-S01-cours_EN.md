# 5.1.1. LPWAN Networks

LPWAN (_Low Power Wide Area Network_) networks, as their name implies, offer low-power communications (about 25 mW) over long distances (from 2 km in an urban environment up to 20 km when there are no obstacles - line of sight -). Of course this "miracle" will take place at the expense of datarate which can go down to a few hundred bits per second, limiting the volume of data exchanged. In Europe, these networks primarily use a frequency range around 868 MHz, which can be deployed without a license, as is the case for Wi-Fi in the 2.4 GHz band.

<figure style="text-align:center">
    <img src="Images/LPWAN_Freq_mapV2.png" alt="" style="width:600px"/><br />
    <figcaption>Fig. 1: Global allocation of frequency bands (source: <a href=https://www.thethingsnetwork.org/docs/lorawan/frequencies-by-country.html>TTN</a>)</figcaption>
</figure>

Unlike Wi-Fi, which is quickly attenuated over distance, frequencies around 868 MHz have an excellent range, good penetration into buildings and hydrometeors insensitivity (rain, snow, ...).

To prevent a user from taking over these frequencies to the detriment of other applications, the regulator imposes a constraint on the transmission time called _duty-cycle_. A transmitter can only transmit a small fraction of the time, usually 1% or even 0.1% of the time. Thus, one second of transmission implies a silence of 99 seconds.
LPWAN networks are therefore not adapted to too frequent or even continuous broadcasts, such as voice or even less video. On the other hand, these networks are well suited for periodic recording of power consumption, the level of liquid in a tank or a garbage can, or even for locating a slow-moving object.

<figure style="text-align:center">
    <img src="Images/LPWAN_duty_cycle.png" alt="" style="width:600px"/><br />
    <figcaption>Fig. 2: Duty Cycle </figcaption>
</figure>


The pioneer for this technology is certainly Sigfox. This Toulouse-based company has defined a 100 bits/s modulation and message formats that can be easily implemented on embedded processors. The Bayard company's fire hydrants are a very good illustration of what can be done with the Sigfox network ([see here](https://www.bayard.fr/nc/produits/nos-produits/produit/page/copernic.html)).
The Copernic module can detect normal or abnormal use of fire hydrants, water leaks and even a collision with a car. The goal is to enable a city to better manage its fleet and save water. Copernicus is aiming for an energy autonomy of 10 years.
The hydrant could have sent SMS messages over a cell phone network, but the autonomy would have been lower. Recharging the hydrants and having teams trained to do so has a significant cost and would affect the sustainability of the service. The operating cost could have been higher than the savings made.

<figure style="text-align:center">
    <img src="Images/LPWAN_Sigfox.png" alt="" style="width:200px"/><br />
    <figcaption>Fig. 3: Sigfox logo</figcaption>
</figure>

Sigfox has been able to develop its own ecosystem. IoT manufacturers (such as fire hydrants) integrate a communication module and Sigfox offers international network coverage. Cities then acquire objects and subscribe to the Sigfox network to receive the information transmitted by their objects then send it to applications that process this data.

LoRa is another technology that has the same properties as Sigfox, but the commercial approach is different. LoRa modulation was also developed in France, at the CEA in Grenoble, and was bought by the American foundry Semtech, which markets chips implementing this modulation.
While Sigfox is in the middle of the value chain and can impose the data exchange format that allows objects and applications to interoperate, Semtech is at the very beginning of the chain and object manufacturers are only provided with the modulation, i.e. a means of transmitting binary information. To avoid any risk of dispersion, making each manufacturer try to impose its own transmission format, Semtech, IBM and Actility founded the LoRa Alliance.
Its goal is to provide a common exchange format that will facilitate interoperability and allow operators to deploy networks collecting data from LoRaWAN objects. In France, Orange and Bouygues Telecom have deployed a national network, but anyone can also deploy their network. Thus the TTN community network (_[The Things Network](https://www.thethingsnetwork.org/)_), that we will be using in this MOOC, offers European coverage as long as you are close to an antenna.
You can also deploy a private network to cover a building or a company. While the LoRa modulation is proprietary, the LoRaWAN protocol specifications are open and available [on LoRa Alliance Web site](https://lora-alliance.org/resource-hub/lorawanr-specification-v101).

What about the operators?
We have seen that some operators are investing in LoRaWAN, but cellular phone companies have also integrated this category of network dedicated to the Internet of Things. When we talk about 4G, 5G, and now 6G, we can imagine increases in speeds, it's true, but there are also advances in LPWAN-type networks.

The international organization for standardization [3GPP](https://www.3gpp.org/about-3gpp) (_3rd Generation Partnership Project_) defines the specifications for cellular phones. Every 10 years or so a new generation is standardized. At mid-term a major update is produced.
Concerning the Internet of Things, the review of the fourth generation has introduced more energy-efficient modes of transmission. The 5G, in its enhanced version, will also provide better object management and drastically reduce the time required to interact with objects, paving the way for communication with robots or autonomous vehicles.
