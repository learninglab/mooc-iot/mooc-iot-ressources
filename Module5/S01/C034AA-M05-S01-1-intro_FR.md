# 5.1.0. Présentation

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence, vous allez découvrir la famille des réseaux LPWAN. Quelles sont les technologies utilisées et leurs caractéristiques ? Quels sont leurs usages pour les applications dans l'Internet des Objets ?
</p>
</div>

Présentation en vidéo des réseaux LPWAN : technologies, spécificités et fonctionnement général.
