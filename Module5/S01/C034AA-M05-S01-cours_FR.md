# 5.1.1. Les réseaux LPWAN

Les réseaux LPWAN (_Low Power Wide Area Network_), comme leur nom l'indique, proposent des communications à faible puissance (environ 25 mW) sur de longues distances (entre 2 km en ville jusqu'à 20 km quand il n'y a pas d'obstacles).

Bien entendu ce "miracle" va se produire au détriment du débit qui peut descendre à quelques centaines de bits par seconde, limitant le volume des données échangées. Ces réseaux utilisent principalement en Europe une gamme de fréquences autour de 868 MHz, qui peuvent être utilisées sans licences, comme pour le Wi-Fi dans la bande des 2.4 GHz.

<figure style="text-align:center">
    <img src="Images/LPWAN_Freq_mapV2.png" alt="" style="width:600px"/><br />
    <figcaption>Fig. 1 : Allocation mondiale des bandes de fréquence (source: <a href=https://www.thethingsnetwork.org/docs/lorawan/frequencies-by-country.html>TTN</a>)</figcaption>
</figure>

Mais contrairement au Wi-Fi, dont les fréquences sont rapidement atténuées par la distance et parfois l'humidité ambiante, les fréquences autour 868 MHz ont une excellente portée, une bonne pénétration dans les bâtiments et une insensibilité aux hydro-météores (pluie, neige,...).

Pour éviter qu'un utilisateur ne les monopolise au détriment des autres applications, le régulateur impose une contrainte sur la durée d'émission appelée _duty-cycle_. Un émetteur ne peut transmettre qu'une petite fraction du temps, généralement 1% voire 0.1% du temps.

Ainsi, une seconde d'émission implique un silence de 99 secondes. Les réseaux LPWAN ne sont donc pas adaptés à des émissions trop fréquentes, voire continues, comme par exemple la voix et encore moins la vidéo. En revanche, ces réseaux sont adaptés pour faire un relevé périodique d'une consommation électrique, du remplissage d'une cuve ou d'une poubelle, voire pour localiser un objet se déplaçant lentement.

<figure style="text-align:center">
    <img src="Images/LPWAN_duty_cycle.png" alt="" style="width:600px"/><br />
    <figcaption>Fig. 2 : Duty Cycle </figcaption>
</figure>


Le pionnier pour cette technologie est certainement Sigfox, cette société Toulousaine a défini une modulation à 100 bits/s et des formats des messages pouvant être facilement mis en oeuvre sur des processeurs embarqués.

Les bornes à incendie de la société Bayard sont une très bonne illustration de ce que l'on peut faire avec le réseau Sigfox ([voir ici](https://www.bayard.fr/nc/produits/nos-produits/produit/page/copernic.html)). Le module Copernic permet de détecter l'utilisation normale ou anormale des bornes à incendie, les fuites d'eau et même un choc provoqué par une collision avec une voiture.

Le but est de permettre à une ville de mieux gérer son parc et d'économiser l'eau. La borne aurait pu émettre des SMS sur un réseau de téléphonie cellulaire, mais l'autonomie aurait été moindre ; Copernic vise une autonomie énergétique de 10 ans. Et recharger les bornes, avoir des équipes entraînées pour le faire a un coût qui n'est pas négligeable et qui nuirait à la pérennité du service ; le coût de gestion aurait pu être supérieur aux économies réalisées.

<figure style="text-align:center">
    <img src="Images/LPWAN_Sigfox.png" alt="" style="width:200px"/><br />
    <figcaption>Fig. 3 : Logo Sigfox</figcaption>
</figure>

Sigfox a su développer son écosystème. Des fabricants d'objets (comme les bornes à incendie) intègrent un module de communication, Sigfox offre un réseau avec une couverture internationale, les villes acquièrent les objets et prennent un abonnement au réseau Sigfox pour recevoir les informations émises par leurs objets et les envoient vers des applications qui font le traitement de ces données.

LoRa est une autre technologie qui possède les mêmes propriétés que Sigfox, mais l'approche commerciale est différente.

La modulation LoRa a été développée également en France dans les locaux du CEA à Grenoble et a été rachetée par le fondeur américain Semtech qui commercialise des puces mettant en oeuvre cette modulation.

Si Sigfox se trouve au milieu de la chaîne de valeur et peut imposer le format d'échange de données permettant aux objets et aux applications d'interopérer, Semtech se trouve en tout début de la chaîne et les fabricants d'objets ne disposent que d'une modulation, c'est-à-dire d'un moyen de transmettre des informations binaires. Pour éviter tout risque de dispersion, faisant que chaque fabricant essaie d'imposer son propre format de transmission, Semtech, IBM et Actility ont fondé la LoRa Alliance. Son but est de fournir un format commun d'échange qui va faciliter l'interopérabilité et permettre à des opérateurs de déployer des réseaux collectant les données émises par les objets LoRaWAN.

En France, Orange et Bouygues Télécom ont déployé un réseau national, mais n'importe qui peut également déployer son réseau. Ainsi le réseau communautaire TTN (_[The Things Network](https://www.thethingsnetwork.org/)_), que nous allons utiliser dans ce MOOC, offre une couverture européenne tant que l'on se trouve près d'une antenne. Vous pouvez également déployer un réseau privé pour couvrir un bâtiment ou une entreprise. Si la modulation LoRa est propriétaire, les spécifications du protocole LoRaWAN sont ouvertes et accessibles [sur le site de la LoRa Alliance](https://lora-alliance.org/resource-hub/lorawanr-specification-v101).

Et les opérateurs ?
Nous avons vu que certains opérateurs investissent dans LoRaWAN, mais la téléphonie cellulaire a également intégré cette catégorie de réseau dédié à l'Internet des Objets. Quand on parle de 4G, 5G, et maintenant de 6G, on imagine des augmentations de débits, c'est vrai, mais il existe aussi des évolutions concernant les réseaux de type LPWAN. L'organisation de standardisation internationale [3GPP](https://www.3gpp.org/about-3gpp) (_3rd Generation Partnership Project_) définit les spécifications concernant la téléphonie cellulaire.

Tous les 10 ans environ une nouvelle génération est standardisée. A mi-parcours une mise à jour majeure est produite. Concernant l'Internet des Objets, la révision de la quatrième génération a introduit des modes de transmission plus économes en énergie. La 5G, dans sa version améliorée, permettra également de mieux gérer les objets et de réduire drastiquement les durées d'interaction avec les objets ouvrant la voie aux communications avec des robots ou des véhicules autonomes.
