# Quiz 14

1. What is the main characteristic of LPWAN networks?

* ( ) They consume little energy but are less far-reaching than IEEE 802.1.5.4 technologies.
* (X) They consume little energy but can be used for distances of several kilometres.
* ( ) They consume much more energy than the IEEE 802.15.4 technology but go much further.
* ( ) They are disturbed by IEEE 802.14.5 communications.

[explanation]
LPWAN stands for Low Power Wide Area Network, i.e. networks with low power consumption with a long range. IEEE 802.15.4 also has low power consumption, but the range is limited to a few metres. IEEE 802.15.4 operates in the 2.4 GHz band and LPWANs mainly in the range of the 868 kHz, there is no interference.
[explanation]

2. 2.	What is the *duty-cycle*?

* ( ) A service bike
* ( ) A means of limiting energy consumption
* ( ) A means of prioritising urgent transmissions
* (X) A means to ensure that all users can send data to the system.
* ( ) It expresses the life of an object between its manufacture, use and destruction

[explanation]
The duty-cycle imposes a silence proportional to the transmission time to allow, during this time, to other equipment to transmit data. It only intervenes at the margin in consumption of energy by restraining equipment that wants to emit too much. Nor can it be used to prioritise a transmitter, as it is shared by all objects using a given frequency.
[explanation]

3. Which parameter, indicating the frequency band, you will have to use during the tests on the Saclay site (near Paris)?

* ( ) AS923
* ( ) AU915
* (X) EU868
* ( ) US915
* ( ) CN470
* ( ) IN865

[explanation]
From Will Wenders, we know « Paris, Texas », where the frequency band would be US915, but after verification, there is no IoTlab site in the vicinity. We only have the range of
of European frequencies EU868.
[explanation]

4. 4.	LoRaWAN can be used for:

* [ ] Streamer WoW live on multiple screens
* [X] Knowing how bins are filled in a city
* [X] Tracking pallets on construction sites
* [ ] Driving autonomous cars on the motorway
* [ ] Controlling traffic lights in a city
* [X] Survey the electricity consumption of dwellings
* [X] Monitor toilet use to inform cleaning teams to intervene after a defined number of door openings.

[explanation]
LoRaWAN and LPWAN networks operating on unlicensed bands in general are more adapted to the sporadic transmission of information, a farewell to video streaming which requires too much time flow, as well as for the control of autonomous cars which should be taken into account by the future versions of the 5G networks and traffic light control, which mainly require transmissions from the network to the object and rather strong time constraints.
[explanation]
