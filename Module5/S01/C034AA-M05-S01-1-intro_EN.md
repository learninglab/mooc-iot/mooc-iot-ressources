# 5.1.0. Presentation

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence, you will discover the LPWAN family of networks. What are the technologies used and their characteristics? How are they used in IoT applications?
</p>
</div>

A video presentation of LPWAN networks: technologies, characteristics and overall operating mode.
