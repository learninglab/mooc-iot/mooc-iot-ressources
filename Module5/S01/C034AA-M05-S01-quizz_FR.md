# Quiz 14

1. Quelle est la principale caractéristique des réseaux LPWAN ?

* ( ) Ils consomment peu d'énergie mais portent moins loin que les technologie IEEE 802.1.5.4
* (X) Ils consomment peu d'énergie mais portent à plusieurs kilomètres
* ( ) Ils consomment beaucoup plus d'énergie que la technologie IEEE 802.15.4 mais portent beaucoup plus loin
* ( ) Ils sont perturbés par les communications IEEE 802.14.5


[explanation]
LPWAN veut dire Low Power Wide Area Network, c'est-à-dire des réseaux à faible consommation
avec une longue portée. IEEE 802.15.4 consomme peu également, mais la portée est limitée à quelques
mètres. IEEE 802.15.4 fonctionne dans la bande des 2.4 GHz et les LPWAN principalement dans la gamme
des 868 kHz, il n'y a pas d'interférence.
[explanation]

2. Qu'est-ce que le *duty-cycle* ?

* ( ) Un vélo de service
* ( ) Un moyen de limiter la consommation d'énergie
* ( ) Un moyen pour prioritiser les transmissions urgentes
* (X) Un moyen pour garantir à tous les utilisateurs de pouvoir émettre des données
* ( ) Il exprime la vie d'un objet entre sa fabrication, son utilisation et sa destruction.

[explanation]
Le duty-cycle impose un silence proportionnel au temps de transmission pour permettre, pendant ce temps à d'autres équipements de transmettre des données. Il n'intervient qu'à la marge dans la consommation d'énergie en bridant un équipement qui voudrait trop émettre. Il ne peut pas non plus servir à prioritiser un émetteur, car il est commun à tous les objets utilisant une fréquence donnée.
[explanation]

3. Quel paramètre, indiquant la bande de fréquences, vous devrez utiliser lors des tests sur le site de Saclay (près de Paris) ?

* ( ) AS923
* ( ) AU915
* (X) EU868
* ( ) US915
* ( ) CN470
* ( ) IN865

[explanation]
Depuis Will Wenders, on connaît « Paris, Texas », où la bande de fréquences serait US915, mais
après vérification, il n'y a pas de site IoTlab aux alentours. Il ne nous reste plus que la gamme de fréquences européennes EU868.
[explanation]

4. On peut utiliser LoRaWAN pour :

* [ ] Streamer WoW live sur plusieurs écrans
* [X] Connaitre le remplissage des poubelles dans une ville
* [X] Suivre des palettes sur des chantiers
* [ ] Piloter des voitures autonomes sur l'autoroute
* [ ] Contrôler des feux tricolores dans une ville
* [X] Relever la consommation électriques d'habitations
* [X] Surveiller l'usage des toilettes pour informer les équipes de nettoyage d'intervenir après un certain nombre d'ouvertures de portes

[explanation]
Les réseaux LoRaWAN et les réseaux LPWAN opérant sur des bandes non licenciées en général, sont
plus adaptés à la remonté sporadique d'information, adieux au streaming vidéo qui demande trop de
débit, de même pour le contrôle des voitures autonomes qui devrait être pris en compte par les
futures versions des réseaux 5G et le contrôle des feux tricolores qui demandent surtout des
transmissions du réseau vers l'objet et des contraintes temporelles assez fortes.
[explanation]
