# 5.3.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence, nous allez découvrir en détails le protocole LoRaWAN : l'infrastructure matérielle sur laquelle il s'appuie, les principes de communication et sa configuration. Puis vous expérimenterez ce protocole avec des activités pratiques.
</p>
</div>

La modulation LoRa est intégrée dans les composants Semtech (et ceux sous licence), mais elle n'est pas suffisante pour fédérer l'ensemble des acteurs. Deux fabricants d'objets, utiliseront certes les mêmes modulations, mais spécifieront leur propre format de messages, rendant l'interoperabilité impossible. Il ne sera pas possible de construire un réseau unique pour tous les objets vu que ceux-ci utiliseront des formats de trame différents, mais également des règles de nommage des objets.

La LoRa Alliance, créée par IBM, Semtech et Actility propose un format de messages, définit des règles d'adressages qui permettront à tous les fabricants d'objets de parler le même langage, favorisant également l'émergence d'opérateurs réseaux et d'applicatifs pour traiter les données.

Présentation en vidéo du protocole LoRaWAN : architecture, comportement et configuration du réseau.
