# 5.3.1. LoRaWAN architecture

Networks, whether Sigfox or LoRaWAN, have very similar architectures. The operator deploys antennas, generally on high points, to cover a given geographical area. These antennas or radio gateways (Radio Gateway) listen to the radio spectrum to discover frames emitted by objects. Processing stops at the physical layer: once the frame has been detected and decoded, it is transmitted unchanged to a central element, called LNS (LoRa Network Server) for LoRaWAN networks. The LNS receives all the frames transmitted in the areas covered by the antennas. Thus an LNS can receive several copies of the same message if it is received by several radio gateways. In the same way, the LNS will receive messages from competing networks since they share the same frequency band.

<figure>
    <img src="Images/LPWAN_Archi.png" alt="" height="400"><br />
    <figcaption>Fig. X : Architecture of a LoRaWAN network.</figcaption>
</figure>
The LNS must therefore eliminate copies of the same message to keep only one, authenticate the objects that are registered on its network to keep only their messages and eliminate the others. The LNS must therefore be configured to authenticate the traffic it manages. It is also essential that it is configured to handle the data and to which application to send it.

Note that this architecture differs from what is done with cellular telephony. In these networks, including their extensions for the IoT, the object is driven by the network. It is attached to a radio relay (node B) which will indicate its transmission times and the frequencies it can use over time. In networks operating in unlicensed bands such as LoRaWAN, this control by the network is almost impossible since the constraints linked to duty-cycles drastically limit the downlink messages.
