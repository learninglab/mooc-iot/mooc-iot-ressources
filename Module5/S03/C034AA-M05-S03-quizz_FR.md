# Quiz 16

1. Qu'est ce qu'un réseau de type Aloha ?

* ( ) un réseau qui n'existe que dans l'ouest de l'Amérique
* ( ) un réseau filaire qui relie des îles entre elles
* (x) un réseau où les émetteurs transmettent quand ils veulent
* ( ) un réseau où un tiki contrôle les instants d'émissions


[explanation]
Un réseau de type Aloha n'impose aucune contrainte aux équipements qui peuvent émettre quand ils le veulent, mais si deux émissions ont lieu en même temps, les messages sont perdus. Dommage qu'il n'y ait pas de Tiki (êtres surnaturels de la mythologie maori) car sinon, en contrôlant les instants d'émission ont pourrait arriver à utiliser la capacité du réseau à 100%.
[explanation]

2. Qu'est ce que l'effet capture ?

* ( ) Quand une station détecte une transmission puissante, elle arrête son émission
* ( ) Quand les données binaires d'un message se retrouvent dans un autre
* (X) Quand le message reçu avec le plus de puissance peut quand même être décodé
* ( ) Quand on reçoit les données d'un autre opérateur LPWAN

[explanation]
L'effet capture est quand le message de plus forte puissance est décodé, alors que les messages de plus faible puissance transmis en même temps ne le sont pas. La collision n'intervient que pour ces derniers. Il est impossible de retrouver des données binaires provenant d'un autre message, car les codes détecteurs d'erreur détecteraient des incohérences et rejetteraient la trame. Dans un réseau LoRaWAN, les différents opérateurs travaillant dans la même bande de fréquences, reçoivent les données des autres opérateurs et les rejettent si les adresses ne correspondent pas. Un équipement ne peut pas détecter qu'un autre transmet car le signal est trop atténué.
[explanation]

3. Que nous dit la théorie sur Aloha ?

* [X] On ne peut pas dépasser la limite de 18% d'utilisation
* [X] Il ne faut pas que les émissions soient synchronisées
* [ ] La capacité du réseau augmente naturellement avec le nombre d'équipement
* [ ] Plus il y a d'équipement plus il faut baisser la puissance d'émission

[explanation]
La théorie trouve une limite d'utilisation de 18%. Au dessus de cette limite, le nombre de collision augmente et donc le nombre de messages correctement transmis diminue. Si les messages sont synchronisés, une collision peut se produire plus facilement, au contraire il faut éviter à tout prix les synchronisations en utilisant le plus de variables aléatoires (durée des temporisateurs, fréquences utilisées,...).
[explanation]

4. Combien y a-t-il de LNS ?

* ( ) Un par passerelle radio
* (x) Un par opérateur
* ( ) Un par pays
* ( ) Un seul pour le monde entier, c'est TTN

[explanation]
Chaque opérateur dispose de son LNS dans lequel il enregistre les objets de ses clients.
Il recevra tous les messages émis par les objets dans sa zone de couverture et rejettera
ceux qui ne sont pas connus.
[explanation]

5. Que fait un LNS qui reçoit le message _join_ d'un objet inconnu ?

* ( ) il émet un message d'erreur
* ( ) il émet un accept
* (x) il fait rien
* ( ) il envoie un drone pour le détruire

[explanation]
Le sens _downlink_ étant critique, il ne faut pas l'utiliser pour émettre des messages d'erreur. Il doit juste servir pour aider à la configuration des objets connus. Quant au drone, bien que LoRa puisse trouver à quelques dizaines de mètres la position d'un objet par triangulation, cela poserait quelques problèmes juridiques.
[explanation]

6. Devez-vous faire acquitter les messages de données d'un objet par le LNS ?

* ( ) Oui, comme ca on est sur que le message est bien reçu
* ( ) Oui, comme ça l'objet sait que le LNS est actif
* ( ) Oui, ca permet de trouver la meilleure fréquence et la meilleure puissance pour émettre
* (X) Non

[explanation]
Si vous avez répondu oui, vous allez faire exploser le duty-cycle de votre passerelle radio.
[explanation]

7. Quels sont les paramètres que vous devez configurer dans le LNS pour recevoir des données ?

* [ ] La fréquence sur laquelle l'objet va émettre le _join_
* [X] Le devEUI
* [X] La clé secrète de l'objet
* [X] L'endroit où envoyer les données reçues
* [ ] L'algorithme de calcul du MIC
* [ ] Les passerelles radio qui vont recevoir les messages

[explanation]
Le LNS a besoin du devEUI pour identifier l'objet et la clé secrète doit également être partagée pour construire des clés symétriques de chiffrement du côté de l'objet et du LNS. Enfin, il faut indiquer où le LNS doit pousser les données pour qu'elles soient traitées. Les fréquences et l'algorithme de MIC sont précisés par le standard. Comme toutes les passerelles radios envoient les données vers le LNS, il n'est pas nécessaire de les préciser.
[explanation]
