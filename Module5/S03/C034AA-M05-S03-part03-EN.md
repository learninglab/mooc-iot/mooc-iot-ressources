# 5.3.3. Objects identification

Each LoRaWAN object has an address, or rather an identifier called **devEUI**, theoretically unique in the world. It is based on the format defined by the IEEE that you may have encountered in IEEE 802.15.4 frames. LoRaWAN also uses this 64-bit representation. The big difference is that in LoRaWAN networks, the devEUI is only used at the start of the object when it attaches to the network: a temporary, shorter, 4-byte address, called **devAddr**, will be used in subsequent frames.

In most cases, this value is set in the object by its manufacturer, but it is also possible to configure it when compiling the code that will run on the object.

Objects are also configured with another 64-bit identifier whose use has evolved over time. Initially, this value, called **appEUI**, was used to identify the applications running on the object and to define different cryptographic keys for each application.
Usage evolved towards an identification of the owner of the object (**joinEUI**) to allow him to access several LoRaWAN networks, for example, when an object moves from one country to another.

A third element that must remain secret is the **appKey**, a 128-bit sequence known only to the object and the LNS, following the principle of symmetrical cryptography.

<figure>
    <img src="Images/LPWAN_TTN.png" alt="" height="400"><br />
    <figcaption>Fig. 1: Object registered on TTN. In the first part the parameters of the object devEUI, appEUI and app key (hidden) are displayed; in the second part the information obtained from the network (devaddr, and two keys for the session).</figcaption>
</figure>

The procedure of attachment to the network begins with the transmission of a **Join** message containing the two previously mentioned identifiers (**devEUI**, **appEUI / joinEUI**) and a random number. This message is sent in clear text on the LoRaWAN network, but contains at the end an authentication field (MIC: _Message Integrity Check_) calculated from the appKey.

All the LNS of the different operators receive it. The devEUI allows them to determine whether or not the object belongs to their network. If the object is recognized, the LNS in turn derives a random sequence of 3 bytes that allows it to derive a key for the duration of the session at the network (**netSkey**) and application (**appSkey**) level. The first is used for message authentication and the second for encrypting the information contained in the data messages.

The object receives 6 seconds after transmission a **Accept** message from the LNS that recognized it. This duration is arbitrarily long to allow a first exchange via satellite links. This message is encrypted with the appkey and can only be understood by the object that sent the attached message. The object with the same information as the LNS can perform the same calculation to determine the temporary netSkey and appSkey keys.

<figure>
    <img src="Images/LPWAN_join.png" alt="" height="200"><br />
    <figcaption>Fig. 2: Connection to the LoRaWAN network. Note that the response can be made at two moments: if the object does not receive it after 5 seconds in its frequency, it will listen one second later to the default channel.</figcaption>
</figure>

This exchange phase is called in the LoRaWAN OTAA (_Over The Air Authentication_) jargon. It is optional and it is possible to set up the LNS directly with the devAddr and session keys. In this case it is called _Activation By Personalization_ (ABP).

Once the encryption keys and the temporary address are known, the object can start transmitting. There are three classes of objects:

- class A which is the most energy efficient. The object emits its data and listens to the network to see if there is an answer. Then the object falls asleep until the next transmission. In this mode, it is therefore only possible to contact the object when it has emitted data. If the LNS has to transmit data to the object after the standby period, it will store the information until it receives a new message from the object.

- Class C on the opposite consumes much more energy because the object is constantly listening to the network to detect transmissions. The LNS does not delay transmissions to the object. It is often reserved for objects plugged on main power.

- Class B is a specific mode where the LNS periodically sends a synchronization frame (Tag or _Beacon_) that allows objects to synchronize.

<figure>
    <img src="Images/LPWAN_format_trames.png" alt="" height="400"><br />
    <figcaption>Fig. 3: Format of the different frames defined by LoRaWAN</figcaption>
</figure>
