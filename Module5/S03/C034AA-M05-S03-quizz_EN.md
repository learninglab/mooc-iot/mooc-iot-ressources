# Quiz 16

1. What is an Aloha-type network?

* ( ) A network that exists only in western America
* ( ) A wired network linking islands together
* (x) A network where transmitters transmit when they want to
* ( ) A network in which a tiki controls the time of the broadcasts


[explanation]
An Aloha-type network imposes no constraints on equipment that can broadcast whenever they want, but if two broadcasts take place at the same time, the messages are lost. Too bad there are no Tiki (supernatural beings from Maori mythology) because otherwise, by controlling the transmission times we could manage to use the network's capacity at 100%.
[explanation]

2. What is the capture effect?

* ( ) When a station detects a powerful transmission, it stops its transmission
* ( ) When the binary data in one message is found in another messager
* (X) When the message received with the most power can still be decoded
* ( )  When receiving data from another LPWAN operator

[explanation]
The capture effect is when the higher-powered message is decoded, while the messages of lower power transmitted at the same time are not. The collision only occurs in order to the latter. It is impossible to retrieve binary data from another message, because the error-detecting codes would detect inconsistencies and reject the frame. In a LoRaWAN network, the different operators working in the same frequency band receive the data of other operators and reject them if the addresses do not match. One device cannot detect that another is transmitting because the signal is too attenuated.
[explanation]

3. What does the theory on Aloha tell us?

* [X] We cannot exceed the 18% limit of use
* [X] Transmissions must not be synchronised
* [ ] The capacity of the network naturally increases with the number of equipments
* [ ] The more equipment there is, the more the transmission power has to be lowered

[explanation]
The theory finds a limit of use of 18%. Above this limit, the number of collisions increases and therefore the number of message correctly transmitted decreases. If the messages are synchronised, a collision can occur more easily, on the contrary, synchronisation should be avoided at all costs using as many random variables as possible (duration of timers, frequencies used, etc.).
[explanation]

4. How many LNS are there?

* ( ) One per radio gateway
* (x) One per operator
* ( ) One per country
* ( ) Only one for the whole world, it is TTN

[explanation]
Each operator has its own LNS in which it registers its customers' objects. It will receive all the messages sent by the objects in its coverage area and will reject those who are not known.
[explanation]

5. What does an LNS do when it receives a _join_ message from an unknown object?

* ( ) It issues an error message
* ( ) It issues an accept
* (x) It does nothing
* ( ) It sends a drone to destroy it

[explanation]
The _downlink_ direction being critical, it should not be used to issue error messages. It should just be used to help configuring known objects. As for the UAV, although LoRa can find the position of an object a few dozen meters away by triangulation, this would pose some legal problems.
[explanation]

6. Does the LNS have to acknowledge the data received from an object?

* ( ) Yes, that way we are sure that the message is well received
* ( ) Yes, so the object knows that the LNS is active
* ( ) Yes, it allows to find the best frequency and the best power to transmit
* (X) No

[explanation]
If you answered yes, you will blow up the duty-cycle of your radio gateway.
[explanation]

7. What are the parameters you need to configure in the LNS to receive data from the LNS?

* [ ] The frequency at which the object will emit the _join_
* [X] The devEUI
* [X] The secret key of the object
* [X] Where to send the received data
* [ ] The algorithm for calculating the MIC
* [ ] The radio gateways that will receive the messages

[explanation]
The LNS needs the devEUI to identify the object and the secret key must also be shared to build symmetrical encryption keys on the object and LNS sides. Finally, the LNS must indicate where the LNS must push the data to be processed. The frequencies and the MIC algorithm are specified by the standard. As all radio gateways send data to the LNS, there is no need to specify them.
[explanation]
