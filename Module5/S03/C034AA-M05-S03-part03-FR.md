# 5.3.3. Identification des objets

Chaque objet LoRaWAN dispose d'une adresse, ou plutôt un identifiant appelé **devEUI**, en théorie unique dans le monde. Il se base sur le format défini par l'IEEE que vous avez pu rencontrer dans les trames IEEE 802.15.4. LoRaWAN utilise également cette représentation sur 64 bits. La grande différence est que dans les réseaux LoRaWAN, le devEUI n'est utilisé qu'au démarrage de l'objet quand celui-ci s'attache au réseau ; une adresse temporaire, plus courte, sur 4 octets, appelée **devAddr**, sera utilisée dans les trames suivantes.

Dans la plupart des cas, cette valeur est inscrite dans l'objet par son fabricant, mais il est également possible de la configurer lors de la compilation du code qui tournera sur l'objet.

Les objets sont également configurés avec un autre identifiant de 64 bits dont l'usage a évolué au cours du temps. Initialement, cette valeur appelée **appEUI**, permettait d'identifier les applications tournant sur l'objet et de définir des clés cryptographiques différentes pour chaque application. L'usage évolue vers une identification du propriétaire de l'objet (**joinEUI**) pour lui permettre d'accéder à plusieurs réseaux LoRaWAN, comme par exemple, quand un objet se déplace d'un pays à un autre.

Un troisième élément qui doit rester secret est l'**appKey**, une séquence de 128 bits connue uniquement de l'objet et du LNS, suivant le principe de la cryptographie symétrique.

<figure>
    <img src="Images/LPWAN_TTN.png" alt="" height="400"><br />
    <figcaption>Fig. 1 : Objet enregistré sur TTN, dans la première partie sont affichés les paramètes de l'objet devEUI, appEUI et app key (masqué), dans la seconde partie les informations obtenues du réseau (devaddr, et deux clés pour la session)</figcaption>
</figure>

La procédure d'attachement au réseau commence par l'émission d'un message **Join** qui contient les deux identifiants précédemment cités (**devEUI**, **appEUI / joinEUI**) et un nombre aléatoire. Ce message est émis en clair sur le réseau LoRaWAN, mais contient à la fin un champ d'authentification (MIC: _Message Integrity Check_) calculé à partir de l'appKey.

Tous les LNS des différents opérateurs le reçoivent. Le devEUI leur permet de déterminer si l'objet appartient ou non à leur réseau. Dans le cas où l'objet est reconnu, le LNS tire à son tour une séquence aléatoire de 3 octets qui lui permet de dériver une clé pour la durée de la session au niveau réseau (**netSkey**) et applicatif (**appSkey**). La première sert pour l'authentification des messages et la seconde pour chiffrer les informations contenues dans les messages de données.

L'objet reçoit 6 secondes après l'émission un message **Accept** du LNS qui l'a reconnu. Cette durée est arbitrairement longue pour permettre un premier échange via des liaisons satellites. Ce message est chiffré avec l'appkey et ne peut être compris que de l'objet ayant émis le message join. L'objet disposant des mêmes informations que le LNS peut effectuer le même calcul pour déterminer les clés temporaires netSkey et appSkey.

<figure>
    <img src="Images/LPWAN_join.png" alt="" height="200"><br />
    <figcaption>Fig. 2 : Connexion au réseau LoRaWAN, a noter que la réponse peut se faire à deux instants, si l'objet ne la reçoit pas au bout de 5 secondes dans sa fréquence, il va écouter une seconde plus tard le canal par défaut.</figcaption>
</figure>

Cette phase d'échange est appelée dans le jargon LoRaWAN OTAA (_Over The Air Authentication_), elle est facultative et il est possible de paramétrer le LNS directement avec les devAddr et les clés de sessions. Dans ce cas on parle d'_Activation By Personalization_ (ABP).

Une fois les clés de chiffrement et l'adresse temporaire connues, l'objet peut commencer à émettre. Il existe trois classes d’objets :

- la classe A qui est la plus économe en énergie ; l'objet émet sa donnée et écoute le réseau pour voir s'il y a une réponse. Ensuite l'objet s'endort jusqu'à la prochaine émission. Dans ce mode, il n'est donc possible de contacter l'objet que quand celui-ci a émis des données. Si le LNS doit lui transmettre des données après la période de veille, il va mémoriser l'information jusqu'à réception d'un nouveau message de l'objet.

- la classe C au contraire consomme beaucoup plus d'énergie car l'objet écoute en permanence le réseau pour détecter des transmissions, le LNS ne retarde pas les transmissions vers l'objet. Elle est souvent réservée à des objets alimentés sur secteur.

- la classe B est un mode particulier où le LNS envoie périodiquement une trame de synchronisation (Balise ou _Beacon_) qui permet aux objets de se synchroniser.

<figure>
    <img src="Images/LPWAN_format_trames.png" alt="" height="400"><br />
    <figcaption>Fig. 3 : Format des différentes trames définies par LoRaWAN</figcaption>
</figure>
