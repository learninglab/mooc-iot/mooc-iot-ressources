# 5.3.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence, we will discover in detail the LoRaWAN protocol: the infrastructure on which it is based, the principles of communication and its configuration. Then you will experiment this protocol with practical activities.
</p>
</div>

The LoRa modulation is implemented in Semtech components (and those under license), but it is not enough to federate all the actors. Two object manufacturers will certainly use the same modulations, but will specify their own message format, making interoperability impossible. It will not be possible to build a single network for all objects since they will use different frame formats, but also their own object naming rules.

The LoRa Alliance, created by IBM, Semtech and Actility, proposes a message format and defines addressing rules that will allow all object manufacturers to speak the same language. This fosters the emergence of network operators and applications to process data.

A video presentation of LoRaWAN protocol: architecture, behavior and network configuration.
