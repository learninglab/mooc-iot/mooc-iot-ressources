# 5.3.4. Conclusion

In 2021, cellular telephone networks are lagging behind their counterparts operating in unlicensed frequencies, both in terms of component cost and energy autonomy. The 4G integrates LTE-M and NB IoT upgrades to reduce power consumption and increase object density. The 5G pursues in this direction by better controlling object transmissions.

In the future, everyone can have their own usage profile (as Bluetooth and WiFi respond to different use cases). Licensed frequency networks target applications requiring a high quality of service and unlicensed frequency networks target very low cost applications.
