# 5.3.4. Conclusion

En 2021, les réseaux de téléphonie cellulaire sont en retard par rapport à leurs homologues opérant dans les fréquences non licenciées, aussi bien en termes de coût des composants que sur l'autonomie énergétique. La 4G intègre des évolutions LTE-M et NB IoT pour réduire les consommations et augmenter la densité des objets. La 5G poursuit dans cette direction en contrôlant mieux les transmissions des objets.

A l'avenir, chacun peut avoir son profil d'utilisation (comme Bluetooth et WiFi répondent à différents cas d'usage). Les réseaux sur fréquences licenciées ciblant des applications demandant une forte qualité de service et les réseaux sur fréquences non-licenciées des applications à très faible coût.
