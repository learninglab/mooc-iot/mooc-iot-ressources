# 5.3.1. Architecture du réseau

Les réseaux, qu'ils soient Sigfox ou LoRaWAN, ont des architectures très similaires. L'opérateur déploie des antennes, généralement sur des points hauts, pour couvrir une zone géographique donnée. Ces antennes ou passerelles radio (Radio Gateway) écoutent le spectre radio pour y découvrir des trames émises par les objets. Le traitement s'arrête à la couche physique ; une fois que la trame a été détectée et décodée, elle est transmise sans modification à un élément central, appelé LNS (LoRa Network Server) pour les réseaux LoRaWAN. Le LNS reçoit l'ensemble des trames émises dans les zones couvertes par les antennes. Ainsi un LNS peut recevoir plusieurs copies d'un même message si plusieurs passerelles radio le captent. De même le LNS va recevoir les messages des réseaux concurrents puisque ceux-ci partagent la même bande de fréquence.

<figure>
    <img src="Images/LPWAN_Archi.png" alt="" height="400"><br />
    <figcaption>Fig. X : Architecture d'un réseau LoRaWAN.</figcaption>
</figure>
Le LNS doit donc éliminer les copies d'un même message pour n'en garder qu'une, authentifier les objets qui sont inscrits à son réseau pour ne garder que leurs messages et éliminer les autres. Le LNS doit par conséquent être configuré pour authentifier les trafics qu’il gère. Il est également indispensable qu'il soit informé de ce qu'il doit faire des données ; il faut donc aussi le configurer pour lui indiquer vers quelle application les envoyer.

A noter que cette architecture diffère de ce qui est fait en téléphonie cellulaire. Dans ces réseaux, y compris dans leurs extensions pour l'IoT, l'objet est piloté par le réseau. Il s'attache à un relais radio (node B) qui va lui indiquer ses instants d'émission et les fréquences qu'il peut utiliser au cours du temps. Dans les réseaux opérant dans des bandes non licenciées comme LoRaWAN, ce contrôle par le réseau est quasiment impossible puisque les contraintes liées aux duty-cycles limitent drastiquement les messages descendants.
