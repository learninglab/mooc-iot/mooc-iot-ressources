# Module 5. Focus on LoRaWAN Networks

Objective: At the end of this module, you will have an expertise on LoRaWAN networks, specifically designed for the Internet of Things. You will become familiar with their main characteristics, the types of applications they are particularly suited for and will learn how to configure them so that the devices can join the network.

Hands-on activities (TP):
TP14: Getting started with TTN
TP15: Send sensor data on a LoRaWAN network

## Contents of Module 5

### 5.1. Introduction to LPWAN Networks

### 5.2. LoRA Modulation

### 5.3. The LoRaWAN Protocol
- 5.3.0 Introduction
- 5.3.1 LoRaWAN architecture
- 5.3.2 Aloha
- 5.3.3 Objets Identification
- 5.3.4 Conclusion
- TP14: Getting started with TTN
- TP15: Send sensor data on a LoRaWAN network

## Thanks

Thanks to Xavier Lagrange and Alexandre Marquet for all the explanations about the
physical layer.
