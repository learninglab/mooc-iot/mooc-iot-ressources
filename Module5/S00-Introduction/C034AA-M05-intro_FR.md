# Module 5. Focus on LoRaWAN Networks

Objectif : À la fin de ce module, vous aurez une expertise des réseaux LoRaWAN,
ces réseaux spécifiquement définis pour l'Internet des Objets. Vous connaîtrez
leurs caractéristiques principales, à quels types d'applications ils sont
particulièrement adaptés et finalement comment les configurer pour y connecter
un objet.

Activités pratiques (TP) :
TP14: Connecter un objet à un réseau
TP15: Produire du trafic

## Sommaire du Module 5

### 5.1. Introduction aux LPWAN

### 5.2. La modulation LoRa

### 5.3. Le protocole LoRaWAN
- 5.3.0 Introduction
- 5.3.1 Architecture du réseau
- 5.3.2 Aloha
- 5.3.3 Identification des objets
- 5.3.4 Conclusion
- TP14: Getting started with TTN
- TP15: Send sensor data on a LoRaWAN network

## Remerciements

A Xavier Lagrange et Alexandre Marquet pour toutes les explications sur la
couche physique.
