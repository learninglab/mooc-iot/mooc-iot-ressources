# 5.2. La modulation LoRa

La modulation LoRa utilise une technique utilisée dans les radars (et par les chauves-souris). Il s'agit d'envoyer un signal dont la fréquence augmente (ou diminue) pendant une durée donnée. Le signal sur cette durée est appelé un chirp. La forme de ce signal a de bonnes propriétés :

- résistance aux [trajets multiples](https://fr.wikipedia.org/wiki/Multipath) produit quand le signal rebondit sur des obstacles et arrive en plusieurs copies décalées dans le temps au récepteur
- résistance correcte à l'[effet Doppler](https://fr.wikipedia.org/wiki/Effet_Doppler) modifiant la fréquence quand l'émetteur se déplace.

La bande passante d'un canal LoRa va délimiter la différence entre la fréquence la plus faible et la plus élevée. Typiquement LoRa utilise une bande passante de 125 kHz, mais il est possible également d'utiliser des plages plus grandes de 250 kHz voire 500 kHz. Si l'on se place dans la bande des 868 MHz, cela permet à LoRa de définir 3 canaux principaux centrés à 868.1 MHz, 868.3 MHz et 868.5 MHz. Quand on émet, chaque chirp voit sa fréquence varier, par exemple, entre 868.0375 MHz et 868.1625 MHz.     

Le temps pour passer entre ces deux fréquences est relié au facteur d'étalement ou Spreading Factor. A chaque fois que la valeur est augmentée de 1, ce temps est doublé.

Une transmission LoRa commence par 8 symboles avec des fréquences montantes (dit __upchirps__) suivis de 2.25 symboles avec des fréquences descendantes (dit __downchirps__) pour indiquer un début de transmissions. Comment sont codées les données ?

<figure style="text-align:center">
    <img src="Images/LPWAN_Physical.png" alt="" height="400"><br />
    <figcaption>Fig. 1 : Capture d'une trame LoRa (crédit Alexandre Marquet)<br/>
    Ce schéma représente une capture de signal radio. En haut, le graphique gris indique la puissance du signal à différentes fréquences (centrées sur 868 Mhz sur l'image). Cette courbe évolue avec le temps. Dans la partie bleue de l'image, le graphique (appelé waterfall) défile vers le bas. Il montre l'évolution du signal au cours du temps.  Une couleur spécifique indique la puissance du signal à une fréquence donnée. Dans le schéma, la ligne jaune représente la modulation LoRaWAN, et la partie bleue représente le bruit. Les chirps sont facilement reconnaissables. A droite du signal, les cases jaunes indiquent les champs dans la trame LoRaWAN.</figcaption><br/>
</figure>

Cette suite de symboles a permis à l'émetteur et aux récepteurs de se synchroniser. La donnée binaire va être codée en décalant le symbole, il ne va pas commencer par la fréquence basse pour arriver à la fréquence haute, mais pour chaque valeur binaire va commencer à une fréquence donnée pour arriver à la fréquence haute et repartir de la fréquence basse. On peut aussi considérer que l'instant où l'on passe de la fréquence haute à la fréquence basse code la valeur binaire à transmettre. Le récepteur peut facilement repérer cet instant en s'appuyant sur l'évolution de la fréquence tout au long du symbole.

<figure>
    <img src="Images/LPWAN_chirps.png" alt="" height="400"><br />
    <figcaption>Fig. 2 : Codage par chirps. Le bloc du haut représente la modulation en amplitudes et le bloc du bas en fréquences. La figure du haut de chaque bloc représente un signal avec une fréquence constante, ce qui se traduit par une droite dans la réprésentation fréquencielle. La figure du milieu représente deux up_chirp qui se traduisent par une droite dans la figure du bas, puisque la fréquence augmente régulièrement. La figure du bas représente un symbole, si la transition de fréquence ne se voit pas clairement dans la partie modulée, elle est évidente lors du passage dans le plan fréquence (crédit Xavier Lagrange). </figcaption>
</figure>

Un symbole qui, en LoRa, prend la forme d'un chirp, va coder plusieurs bits. Par convention, un Spreading Factor va coder SF bits ; un SF de 7 permettra à chaque symbole de coder 7 bits, il devra donc y avoir 2^7 symboles possibles. Pour un SF de 8, il faudra 2^8 symboles, la durée du symbole est doublée, donc le récepteur aura plus de chance de la trouver dans le bruit ambiant. On voit également que, comme les pentes des symboles sont différentes entre 2 SF, ceux-ci sont considérés comme orthogonaux (le codage n'interfère pas), c'est-à-dire qu'un récepteur pourra faire la différence entre deux transmissions utilisant des SF différents.
<figure>
    <img src="Images/LPWAN_SF.png" alt="" height="400"><br />
    <figcaption>Fig. 3 : Capture d'une trame LoRa de 50 octets à différents SF.</figcaption>
</figure>
