# Quiz 15

1. Que veux dire SF ?

* (x) Spreading Factor: Durée pendant laquelle un symbole est transmis
* ( ) Sigfox
* ( ) Single Frequency: émission sur uen fréquence unique du message
* ( ) Sender First: l'objet émet et attend une réponse du réseau


[explanation]
Puisque l'on parle de Sigfox, que l'on n'abrège pas en SF, l'objet va émettre sur une bande toute petite (Ultra Narrow Band) qui pourrait correspondre à l'acronyme Single Frequency. Mais on associe SF à Spreading Factor. Il s'agit de la durée pour transmettre un symbole.
[explanation]

2. Si dans votre programme vous changez le paramètre SF7 par SF12, vous allez:

* [ ] rien changer
* [x] être plus sûr que votre message va être reçu
* [ ] diminuer les temps de transmission en codant plus de bits par symbole
* [x] réduire l'autonomie de votre objet.
* [ ] brouiller potentiellement des équipements dans une zone plus importante.

[explanation]
Passer de SF7 à SF12 va multiplier par 32 la durée d'émission, en contre partie, le récepteur a plus de chance de détecter le message, s'appuyant sur un signal cohérent plus long. Cela va par contre demander plus d'énergie pour transmettre la même quantité d'information, l'autonomie de votre objet sera donc réduite. Par contre, cela ne change pas la puissance de transmission et donc ne pas augmenter la zone de brouillage potentiel.
[explanation]

3. Pourquoi deux SF différents sont-ils orthogonaux ?

* (X) Parce que l'on peut recevoir en même temps des signaux émis dans différents SF,
* ( ) Parce que la phase est décalée de 20° entre deux SF,
* ( ) Parce que le codage des bits est orthogonal dans un corps de Galois à 5 dimensions,
* ( ) Parce que les antennes sont orientées différemment

[explanation]
L'orthogonalité n'a rien de physique, elle indique la possibilité de recevoir des signaux qui vont peu se perturber.
[explanation]
