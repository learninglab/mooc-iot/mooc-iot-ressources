# Quiz 15

1. What does SF mean?

* (x) Spreading Factor: The length of time a symbol is transmitted
* ( ) Sigfox
* ( ) Single Frequency: transmission on a single frequency of the message
* ( ) Sender First: the object transmits and waits for a response from the network


[explanation]
Since we are talking about Sigfox, which is not abbreviated to SF, the object will transmit on a very small band (Ultra Narrow Band) which could correspond to the acronym Single Frequency. But we associate SF with Spreading Factor, it is the time to transmit a symbol.
[explanation]

2. If in your programme you change the parameter SF7 to SF12, you will:

* [ ] Change nothing
* [x] Be more confident that your message will be received
* [ ] Reduce transmission times by encoding more bits per symbol
* [x] Reduce the autonomy of your object
* [ ] Potentially jamming equipment in a larger area

[explanation]
Switching from SF7 to SF12 will multiply the transmission time by 32, while the receiver has a better chance of detecting the message, relying on a longer coherent signal. This on the other hand, will require more energy to transmit the same amount of information, autonomy of your object will therefore be reduced. On the other hand, this does not change the transmission power and so do not increase the area of potential interference.
[explanation]

3. Why are two different SF orthogonal?

* (X) Because it is possible to receive signals transmitted in different FS at the same time
* ( ) Because the phase is shifted by 20° between two SF
* ( ) Because the bit coding is orthogonal in a 5-dimensional Galois body
* ( ) Because the antennas are oriented differently

[explanation]
Orthogonality is not physical, it indicates the possibility of receiving signals that will can be disturbed.
[explanation]
