# 5.2. LoRa modulation

LoRa modulation uses a technique used in radar (and by bats). It consists of sending a signal whose frequency increases (or decreases) for a given period of time. The signal over this time is called a chirp. The shape of this signal has good properties:

- Robustness to the [multipaths](https://en.wikipedia.org/wiki/Multipath_propagation) produced when the signal bounces off obstacles and arrives in several time-shifted copies at the receiver
- Quite good [Doppler effect](https://en.wikipedia.org/wiki/Doppler_effect) robustness.

The bandwidth of a LoRa channel is the difference between the lowest and highest frequency. Typically LoRa uses a bandwidth of 125 kHz, but it is also possible to use larger ranges of 250 kHz or even 500 kHz.
In the 868 MHz band, LoRa defines 3 main channels centered at 868.1 MHz, 868.3 MHz and 868.5 MHz. When transmitting, each chirp sees its frequency vary, for example, between 868.0375 MHz and 868.1525 MHz.      

The time it takes to switch between these two frequencies is related to the Spreading Factor. Each time the value is increased by 1, this time is doubled.

A LoRa transmission starts with 8 symbols with increasing frequencies (known as __upchirps__) followed by 2.25 symbols with decreasing frequencies (known as __downchirps__) which indicate the start of transmissions. How is the data encoded?  

<figure style="text-align:center">
    <img src="Images/LPWAN_Physical.png" alt="" height="400"><br />
    <figcaption>Fig. 1: Capture of a LoRa frame (copyright Alexandre Marquet)<br/>
    This figure represents a radio signal capture. On top, the grey graphic indicates the signal power at different frequencies (centered on 868 Mhz on the picture). This curve evolves with time. In the blue part of the picture, the graphic (called a waterfall) is scrolling down. It shows the signal evolution during time. A specific color indicates the signal power at a given frequency. In the picture, the yellow line represents the LoRaWAN modulation, and the blue part reflects the noise. The chirps are easily recognizable. On the right-hand side, yellow boxes represent the fields in a LoRaWAN frame.
    </figcaption><br/>
</figure>

This sequence of symbols allows the transmitter and receivers to synchronize. The binary data is coded by shifting the symbol. It will not start from the low frequency to reach the high frequency, but for each binary value it will start at a given frequency to reach the high frequency and start again from the low frequency. We can also consider that the time at which we switch from the high frequency to the low frequency codes the binary value to be transmitted. The receiver can easily locate this time, based on the evolution of the frequency throughout the symbol.

<figure>
    <img src="Images/LPWAN_chirps.png" alt="" height="400"><br />
    <figcaption>Fig. 2: Chirp coding. The upper block represents the modulation in amplitudes and the lower block in frequencies. The figure at the top of each block represents a signal with a constant frequency, which is expressed as a straight line in the frequency representation. The middle figure represents two up_chirp which result in a straight line in the lower figure, since the frequency increases regularly. The lower figure represents a symbol. If the frequency transition is not clearly seen in the modulated part, it is obvious when passing through the frequency plane. (copyright Xavier Lagrange). </figcaption>
</figure>

A symbol which, in LoRa, takes the form of a chirp, codes several bits. By convention, a Spreading Factor codes SF bits; an SF of 7 will allow each symbol to code 7 bits, so there should be 2^7 possible symbols. For a SF of 8, 2^8 symbols will be needed, the duration of the symbol is doubled, so the receiver will have more chance to find it in the ambient noise. We can also see that since the slopes of the symbols are different between 2 SFs, they are considered orthogonal (the coding will not interfere), i.e. a receiver will be able to tell the difference between two transmissions using different SFs.
<figure>
    <img src="Images/LPWAN_SF.png" alt="" height="400"><br />
    <figcaption>Fig. 3: Capture of a 50-byte LoRa frame with different SFs.</figcaption>
</figure>
