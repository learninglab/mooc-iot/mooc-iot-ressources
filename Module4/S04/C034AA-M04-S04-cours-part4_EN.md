
# 4.4.4. Resource discovery

In an IoT context where machines can interact with other machines (M2M), devices must be able to discover the resources exposed by others. The main function of such a discovery mechanism is to provide URIs (called links) for the resources hosted by the server, complemented by attributes about these resources and possible other link relationships. A well-known URI **/.well-known/core** is defined as the default entry point for requesting the list of links to resources hosted by a server. This link list is represented with a compact and extensible "Core Link" format that is specified in ABNF (Augmented Backus-Naur Form) notation. Thus the client can send a GET request to the server on this URI and in return receive the list of links to available resources such as a temperature or brightness sensor.

Here is an example of a request whose response contains two sensor resources:

- **REQ:**

      GET /.well-known/core

- **RES:**

      2.05 Content
      </sensors/temp>;rt="temperature-c";if="sensor",
      </sensors/light>;rt="light-lux";if="sensor"

In the answer we can see that a description of multiple resources is separated by a comma. There are also attributes that describe
information useful for accessing the link such as **rt** (Resource type) which identifies the service and **if** (Interface) which contains a generic definition of the service.

Then you can access the temperature sensor resource in the following way:

- **REQ:**

      GET /sensors/temp

- **RES:**

      2.05 Content
      22.5 C
