
# 4.4.4. Découverte de ressources

Dans un contexte IoT où des machines peuvent entrer en interaction avec d'autres machines (M2M), les dispositifs doivent être en mesure de découvrir les ressources exposées par les autres. La fonction principale d'un tel mécanisme de découverte est de fournir des URI (appelés liens) pour les ressources hébergées par le serveur, complétés par des attributs concernant ces ressources et d'éventuelles autres relations de liens. Une URI bien connu **/.well-known/core** est défini comme point d'entrée par défaut pour demander la liste des liens sur les ressources hébergées par un serveur. Cette liste de liens est représentée avec un format compact et extensible "Core Link" qui est spécifié en notation ABNF (Augmented Backus-Naur Form). Ainsi le client peut envoyer une requête GET au serveur sur cette URI et en retour recevoir la liste de liens vers des ressources disponibles comme un capteur de température ou de luminosité.

Voici un exemple de requête avec une réponse qui contient deux ressources de capteurs:

- **REQ:**

      GET /.well-known/core

- **RES:**

      2.05 Content
      </sensors/temp>;rt="temperature-c";if="sensor",
      </sensors/light>;rt="light-lux";if="sensor"

Dans la réponse on peut voir qu'une description de ressources multiples est séparée par une virgule. On retrouve aussi des attributs qui décrivent
des informations utiles pour accéder au lien comme **rt** (Resource type) qui identifie le service et **if** (Interface) qui contient une définition générique du service.

Ensuite on peut donc accéder à la ressource capteur de température de la façon suivante:

- **REQ:**

      GET /sensors/temp

- **RES:**

      2.05 Content
      22.5 C
