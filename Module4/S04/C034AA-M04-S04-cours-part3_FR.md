
# 4.4.3. Exemples d'interactions

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [4.4.3a Message non confirmable](#443a-message-non-confirmable)
- [4.4.3b Message confirmable](#443b-message-confirmable)
- [4.4.3c Réponse asynchrone](#443c-réponse-asynchrone)
<!-- TOC END -->

## 4.4.3a Message non confirmable

Un message qui ne nécessite pas de transmission fiable, sans aucune garantie de bonne réception,  peut être envoyé comme message non confirmable (NON). On peut citer comme exemple une mesure de capteur de température régulière sur une longue période, où la non réception d'un message n'étant pas critique et où il est possible d'attendre la prochaine requête. Dans ce cas, les messages ne sont pas acquittés, mais conservent l'information de Token pour la détection de doublons. La figure 3 illustre ce scénario.

<img src="Images/figure5.png" width="233" height="212" />

Figure 3: message non confirmable

## 4.4.3b Message confirmable

La figure 4 contient un exemple basique de message de type confirmable (CON). Un client demande, par une requête GET, la température sur le serveur. Le serveur répond immédiatement, avec une réponse ACK, la valeur lue par son capteur. On peut noter deux points importants :

* La requête (CON) et l’acquittement (ACK) ont le même “Message ID”. Cela permet l’implémentation d’un mécanisme de fiabilité avec le client qui peut retransmettre un message confirmable identique jusqu’à ce qu’il reçoive l’acquittement correspondant ou qu’il excède une limite maximum de tentatives (MAX_RETRANSMIT qui vaut 4 par défaut). L’intervalle entre les retransmissions croît exponentiellement en fonction du nombre de tentatives échouées.  
* Le message d’acquittement contient aussi les données de la réponse avec la valeur de la température. CoAP utilise la technique le [piggybacking](https://en.wikipedia.org/wiki/Piggybacking_(data_transmission)) pour optimiser les transferts et réduire la bande passante.

<img src="Images/figure3.png" width="225" height="220" />

Figure 4 : Requête GET avec un message confirmable

## 4.4.3c Réponse asynchrone

Si le serveur n’est pas en mesure de répondre immédiatement à une requête, il peut envoyer un simple acquittement au client pour signaler qu’il a bien reçu sa demande et qu’il essaye d’obtenir la réponse. Une fois qu’il aura les données nécessaires, il les enverra au client dans un message contenant le même token. De cette manière, le client peut identifier la requête d’origine et interpréter correctement la réponse. La figure 5 illustre ce cas. La réponse contenant la valeur demandée étant un message confirmable, le client renvoie un acquittement après sa réception.

<img src="Images/figure4.png" width="223" height="335" />

Figure 5: message confirmable avec acquittement vide

On peut aussi ajouter que, dans le cas d'un serveur qui n'est pas en mesure de traiter un message, il peut répondre par un message de réinitialisation (RST). Le message de réinitialisation doit simplement contenir lui aussi le Message ID pour la détection de duplication.
