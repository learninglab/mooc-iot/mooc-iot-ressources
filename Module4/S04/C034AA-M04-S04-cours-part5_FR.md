
# 4.4.5. Observation de ressources

Une extension intéressante à CoAP permet d’observer les changements d’état d’un serveur dans le temps. Cela se fait par un mécanisme de “souscription”, à la manière du patron de conception [Observer](https://en.wikipedia.org/wiki/Observer_pattern):

1.  le client envoie au serveur une requête du type GET sur l'URI qui l'intéresse en ajoutant une option "Observe";
2.  le serveur répond en indiquant l’état actuel de la ressource et enregistre l’abonnement. À chaque changement d’état de la ressource, une notification sera envoyée aux clients enregistrés pour celle-ci.

<img src="Images/figure6.png" width="373" height="388" />

Figure 6 : Observation de ressources

L'option "Observe" est ajoutée pour la souscription, mais également pour la notification de valeur. Dans ce dernier cas, une valeur est affectée à l'option par le serveur pour indiquer l'ordre d'émission au(x) client(s).

Cette extension permet, d’un côté, d’optimiser l’utilisation de la bande passante, vu que les multiples requêtes ne sont plus nécessaires, et de l’autre, la possibilité de n’envoyer des messages qu'à une période donnée et/ou lors d'un changement significatif de valeur, rendant possible une sensible économie d’énergie côté serveur.
