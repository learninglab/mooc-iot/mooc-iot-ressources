# 4.4.1. Architecture du protocole CoAP

CoAP s'appuie sur un modèle client-serveur, comme HTTP, où les clients envoient des requêtes sur des ressources exposées par le serveur et identifiées par des URIs. Au contraire de HTTP, CoAP fonctionne lui de manière asynchrone en s'appuyant sur des datagrammes UDP.

<figure style="text-align:center">
    <img src="Images/figure1.png" alt="Architecture CoAP"><br />
    <figcaption>Figure 1: Architecture CoAP</figcaption>
</figure>

Il s'appuie sur deux couches :

* une couche "Message" pour gérer la fiabilité et le séquencement des échanges de bout en bout ;
* une couche "Requests/Responses" pour gérer les méthodes et les codes réponses dans les interactions de type requête-réponse.

La couche "Message" va gérer la fiabilité et le séquencement en traitant dans les messages, leur identification, mais aussi les correspondances entre requêtes et réponses. Ceci se fera en indiquant/lisant les Message ID et les jetons présents dans les entêtes de message.

La couche "Requests/Responses" va, elle, s'appuyer sur une codification des requêtes et des réponses. Pour les requêtes, elle reposera sur les méthodes utilisées par HTTP :

  - `GET` pour récupérer la représentation d'une ressource;
  - `POST` pour la création d'une nouvelle ressource ou sa mise à jour;
  - `PUT` pour la mise à jour de la ressource avec la représentation jointe;
  - `DELETE` pour la suppression d'une ressource.

Les réponses vont elles aussi être codifiées à la manière d'HTTP :

  - `2.xx` "Success" pour une requête correctement reçue, comprise et acceptée;
  - `4.xx` "Client error" pour indiquer une erreur côté client;
  - `5.xx` "Server error" pour indiquer une erreur côté serveur.
