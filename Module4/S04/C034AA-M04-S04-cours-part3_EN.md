
# 4.4.3. Examples of interactions

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [4.4.3a Non-confirmable messages](#443a-non-confirmable-messages)
- [4.4.3b Confirmable messages](#443b-confirmable-messages)
- [4.4.3c Asynchronous requests](#443c-asynchronous-requests)
<!-- TOC END -->

## 4.4.3a Non-confirmable messages

A message that does not require reliable transmission, without any guarantee of good reception, may be sent as a Non-Confirmable Message (NON). An example is a regular temperature sensor measurement over a long period of time, where the non-receipt of a message is not critical and it is possible to wait for the next request. In this case, messages are not acknowledged, but retain token information for duplicate detection. Figure 3 illustrates this scenario.

<img src="Images/figure5.png" width="233" height="212" />

Figure 3: a non-confirmable message

## 4.4.3b Confirmable messages

Figure 4 contains a basic example of a confirmable message (CON). A client requests the temperature on the server with a GET request. The server responds immediately with an ACK response, the value read by its sensor. There are two important points to note:

* The request (CON) and the acknowledgement (ACK) have the same "Message ID". This allows the implementation of a reliability mechanism with the client which can retransmit an identical confirmable message until it receives the corresponding acknowledgment or until it exceeds a maximum limit of attempts (MAX_RETRANSMIT which is 4 by default). The interval between retransmissions increases exponentially with the number of failed attempts.
* The acknowledgement message also contains the response data with the temperature value. CoAP uses the [piggybacking](https://en.wikipedia.org/wiki/Piggybacking_(data_transmission)) technique to optimize transfers and reduce bandwidth.


<img src="Images/figure3.png" width="225" height="220" />

Figure 4: A GET request with a confirmable message

## 4.4.3c Asynchronous requests

If the server is not able to respond immediately to a request, it can send a simple acknowledgement to the client to indicate that it has received the request and is trying to get a response. Once it has the necessary data, it will send it to the client in a message containing the same token. In this way, the client can identify the original request and interpret the response correctly. Figure 5 illustrates this case. Since the response containing the requested value is a confirmable message, the client returns an acknowledgement after receiving it.

<img src="Images/figure4.png" width="223" height="335" />

Figure 5: confirmable messages with empty acknowledgements

We can also add that, in the case of a server that is unable to process a message, it can respond with a reset message (RST). The reset message must simply also contain the Message ID for duplication detection.
