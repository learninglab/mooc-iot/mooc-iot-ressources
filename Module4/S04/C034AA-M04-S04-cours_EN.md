# 4.4.0. Introduction

The CoAP constrained application protocol is a web protocol standardized by the IETF (RFC 7252). It is designed for IoT applications with resource-constrained devices (CPU, memory, battery) connected through rate-limited radio links, such as in LRWPAN networks. It is a protocol that uses many of the terms and concepts of the HTTP protocol with the Core model (Constrained RESTful Environments). It is based on standard Web models with a request-response interaction principle, uniform interfaces allowing interoperability and resources addressable by URIs.


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>In this sequence we will describe the architecture of the CoAP protocol and the format messages take, before giving some examples of interactions.
</p>
</div>


## References
* The Constrained Application Protocol (CoAP) - https://tools.ietf.org/html/rfc7252
* Constrained RESTful Environments (CoRE) Link Format - https://tools.ietf.org/html/rfc6690
* Observing Resources in CoAP - https://tools.ietf.org/html/draft-ietf-core-observe-08
* Block-Wise Transfers in the Constrained Application Protocol - https://tools.ietf.org/html/rfc7959
* Datagram Transport Layer Security - https://tools.ietf.org/html/rfc6347
* Code register in CoAP messages - https://www.iana.org/assignments/core-parameters/core-parameters.xhtml#response-codes
