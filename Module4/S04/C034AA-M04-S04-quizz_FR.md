# Question 4.4.1

Sur quelle pile protocolaire le protocole CoAP peut-il fonctionner ?

[] TCP
[X] UDP
[X] IPv6 sur Ethernet
[X] IPv4 sur Ethernet
[X] 6LowPAN sur 802.15.4

[explanation]
CoAP est un protocole qui a été écrit pour fonctionner sur le protocole UDP. Par exemple dans la couche message des mécanismes ont été mis en place pour répondre à la non garantie de réception d'un paquet (message CON) alors que ce service est fournit par défaut dans le protocole TCP. Ensuite il est possible de faire fonctionner le protocole UDP sur IPv4 et IPV6 en ethernet comme en sans-fil avec 6LowPAN.
[explanation]

# Question 4.4.2

Quelle partie du message CoAP est utilisée pour faire correspondre une requête et une réponse ?

() Une option
() Le code
() Le message ID
(X) Le token

[explanation] Un jeton (token) est utilisé pour associer une requête à une réponse indépendamment des messages qui les transportent. Le Token est indépendant de l’identificateur de message (message ID). Le code sert à determiner si le message est une requête, une réponse ou un message vide. Les options peuvent être utilisées par exemple pour la découverte de ressources.
[explanation]


# Question 4.4.3

Quel type de message peut intervenir dans un échange de messages confirmable (CON) ?

[X] Acquittement
[X] Réinitialisation
[X] Confirmable
[] Non Confirmable

[explanation] Le seul qui ne peut pas intervenir est le Non confirmable. Dans le cas d'un message confirmable l'acquittement est envoyé par le serveur pour prévenir le client de la
bonne réception et le message de réinitialisation si le serveur n'est pas en mesure de répondre directement.
[explanation]

# Question 4.4.4

Comment peut-on découvrir les ressources d'un serveur CoAP ?

[X] Avec une requête GET
[] En utilisant l'option Observe
[X] Avec une URI réservée
[] Avec une requête POST

[explanation] Pour découvrir les resources d'un serveur CoAP le client doit executer une requête GET avec l'URI well-known core et obtenir une réponse de type Core Link.[explanation]

# Question 4.4.5

Quel(s) mécanisme(s) permet(tent) au client d'être notifié du changement d'état d'une ressource ?

[] Le block-wise transfert
[X] L'option Observe
[] Une requête PUT
[X] Le pattern Observer

[explanation] Un client pour être notifié du changement d'état d'une resource doit souscrire auprès du serveur selon le pattern Observer. Pour cela il ajoute dans sa requête
GET une option Observe. Le block-wise transfert permet de transférer des données de tailles importantes en segmentant les requêtes.[explanation]

# Question 4.4.6

Donner la réponse (uniqement la liste des ressources) de la requête CoAP découverte de ressources (`/.well-known/core`) sur le client.

= </riot/board>,</riot/cpu>,</temperature>,</value>
