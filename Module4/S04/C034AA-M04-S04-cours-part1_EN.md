# 4.4.1. The architecture of the CoAP protocol

CoAP is based on a client-server model, such as HTTP, where clients send requests on resources exposed by the server and identified by URIs. Unlike HTTP, CoAP works asynchronously based on UDP datagrams.

<figure style="text-align:center">
    <img src="Images/figure1.png" alt="CoAP Architecture"><br />
    <figcaption>Figure 1: CoAP Architecture</figcaption>
</figure>

It is based on two layers:

* a "Message" layer to manage the reliability and sequencing of end-to-end exchanges;
* a "Requests/Responses" layer to manage the methods and response codes in request-response type interactions.

The “Message” layer controls reliability and sequencing by processing identification in messages, but also the correspondence between requests and responses. This will be done by indicating/reading the Message IDs and tokens present in the message headers.

The “Requests/Responses” layer is based on a method of codification for requests and responses. For requests, it's based on the methods used by HTTP:

  - ‘GET’ to obtain the representation of a resource
  - ‘POST’ to create a new resource or to update an existing resource
  - ‘PUT’ to update the resource with the attached representation
  - ‘DELETE’ to delete a resource

Responses are also codified in the same way as in HTTP:

  - ‘2.xx’ “Success” for a request that has been correctly received, understood and accepted
  - ‘4.xx’ “Client error” to indicate an error on the client side
  - ‘5.xx’ “Server error” to indicate an error on the server side
