
# 4.4.5. Resource observation

A useful extension to CoAP allows you to observe changes in the state of a server over time. This is done through a "subscription" mechanism, like the [Observer] design pattern (https://en.wikipedia.org/wiki/Observer_pattern):


1. the client sends to the server a GET request on the URI it is interested, adding the option “Observe”
2. the server replies with the current status of the resource and registers the subscription. Each time the status of the resource changes, a notification will be sent to the clients registered for the resource.

<img src="Images/figure6.png" width="373" height="388" />

Figure 6: Resource observation

The "Observe" option is added for subscription, but also for value notification. In the latter case, a value is assigned to the option by the server in order to indicate the order for transmitting to clients.

This extension makes it possible, on the one hand, to optimize the use of bandwidth, since multiple requests are no longer necessary, and, on the other hand, the possibility of sending messages only at a given period and/or when there is a significant change in value, making it possible to make significant energy savings on the server side.