
# 4.4.2. Le format des messages CoAP

Comme nous venons de le voir, en plus de l'information à transmettre, les messages CoAP doivent contenir un ensemble d'informations relatives aux deux couches décrites précédemment. Les messages sont encodés dans un format binaire simple, avec une structure bien définie : un message commence par un entête fixe de 4 octets (soit 32 bits), suivi par un champ "Token" de taille variable comprise entre 0 et 8 octets, puis un champ pour les options, et enfin les données qui occupent le reste du datagramme, précédées par un séparateur d'un octet contenant la valeur "111111111".

<img src="Images/figure2.png" width="538" height="180" />

Figure 2: Format d'un message CoAP

De manière détaillée, un message CoAP contient les informations suivantes :
* **Version (Ver)** (2 bits) : indique le numéro de version de CoAP utilisé (`01` par défaut).
* **Type (T)** (2 bits) : indique le type du message dans le contexte d'une requête ou d'une réponse.
    - Requête :
      + 0 : ___Confirmable (CON)___ : Ce message attend un message d'accusé de réception correspondant, appelé aussi acquittement ou _acknowledgement_ en anglais.
      + 1 : ___Non-confirmable (NON)___ : Ce message n’attend pas de message d'acquittement.
    - Réponse :
      + 2 : ___Acknowledgement (ACK)___ : Ce message est une réponse qui acquitte un message confirmable.
      + 3 : ___Reset (RST)___ : Ce message indique la réception d'un message qui ne peut pas être traité.
* **Token Length (TKL)** (4 bits) : indique la longueur du champ Token, qui peut varier de 0 à 8 octets.
* **Code** (8 bits) : indique (avec le format X.xx: 3-bits class et 5-bits détail) si le message est une requête, une réponse ou un message vide. Dans le cas d’une requête, indique également la méthode utilisée (GET, POST, PUT ou DELETE) ; pour une réponse, contient un code qui permet d’identifier la nature de la réponse. À la différence de HTTP il est codé en binaire dans le message.
* **Message ID** (16 bits) : numéro de série du message, utilisé pour détecter les duplications de message et pour faire la correspondance entre un message de type ACK/RST avec son message CON/NON initial.
* **Token** (0 - 8 octets): champ dont la valeur est générée par le client, et dont la taille est indiqué par le champ TKL, permettant de faire correspondre une requête et la réponse dans le cas par exemple d'un acquittement vide qui précède une réponse ultérieure.
* **Options** : contient des informations supplémentaires liées ou non au type du message. C'est l'équivalent des entêtes HTTP avec par exemple le paramètre “Max-Age” pour définir la durée de validité des données transmises.
* **Payload** : contient les données transmises par l’application (le marqueur du début de payload est 0xFF). La taille du payload ne doit pas excéder 1024 octets mais un mécanisme de transfert de bloc de données CoAP appelé "Block-Wise" permet l'envoi de différents fragments considérés chacun comme un message.
