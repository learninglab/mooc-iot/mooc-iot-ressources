# 4.4.0. Introduction

Au cours des différentes parties de cette séquence, nous allons décrire l'architecture du protocole CoAP et le format des messages puis présenter des exemples d'interactions par requête-réponse. Ensuite, nous ferons la démonstration d'une installation CoAP sur la plateforme d'expérimentation FIT IoT-LAB.

Le protocole d'application contraint CoAP est un protocole Web normalisé par l'IETF (RFC 7252). Il est conçu pour les applications IoT avec des dispositifs à ressources contraintes (CPU, mémoire, batterie) et reliées au travers de liens radio limités en débit, comme dans les réseaux LRWPAN.
C'est un protocole qui reprend beaucoup de termes et concepts du protocole HTTP avec le modèle Core (Constrained RESTful Environments). Il est basé sur des modèles standards du Web avec un principe d'interaction par requête-réponse, des interfaces uniformes permettant l'interopérabilité et des ressources adressables par des URIs.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>A la fin de cette séquence, vous saurez utiliser le protocole CoAP.
</p>
</div>

Présentation en vidéo du protocole IoT le plus utilisé : CoAP (Constrained Application Protocol), le protocole optimisé pour réseaux contraints.

## Références
* The Constrained Application Protocol (CoAP) - https://tools.ietf.org/html/rfc7252
* Constrained RESTful Environments (CoRE) Link Format - https://tools.ietf.org/html/rfc6690
* Observing Resources in CoAP - https://tools.ietf.org/html/draft-ietf-core-observe-08
* Block-Wise Transfers in the Constrained Application Protocol - https://tools.ietf.org/html/rfc7959
* Datagram Transport Layer Security - https://tools.ietf.org/html/rfc6347
* Registre des codes dans le message CoAP - https://www.iana.org/assignments/core-parameters/core-parameters.xhtml#response-codes
