# 4.1.0. Introduction

Dans cette séquence, nous allons présenter les réseaux sans fils à l'échelle dite personnelle (Wireless Personal Area Networks, WPAN) et en particulier ceux à faible débit (Low Rate WPAN ou LRWPAN).

Pour ces réseaux LRWPAN dans le contexte de l'Internet des objets, nous allons étudier :
- leurs topologies, c'est à dire les schémas d'interconnexions possibles entre objets communicants,
- leurs contraintes de communication (limitation d'énergie, perte de connexion) et caractéristiques attendues (portée, débit, latence, fiabilité, etc.),
- et le modèle OSI (Open System Interconnection) qui est le modèle de référence pour l'interconnexion des systèmes ouverts à la communication avec d’autres systèmes et qui permet de décrire tout système de communication.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>A la fin de cette séquence, vous saurez spécifier une architecture de réseaux sans fils et ses protocoles de communication : quel canal de communication radio utiliser, comment partager ce moyen de communication entre les différents objets sans fil et quels protocoles de routage et de transport de données utiliser.
</p>
</div>

Présentation en vidéo des réseaux basse consommation WPAN : architecture, protocoles de communication existants et contraintes associées.
