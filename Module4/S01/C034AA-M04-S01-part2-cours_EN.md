# 4.1.2. The OSI / IETF model

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [4.1.2a Network architecture structure](#412a-network-architecture-structure)
- [4.1.2b Separating preoccupations into layers](#412b-separating-preoccupations-into-layers)
- [4.1.2c The OSI model](#412c-the-osi-model)
- [4.1.2d The IETF IoT protocol stack](#412d-the-ietf-iot-protocol-stack)
<!-- TOC END -->

We are going to take a look at a network architecture based on layer models and how the specificities of the IoT are taken into account.

## 4.1.2a Network architecture structure

In order to implement an IoT network, there are certain requirements in terms of connectivity,
data delivery, information transport and application security and reliability
that must be met. The variety of hardware, the diversity
of radio environments, the complexity of multi-hop topologies and the needs
of the vast array of possible applications on these networks have to be taken
into account. Such preoccupations are nothing new - indeed, they were
addressed while the internet as we know it today was being created
and as it evolved.

Let’s take a look back at the traditional approaches used to solve the range of problems posed. We will outline how these models are employed in the context of IoT networks.

A WPAN is comprised of a set of appliances communicating wirelessly
around an individual person’s workspace. We will focus here on the structure
of the communication protocols found in connected objects forming WPANs.

Reliable wireless communication and appropriate data collection are
needed in order to supply end applications. To that end, the following
characteristics must be carefully examined: target appliances produce
relatively low volumes of data periodically. They use
low-capacity batteries and limited ranges for wireless
communication, given the main constraints regarding energy consumption and design.

There are a range of different communication protocols, which enable objects to exchange,
structure and validate data. Generally speaking,
communication between such systems requires various different mechanisms
being set up in order to enable information (e.g. web pages) to be converted into
the electrical signals used for transmission (over cables or via radio links).
These include information coding; shared environment
management; synchronizing, detecting and correcting errors;
identifying objects (addressing); establishing links and communication
routes; transporting information; security, etc.

## 4.1.2b The OSI model

The issue of network organisation prompted the standardisation of a communication
model based on so-called layered architectures. The OSI (Open System Interconnection)
model was the first to have been proposed, by
the ISO (the International Standardisation Organisation). It constitutes the basic
reference model for interconnecting systems open to
communication with other systems. It describes the architecture of network
communications, the goal being to identify the main functions linked to
communication and to sort these into layers:

<figure>
<img src="Images/C034AA-M04-S01.png" height="303" />
<figcaption>OSI model.</figcaption>
</figure>

The OSI model also provides abstraction principles through
concepts such as layers, interfaces, services and protocols. A layer thus
presents a set of system calls and functions in a program (interface).
It defines the functions it is responsible for (services) using primitives
(commands or events). It can also be used to define the communication
protocol relating to this layer (the format, the signification for packets/messages
exchanged, etc.). This protocol is a set of rules that can be used to establish and
maintain communication at this level (or layer). A data layer K
will use the service provided by the layer K-1, as described by the
latter’s interface. In return, the data used by a layer K will only have any
meaning (i.e. becoming information) for the upper layer K+1. This
data encapsulation is accompanied by the addition of a header specific to each layer.
This mechanism is illustrated below:

<figure>
<img src="Images/osi-encaps_EN.png" height="303" />
<figcaption>Encapsulation according to the OSI model.</figcaption>
</figure>


In this way, in a full architecture for communication between open systems,
the mechanisms to be implemented range from physical-level protocols (synchronization,
coding data into signals and vice versa, handling data bits)
to application protocols (e.g. HTTP), in addition to routing
protocols (where the packet is the unit of information used) or
session protocols (e.g. transmission security).

In the standard systems we use on a daily basis, a distinction is made between, for
example:

- communication devices and their drivers (roughly corresponding to the physical and link layers)
- the operating system (in which you will find the mechanisms specific to the transport and network layers)
- the applications where the application, presentation and sessions level functions can be found

The OSI model is a layered structure designed to connect open
systems together. This is an ideal, theoretical stack, where each layer is
responsible for a set of preoccupations. A data layer enables the one
above it to use its service and to implement its own service. The role of the
data link layer, for example, is to establish the radio vicinity the routing layer
will use to construct the routing topology. The OSI model
can be used to write an entire communication system. However, existing
systems (and the protocols they use) won’t necessarily contain
an equivalent for each layer of the OSI model. It should be noted that this does not define any
protocol.


## 4.1.2c The IETF IoT protocol stack

The majority of communication protocols have been standardised by the IEEE
(the Institute of Electrical and Electronics Engineers) and the IEFT (the Internet
Engineering Task Force). While the first covers the physical layer and a
sub-section of the link layer, the second is responsible for everything found
“above the wire (of the communication medium, strictly speaking - Ed.)
and below the application”.

In the context of the IoT, communication takes place via wireless
links. These employ various types of technology (e.g. Wi-Fi,
Bluetooth, ZigBee, LoRa, Sigfox). The radio frequencies used are either separate
or shared, meaning the radio environment has to be well managed
as a shared resource. Different transmission schemes are used,
enabling wireless communication across a range of propagation environments.
What this means is that a large number of radio chips are available for building connected
objects. We will focus here on the physical layer defined by
study group 4, group 15, committee 802 (i.e. the IEEE 802.15.4 standard),
which is used by the majority of available IoT objects.


The protocols embedded into a system will depend on its constraints
on it and the use to which it is put. For example, in order to ensure
that IoT deployments are able to operate long-term, appliances must
use as little energy as possible. A significant amount of research
and engineering in the context of the IoT has focused on energy
efficiency. Given that radio chips use up the most energy, their use
must be limited. Appliances alternate between inactive and active
modes. They only communicate where necessary, while limiting the amount of time
they listen to the medium for ongoing transmissions from other nodes. As a
result, thorough medium access control is needed
(also known as MAC). Excessive activity results in huge amounts of radio noise,
interference, etc., while periods of inappropriate activity can
lead to the topology becoming disconnected and the nodes becoming isolated. Standards such as
IEEE 802.15.4 provide solutions for this type of coordination, enabling
each appliance to discover which objects in its environment
it will be able to communicate with. Together, these components form a whole that is equivalent to the physical
and link layers found in the OSI model.

Based on this logical topology, hardware devices must find a
way of sharing their data with receiver stations. Given the
limited radio range, this is done through paths made up of
wireless links. The right decisions have to be taken in order to select the
neighbouring node that will be the next hop on this path. This object will be tasked with
relaying the data packet to its own next hop towards its destination.
Once again, a whole host of research and engineering projects have focused on
energy-efficient routing in IoT networks. Recent standards such as
RPL can be used to build routing topologies in difficult
radio environments. These establish the functions of the routing layer.

Once it is possible to transport the data from one point to another in the
network, the issue of the reliability of end-to-end communication must be tackled.
The goal is to prevent the loss of packets or the desynchronization of data
packets in order to ensure relevant and efficient data collection. In
the internet that we use everyday, transport protocols such as
TCP are used to apply these properties. In IoT networks, the
constraints of embedded equipment require adapted solutions,
a common example of which is the use of the UDP transport protocol. In reality, these don’t provide
the upper layer protocol with any guarantees that a message will be delivered.

On the whole, the desired reliability will depend on the needs and the requirements of
the application, which can vary considerably. For remote healthcare
or home monitoring applications, for example,
the data that is collected has to be highly reliable. With
devices such as smart lights or watches, meanwhile,
less attention is required.

We have listed the main preoccupations to factor in when
assessing the architecture and protocols for low energy-consumption WPANs: the
radio medium and the method used to share it between the wireless appliances;
routing; transport; and the final application. These problems are separate,
and so are dealt with separately.

The IETF (the Internet Engineering Task Force) is made up of working groups
tasked with identifying specific solutions for each layer. In the context of the IoT,
for example, the proposed structure supposes the use of IEEE 802.15.4 for
the physical layers and MAC. The IEFT also provides higher level
protocols for routing, transport and application. The global stack
is typical of what you would find in an open connected object.

<figure style="text-align:center">
    <img src="Images/C034AA-M04-S04-1.png" alt="IETF protocol stack"><br />
    <figcaption>IETF protocol stack</figcaption><br />
</figure>
