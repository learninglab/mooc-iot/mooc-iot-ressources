# 4.1.0. Introduction

In this sequence, we will introduce Wireless Personal Area Networks (WPAN), with a particular focus on low-rate WPANs or LRWPANs.

For these LRWPANs in the context of the Internet of Things, we will be studying:
- their topologies, i.e. the possible interconnections between communicating objects
- their communication constraints (energy limitations, loss of connection) and expected characteristics (range, rate/speed, latency, reliability, etc.)
- the OSI (Open System Interconnection) model, which is the reference model for interconnecting systems open to communicating with other systems, and which can be used to write an entire communication system

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>By the end of this sequence, you will know how to specify a wireless network architecture and its communication protocols: which radio communication channel to use, how to share this communication medium between different wireless objects and which routing and data transport protocols to use.
</p>
</div>

A video presentation on low energy-consumption WPANs: their architecture, existing communication protocols and associated constraints.
