# 4.1.1. Topologies, constraints and objectives

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [4.1.1a Topologies](#411a-topologies)
- [4.1.1b Constraints](#411b-constraints)
- [4.1.1c Objectives](#411c-objectives)
<!-- TOC END -->

## 4.1.1a Topologies

Wireless networks, whether those used initially for
mobile communications or those used more recently with technology such as Bluetooth or
Wi-Fi, all share a common point: they rely on a centralised infrastructure. Objects communicate with each other via access points,
which are responsible for organising pairing, resource allocation and
a range of other essential functions (e.g. energy efficiency, security).

These are known as star topologies, which are widely used
in standards dealing with wireless local area networks
(WLANs). It's the default case for Wi-Fi, (the IEEE 802.11 standard), with an access point and objects linked to it:

<figure>
    <img src="Images/wifi-star-topology_EN.png" alt="" width="300"><br />
    <figcaption>Fig. : Star topology</figcaption><br />
</figure>


Other types of network topologies exist, one of which is
mesh topology. In this case, there are links
between each pair of nodes:

<figure>
    <img src="Images/wireless-full-mesh_2.png" alt="" width="300"><br />
    <figcaption>Fig. : Full mesh topology</figcaption><br />
</figure>


or partially, i.e. only certain links actually exist and can be used:

<figure>
    <img src="Images/wireless-mesh_2.png" alt="" width="300"><br />
    <figcaption>Fig. : Partial mesh topology</figcaption><br />
</figure>


However, there is practically no transposition for token ring or bus
topologies, which are used most notably in wired networks, as is the case with Ethernet, for example.

As we will see further on, mesh topologies are the best suited
for LRWPAN networks, but they are also the most complex to set up,
if only because of the routing paths that have to be calculated in order for data to be transmitted between objects.


## 4.1.1b Constraints

In the context of the Internet of Things, communicating devices are
constrained. They are battery-operated and have limited resources
(memory, processing). What this means is that the devices in question are roughly 150 times less powerful
in terms of their processing power than next-gen smartphones, in addition
to having 1000 times less memory (e.g. a 16 MHz clock with 2 K of
RAM and 32 K of flash memory on an Arduino Uno microcontroller).

However, they must be capable of carrying out a range of potentially costly and complex
operations in order to communicate (e.g. accessing shared
resources, identifying routing paths for data, ensuring exchanges are
reliable and secure, etc.). For transmissions, for example,
it is necessary to have the listening time of the radio channel in order to detect
incoming messages or to identify the resource as being available in order for it to transmit
itself. This radio listening activity consumes a significant amount of an
object’s energy budget.

Generally speaking, wireless transmission and reception are
very costly operations. The further an object
is looking to transmit, the more it must increase its transmitting power and, as a result,
its energy consumption. This is just one reason (antenna
quality is another) why IoT objects are considered as using low ranges.
This makes it difficult or even impossible to use star topologies,
as is the case with other wireless networks, and so mesh
topologies are used, where each object is only able to communicate with a
sub-section of the network. This is known as multi-hop routing, which is used
for gradually transmitting data from a source to a
destination. Lastly, it should be noted that the radio links considered
in these networks have a high loss rate, meaning they are not stable long-term and
are not necessarily symmetrical. These constraints must be taken into account when
defining the communication protocols used between objects.

What this means is that this mesh (and unstable) topology must be built and maintained,
requiring data to be exchanged between objects. As we will see,
a hierarchy can be established based on objects’ capacities
(e.g. objects are referred to as having total or reduced functions in the IEEE 802.15.4
standard). This multi-hop topology also requires data to be relayed
from its source to its destination. In such cases, the routing paths must
be calculated, evaluated and updated on a regular basis. This leads to costly
processing, in much the same way as the cryptographic operations which have to be carried out
during secure exchanges. These occupy part of the limited
resources of each communicating object (e.g. time, processing power, energy).


## 4.1.1c Objectives

Protocol stacks used in LRWPANs have a number of objectives.

As mentioned earlier, objects must have the necessary communication
resources for exchanging with other parts of the network.
Most importantly, a decision has to be made as to the type of radio technology used and
how it is to be configured (e.g. frequency, modulation, coding), in addition to defining the
expected characteristics of the radio links (e.g. loss rate, latency,
speed, etc.).

Among other factors, these define the communication ranges and, as
a consequence, the topology formed by the different objects. We will see how
to establish this topology and to maintain it (e.g. linking together objects, inlets/outlets
for network elements).

Once the topology has been established, objects can then use it to exchange
data. We will see how to identify each of these using IP
addresses (e.g. the IPv6 protocol), which are also essential to integrating these networks
into the internet.

Before that, addressing makes it possible to send data to destinations and
to identify the source of received data. In order for exchanges to take place on
multi-hop topologies, the paths the data will follow must be established. This
is the purpose of routing solutions, which must guarantee the delivery of
data and resilience in dynamic environments (e.g. link
breakdowns, node errors, mobility).

Lastly, we will see how to transport data from applications and to ensure exchanges
are reliable, before providing some basics regarding communication
security.

We will then see how all these preoccupations can be separated using
layered architectures. The other sequences within this module will outline
each aspect of the communication stack cited above.
