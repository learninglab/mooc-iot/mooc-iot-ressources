# 4.1.1. Topologies, contraintes et objectifs

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [4.1.1a Topologies](#411a-topologies)
- [4.1.1b Contraintes](#411b-contraintes)
- [4.1.1c Objectifs](#411c-objectifs)
<!-- TOC END -->

## 4.1.1a Topologies

Les réseaux sans fils déployés, qu'il s'agisse initialement de
téléphonie mobile ou plus récemment de technologies comme le Bluetooth ou le
WiFi, ont tous comme point commun de reposer sur une infrastructure
centralisée. Les objets communiquent entre eux au travers de points d'accès,
chargés d'organiser les associations, le partage des ressources ainsi que
diverses fonctions essentielles (e.g., économie d'énergie, sécurité).

On parle alors de topologies en étoile communément adoptées
dans les standards traitant de réseaux locaux sans fils (Wireless Local Area
Networks, WLAN). C'est notamment le cas du WiFi (standard IEEE 802.11) par
défaut, avec un point d'accès et des objets qui s'y associent :

<figure>
    <img src="Images/wifi-star-topology_FR.png" alt="" width="300"><br />
    <figcaption>Fig. : Topologie en étoile</figcaption><br />
</figure>

D'autres topologies existent dans les réseaux en général, parmi lesquelles nous
pouvons citer les topologies maillées. Dans ce cas il existe un lien entre
chaque paire de noeuds :

<figure>
    <img src="Images/wireless-full-mesh_2.png" alt="" width="300"><br />
    <figcaption>Fig. : Topologie totalement maillée</figcaption><br />
</figure>

ou partiellement, i.e., seuls certains liens existent et sont utilisables :

<figure>
    <img src="Images/wireless-mesh_2.png" alt="" width="300"><br />
    <figcaption>Fig. : Topologie partiellement maillée</figcaption><br />
</figure>

Il n'existe en revanche quasiment aucune transposition des topologies en bus ou
à jeton, utilisées en particulier dans les réseaux filaires avec comme exemple Ethernet.

Nous verrons plus tard que les topologies maillées sont à la fois les plus
adaptées pour les réseaux LRWPAN et en même temps les plus complexes à établir,
ne serait-ce qu'en raison des chemins de routage devant être calculés pour transmettre des données depuis un objet vers un autre.


## 4.1.1b Contraintes

Dans le cas de l'Internet des objets, les appareils communicants sont
contraints. Ils fonctionnent sur batterie et disposent de peu de ressources
(mémoire, calcul). Les dispositifs visés sont ainsi environ 150 fois moins puissants
en termes de calcul qu'un processeur de smartphone dernière génération, tout en
disposant de 1000 fois moins de mémoire (e.g. une horloge à 16MHz avec 2K de
mémoire RAM et 32K de mémoire Flash pour un microcontrôleur Arduino Uno).

Ils doivent cependant réaliser un ensemble d'opérations potentiellement
coûteuses et complexes afin de parvenir à communiquer (ex : accès à la ressource
partagée, identification des chemins de routage pour les données, fiabilisation
et sécurisation des échanges). Pour les transmissions, il est par exemple
obligatoire d'avoir des temps d'écoute du canal radio afin de détecter les
messages entrants, ou d'identifier la ressource comme disponible afin d'émettre
soi-même. Cette activité d'écoute de la radio consomme une part importante du
budget énergétique de l'objet.

De manière plus générale, l'émission et la réception sans fil sont des
opérations très coûteuses. Plus un objet souhaite
émettre loin, plus il doit augmenter sa puissance d'émission et donc sa
consommation d'énergie. Pour cette raison entre autres (e.g., qualité
d'antenne), les objets IoT sont considérés comme utilisant de faibles portées.
Ceci rend alors difficile voire impossible d'utiliser des topologies en étoile,
comme c'est le cas dans d'autres réseaux sans fils. Les topologies deviennent
maillées, chaque objet ne pouvant communiquer directement qu'avec une
sous-partie du réseau. On parle alors de communications multi-sauts requises
pour transmettre les données de proche en proche, d'une source vers une
destination. En dernier point, il faut noter que les liens radios considérés
dans ces réseaux sont à fort taux de perte, non stables dans le temps et non
forcément symétriques. Ces contraintes sont à prendre en compte lors de la
définition des protocoles de communication utilisés entre les objets.

Ainsi, cette topologie maillée (et instable) doit être construite et maintenue,
nécessitant des échanges de données entre les objets. Nous verrons qu'une
hiérarchie entre les objets peut être instaurée en fonction de leurs capacités
(ex : on parle d'objets à fonctions totales ou réduites dans le standard IEEE
802.15.4). Cette topologie multi-sauts impose également de relayer les données
de leur source vers leur destination. Dès lors, les chemins de routage doivent
être calculés, évalués et mis à jour régulièrement. Ceci induit des traitements
coûteux, au même titre que les opérations cryptographiques devant être réalisées
lors d'échanges sécurisés. Ces dernières  mobilisent une partie des ressources
limitées de chaque objet communicant (e.g., temps, calcul, énergie).


## 4.1.1c Objectifs

Les objectifs d'une pile de protocoles dédiée aux LRWPAN sont nombreux.

Comme évoqué précédemment, les objets doivent disposer des moyens de
communication nécessaires pour échanger avec les autres éléments du réseau.
Avant toute chose, il faut donc s'accorder sur la technologie radio et sa
configuration (e.g., fréquence, modulation, codage) et ainsi définir les
caractéristiques attendues des liens radios (e.g., taux de perte, latence,
débits).

Ces dernières définissent entre autres les portées de communication, et par
conséquent la topologie formée par les différents objets. Nous verrons comment
l'établir et la maintenir (e.g., association des objets, arrivées/départs
d'éléments du réseau).

Une fois la topologie établie, les objets peuvent l'utiliser pour échanger des
données. Nous verrons comment identifier chacun d'entre eux à l'aide d'adresses
IP (e.g., protocole IPv6), par ailleurs essentielles à l'intégration de ces
réseaux dans l'Internet.

Avant cela, l'adressage permet d'envoyer des données vers des destinations et
d'identifier les sources de celles reçues. Pour que ces échanges aient lieu sur
la topologie multi-sauts, il faut établir les chemins à emprunter. C'est
l'objectif des solutions de routage qui doivent garantir l'acheminement des
données et la résilience dans des environnements dynamiques (e.g., ruptures de
liens, pannes de noeuds, mobilité).

Enfin, nous verrons comment transporter les données d'applications et fiabiliser
leurs échanges, avant de fournir quelques bases concernant la sécurisation des
communications.

Par la suite, nous allons voir comment séparer toutes ces préoccupations à l'aide
d'architectures en couches. Les autres séquences de ce module détailleront
chacun des aspects de la pile de communication cités ci-dessus.
