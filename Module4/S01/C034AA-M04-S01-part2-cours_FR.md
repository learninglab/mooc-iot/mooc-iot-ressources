# 4.1.2. Le modèle OSI / IETF

Nous allons voir l'organisation d'une architecture réseau basée sur des modèles en couche et comment sont prises  en compte les spécificités de l'IoT.

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [4.1.2a Organisation d'une architecture réseau](#412a-organisation-dune-architecture-réseau)
- [4.1.2b Séparation des préoccupations en couches](#412b-séparation-des-préoccupations-en-couches)
- [4.1.2c Le modèle OSI](#412c-le-modèle-osi)
- [4.1.2d La pile protocolaire IoT de l'IETF](#412d-la-pile-protocolaire-iot-de-lietf)
<!-- TOC END -->


## 4.1.2a Organisation d'une architecture réseau

La mise en oeuvre d'un réseau IoT impose de répondre aux besoins de connectivité,
d'acheminement des données, de transports des informations, ou encore de sécurité
et de fiabilité des applications. Il faut ainsi prendre en compte la variété des
matériels, la diversité des environnements radio, la complexité des topologies
multi-sauts et les besoins de la multitude d'applications envisageables sur ces
réseaux. Ces préoccupations ne sont pas nouvelles, elles ont d'ailleurs déjà été
adressées durant la création et l'évolution de l'Internet tel que nous le
connaissons aujourd'hui.

Revenons sur les approches utilisées classiquement pour résoudre les nombreux problèmes posés. 
Nous allons détailler la manière dont ces modèles sont aujourd'hui déclinés 
dans le cadre des réseaux IoT.

Un WPAN consiste en un ensemble d'appareils qui communiquent sans fil à
l'échelle de la zone d'une personne. Nous nous concentrons ici sur l'organisation
des protocoles de communication embarqués dans les objets connectés formant un WPAN.

Des communications sans fil fiables et une collecte de données appropriée sont
nécessaires pour alimenter les applications finales. À cette fin, les caractéristiques
suivantes doivent être soigneusement examinées; les appareils cibles produisent
d'assez faibles volumes de données, de manière périodique. Ils utilisent des
batteries de faible capacité et des portées limitées pour les communications
sans fil, en raison de contraintes énergétiques ou de conception principalement.

De nombreux protocoles de communication existent et permettent aux objets d'échanger
des données, de les structurer et de les fiabiliser. De manière générale, les
communications entre de tels systèmes nécessitent la mise en place de nombreux
mécanismes permettant la transformation de l'information (ex : une page web) en
signaux électriques utilisés pour la transmission (sur câbles ou par lien radio).
Parmi eux, on peut citer le codage de l'information, la gestion d'environnement
partagé, la synchronisation, la détection et la correction d'erreurs,
l'identification des objets (adressage), l 'établissement de liens et de routes
de communication, le transports des informations, leur sécurisation, etc.


## 4.1.2b Le modèle OSI

Cette problématique d'organisation des réseaux a nécessité de formaliser un modèle
de communication basé sur des architectures dites en couches. Le modèle OSI
(Open System Interconnection) constitue la première à avoir été proposée, par
l'ISO (International Standardization Organisation). Il constitue le modèle
basique de référence pour l'interconnexion des systèmes ouverts à la
communication avec d’autres systèmes. Il décrit l'architecture des communications
en réseau avec l'objectif d'identifier les principales fonctions liées à la
communication, de les hiérarchiser en couches :

<figure>
<img src="Images/C034AA-M04-S01.png" height="303" />
<figcaption>Modèle OSI.</figcaption>
</figure>

Le modèle OSI propose également des principes d’abstraction au travers des
notions de couche, d'interface, de service, et de protocole. Une couche présente
ainsi un ensemble de fonctions et appels systèmes dans un programme (interface).
Elle définit les fonctionnalités qu'elle assure (service) à l'aide de primitives
(commandes ou événements). Elle permet également la définition du protocole de
communication relatif à cette couche (format, signification des paquets/messages
échangés, etc.). Ce protocole est un ensemble de règles permettant d'établir et
d'entretenir une communication à ce niveau (ou couche). Une couche donnée K
utilise le service rendu par la couche K-1, tel que décrit par l'interface de
cette dernière. Inversement, les données manipulées par une couche K ne prennent
sens (i.e. devienne information) que pour la couche supérieure K+1. Cette
encapsulation des données s'accompagne de l'ajout d'entête propre à chaque couche.
Ce mécanisme est illustré ci-après :

<figure>
<img src="Images/osi-encaps_FR.png" height="303" />
<figcaption>Encapsulation selon le modèle OSI.</figcaption>
</figure>

Ainsi, dans une architecture complète de communication entre systèmes ouverts,
les mécanismes à implémenter vont des protocoles de niveau physique (synchronisation,
codage des données en signaux et inversement, manipulation de bits de données)
aux protocoles d'application (ex : HTTP), en passant par les protocoles de
routage (le paquet étant l'unité d'information manipulée) ou encore les
protocoles de session (ex : sécurité d'une transmission).

Dans les systèmes classiques que nous utilisons au quotidien, on distingue par
exemple :

- les périphériques de communication et leurs pilotes (correspondant à peu près aux couches physique et liaison);
- le système d'exploitation (dans lequel on retrouve les mécanismes propres aux couches réseau et transport);
- les applications où se trouvent implantées les fonctionnalités de niveaux applicatif, présentation et session.

Le modèle OSI est une organisation en couches conçue pour l'interconnexion de
systèmes ouverts. C'est une pile théorique et idéale où chaque couche est
responsable d'un ensemble de préoccupations. Une couche donnée permet à celle
ci-dessus d'utiliser son service et de mettre en œuvre le sien. Par exemple,
la couche liaison de données est chargée d'établir le voisinage radio à partir
duquel la couche de routage construira la topologie de routage. Le modèle OSI
permet de décrire tout système de communication. Cependant, les systèmes
existants (et les protocoles qu'ils utilisent) ne contiennent pas nécessairement
d'équivalent à chaque couche du modèle OSI. Notons que celui-ci ne définit aucun
protocole.


## 4.1.2c La pile protocolaire IoT de l'IETF

La plupart des protocoles de communication sont standardisés par l'IEEE
(Institute of Electrical and Electronics Engineers) et l'IETF (Internet
Engineering Task Force). Alors que le premier définit la couche physique et une
sous-partie de la couche liaison, le deuxième s'occupe de tout ce qui se trouve
"au-dessus du câble (n.d.l.r., du medium de communication à proprement parler)
et en-dessous de l'application" (above the wire and below the application).

Dans notre contexte IoT, les communications sont réalisées à l'aide de liens
sans fil. Ces derniers reposent sur de nombreuses technologies (e.g., Wi-Fi,
Bluetooth, ZigBee, LoRa, Sigfox). Les fréquences radio employées sont distinctes
ou communes, et imposent donc une bonne gestion de l'environnement radio, en tant
que ressource partagée. Différents schémas de transmission sont utilisés et
permettent des communications sans fil sur divers environnements de propagation.
Un grand nombre de puces radio sont ainsi disponibles pour construire des objets
connectés. Ici, nous nous concentrons sur la couche physique définie dans le
groupe d'étude 4 du groupe 15 du comité 802 (i.e. dans la norme IEEE 802.15.4),
utilisée par la plupart des objets IoT disponibles.


Les protocoles embarqués au sein d'un système dépendent des contraintes de ce
dernier et de l'utilisation qui est faite l'objet. Par exemple, afin d'assurer
le fonctionnement à long terme des déploiements IoT, les appareils devraient
économiser autant d'énergie que possible. Une partie importante des travaux de
recherche et d'ingénierie autour de l'IoT s'est donc concentrée sur l'efficacité
énergétique. La puce radio étant le matériel le plus énergivore, son utilisation
doit être limitée. Les appareils alternent ainsi entre les modes inactif et
actif. Ils ne communiquent qu'en cas de besoin, tout en limitant les périodes
d'écoute du support pour les transmissions en cours des autres nœuds. Par
conséquent, il est essentiel d'assurer un contrôle d'accès médium soigneux
(alias MAC). Une activité excessive induit d'énormes parasites radio, des
interférences, etc., tandis que des périodes d'inactivité inappropriées peuvent
entraîner une topologie déconnectée et des nœuds isolés. Des normes telles que
IEEE 802.15.4 proposent des solutions pour réaliser cette coordination. Ils
permettent à chaque appareil de découvrir les objets voisins avec lesquels il
pourrait communiquer. Ces composants forment un tout équivalent aux couches
physique et liaison du modèle OSI.

Sur la base de cette topologie logique, les périphériques doivent trouver un
moyen de propager leurs données vers les stations réceptrices. En raison de la
portée radio limitée, cette propagation se fait le long de chemins composés de
liaisons sans fil. De bonnes décisions doivent être prises pour sélectionner le
voisin radio qui sera le prochain saut sur ce chemin. Cet objet sera chargé de
relayer le paquet de données à son propre prochain bond vers la destination.
Encore une fois, de nombreux travaux de recherche et d'ingénierie ont ciblé le
routage économe en énergie dans les réseaux IoT. Des normes récentes telles que
RPL permettent de construire une topologie de routage dans des environnements
radio difficiles. Elles implantent les fonctionnalités de la couche routage.

Une fois que les données peuvent être acheminées d'un point à un autre dans le
réseau, la fiabilité des communications de bout en bout doit être abordée.
L'objectif est d'éviter les pertes de paquets ou la désynchronisation des paquets
de données afin d'atteindre une collecte de données efficace et pertinente. Dans
Internet que nous utilisons tous les jours, des protocoles de transport tels que
TCP sont utilisés pour appliquer ces propriétés. Dans les réseaux IoT, les
contraintes des équipements embarqués nécessitent des solutions adaptées et on retrouve
communément l'utilisation du protocole de transport UDP. En effet il ne fournit pas de
garantie au protocole de la couche supérieure quant à la livraison d'un message

Dans l'ensemble, la fiabilité cible dépend des besoins et des exigences de
l'application, qui peuvent varier considérablement. Par exemple, d'une part,
les applications de soins de santé à distance ou de surveillance à domicile
imposeraient une collecte et une signalisation des données très fiables. D'un
autre côté, les soi-disant appareils liés au confort comme les lumières
intelligentes ou les montres exigeraient moins d'attention.

Nous avons énuméré les principales préoccupations à prendre en compte lors de
l'examen de l'architecture et des protocoles des WPAN basse consommation: le
support radio et la manière de le partager entre les appareils sans fil, le
routage, le transport et l'application finale. Ces problèmes sont séparés et
traités individuellement.

L'Internet Engineering Task Force (ou IETF) comprend des groupes de travail
axés sur des solutions dédiées à chaque couche. Par exemple, dans le contexte
de l'IoT, l'organisation proposée suppose l'utilisation d'IEEE 802.15.4 pour
les couches physiques et MAC. En plus de cela, l'IETF fournit les protocoles
supérieurs de routage, de transport et d'application. La pile globale est
typique de ce que vous trouverez dans un objet connecté ouvert.

<figure style="text-align:center">
    <img src="Images/C034AA-M04-S04-1.png" alt="Pile protocolaire de l'IETF"><br />
    <figcaption>Pile protocolaire de l'IETF</figcaption><br />
</figure>


