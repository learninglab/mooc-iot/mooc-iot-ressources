# Quiz

1. Un protocole est :

* [ ] une utilisation d'interface
* [X] une façon de rendre un service
* [ ] un service particulier
* [ ] spécifie les entrées et sorties d'une couche

[explanation]
Un protocole correspond à la façon dont une couche rend un service. Il n'a pas
vocation à être exposé à la couche supérieure. Cette dernière doit uniquement
maîtriser l'interface nécessaire à son utilisation. Elle doit également savoir
quelles sont les entrées et sorties de la couche afin de pouvoir communiquer les
données nécessaires et de savoir interpréter celles qu'elles reçoit en retour.
[explanation]


2. Identifiez 2 services rendus par la couche physique.

* [ ] Contrôle de l'échange
* [X] Synchronisation (délimitation des informations significatives)
* [ ] Délimitation de trames
* [ ] Contrôle de l'accès au medium
* [X] Modulation (représentation des bits en signaux)

[explanation]
La couche physique se charge de synchroniser les équipements d'émission/réception,
afin de pouvoir ensuite échanger des signaux. Ces derniers représentent les bits
de données issues de la couche supérieure (liaison). Cette dernière est quant à elle
responsable du contrôle de l'accès au medium, de l'échange, et de la délimitation
des trames d'information.
[explanation]


3. Dans un modèle en couches, les services sont fournis

* [ ] par une couche à n'importe quelle couche
* [X] par une couche à la couche supérieure
* [ ] par une couche à la couche directement inférieure
* [ ] par une couche à la couche de même niveau dans le système ouvert destinataire de la communication

[explanation]
Dans une architecture en couche, une couche donnée fournit un service à la couche
supérieure. Cette dernière l'utilise en exploitant l'interface fournie par la
couche inférieure. Par exemple, la couche liaison utilise l'interface de la
couche physique pour transmettre ses trames de données devant être transformées
en signaux à destination d'un autre objet.
[explanation]


4. Un système dispose d'une hiérarchie de N protocoles. Une application génère
des messages de M octets. Chaque couche ajoute un en-tête de E octets.
Quelle est la portion de bande passante occupée par ces entêtes ?

* [ ] Nx(E+M)
* [X] NxE/(M + NxE)
* [ ] (NxM + ExM) / N
* [ ] NxExM/(M + E)

[explanation]
M octets initiaux auxquels on ajoute NxE octets pour les en-têtes des N protocoles.
Pour calculer la portion de bande passante occupée uniquement par les en-têtes,
on calcule donc NxE/(M + NxE)
[explanation]
