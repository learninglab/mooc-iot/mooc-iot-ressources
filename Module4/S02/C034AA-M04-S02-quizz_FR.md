# Quiz

1. Dans le mode *beacon* de IEEE 802.15.4, CAP signifie :

* [ ] Contention Allowed Processes
* [ ] Contactless Acknowledgment Period
* [ ] Contiguously Allocated Peers
* [X] Contention Access Period
* [ ] Constrained Activity Period

[explanation]
Durant la "Contention Access Period", les objets sont en compétition
pour accéder à la ressource partagée, i.e. l'air comme medium de transmission.
Ils utilisent un mécanisme de délai aléatoire et d'écoute avant transmission afin
d'éviter les collisions.
[explanation]

2. Dans le mode *beacon* de IEEE 802.15.4, CFP signifie :

* [ ] Colocation Forced Peers
* [X] Contention Free Period
* [ ] Contiguous Frame Preamble
* [ ] Contention Facing Preamble
* [ ] Coordinator Frame Period

[explanation]
Durant la "Contention Free Period", les objets ne sont pas en compétition
pour accéder à la ressource partagée, i.e. l'air comme medium de transmission.
Ils disposent d'un créneau de temps dédié durant lequel ils sont assurés d'être
les seuls à pouvoir transmettre, évitant ainsi toute collision. La demande de ces
créneaux se fait auprès du coordinateur et permet aux récepteurs visés d'être informés
des créneaux durant lesquels ils devront écouter.
[explanation]

3. Rangez les différentes périodes du mode *beacon* de IEEE 802.15.4 dans le bon ordre de succession chronologique :

> en mode glisser-déposer pour mettre dans le bon ordre

* [4] période d'inactivité
* [1] *beacon*
* [3] CFP
* [2] CAP

> en mode case à cocher pour choisir la bonne proposition

* [ ]  CFP puis CAP puis période d'inactivité puis *beacon*
* [ ] période d'inactivité puis CAP puis CFP puis *beacon*
* [X] *beacon* puis CAP puis CFP puis période d'inactivité
* [ ] *beacon* puis CFP puis CAP puis période d'inactivité
* [ ] *beacon* puis période d'inactivité puis CFP puis CAP

[explanation]
La balise (beacon) est la première information transmise par le noeud
coordinateur. Elle contient toutes les informations relatives à la séquence qui
va suivre, à savoir la durée des périodes de contention (CAP) et de créneaux
garantis (CFP) ainsi que celle de la période d'inactivité. Ces trois durées
permettent aux noeuds associés de calculer le temps auquel arrivera la prochaine
balise du coordinateur.
[explanation]

4. A quel niveau du modèle OSI place-t-on les protocoles d'anti-collision ?

* [ ] LLC
* [X] MAC
* [ ] PHY

[explanation]
Les protocoles d'anti-collision se situent au niveau MAC, c'est-à-dire
littéralement là où l'accès au medium est contrôlé (Medium Access Control).
La couche physqiue (PHY) est quant à elle chargée de (dé)coder
les signaux pour les transformer en données (et vice-versa). C'est elle également
qui se charge de la synchronisation temporelle entre émetteurs et récepteurs pour
garantir le bon alignement des signaux. La couche de contrôle du
lien logique (LLC) s'occupe de lier les données décodées, tout en détectant,
voire corrigeant (lorsque c'est possible) les éventuelles erreurs issues du
décodage des signaux.
[explanation]

5. En quoi le mode TSCH du standard IEEE 802.15.4 assure-t-il la 
robustesse des réseaux sans fils qui l'utilisent ?

* [X] Un créneau de temps dédié et un décalage de canal sont utilisés à chaque transmission
* [ ] Une balise (beacon) permet la synchronisation en chaque début de slotframe
* [ ] Un créneau de temps dédié ou un décalage de canal est utilisé à chaque transmission

[explanation]
L'allocation de créneaux de temps dédiés et le saut de fréquence (déterminé
à l'aide du channel offset) permettent aux communications d'être établies, même dans 
des environnements radio bruyants ou propices aux interférences. 
[explanation]
