# 4.2.2. Un médium à partager

L'air est un médium en commun, il faut savoir se le partager.

![La Cène (Léonard de Vinci)](Images/la-cene.jpg)

Dans un dîner entre amis, pour être capable d'avoir une discussion de groupe, un principe assez simple va être utilisé : une seule personne à la fois prendra la parole. Et bien, pour permettre à un ensemble d'objets de partager des messages dans un environnement sans fil, le standard 802.15.4 propose de suivre ce même principe, en utilisant le protocole CSMA/CA (CA pour _Collision Avoidance_, ou évitement de collision en français).

Dans ce type de situation, nous avons un coordinateur qui est en attente de données et plusieurs dispositifs qui ont des données à transmettre. Le dispositif qui veut transmettre regarde tout d'abord si le médium est libre. Si il ne l'est pas, le dispositif tire un délai aléatoire après lequel il essaiera à nouveau de transmettre. Si le médium est libre, alors le dispositif transmet sa donnée.

Dans ce type de scénario, un réseau organisé en étoile autour du coordinateur demandera à avoir un coordinateur branché sur secteur pour être capable d'être continuellement à l'écoute de messages. Les dispositifs pourront, eux, fonctionner sur batterie, se mettre en veille une grande partie du temps, et se réveiller pour émettre leur message.

Néanmoins, cette méthode montre un inconvénient important : l'accès au médium n'est pas garanti dans une période de temps donné. Il dépendra fortement de la densité du réseau et du nombre de dispositifs voulant émettre.
