# 4.2.1. L'air comme médium de communication

De nombreuses technologies de communication sans fil sont aujourd’hui disponibles.
Une partie d’entre elles reposent sur les communications radio. Un système de communication
radio modifie une onde radio (e.g., fréquence,
amplitude, phase), dite porteuse, chargée de transporter les données de l’émetteur.
Nous pouvons faire le parallèle avec la communication humaine, rendue possible
par les cordes vocales faisant vibrer l’air pour assurer la propagation des sons.
Dans le cas des radiotransmissions, on parle de modulation de l’onde porteuse,
qui est propagée grâce à une antenne générant un champ électromagnétique. Les
fréquences et les caractéristiques de la bande passante utilisées déterminent
quant à elles les débits pouvant être atteints.

Le standard 802.15.4 définit l'utilisation de :
- 16 canaux dans la bande de fréquence ISM des 2,4 GHz,
- 10 canaux dans la bande de fréquence ISM des 915 MHz, aux Etats-Unis et en Australie,
- 1 canal dans la bande de fréquence ISM des 868 MHz, en Europe.

| <img src="Images/channels.png" width="600"/> | 
|:--:| 
| *Canaux utilisés par IEEE 802.15.4.* |

Les opérations de communication sont coûteuses et représentent donc une part importante
du budget énergétique des objets connectés.

Dans le cadre de l'IoT, les standards proposés doivent non seulement définir
le moyen (ou medium) de communication (i.e., fréquences, modulations), mais aussi
la manière dont il sera partagé en tant que ressource, par plusieurs objets ou
avec d'autres technologies radios qui peuvent être présentes. Pour 802.15.4, la
bande de fréquence des 2,4 GHz par exemple est en compétition avec les canaux
alloués au WiFi (IEEE 802.11), comme le montre la figure ci-dessous.

| <img src="Images/wifi_vs_zigbee.png" width="600" /> | 
|:--:| 
| *Chevauchements de canaux entre les normes IEEE 802.11 et 802.15.4.* |

Ces préoccupations sont au coeur des couches physique et liaison du modèle OSI.
Plus particulièrement, la façon dont les objets partagent la ressource de
communication est définie par la sous-couche de contrôle d'accès au medium (MAC).
