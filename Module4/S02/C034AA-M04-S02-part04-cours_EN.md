# 4.2.4. Deterministic networks and guarantees

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [4.2.4a Guarantee constraints](#424a-guarantee-constraints)
- [4.2.4b Time division and frequency hops](#424b-time-division-and-frequency-hops)
- [4.2.4c The TSCH mode in the IEEE 802.15.4 standard](#424c-the-tsch-mode-in-the-ieee-802154-standard)
<!-- TOC END -->


## 4.2.4a Guarantee constraints

The medium access techniques discussed so far enable sharing.
A range of scenarios in the modern world (e.g. the factory of the future, smart buildings,
assisted driving, etc.) involve decisions being made based on data from
connected objects. In these new, critical applications, the underlying
networks must provide guarantees in terms of access to the communication resource
and data delivery in environments that are often difficult for
radio communication (e.g. physical obstacles, external interference linked to
the presence of other wireless technologies in the environment).

Medium access control centred around CSMA/CA does not enable these constraints
to be met. It defers emissions when the channel
is not free, but cannot guarantee that it will be free at a later stage.

## 4.2.4b Time division and frequency hops

As we saw earlier, the IEEE 802.15.4 standard enables objects to
ask for and obtain guaranteed time slots (GTS), during which only
they will be able to communicate. This *time-division multiple access* (or TDMA)
was covered in an amendment presented in 2012 (IEEE 802.15.4e),
aimed at providing better support for “industrial
markets”, by dedicating these *time slots* (TS) to certain
communications. In this proposal, the entire superframe was
thus controlled in accordance with TDMA. The amendment IEEE 802.15.4e also proposed performing *channel
hopping* (CH) in order to obtain a greater degree of robustness when faced with
external interference or obstacles.

In 2015, the TSCH mode was integrated into the standard and works as follows.

## 4.2.4c The TSCH mode in the IEEE 802.15.4 standard

The TSCH mode in the IEEE 802.15.4 standard is derived from the beacon mode presented earlier.
Between two coordinator beacons, we now use the term _slotframe_,
 which represents the time division (*absolute slot number*, or ASN) and the channel
 hop (*channel offset*), as represented below.

<figure>
    <img src="Images/slotframe-101-slots.png" alt="" width="600"><br />
    <figcaption>Fig. : A slotframe containing 101 slots in TSCH mode in the IEEE 802.15.4 standard (RFC 8180, IETF).</figcaption>
</figure>

 A frequency/time block must then be allocated to each communication link.
 There are different strategies for doing this, one of which is to use routing
 topology. This is the case for the *minimal scheduling function* (MSF),
 which was adopted by the IETF and which enables objects
 to request blocks with their next hop in the routing topology.

 The figure below illustrates a simple topology comprised of an object (object A) to which
 two nodes (B and C) must send data. The *slotframe* can be
 built by taking this transmission scheme into account; B and C have thus
 obtained cells for communicating with A. We can observe that B will use the first
 timeslot of each slotframe to send data to A, while C will be using the second.
 Those cells also indicate the channel offsets that both B and C must use (i.e., 1 and 2 
 respectively here). Those fixed offsets are used by each object to compute the frequency to use.
 This frequency varies for each slotframe and is determined as follows:
 
 frequency = F {(ASN + channelOffset) mod nFreq}
   
The F function consists in a lookup table containing all available channels. Those 
correspond to the available frequencies (e.g., 16 by default, as defined in the IEEE 
802.15.4, 2,4 GHz standard). In order to ensure channel hopping (i.e., different frequency at each
slotframe), the number of timeslots elapsed since the network initialization (*absolute slot number*, ASN)
is also used.

<figure>
    <img src="Images/topology-to-slotframe.png" alt="" width="800"><br />
    <figcaption>Fig. : B and C’s communications with A are programmed in the slotframe.</figcaption>
</figure>


 The initial communications enabling
 the object to be inserted into the routing topology take place in the shared
 cells. A shared cell will occur at a frequency
 chosen at random by the coordinating object. The latter will then use this to transmit
 control packets enabling it to build and maintain the routing topology.
 It will also use this to send its *enhanced beacons* (EB),
 containing information relating to the *slotframe*.

 Any objects looking to join the network must listen to each of the
 16 available frequencies until a beacon has been received. The latter will supply
 them with the information needed for synchronisation with the
 coordinator and for insertion into the routing topology.
