# 4.2.0. IEEE 802.15.4: a communication standard for the IoT



<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>This sequence will introduce you to the IEEE 802.15.4 standard, one of the communication standards for objects with constraints in terms of processing power or energy. We will see how air is used as a communication medium and how it is shared, in addition to how energy consumption can be optimised and how guarantees in terms of access and robustness can be delivered.
</p>
</div>

A video presentation on the main concepts behind the 802.15.4 communication standard: modulation of the radio wave, managing collisions and coordinating communication periods in order to ensure the robustness and the reliability of radio transmissions while limiting energy consumption.    
