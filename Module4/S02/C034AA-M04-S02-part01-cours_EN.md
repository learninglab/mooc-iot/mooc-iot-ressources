# 4.2.1. Air as a communication medium

There are now many different types of wireless communication technologies,
some of which rely on radio communications. A radio communication
system modifies a radio carrier wave (e.g. frequency,
amplitude, phase), whose job it is to transport data from the emitting device.
Parallels can be drawn here with human communication, which is made possible
by the vocal cords vibrating air in order for sound to propagate.
For radio transmissions, we refer to modulation of the carrier wave,
which propagates by means of an antenna generating an electromagnetic field. The
frequencies and the characteristics of the bandwidth used, meanwhile,
determine which speeds can be reached.

The 802.15.4 standard defines the use of:
- 16 channels in the 2.4 GHz ISM frequency band
- 10 channels in the 915 MHz ISM frequency band in the USA and in Australia
- 1 channel in the 868 MHz frequency band in Europe

![channels](Images/channels.png)

Communication operations are costly, and use up a significant chunk of
the energy budget for connected objects.

In the context of the IoT, the standards proposed must define not only
the communication medium (frequency, modulation, etc.) but also
the way in which it will be shared as a resource, either by multiple objects or
with any other types of radio technology that may be present. For 802.15.4, the
2.4 GHz frequency band is in competition with the channels
allocated to Wi-Fi, as shown by the figure below.

![wifi vs zigbi](Images/wifi_vs_zigbee.png)


These preoccupations are central to the physical and link layers found in the OSI model.
More specifically, the way in which objects share the communication
resource is defined by the medium access control (MAC) sublayer.
