# 4.2.3. Saving energy through synchronization

Having to listen and analyse before being able to speak uses up a lot of energy. The constrained nature of objects meant that a solution had to be found in order to save energy.

With this in mind, a mode including the sending of _beacons_ was added to the CSMA/CA protocol. The purpose of this mode is to synchronise devices with the coordinator, enabling all devices to become independent, including the coordinator itself. The coordinator organises access to the channel and data transfer using a structure known as a _Superframe_, which repeats itself while remaining the same size and which is comprised of an active period and an inactive period (optional).

![The _Superframe_ Structure](Images/superframe.png)

The Active period is subdivided into 16 _Slots_. The first is dedicated to the transmission of the _Beacon_. The remaining slots form 2 periods, which differ according to their medium access method:

1. A first with contention, the _Contention Access Period_ (CAP)
2. A second without any contention, the _Contention Free Period_ (CFP).

__The _Beacon___ will contain specifications regarding the current _Superframe_, including a description for the different periods cited above and their duration in terms of the number of _Slots_.

__During the CAP,__ the network will operate in standard CSMA/CA mode, where each device waits until the medium is free before transmitting and where each transmission is aligned to a _Slot_ start. Transmission is limited by the remaining size of the CAP. The emitting device must wait for the next _Superframe_ if it is unable to emit during the CAP. The same shall apply if the number of _Slots_ needed for the transmission is higher than the remaining number of _Slots_.

__During the CFP,__ the network will operate in TDMA mode (_Time Division Multiple Access_). This medium access control technique divides up the time on the bandwidth and distributes the available time among the devices wishing to emit. The medium will be allocated in turns to the different emitting devices. In the _Beacon_, the coordinator will specify how the CFP is to be broken down into guaranteed time slots for an emitting device.

Lastly, a __period of radio inactivity__ may be defined. As a minimum, this will make it possible to switch radio components to standby mode, giving the upper layers the time to process data. In cases involving applications collecting low frequency data, the device can be put into deep sleep mode, thus saving energy between readings.

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-bookmark"></i> 
<p><b>What happens with a <i>Superframe</i> can be broken down as follows:</b>

1. The coordinator sends a _Beacon_   
2. Devices looking to emit compete with each other during the CAP, adopting the CSMA/CA mode. They may emit data, in addition to requesting to be allocated a GTS in an upcoming _Superframe_.   
3. The devices allocated a GTS by the coordinator, which will be communicated in the _Beacon_, will have a free channel for emitting or receiving data.   
4. The devices observe a period of inactivity.</p>
</div>

In terms of energy economy, the coordinator will be active during the _Superframe’s_ Active period. The other devices will only be active during the CAP if they have something to transmit, and/or during a GTS if they have been assigned an interval.
