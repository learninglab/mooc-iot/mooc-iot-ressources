# 4.2.2. A shared medium

Air is a common medium, and so we must share it.

![The Last Supper (Leonardo da Vinci)](Images/la-cene.jpg)

When friends meet up for dinner, in order for the group to have a conversation, a relatively simple principle has to be followed: only one person may speak at any one time. In much the same way, in order to enable a set of objects to exchange messages in a wireless environment, the 802.15.4 standard proposes following this same principle, using the CSMA/CA protocol (CA stands for _Collision Avoidance_).

In this type of situation, we have a coordinator waiting for data from multiple devices with data to transmit. The device wishing to transmit must first check to see if the medium is free. If it is not, the device will come up with a random delay, after which it will try once again to transmit. If the medium is free, the device will transmit its data.

In this type of scenario, a network arranged in a star-shape around a coordinator will issue a request to be given a coordinator plugged into the mains in order to enable it to continuously listen out for messages. The devices themselves will be capable of running off batteries, operating in standby mode for a significant chunk of the time and waking up to share their message.

There is, however, one major disadvantage to this method: access to the medium is not guaranteed within a given time period. This will depend primarily on the density of the network and the number of devices looking to emit.
