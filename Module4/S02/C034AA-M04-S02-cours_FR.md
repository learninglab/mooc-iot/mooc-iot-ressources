# 4.2.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Dans cette séquence vous allez découvrir le standard IEEE 802.15.4, un des 
standards de communication pour des objets contraints, en calcul, et en énergie. 
Nous verrons comment l'air est utilisé comme medium de communication, comment 
ce moyen de communication est partagé, et comment optimiser la consommation 
énergétique et fournir des garanties d'accès et de robustesse.
</p>
</div>

Présentation en vidéo des principaux concepts du standard de communication 802.15.4 : la modulation de l'onde radio, la gestion des collisions et la coordination des périodes de communication pour garantir la robustesse et la fiabilité des transmission radio tout en limitant la consommation d'énergie.    
