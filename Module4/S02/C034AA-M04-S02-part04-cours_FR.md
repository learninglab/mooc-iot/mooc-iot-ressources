# 4.2.4. Garanties et réseaux déterministes

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [4.2.4a Contraintes de garantie](#424a-contraintes-de-garantie)
- [4.2.4b Division en temps et sauts de fréquence](#424b-division-en-temps-et-sauts-de-fréquence)
- [4.2.4c Le mode TSCH du standard IEEE 802.15.4](#424c-le-mode-tsch-du-standard-ieee-802154)
<!-- TOC END -->

## 4.2.4a Contraintes de garantie

Les techniques d'accès au medium discutées jusqu'à présent permettent le partage.
Aujourd'hui, de nombreux scenarios IoT (e.g., usine du futur, bâtiment intelligent,
conduite assistée) impliquent la prise de décision sur la base de données issues
d'objets connectés. Ces nouvelles applications, critiques, imposent aux réseaux
sous-jacents de fournir des garanties, en termes d'accès à la ressource de communication
et d'acheminement des données dans des environnements souvent difficiles pour les
communications radio (e.g., obstacles physiques, interférences externes liées à
la présence d'autres technologies sans fil dans l'environnement).

Un contrôle d'accès au medium reposant sur CSMA/CA ne permet pas de satisfaire
ces contraintes. Il utilise en effet un report des émissions lorsque le canal
n'est pas libre, mais ne garantit pas qu'il le sera ultérieurement.

## 4.2.4b Division en temps et sauts de fréquence

Comme nous l'avons vu précédemment, le standard IEEE 802.15.4 permet cependant à un objet de
demander et d'obtenir des créneaux de temps garantis (GTS), pendant lesquels il
sera seul à communiquer. Cet accès au medium par division du temps
(ou *time-divsion multiple access*, TDMA) a fait l'objet d'un
amendement au standard présenté dès 2012 (IEEE 802.15.4e), visant à mieux supporter les "marchés
industriels" en dédiant des créneaux de temps (*time slots*, TS) à certaines
communications. Dans cette proposition, la totalité de la superframe était
ainsi régie selon TDMA. L'amendement IEEE 802.15.4e proposait également de réaliser du saut de
fréquence (*channel hopping*, CH), afin d'obtenir davantage de robustesse face aux
obstacles et interférences externes.

En 2015, le mode TSCH a ainsi été intégré au standard et fonctionne de la manière suivante.

## 4.2.4c Le mode TSCH du standard IEEE 802.15.4

Le mode TSCH du standard IEEE 802.15.4 dérive du mode balise présenté précédemment.
Entre deux balises du coordinateur, on parle désormais de _slotframe_,
 qui représente la division en temps (*absolute slot number*, ASN) et le saut de
 fréquence (*channel offset*), comme représenté ci-après.

<figure>
    <img src="Images/slotframe-101-slots.png" alt="" width="600"><br />
    <figcaption>Fig. : Une slotframe contenant 101 slots dans le mode TSCH de IEEE 802.15.4 (RFC 8180, IETF).</figcaption>
</figure>

 Il faut alors allouer un bloc temps/fréquence à chaque lien de communication.
 Différentes stratégies existent pour ce faire, par exemple en utilisant la topologie
 de routage. C'est le cas pour la fonction d'ordonnancement minimale (*minimal
 scheduling function*, MSF), adoptée au sein de l'IETF, et qui permet à un objet
 de demander des blocs avec son prochain saut dans la topologie de routage.

 La figure suivante illustre une topologie simple constituée d'un objet A auquel
 deux noeuds (B et C) doivent transmettre des données. La *slotframe* est alors
 construite en tenant compte de ce schéma de transmission ; B et C ont ainsi
 obtenu des cellules pour leurs communications vers A. On observe que B utilisera le
 premier créneau de temps de chaque slotframe, tandis que C utilisera le deuxième.
 Ces cellules indiquent également les décalages de canal que devront appliquer 
 B et C (soit 1 et 2 ici). Ces décalages fixes sont utilisés par chaque objet 
 pour calculer la fréquence sur laquelle émettre. Cette dernière, variable à chaque 
 slotframe, s'obtient de la manière suivante :

 fréquence = F {(ASN + channelOffset) mod nFreq}

La fonction F consiste en une table de correspondance dans laquelle se trouvent
les canaux disponibles (nFreq étant la taille de cette table), correspondant 
aux fréquences disponibles (ex : 16 par défaut, tel que définies dans la norme 
 IEEE 802.15.4, 2.4 GHz). Afin d'assurer un changement de fréquence à chaque slotframe, 
 le nombre de créneaux de temps écoulés depuis l'initialisation du réseau 
 (*absolute slot number*, ASN) est également utilisé.

<figure>
    <img src="Images/topology-to-slotframe.png" alt="" width="800"><br />
    <figcaption>Fig. : Les communications de B et C vers A sont programmées dans la slotframe (temps et canal dédié pour chacune), qui se répète cycliquement.</figcaption>
</figure>

 Les communications initiales qui permettent
 l'insertion de l'objet dans la topologie de routage ont lieu dans des cellules
 partagées. Une cellule partagée a lieu sur une fréquence
 choisie aléatoirement par l'objet coordinateur. Ce dernier l'utilise pour transmettre
 les paquets de contrôle permettant de construire et maintenir la topologie de routage.
 Il l'emploie également pour transmettre ses balises (*enhanced beacons*, EB)
 qui contiennent les informations relatives à la *slotframe*.

 Les objets souhaitant rejoindre le réseau doivent ainsi écouter sur chacune des
 16 fréquences disponibles jusqu'à réception d'une balise. Cette dernière leur
 fournit alors les informations nécessaires à la synchronisation avec le
 coordinateur et à leur insertion dans la topologie de routage.
