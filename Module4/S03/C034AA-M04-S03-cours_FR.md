# 4.3.0. Introduction

La couche d'adaptation 6LoWPAN est définie au sein de l'IETF. Elle permet aux objets connectés contraints d'utiliser des adresses IPv6 et peuvent ainsi échanger au travers de l'Internet existant.


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>Avec les différentes parties de cette séquence, il s'agit de comprendre les enjeux liés à l'utilisation d'IPv6 dans le contexte de l'IoT et d'appréhender les  difficultés que posent son intégration au sein d'objets contraints (compression,  fragmentation). Cette séquence permet aussi d'aborder l'autoconfiguration d'adresse IPv6 sans état sur chaque équipement.
</p>
</div>
