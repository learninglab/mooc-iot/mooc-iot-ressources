# 4.3.1. Discover IPv6 and 6LoWPAN

## Content
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Content](#content)
- [4.3.1a Why 6lowPAN](#431a-why-6lowpan)
- [4.3.1b IPv6 recap](#431b-ipv6-recap)
- [4.3.1c The challenges of encapsulating IPv6 packets in IEEE 802.15.4 frames](#431c-the-challenges-of-encapsulating-ipv6-packets-in-ieee-802154-frames)
- [4.3.1d IPv6 header compression](#431d-ipv6-header-compression)
- [4.3.1e Fragmentation in 6LoWPAN](#431e-fragmentation-in-6lowpan)
- [4.3.1f Address self-configuration](#431f-address-self-configuration)
- [4.3.1g References](#431g-references)
<!-- TOC END -->


## 4.3.1a Why 6lowPAN

A so-called LowPAN network is comprised of objects communicating wirelessly using
limited resources (e.g. speed, energy). The radio links employ the use of
standards such as IEEE 802.15.4, for example. The purpose of this sequence is to
explain how these objects can be integrated into the internet as we
know it. To put it another way, by taking space and processing constraints into account,
we will see how IP (Internet Protocol) can be established within these
networks in order to be able to access these objects in a way that is as transparent
and as upscalable as possible.

Deploying IP-compatible objects means facilitated integration
of objects in existing infrastructures, which have demonstrated their
robustness and their capacity for upscaling. With IP-compatible
objects, there is no need for intermediary gateway machines, and so
the end-to-end principle, an essential aspect of internet architecture, is maintained.

IP connectivity requires a significant amount of effort given the objects in question.
This is as much about being able to address objects (i.e. making them exist
on the internet), as it is about being able to reach them (i.e. routing).

First and foremost, the sheer quantity of connected objects, both those around currently and those expected to arrive in the future,
means a vast addressing space is required. Then, there is the fact that the density of LowPAN networks
prevents users or administrators from attempting manual operations
on each object. This means that auto-configuration mechanisms
are required, through which each object can determine the IP address
it will use.

These two constraints can be met by using IPv6, which is why this
was chosen as the protocol to use for interconnectivity with the internet.

We will now take a look at what this protocol consists of, how it
handles lower-level information and addresses (e.g. IEEE 802.15.4)
and how it can be adapted for use
on the constrained objects in question.

## 4.3.1b IPv6 recap

An IPv6 address is coded on 128 bits, meaning a huge addressing space
is required (i.e. roughly 100 IP addresses for every atom on Earth).

These addresses are written hexadecimally in order to make them easier to read.
This can also be compressed. In this way, an address such as
2001:0db8:0000:85a3:0000:0000:ac1f:8001 can be abridged to 2001:db8:0:85a3::ac1f:8001.
“::” designate the longest sequence of 0s, while “0” summarises a sequence of
consecutive 0s over 2 bytes (between two consecutive “:”).

As is the case for IPv4 addresses, a part of the IPv6 address is specific to the communication
interface being addressed, with the other corresponding to that of the network in
which it is located.

Aside from these addresses, IPv6 provides simplified packet headers, making it easier
for them to be processed by routers responsible for conveying them.
Lastly, the IPv6 protocol comes natively with security (IPsec),
service-quality and auto-configuration mechanisms.

It was these characteristics that prompted the research into IPV6 being integrated
into LowPANs formed of constrained objects. These contributions were made
in the context of the IETF's 6LoWPAN working group, and we will now take a look
at their most important contributions:
- header compression
- packet fragmentation
- address auto-configuration


## 4.3.1c The challenges of encapsulating IPv6 packets in IEEE 802.15.4 frames

As mentioned in RFC 2460, IPv6 requires that every link in the internet have a 
maximum transmission unit (MTU) of 1280 octets or greater. On any link that 
cannot convey a 1280-byte packet in one piece, link-specific fragmentation and 
reassembly must be provided at a layer below IPv6.

The total size of an IPv6 packet is thus minimum 1280 bytes. These IPv6 packets
must then be encapsulated in IEEE 802.15.4 frames, the maximum size
of which is only 127 bytes.

What’s more, the header itself takes up 25 bytes. In addition, it is highly
recommended that 21 out of the remaining 102 bytes be used to encrypt the data
being transmitted. This leaves 81 bytes for encoding the IPv6 packet,
but the IPv6 header alone takes up 40 of these. If we are to suppose that a protocol
such as UDP is being used, which uses 8-byte headers, that only leaves
33 bytes for the data from the application. If TCP is being used, its
20-byte header will leave only 21 bytes for data.

In order to be able to use or even to consider applications generating
more data, it is essential to free up some room in the
IPv6 packet, by compressing the header, for example. If the packets are still
too large, transmissions can be fragmented,
and put back together again once they reach their destination.

These header compression and fragmentation mechanisms were standardised
within the IETF’s 6LoWPAN working group (RFC 6282 and 4944 respectively).


## 4.3.1d IPv6 header compression

The IPv6 header compression mechanism (LOWPAN_IPHC) uses information
available in the 6LoWPAN layer. Let’s suppose that the communications
using LOWPAN_IPHC rely on IPv6, that the size of the payload can be
subtracted from the lower layers (e.g. from the fragmentation header detailed
below), or that the interface addresses are generated based on
prefixes specific to the underlying IEEE 802.15.4 links.

The LOWPAN_IPHC compressed header contains information which can be used
to re-establish the source and destination addresses, in addition to the headers following them.

The LOWPAN_IPHC header has the following format:

      0                                       1
       0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     | 0 | 1 | 1 |  TF   |NH | HLIM  |CID|SAC|  SAM  | M |DAC|  DAM  |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

The first three bits (011) indicate that this is an IPv6 packet from a
UDP traffic stream and that it has been compressed. The LOWPAN_IPHC header starts with the
bits 3 and 4 (TF), which represent the traffic class and the flow label. This
information takes up 28 bits in a standard IPv6 header. In 6LoWPAN,
only 4 possible configurations have been retained, meaning 2 bits are enough
to identify them.

The field NH (Next Header, 8 bits in an uncompressed header) is reduced to
one single bit while the field HLIM (Hop Limit) only covers 4 possible values
(no compression, 1, 64 or 255), moving from one byte to just 2 bits.

On the next bit, the field CID (Context Identifier Extension) indicates
which context elements are to be taken into consideration for address compression.
This latter is specified using the fields SAC (Source Address Compression) and
DAC (Destination Address Compression), with 1 bit each indicating
whether or not this was a case of context-based compression.
The fields SAM (Source Address Mode) and DAM (Destination Address Mode), meanwhile,
specify how the addresses were compressed (only 2 bits
needed to code the 4 possibilities corresponding to each SAC or DAC
value, giving 8 possibilities in total for address compression), while
the field M (Multicast Compression) needs only one bit to indicate whether or not
the recipient address is a multicast address.

In this way, only 8 bits in total are used to compress the source and destination
addresses, which would take up 128 bits each in a non-compressed
header.

A LOWPAN_IPHC compressed header takes the following shape:

    +-------------------------------------+----------------------------
    | Dispatch + LOWPAN_IPHC (2-3 bytes) | In-line IPv6 Header Fields
    +-------------------------------------+----------------------------

where the field Dispatch can be used to identify the type of header to follow, i.e. the
type of packet encapsulated in the IEEE 802.15.4 frame.

Here are a few examples of values and their correspondences for the Dispatch field:

           Pattern    Header Type
         +------------+-----------------------------------------------+
         | 00  xxxxxx | NALP       - Not a LoWPAN frame               |
         | 01  000001 | IPv6       - Uncompressed IPv6 Addresses      |
         | 01  000010 | LOWPAN_HC1 - LOWPAN_HC1 compressed IPv6       |
         | 01  010000 | LOWPAN_BC0 - LOWPAN_BC0 broadcast             |
         | 01  111111 | ESC        - Additional Dispatch byte follows |
         | 10  xxxxxx | MESH       - Mesh Header                      |
         | 11  000xxx | FRAG1      - Fragmentation Header (first)     |
         | 11  100xxx | FRAGN      - Fragmentation Header (subsequent)|
         +------------+-----------------------------------------------+

Note here that the LOWPAN_IPHC header can use 5 bits from Dispatch in addition to
its byte, meaning it can be made up of 13 bits. This contains all fields of
the IPv6 header that were compressed. The others then come after
(In-line IPv6 Header Fields). The best-case scenario occurs with
link-local communications, where IPv6 headers can be reduced
to 2 bytes (instead of 40) - one for the Dispatch and the other for
LOWPAN_IPHC. In cases where other IP hops have to be borrowed by the packet,
the IPv6 header must include 7 bytes: 1 for the Dispatch, 1 for the LOWPAN_IPHC,
1 for the field Hop Limit and 2 each for the source and destination addresses.

This compression can increase the payload of the packet to 75 bytes.

## 4.3.1e Fragmentation in 6LoWPAN

In cases where an IPv6 packet (irrespective of whether the header has been compressed), can be included in
an IEEE 802.15.4 frame, normal encapsulation takes place, without any
fragmentation. If this is not the case, it is possible to generate a number of fragments
and to add a fragmentation header that will enable reassembly once it reaches
its destination.
The first fragment will contain the following header:

                     1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |1 1 0 0 0|    datagram_size    |         datagram_tag          |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Here you will find the 5 bits of the Dispatch indicating that this is a first fragment.
The field datagram_size specifies the size of the IP packet prior to fragmentation, while
the field datagram_tag is a label, which is identical for all fragments of
the same IP packet.

The following fragments must then contain the following fragmentation
header:

                           1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |1 1 1 0 0|    datagram_size    |         datagram_tag          |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |datagram_offset|
      +-+-+-+-+-+-+-+-+

Here you will find the 5 bits from the Dispatch positioned at 11100, signifying that this
is a fragment of an IPv6 packet, but not the first fragment. The fields datagram_size
and datagram_tag will remain, while the field datagram_offset indicates the position
of this fragment within the IPv6 packet.

## 4.3.1f Address self-configuration

We have seen how the IPv6 header can be compressed. One of the most
important factors in this compression concerns the absence of the source and destination
addresses (32 bytes), which are replaced by one single allocated byte in LOWPAN_IPHC.
A total of 31 bytes out of a standard IPv6 header’s initial 40 are saved
in this way.

The goal is now to understand how these source and destination addresses
can be re-established based on the information contained within this byte
from the LOWPAN_IPHC and other context elements available within the 6LoWPAN
adaptation layer.

Among the available information is the information on the lower layer,
IEEE 802.15.4. First and foremost, all objects employing the use of this technology
have an extended unique identifier. Globally, all communication
interfaces (whether wired or wireless) are given a unique number
by their manufacturer (an Organisationally Unique Identifier, OUI). This is comprised of three
bytes representing the manufacturer, followed by three others forming the object’s
serial number. This unique number (OUI) can be used as a basis for extended
identifiers, such as EUIs.

This means that a 64-bit EUI number is available for IEEE 802.15.4 objects.
The network in which an object operates also has an identifier,
called a prefix, which has 64 bits. Put together, these two pieces of information
(the network prefix followed by the interface identifier) form an IPv6 address
that is specific to this item of equipment:

          10 bits            54 bits                  64 bits
       +----------+-----------------------+----------------------------+
       |1111111010|         (zeros)       |    Interface Identifier    |
       +----------+-----------------------+----------------------------+

This is called stateless auto-configuration.
There are many other use cases, meaning objects must configure
their IPv6 address depending on the messages received from border
routers.

## 4.3.1g References
* Compression Format for IPv6 Datagrams over IEEE 802.15.4-Based Networks (IETF
RFC 6282)
* Transmission of IPv6 Packets over IEEE 802.15.4 Networks (IETF RFC 4944)
