# 4.3.1. Découvrir IPV6 et 6LoWPAN

## Sommaire
<!-- TOC START min:2 max:2 link:true asterisk:false update:true -->
- [Sommaire](#sommaire)
- [4.3.1a Pourquoi 6lowPAN ?](#431a-pourquoi-6lowpan-)
- [4.3.1b Rappels IPv6](#431b-rappels-ipv6)
- [4.3.1c Défis liés à l'encapsulation de paquets IPv6 dans des trames IEEE 802.15.4](#431c-défis-liés-à-lencapsulation-de-paquets-ipv6-dans-des-trames-ieee-802154)
- [4.3.1d La compression d'en-tête IPv6](#431d-la-compression-den-tête-ipv6)
- [4.3.1e La fragmentation dans 6LoWPAN](#431e-la-fragmentation-dans-6lowpan)
- [4.3.1f L'autoconfiguration d'adresse](#431f-lautoconfiguration-dadresse)
- [4.3.1g Références](#431g-références)
<!-- TOC END -->

## 4.3.1a Pourquoi 6lowPAN ?

Un réseau dit LowPAN consiste en des objets communicant en sans-fil et reposant
sur des ressources limitées (e.g., débit, énergie). Les liens radio utilisent des
standards comme IEEE 802.15.4 par exemple. Cette séquence a pour objectif de
comprendre comment ces objets peuvent être intégrés à l'Internet tel que nous le
connaissons. Autrement dit, en tenant compte des contraintes de place et de traitement,
nous allons voir comment IP (Internet Protocol) peut être implanté au sein de ces
réseaux, afin de pouvoir accéder à ces objets de façon aussi transparente et
évolutive que possible.

En effet, déployer des objets compatibles IP implique une intégration facilitée
des objets dans les infrastructures existantes, qui ont par ailleurs démontré leur
robustesse et leur capacité à passer à l'échelle. Disposer d'objets compatibles
IP permet en outre de se passer de machines passerelles intermédiaires et ainsi
de perpétuer le principe de bout en bout, essentiel dans l'architecture de l'Internet.

La connectivité IP impose cependant des efforts importants au vu des objets considérés.
Il s'agit autant de savoir adresser les objets (i.e., leur donner une existence
dans l'Internet), que de pouvoir les atteindre (i.e., routage).

Pour commencer, le très grand nombre d'objets connectés actuels et envisagés
requiert un vaste espace d'adressage. Ensuite, la densité des réseaux LowPAN
empêche les utilisateurs ou administrateurs d'envisager des opérations manuelles
sur chaque objet. Il est alors impératif de reposer sur des mécanismes
d'auto-configuration grâce auxquels chaque objet peut déterminer l'adresse IP
qu'il doit utiliser.

Ces deux contraintes peuvent être satisfaites par l'utilisation d'IPv6, qui a donc
été choisi comme le protocole à utiliser pour l'interconnexion avec l'Internet.

Nous allons maintenant voir en quoi consiste ce protocole, de quelle manière il
permet la prise en charge d'adresses et d'informations de plus bas niveau (e.g.,
IEEE 802.15.4), ainsi que ce qu'il nécessite comme adaptation pour une utilisation
sur les objets contraints envisagés.

## 4.3.1b Rappels IPv6

Une adresse IPv6 est codée sur 128 bits, impliquant un espace d'adressage
extrêmement vaste (i.e., environ 100 adresses IP par atome présent sur terre).

Ces adresses sont écrites sous une forme hexadécimale afin d'en faciliter la lecture.
Cette écriture peut également être compressée.
Ainsi, une adresse comme :

```
   2001:0db8:0000:85a3:0000:0000:ac1f:8001
```
peut être abrégée en :

```
   2001:db8:0:85a3::ac1f:8001
```

Les «::» désignent la plus longue suite de 0 tandis que «0» résume une suite de 0
consécutifs sur 2 octets (entre deux «:» consécutifs).

Comme pour les adresses IPv4, une partie de l'adresse IPv6 est propre à l'interface
de communication adressée tandis que l'autre correspond à celle du réseau dans
lequel elle se trouve.

Au-delà des adresses, IPv6 apporte des en-têtes de paquets simplifiées, facilitant
ainsi leur traitement par les routeurs en charge de leur acheminement.
Le protocole IPv6 intègre enfin nativement des mécanismes de sécurisation (IPsec),
de qualité de service ou encore d'auto-configuration.

Toutes ces caractéristiques ont motivé les travaux portant sur l'intégration d'IPv6
au sein d'objets contraints constituant les LowPAN. Ces contributions ont été faites
dans le cadre du groupe de travail 6LoWPAN de l'IETF, dont nous allons maintenant
détailler les principales contributions :
- la compression d'en-tête,
- la fragmentation de paquets,
- l'autoconfiguration d'adresse.


## 4.3.1c Défis liés à l'encapsulation de paquets IPv6 dans des trames IEEE 802.15.4

Comme indiqué dans la RFC 2460, IPv6 exige que chaque lien sur Internet ait une
unité de transmission maximale (MTU) de 1280 octets ou plus. Pour tout lien ne pouvant
pas transmettre un paquet de 1280 octets en une seule fois, la fragmentation
(spécifique à ce lien) et le réassemblage doivent être assurés par une couche inférieure.

La taille totale d’un paquet IPv6 est par conséquent d’au moins 1280 octets. Ces
paquets IPv6 doivent ensuite être encapsulés dans des trames IEEE 802.15.4, dont
la taille maximale n’est cependant que de 127 octets.

De plus, l’en-tête 802.15.4 occupe déjà 25 octets. En outre, il est fortement
recommandé que parmi les 102 octets, 21 soient employés au chiffrement les données
transmises. Ainsi, 81 octets restent disponibles pour encoder le paquet IPv6.
Or, la seule en-tête IPv6 en monopolise 40. En supposant l’utilisation d’un
protocole comme UDP qui utilise des en-têtes de 8 octets, il ne reste alors plus
que 33 octets pour les données issues de l’application. Si TCP est employé, son
en-tête de 20 octets ne laisse alors que 21 octets pour les données.

Afin de pouvoir utiliser ou, à défaut, envisager des applications générant
davantage de données, il est impératif de pouvoir gagner de la place dans le
paquet IPv6, par exemple en en compressant l’en-tête. Si les paquets restent de
taille trop importante, il est ensuite possible de fragmenter les émissions tout
en garantissant un réassemblage à destination.

Ces mécanismes de compression d'en-tête et de fragmentation ont été standardisés
au sein du groupe de travail 6LoWPAN de l'IETF (RFC 6282 et 4944 respectivement).


## 4.3.1d La compression d'en-tête IPv6

Le mécanisme de compression d'en-tête IPv6 (LOWPAN_IPHC) exploite des informations
disponibles dans la couche 6LoWPAN. On suppose alors que les communications qui
utilisent LOWPAN_IPHC reposent sur IPv6, que la taille de la charge utile peut être
déduite des couches inférieures (ex : depuis l'en-tête de fragmentation détaillée
ci-après) ou encore que les adresses des interfaces sont générées à partir de
préfixes propres aux liens IEEE 802.15.4 sous-jacents.

L'en-tête compressée LOWPAN_IPHC intègre notamment des informations permettant
de reconstituer les adresses source et destination, ainsi que les en-têtes qui suivent.

L'en-tête LOWPAN_IPHC a le format suivant :

      0                                       1
       0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     | 0 | 1 | 1 |  TF   |NH | HLIM  |CID|SAC|  SAM  | M |DAC|  DAM  |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

Les trois premiers bits (011) indiquent ici qu'il s'agit d'un paquet IPv6 issu d'un
trafic UDP et qu'il y a compression. L'en-tête LOWPAN_IPHC commence donc avec les
bits 3 et 4 (TF) qui représentent la classe de trafic et le label de flux. Ces
informations représentent 28 bits dans une en-tête IPv6 classique. Dans 6LoWPAN,
seuls 4 configurations possibles ont été conservées et 2 bits suffisent donc à
les identifier.

Le champ NH (Next Header, 8 bits dans une en-tête non compressée) est réduit à
un seul bit tandis que le champ HLIM (Hop Limit) ne couvre que 4 valeurs possibles
(pas de compression, 1, 64 ou 255) et passe ainsi d'un octet à seulement 2 bits.

Sur le bit suivant, le champ CID (Context Identifier Extension) indique quels
éléments de contexte sont à prendre en considération pour la compression des adresses.
Cette dernière est précisée grâce aux champs SAC (Source Address Compression) et
DAC (Destination Address Compression), 1 bit chacun permettant d'indiquer s'il
s'agit d'une compression basée sur les éléments de contexte ou non.
Les champs SAM (Source Address Mode) et DAM (Destination Address Mode) précisent
quant à eux la façon dont les adresses ont été compressées (seulement 2 bits
nécessaires pour coder les 4 possibilités correspondant à chaque valeur de SAC
ou de DAC, soit 8 possibilités au total pour la compression d'adresse) tandis que
le champ M (Multicast Compression) se contente d'un bit pour indiquer si l'adresse
de destination est une adresse multicast ou non.

De cette façon, seuls 8 bits au total sont consacrés à la compression des adresses
source et destination qui représentent 128 bits chacune dans une en-tête non
compressée.

Ainsi, une en-tête compressée LOWPAN_IPHC prend la forme suivante :

    +-------------------------------------+----------------------------
    | Dispatch + LOWPAN_IPHC (2-3 octets) | In-line IPv6 Header Fields
    +-------------------------------------+----------------------------

où le champ Dispatch permet d'identifier le type de l'en-tête à suivre, i.e., le
type de paquet encapsulé dans la trame IEEE 802.15.4.

Voici quelques exemples de valeurs et leurs correspondances pour le champ Dispatch :

           Pattern    Header Type
         +------------+-----------------------------------------------+
         | 00  xxxxxx | NALP       - Not a LoWPAN frame               |
         | 01  000001 | IPv6       - Uncompressed IPv6 Addresses      |
         | 01  000010 | LOWPAN_HC1 - LOWPAN_HC1 compressed IPv6       |
         | 01  010000 | LOWPAN_BC0 - LOWPAN_BC0 broadcast             |
         | 01  111111 | ESC        - Additional Dispatch byte follows |
         | 10  xxxxxx | MESH       - Mesh Header                      |
         | 11  000xxx | FRAG1      - Fragmentation Header (first)     |
         | 11  100xxx | FRAGN      - Fragmentation Header (subsequent)|
         +------------+-----------------------------------------------+

Notons ici que l'en-tête LOWPAN_IPHC peut utiliser 5 bits du Dispatch en plus de
son octet et être ainsi constituée de 13 bits. Elle contient tous les champs de
l'en-tête IPv6 qui ont pu être compressés. Les autres se trouvent à la suite
(In-line IPv6 Header Fields). Le cas le plus favorable se retrouve sur les
communications sur lien local, pour lesquelles l'en-tête IPV6 peut être réduite
à 2 octets (contre 40 normalement), l'un pour le Dispatch et l'autre pour
LOWPAN_IPHC. Lorsque d'autres sauts IP doivent être empruntés par le paquet,
l’en-tête IPv6 doit comporter 7 octets, pour le Dispatch (1), le LOWPAN_IPHC (1),
le champ Hop Limit (1) et les adresses source et destination (2 octets chacune).

Grâce à cette compression, la charge utile du paquet peut atteindre 75 octets.

## 4.3.1e La fragmentation dans 6LoWPAN

Lorsqu'un paquet IPv6 (avec compression d'en-tête ou non) peut être inclus dans
une trame IEEE 802.15.4, l'encapsulation se déroule classiquement, sans
fragmentation. Si ce n'est pas le cas, il est possible de générer plusieurs fragments
et d'ajouter une en-tête de fragmentation qui permettra le réassemblage à la
destination.
Le premier fragment contient l'en-tête suivante :

                     1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |1 1 0 0 0|    datagram_size    |         datagram_tag          |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

On retrouve ici les 5 bits du Dispatch indiquant qu'il s'agit d'un premier fragment.
Le champ datagram_size précise la taille du paquet IP avant fragmentation tandis que
le champ datagram_tag est une étiquette, identique pour tous les fragments d’un
même paquet IP.

Les fragments suivants devraient ensuite contenir l'en-tête de fragmentation
suivante :

                           1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |1 1 1 0 0|    datagram_size    |         datagram_tag          |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |datagram_offset|
      +-+-+-+-+-+-+-+-+

On retrouve ici les 5 bits du Dispatch positionnés à 11100, ce qui signifie qu'il
s'agit d'un fragment de paquet IPv6 mais pas le premier. Les champs datagram_size
et datagram_tag demeurent tandis que le champ datagram_offset indique la position
de ce fragment au sein du paquet IPv6.

## 4.3.1f L'autoconfiguration d'adresse

Nous avons vu comment l'en-tête IPv6 peut être compressée. L'un des facteurs les
plus importants de cette compression tient à l'absence des adresses source et
destination (32 octets), remplacées par un seul octet alloué dans LOWPAN_IPHC.
Au total, 31 octets sont ainsi économisés sur les 40 initiaux de l'en-tête IPv6
classique.

L'objectif est maintenant de comprendre comment ces adresses source et destination
peuvent être reconstruites sur la base des informations contenues dans cet octet
du LOWPAN_IPHC et d'autres éléments de contexte disponibles au sein de la couche
d'adaptation 6LoWPAN.

Parmi les informations disponibles se trouvent celles de la couche inférieure,
IEEE 802.15.4. Pour commencer, tout objet reposant sur cette technologie
possèdent un identifiant unique étendu (EUI). Globalement, toute interface de
communication (filaire ou non) se voit attribuer par son constructeur un numéro
unique (Organizationally unique identifier, OUI). Celui-ci est constitué de trois
octets représentant le constructeur, suivis de trois autres qui forment le numéro
de série de l'objet. Ce numéro unique, OUI, peut servir de base à des identifiants
étendus, comme EUI.

Dans le cas des objets IEEE 802.15.4, un numéro EUI de 64 bits est ainsi disponible.
Ensuite, le réseau dans lequel évolue l'objet dispose lui aussi d'un identifiant,
appelé préfixe, et comportant 64 bits. L'assemblage de ces deux informations
(préfixe réseau suivi de l'identifiant d'interface) forme une adresse IPv6
propre à cet équipement :

          10 bits            54 bits                  64 bits
       +----------+-----------------------+----------------------------+
       |1111111010|         (zeros)       |    Interface Identifier    |
       +----------+-----------------------+----------------------------+

On parle alors d'autoconfiguration sans état.
De nombreux autres cas de figure existent et les objets doivent alors configurer
leur adresse IPv6 en fonction des messages reçus en provenance des routeurs de
bordure.

## 4.3.1g Références
* Compression Format for IPv6 Datagrams over IEEE 802.15.4-Based Networks (IETF
RFC 6282)
* Transmission of IPv6 Packets over IEEE 802.15.4 Networks (IETF RFC 4944)
