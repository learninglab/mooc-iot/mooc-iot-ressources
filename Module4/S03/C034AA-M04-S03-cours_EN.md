# 4.3.0. Discovering 6LoWPAN and UDP: IPv6 for the IoT


The 6LoWPAN adaptation layer is defined within the IETF. It enables constrained connected objects to use IPv6 addresses and to exchange over the existing internet.


<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>The goal of this sequence is to understand the challenges linked to the use of IPv6 in the context of the IoT and the issues raised by its integration within constrained objects (compression, fragmentation). This sequence also introduces the concept of IPv6 stateless address auto-configuration on each item of equipment.
</p>
</div>
