# 4.5.1. The RPL routing graph

RPL defines a routing structure in the form of a _Destination-Oriented Directed Acyclic Graph_ (__DODAG__). This is a graph directed towards a _root_ node which does not form a cycle.

In order to prevent loops from forming, the standard is based on a “ranking” concept, e.g. the relative position of a node in relation to the root compared to other neighbouring nodes. If a node is able to communicate with more than one node of a lower rank, it will select one as a preferred parent for feeding back the message. This structure meets optimisation requirements for M2P communications, with information fed back from the root.

<figure>
    <img src="Images/C034AA-M04-S05.png" alt="" width="300">
    <figcaption>Figure 4: Communication links</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05-2.png" alt="" width="300">
    <figcaption>Figure 5: DODAG</figcaption>
</figure>

Although we will describe the RPL mechanisms for a simple DODAG during the next part of this sequence, it should be noted that an RPL deployment may contain several instances. Each instance is defined by an objective function, which is used to define:
- how nodes select and optimise routes
- how one or more metrics can be used to calculate the rank
- how nodes select their preferred parent

An instance may contain more than one DODAG, which will share the same objective function - each node is only able to participate in one single DODAG. An RPL deployment, however, may contain more than one instance, while a node can participate in multiple instances. Consider a sensor network, for example, the main purpose of which is to feed back information on a regular basis for the purposes of monitoring equipment or areas. An RPL instance will be defined with an appropriate objective function in order to optimise the energy consumption of this network. However, although this same network must also be capable of sending alerts, messages must be sent as quickly as possible, and must be given priority. What this means is that its needs are different from the first. The network nodes will belong to a second instance, RPL, with a different objective function, which will concern itself less with energy consumption but which will favour quick, secure feedback.
