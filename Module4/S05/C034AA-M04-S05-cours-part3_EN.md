# 4.5.3. Managing the impact on memory

As we have just discussed, P2M and P2P flows are made possible through information being stored on DAOs, identifying routes leading to nodes of higher rank. This information can be stored in one of two ways.

_Storing_ mode involves distributing the storage of this information in the graph, each node having a routing table between all destinations of its sub-tree. Messages will only go as far in the graph as the first common ancestor, thus optimising P2P flows. However, the protocol will only function if the objects have a sufficient storage capacity for the routing table of their sub-network.

_Non-storing_ mode involves storing this information centrally in the root, making this the only node to retain routing information. As a result, it is necessary to have a root node with the requisite memory capacities. All messages will automatically go as far as the root before dropping back down towards their destination. For a P2P flow, even if the nodes are close together in the graph, this will be fed all the way back. For a P2M flow, the way in which the message drops back down the tree can be optimised.

The figure below illustrates the difference between these two modes when it comes to transmitting from a source node (S) to a destination node (D).

<figure>
    <img src="Images/storing_EN.png" alt="P2P communication comparison"><br />
    <figcaption>Figure 7: P2P communication comparison</figcaption>
</figure>

The decision of whether to opt for _storing_ or _non-storing mode_ will depend on the memory capacities of the intermediary nodes and of the root node, as well as on the flows used most recently in the application.
