# 4.5.2. Création et maintenance de la topologie

La topologie du DODAG décrite précédemment est créée et maintenue à l'aide de paquets de contrôle.

Les premiers sont les __DIO__ (_DODAG Information Object_). Initiés par la racine, qui y indique les métriques de routage et la fonction d'objectif, ils sont relayés aux noeuds voisins, afin de construire les routes ascendantes. Les paquets DIO venant de noeuds de rang égal ou supérieur seront ignorés pour éviter de créer des boucles dans le graphe.

Pour atténuer la consommation énergétique de cette multi-diffusion qui se répétera pour maintenir le réseau, cette propagation se base sur l'algorithme Trickle, qui fournit un compromis avec la réactivité aux changements de topologie. Les annonces se feront plus fréquentes quand le réseau est instable, et plus espacées quand le réseau est stable.

<figure>
    <img src="Images/C034AA_M04_S05.gif" alt="DODAG built using DIOs"><br />
    <figcaption>Figure 6: DODAG construit à l'aide de messages DIO</figcaption>
</figure>

Les seconds sont les __DIS__ (_DODAG Information Sollicitation_). Ils correspondent à des demandes d'information de la part de noeuds qui veulent rejoindre le réseau ou de noeuds souhaitant avoir des informations plus récentes. Le noeud qui reçoit un DIS répondra directement au noeud source par un paquet DIO.

Les derniers sont les __DAO__ (_Destination Advertisement Object_). Ils permettent à RPL de supporter le trafic descendant. Chaque noeud émet un DAO vers son parent préféré, lui signalant qu'il peut être contacté. Selon le mode choisi (voir section suivante), ces informations sont soit stockées et compilées au niveau du parent préféré soit remontées jusqu'à la racine. Si l'application ne nécessite pas de flux P2P ou P2M, alors ces derniers paquets de contrôle peuvent être désactivés pour réduire l'impact mémoire.
