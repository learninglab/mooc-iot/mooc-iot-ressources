# 4.5.3. Gérer l'impact sur la mémoire

Comme nous venons de l'évoquer, les flux P2M et P2P vont être possibles grâce au stockage des informations des DAOs pour connaître les routes vers les noeuds de rangs supérieurs. Le stockage de ces informations peut se faire selon deux modes.

Le mode _storing_ propose de stocker ces informations de manière distribuée dans le graphe, chaque noeud ayant une table de routage entre toutes les destinations de son sous-arbre. Les messages ne remonteront dans le graphe que jusqu'au premier ancêtre commun, ce qui permet d'optimiser les flux P2P. Mais le protocole ne fonctionnera que si les objets ont une capacité de stockage suffisante pour la table de routage de leur sous-réseau.

Le mode _non-storing_ propose lui de stocker ces informations de manière centralisée, au niveau de la racine, qui est donc le seul noeud à maintenir des informations de routage. Il faudra donc s'assurer d'avoir un noeud racine ayant des capacités mémoire suffisantes. Tous les messages remonteront obligatoirement jusqu'à la racine avant de redescendre vers la destination. Pour un flux P2P, même si les noeuds sont proches dans le graphe, celui-ci sera obligatoirement remonté complètement. Pour un flux P2M, alors des optimisations peuvent être envisagées dans la re-descente du message dans l'arbre.

La figure ci-dessous illustre la différence entre ces deux modes pour la transmission depuis un noeud source (S) vers un noeud de destination (D).

<figure>
    <img src="Images/storing_FR.png" alt="Comparaison de communication P2P"><br />
    <figcaption>Figure 7 : Comparaison de communication P2P</figcaption>
</figure>

Selon les capacités en mémoire des noeuds intermédiaires et du noeud racine, mais également des flux les plus fréquemment utilisés dans l'application, l'on choisira le mode _storing_ ou _non-storing_.
