# 4.5.2. Creating and maintaining topologies

The topology of the DODAG described previously is created and maintained using control packets.

The first of these are __DIOs__ (_DODAG Information Objects_). Initiated by the root, which will indicate the routing metrics and the objective function, these are relayed to neighbouring nodes in order to build ascending routes. DIO packets coming from nodes of a equal or higher rank will be ignored in order to prevent loops from forming in the graph. In order to reduce the energy consumption of this multi-distribution system, which will repeat itself in order for the network to be maintained, this propagation is based on the Trickle algorithm, which provides a compromise with the reactivity to changes in topology. Announcements will be more frequent when the network is unstable, and more spaced out when the network is stable.

<figure>
    <img src="Images/C034AA_M04_S05.gif" alt="DODAG built using DIOs"><br />
    <figcaption>Figure 6: DODAG built using DIOs</figcaption>
</figure>

The second of these are __DISs__ (_DODAG Information Sollicitations_). These correspond to information requests made by nodes wanting to join the network or by nodes seeking more recent information. Any node receiving a DIS will respond directly to the source node using a DIO packet.

The last of these are __DAOs__ (_Destination Advertisement Objects_). These enable RPLs to support descending traffic. Each node transmits a DAO towards its preferred parent, signalling to them that it may be contacted. Depending on the mode chosen (see following section), this information will either be stored and compiled with the preferred parent or fed back as far as the root. In cases where the application does not require P2P or P2M flows, these last control packets may be deactivated in order to reduce the impact on memory.
