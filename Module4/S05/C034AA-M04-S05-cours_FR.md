<br/>

Dans les réseaux d'objets contraints, les limitations de portée radio ou de puissance de transmission ne vont pas toujours permettre à un émetteur et un récepteur de communiquer directement. Leurs voisins dans le réseau vont être sollicités pour relayer le message de proche en proche : on parle de communication multi-saut. Mais dans ce cas, avec plusieurs voisins disponibles, les chemins possibles sont multiples. On a donc besoin de  définir un ensemble de mécanismes pour sélectionner un chemin/une route pertinent(e) pour acheminer le message, que l'on appelle routage.

Pour définir une solution de routage, il faut s'intéresser aux échanges d'information qui peuvent être catégorisés selon différents flux de trafic :
- point à point, pour un échange entre deux points du réseau (_P2P, Point-to-point_),
- point à multipoint, pour une diffusion d'un point d'origine vers plusieurs points destinataires (_P2M, Point-to-multipoint_),
- multipoint à point, pour une remontée de plusieurs points sources vers un même point de récolte (_M2P, Multipoint-to-point_).

Les figures ci-dessous illustrent les flux depuis un ou plusieurs noeuds source (S) vers un ou plusieurs noeuds de destination (D).

<figure>
    <img src="Images/C034AA-M04-S05_M2P.png" alt="" width="500">
    <figcaption>Figure 1 : Multipoint-to-point (M2P)</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05_P2M.png" alt="" width="500">
    <figcaption>Figure 2 : Point-to-multipoint (P2M)</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05_P2P.png" alt="" width="500">
    <figcaption>Figure 3 : Point-to-point (P2P)</figcaption>
</figure>

Dans les types de réseaux étudiés ici, les déploiements se font le plus souvent de manière ad-hoc et gardent une nature instable, de par l'usage de communications sans fils de basse consommation. Le réseau devra donc être dynamique pour savoir palier à la disparition, apparition ou défection d'un ou plusieurs noeuds du réseau. Il faut également prendre en compte le fait qu'il repose sur du matériel contraint et peu robuste. Les solutions existantes pour les réseaux classiques demandaient donc des adaptations prenant en compte ces contraintes.

L'IETF (organisme de standardisation mondial) a donc défini le standard RPL (prononcez 'ripeule'), un protocole de routage IPv6 pour les Low Power and Losy Networks (LLNs), optimisé pour le multi-saut et les communications M2P, flux de trafic le plus présent dans les réseaux de capteurs sans fil (mais il définit aussi des mécanismes pour le P2M et le P2P).

Dans la suite de cette séquence, nous allons passer en revue les principes de routage mis en place par le standard RPL. Puis vous mettrez en pratique ces principes avec les objets connectés de la plateforme FIT IoT-LAB.

## Références

- RPL's RFC - https://tools.ietf.org/html/rfc6550
- RPL, the Routing Standard for the Internet of Things . . . Or Is It? - https://hal.archives-ouvertes.fr/hal-01647152/
- Configuration dynamique et routage pour l’internet des objets - https://tel.archives-ouvertes.fr/tel-01687704/
