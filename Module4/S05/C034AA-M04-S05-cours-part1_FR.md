# 4.5.1. Le graphe de routage de RPL

RPL définit une organisation de routage sous la forme d'un graphe orienté : le __DODAG__ (_Destination-Oriented Directed Acyclic Graph_). Il s'agit d'un graphe orienté vers un noeud racine (_root_) ne comportant pas de cycle.

Pour éviter la formation de boucles, le standard se base sur une notion de rang, e.g la position relative d'un noeud vis-a-vis de la racine comparée aux noeuds du voisinage. Si un noeud sait communiquer avec plusieurs noeuds de rang inférieur, alors il en sélectionnera un comme parent préféré pour la remontée de message. Cette organisation répond bien à une optimisation pour les communications M2P, avec une remontée d'information vers la racine.

<figure>
    <img src="Images/C034AA-M04-S05.png" alt="" width="300">
    <figcaption>Figure 4 : Liens de communication</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05-2.png" alt="" width="300">
    <figcaption>Figure 5 : DODAG</figcaption>
</figure>

Même si dans la suite de cette séquence nous décrivons les mécanismes de RPL pour un simple DODAG, il faut noter qu'un déploiement RPL peut contenir plusieurs instances. Chaque instance est définie par une fonction d'objectif qui permet de définir :
- comment les noeuds sélectionnent et optimisent les routes,
- comment transformer une ou plusieurs métriques pour calculer le rang,
- comment les noeuds sélectionnent leur parent préféré.

Une instance peut contenir plusieurs DODAGs qui partageront la même fonction d'objectif, chaque noeud ne pouvant participer qu'à un seul DODAG. Par contre, un déploiement RPL peut contenir plusieurs instances, et un noeud peut participer à plusieurs instances.

Prenons l'exemple d'un réseau de capteurs qui a pour objet principal la remontée régulière des données pour surveiller des équipements ou une zone.
Une instance RPL sera définie avec une fonction d'objectif adéquate pour optimiser notamment la consommation énergétique de ce réseau.
Mais si ce même réseau doit aussi être capable de remonter des alertes, la remontée des messages doit se faire le plus rapidement possible et être prioritaire. Les besoins sont donc différents du premier. Les noeuds du réseau vont alors appartenir à une seconde instance RPL, avec une fonction objectif différente, qui se souciera moins de la consommation énergétique mais qui avantagera les remontées rapides et sûres.
