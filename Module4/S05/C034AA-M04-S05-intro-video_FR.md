# 4.5.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>A la fin de cette séquence, vous saurez comment mettre en place et utiliser RPL, le mécanisme standard de routage de message entre les points d'un réseau d'objets connectés.</p>
</div>

Présentation en vidéo des principes de routage du standard RPL, un protocole de routage IPv6 dédié aux réseaux sans fil à faible puissance.
