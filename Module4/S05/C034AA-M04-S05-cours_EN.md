<br/>

In constrained networks, limitations in terms of radio range or transmitting power mean it is not always possible for emitting devices or receivers to communicate directly with each other. Their neighbours within the network will be called upon to relay the message gradually: this is known as multi-hop communication. However, in such cases, where multiple neighbours are available, there are a variety of possible paths to choose from. What this means is that it is necessary to define a set of mechanisms for selecting a relevant route/path for delivering the message, which is called routing.

In order to define a routing solution, we must focus on information exchanges, which can be categorised in accordance with different flows of traffic:
- point-to-point, for exchanging between two points on the network (_P2P_)
- point-to-multipoint, for sending from one original point to multiple recipient points (_P2M_)
- multipoint-to-point, for sending from multiple source points to the same collection point (_M2P_).

The figures below illustrate flows from one or more source nodes (S) to one or more destination nodes (D).

<figure>
    <img src="Images/C034AA-M04-S05_M2P.png" alt="" width="500">
    <figcaption>Figure 1: Multipoint-to-point (M2P)</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05_P2M.png" alt="" width="500">
    <figcaption>Figure 2: Point-to-multipoint (P2M)</figcaption>
</figure>

<figure>
    <img src="Images/C034AA-M04-S05_P2P.png" alt="" width="500">
    <figcaption>Figure 3: Point-to-point (P2P)</figcaption>
</figure>

In the network types studied here, objects are most often deployed on an ad-hoc basis and retain a degree of instability as a result of using low energy-consumption wireless communications. What this means is that the network must be dynamic in order to be able to compensate for the disappearance, appearance or withdrawal of one or more nodes in the network. We also have to take into account the fact that this is reliant upon constrained material that is not particularly robust. Existing solutions for standard networks needed to be adapted in such a way that took these constraints into account.

The IETF (a global standardisation body) published the RPL standard, an IPv6 routing protocol for Low Power and Lossy Networks (LLNs), which was optimised for multi-hop and M2P communication, the most common type of traffic flow in wireless sensor networks (it also defines mechanisms for P2M and P2P).

During the rest of this sequence, we will go over the routing principles established by the RPL standard.

## References

- RPL's RFC - https://tools.ietf.org/html/rfc6550
- RPL, the Routing Standard for the Internet of Things . . . Or Is It? - https://hal.archives-ouvertes.fr/hal-01647152/
- Dynamic configuration and routing for the internet of things - https://tel.archives-ouvertes.fr/tel-01687704/
