# 4.5.0. Introduction

<div class="ill-alert alert-grey">
<i class="ill-icon ill-icon-check"></i>
<p>At the end of this sequence, you will know how to set up and use RPL, the standard routing mechanism between the nodes of a connected object network.</p>
</div>

Video presentation of the RPL standard, an IPv6 routing protocol dedicated to low-power wireless networks.
