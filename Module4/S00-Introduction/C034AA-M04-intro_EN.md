
# Module 4. Focus on Low-Power Communication Networks

Objective: At the end of this module you will be able to describe IoT communication protocols with the various network layers. You will also be able to write your first IoT application using the Internet protocol CoAP in order to retrieve the values from a temperature sensor.

Hands-on activities (TP):
TP8: Discover 802.15.4
TP9: Discover IPv6 and 6LoWPAN
TP10: Discover UDP socket
TP11: Discover CoAP protocol
TP12: Discover LwM2M protocol
TP13: RPL


## Contents of Module 4

### 4.1. Low-Power Wireless Networks
- 4.1.0. Introduction
- 4.1.1. Topologies, constraints and objectives
- 4.1.2. The OSI / IETF model

### 4.2. The 802.15.4 Communication Protocol
- 4.2.0. Introduction
- 4.2.1. Air as a communication medium
- 4.2.2. A shared medium
- 4.2.3. Saving energy through synchronization
- 4.2.4. Deterministic networks and guarantees
- TP8.   Discover 802.15.4

### 4.3. 6LoWPAN : IPv6 for the IoT
- 4.3.0. Introduction
- 4.3.1. Discovering IPv6 and 6LoWPAN
- TP9.   Discover IPv6 and 6LoWPAN
- TP10.  Discover UDP socket

### 4.4. CoAP: le protocole d'application Internet
- 4.4.0. Introduction
- 4.4.1. The architecture of the CoAP protocol
- 4.4.2. The format of CoAP messages
- 4.4.3. Examples of interactions
- 4.4.4. Resource discovery
- 4.4.5. Resource observation
- 4.4.6. CoAP installation on FIT IoT-LAB
- TP11.  Discover CoAP protocol
- TP12.  Discover LwM2M protocol

### 4.5. Routing in Low-Power Wireless Networks
- 4.5.0. Introduction
- 4.5.1. The RPL routing graph
- 4.5.2. Creating and maintaining topologies
- 4.5.3. Managing the impact on memory
- TP13.  RPL
