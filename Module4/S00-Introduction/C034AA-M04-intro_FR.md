
# Module 4. Focus sur les réseaux basse consommation

Objectif : À la fin de ce module, vous serez capable de décrire les protocoles de communication IoT avec les différentes couches réseaux. Vous serez également en mesure d'écrire votre première application IoT avec l'utilisation du protocole Internet CoAP pour récupérer les valeurs d'un capteur de température.

Activités pratiques (TP) :
TP8: Discover 802.15.4
TP9: Discover IPv6 and 6LoWPAN
TP10: Discover UDP socket
TP11: Discover CoAP protocol
TP12: Discover LwM2M protocol
TP13: RPL


## Sommaire du Module 4

### 4.1. Les réseaux basse consommation
- 4.1.0. Introduction
- 4.1.1. Topologies, contraintes et objectifs
- 4.1.2. Le modèle OSI / IETF

### 4.2. Le protocole de communication 802.15.4
- 4.2.0. Introduction
- 4.2.1. L’air comme médium de communication
- 4.2.2. Un médium à partager
- 4.2.3. Se synchroniser pour économiser
- 4.2.4. Garanties et réseaux déterministes
- TP8.   Discover 802.15.4

### 4.3. 6LoWPAN et UDP: IPv6 pour l'IoT
- 4.3.0. Introduction
- 4.3.1. Découverte de IPv6 et 6LoWPAN
- TP9.   Discover IPv6 and 6LoWPAN
- TP10.  Discover UDP socket

### 4.4. CoAP: le protocole d'application Internet
- 4.4.0. Introduction
- 4.4.1. Architecture du protocole CoAP
- 4.4.2. Le format des messages CoAP
- 4.4.3. Exemples d'interactions
- 4.4.4. Découverte de ressources
- 4.4.5. Observation de ressources
- 4.4.6. Installation CoAP sur FIT IoT-LAB
- TP11.  Discover CoAP protocol
- TP12.  Discover LwM2M protocol

### 4.5. Routage pour les réseaux basse consommation
- 4.5.0. Introduction
- 4.5.1. Le graphe de routage de RPL
- 4.5.2. Création et maintenance de la topologie
- 4.5.3. Gérer l'impact sur la mémoire
- TP13.  RPL
