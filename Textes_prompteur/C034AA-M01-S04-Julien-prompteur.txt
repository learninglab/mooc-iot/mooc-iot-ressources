In this course, we propose practical activities on real hardware. You don't have to buy anything since you will use IoT-LAB, a remote experimentation platform, accessible online.

--- FIT IoT-LAB usage ---

The easiest way to submit an experiment on the IoT-LAB testbed is to use its web portal ...
... accessible from the testbed’s website.

Simply create an account online and log in.

The "New experiment" button allows you to access the experiment creation form.

First, we choose its name and duration.

It is also possible to schedule it over time.

Then we choose the nodes we want to use:
 - Either by selecting their properties
 - or via the 3D map, if you want to choose a particular physical topology.

Here, we will use M3 nodes from the dedicated room in the platform in Lille using the last possibility: specify directly their ids.

It remains to specify the firmware/program they will run and submit the experiment.

The scheduler puts our submission in the queue.
As the nodes are free, the scheduler starts the experiment directly.

The M3’s LEDs blink because it is the behavior encoded in the firmware that we provided and it is running.

Now that the experiment is running, we have access to a set of interactions with the nodes, like stopping the power supply, restarting it, or updating the firmware.

--- The use of FIT IoT-LAB in this MOOC ---

To make things easier, you will use IoT-LAB through the MOOC platform.

Practical activities will be carried out in notebooks.
In this environment, you will already be authenticated and the IoT-LAB tools will already be installed.

You can easily
 - edit the source code of embedded applications,
 - and build firmwares.

And, instead of the web portal, you will use IoT-LAB cli tools
 - to submit an experiment
 - and interact with the nodes.

---

We hope you'll enjoy the use of the IoT-LAB testbed through the practical activities and that you'll find it easy to use. Anyway, don't hesitate, you'll have its developper team with you if you need support.
