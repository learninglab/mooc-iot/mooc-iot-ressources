/---
To illustrate concretely the Internet of Things, we will focus on 3 applications
based on three different radio technologies: BLE, LoRa and 802.15.4.

Each of these technologies is well adapted to the requirements of each
application regarding the ease of use, the range and the energy consumption.
---/

/---
The first example we will talk about is the activity tracker of a smart watch
---/

/---
This is one of the most popular IoT application: more and more people are using
it nowadays.

An activity tracker can monitor, in real time, several physiological and
biological measures of the person wearing the device.

It is especially useful for people suffering heart failures or arrhythmia, but
also to people practicing sports, to monitor their heart rate during the day or
during an effort.
---/

/---
In this kind of application, the wearable device is in general connected to a
SmartPhone via the Bluetooth Low Energy protocol.

On the SmartPhone, a compatible and shiny application is able to receive
notifications from the device/watch and to display in real-time the information.

Then, via the internet connection of the SmartPhone, the application sends the
data to a Cloud on the Internet.

The data are then available on a dedicated platform and can then be fetched and
displayed on the web browser of a computer.

Higher level information can also be extracted: for the analysis of data
and problem detection, for getting health improvement curves, etc
---/

/---
The BLE protocol defines multiple application profiles. These profiles are
designed to guarantee the interoperability of a device providing a given
function with a compatible smartphone application. Thanks to this
interoperability, there is no need to install different applications for
different Activity Trackers.---/

/---
The second example we want to talk about is an application used to monitor soil
moisture sensors in the context of smart farming.
---/

/---
This application is very helpful to make sure all plants in a field are properly
watered. It is also a good indicator to avoid water waste, and, as a
consequence, to save money and natural resources.
With this application the farmer is able to remotely monitor and control the
soil moisture level of his fields and better organize his work.
For this kind of application, long range and low-power technologies, such as
LoRaWAN and SigFox, suit well.
---/

/---
Let's describe a bit more what is a LoRaWAN network and give an overview of how
it works in the context of smart farming.
A LoRaWAN network is a cellular technology designed to easily exchange very
small data between end-devices and users.
In a LoRaWAN network, the end-devices only perform simple operations, like
collecting environmental information, and send their data to an application
server hosted in a Cloud, via a gateway.
The data is exchanged between the end-devices and the gateway using the LoRa
radio technology, which is a long-range and low-power communication technology
with a very low data rate.
The communication between the gateways and the application server is simply
using a regular Internet connection.
The users just have to access the application server via the web or other IoT
protocols, like MQTT, to retrieve or send data.
---/

/---
Devices in a LoRaWAN network are designed to consume the minimum energy: they
periodically wake up to send some data and then they go back to their sleeping
mode. Depending on the application and the required frequency of
measurements, the device can stay in low-power mode during several minutes or
hours between each send. This behaviour ensures a very minimal power
consumption. Indeed, a LoRaWAN device can run on battery during several years
and this is very important in the context of smart farming: devices can be
deployed outdoor in fields and will work during a long period without having to
take care of them.
---/

/---
The last example we want to talk about is an application used in the context of
smart building
---/

/---
This application illustrates how a wireless sensor network can be useful for
monitoring the air quality inside a building.
To collect the air quality data inside a building, sensors can be deployed in
different areas: rooms, corridors, etc
They can be used to detect fire or equipment malfunctions and raise an alarm
early in case of a problem or just enable the ventilation system.
---/

/---
In general wireless sensor networks use short-range radio technologies, mainly
in the 2.4GHz radio frequency band, to transmit the data.
They are also saving money in terms of infrastructure costs: indeed deploying
new wires for connecting monitoring devices can be very expensive in an existing
building. So using radio technologies with autonomous devices is a real money
saver in this context.
---/

/---
In a wireless sensor network, because of their use of short-range radio,
it is not always possible for a device to reach the gateway to the local area network.
The solution is then to define a network with a mesh topology where each device
can act as a sensor or a router. This means that each device is able to transfer
the network traffic coming from its neighbors.
In a wireless sensor network, the base station, also called a border router,
provides 2 interfaces: a wireless interface, compatible with the radio
technology used in the wireless sensor network and another interface connected
to the local network.
The data collected by the sensors transits via the neighbor sensors to the border
router and are then routed on the internal network to the building management
system.
---/

/---
As you can understand with the examples presented, when designing an IoT
applications, you have to choose the radio technology best suited to your
project requirements.
---/

