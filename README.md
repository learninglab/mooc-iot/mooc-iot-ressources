# Elaboration des contenus 

* [Structuration du cours](#structuration-du-cours)
* [Principes d'élaboration des contenus de cours](#principes-délaboration-des-contenus-de-cours)
* [Nomenclature des fichiers](#nomenclature-des-fichiers)
* [Quiz et exercices](#quiz-et-exercices)
	* [Ce qu'il faut dans le MOOC](#ce-quil-faut-dans-le-mooc)
	* [Templates pour les quiz sur FUN](#templates-pour-les-quiz-sur-fun)
	* [Autres exercices possibles sur FUN](#autres-exercices-possibles-sur-fun)
	* [Exemples de mises en page spécifiques](#exemples-de-mises-en-page-spécifiques)
* [Export pdf et html du cours](#export-pdf-et-html-du-cours)

## Structuration du cours

**Chaque module proposera une séquence introductive avec** :
* présentation textuelle des objectifs du module et des points qui seront abordés (sommaire du module)
* et une vidéo d'introduction qui permet également de présenter les intervenants ?


**Les séquences de cours suivantes seront composées** :
* d'une ou plusieurs pages de contenus de cours. Cette structuration est fonction des contenus. Pour chaque page créer un fichier .md distinct dans le répertoire adéquat (moduleX/SXX). Ces pages de contenus de cours peuvent être agrémentées d'images, schémas, etc.
* d'une ou plusieurs vidéos. Principe : vidéos qui mettent en évidence les points clés de la séquence, présentent des procédures (démo), etc. Des vidéos courtes, qui vont à l'essentiel. On pourra être plus exhaustifs dans les contenus textuels.
* de quiz formatifs : leur but est de mettre l'accent sur les points-clés à retenir ou les compétences à acquérir (en lien avec les objectifs pédagogiques formulés pour la séquence)
* le cas échéant : 
    * d'activités de type TP
    * d'autres ressources complémentaires : liens, réf biblio, etc.

## Principes d'élaboration des contenus de cours

* Dans un premier temps : rédiger les contenus de cours sous forme textuelle avec, quand nécessaire, schémas / screenshots, toute illustration utile... Ces éléments textuels sont à rédiger dans un fichier markdown.
* Dans un second temps : nous identifierons les vidéos qui devront être réalisées (soit des vidéos de synthèse, soit des vidéos qui vont plus loin ou illustrent des notions présentées dans les ressources textuelles)
* Dans un troisième temps : préparer les textes des vidéos pour le tournage (utilisation d'un prompteur recommandée = rédiger le script de son intervention).


Langue : 
* Vidéos : anglais / Sous-titrage anglais et français
* Textes : si tournages en anglais, il est préférable que les textes soient écrits directement en anglais ? On peut faire appel à un presta pour correction éventuellement ? ou utiliser un outil comme deepl pour la traduction ? Dans ce dernier cas, cela signifie qu'on se charge nous -mêmes de produire les textes en Français et Anglais.


## Nomenclature des fichiers 
* code du cours (interne Learning Lab) : C034AA
* numéro du module : ex. M04
* numéro de la séquence : ex. S01
* type de ressource : ex. cours, exercice, biblio, quiz, etc.  
* langue : _FR ou _EN 

Exemple complet :
C034AA-M04-S01-cours_FR.md

## Quiz et exercices


### Ce qu'il faut dans le MOOC

- au minimum 3 questions de quiz associées à chaque séquence de cours *= évaluation formative*

- un ou 2 exercices d’application/synthèse pour l’ensemble d'un module *= évaluation formative*

- idéalement une évaluation finale avec des quiz et/ou exercices couvrant l’ensemble du module *= évaluation sommative sur laquelle est principalement basée la délivrance des attestations de suivi.*


**Questions de quiz :**

- Utiliser les **templates présentées ci-après** pour formaliser les quiz.&nbsp;

**Pour les quiz de types "boutons radios" (réponse unique) et "cases à cocher" (réponses multiples) : utilisez la présentation indiquée plus bas (parenthèses dans le premier cas et crochets dans le second).**

Pour les autres types d'exercices, expliquez-nous ce que vous souhaitez et nous nous chargerons de la mise en forme.

- Ne pas oublier de compléter la partie «&nbsp;**explanation**&nbsp;» pour chaque question. C’est le feedback qui apparaît lorsque que l’apprenant clique sur solution. L’idée avec ce feedback est d’expliquer pourquoi la bonne réponse est celle indiquée.

- **Numérotation des questions**. Exemple "Question 3.1.1" : 3= numéro du module, 1= numéro de la séquence, 1=numéro de la question. Il s'agit donc ici de la première question de la séquence 01 du module 3.


**Exercices de fin de module :**

* des **exercices d’application** dont on peut vérifier s’ils ont été correctement effectués par l’intermédiaire d’un quiz ou autre système de score connecté à la plateforme FUN via LTI,
* la **correction par les pairs** est également possible 
** Principe = l’apprenant A soumet un travail sous forme de texte. Un apprenant B reçoit ce travail et l’évalue à l’aide d’une grille d’évaluation préalablement définie par l’enseignant.

* autres solutions sans possibilités de notation : 
** proposer un **exercice ou TP** dont un **corrigé type** sera fourni la semaine suivante
** des consignes pour **faire découvrir une application pas à pas**, etc. dont on ne fournira pas de corrigé
** des **sujets à débattre** dans les forums de discussion
** des **productions** à réaliser et à partager (présentation type ppt, vidéo, photos, texte à rédiger sur un sujet…), 
** des informations à partager à travers la création d’un **wiki,**
** **…**

**Cette liste n’est pas exhaustive. Les possibilités sont nombreuses&nbsp;: vos idées sont les bienvenues&nbsp;!** N’hésitez pas à les soumettre à l’équipe du Learning Lab qui réfléchira à une solution pour les mettre en œuvre.


### Templates pour les quiz sur FUN


Vous trouverez ci-dessous la liste des activités auto-correctives proposées sur la plateforme FUN.

Contrairement aux contenus de cours textuels, l'intégration des quiz et leur mise à jour ne se font pas "automatiquement" depuis Gitlab. L'équipe du Learning Lab s'en chargera. 



#### Multiple Choice Questions = question à réponse unique (boutons radios)
Question / Enoncé

( ) Distracteur 1

( ) Distracteur 2

(x) Bonne réponse

( ) Distracteur 4


[explanation]

Rédiger ici le texte de feedback qui s’affichera en même temps que la solution.

[explanation]

#### Checkbox = question à réponses multiples (cases à cocher)
Question / Enoncé

[x] Réponse correcte 1

[ ] Distracteur

[x] Réponse correcte 2


[explanation]

Rédiger ici le texte de feedback qui s’affichera en même temps que la solution.

[explanation]

#### Dropdown = liste déroulante

Enoncé


[[(Bonne réponse), Distracteur 1, Distracteur 2, Distracteur 3, Distracteur 4]]


[explanation]

Rédiger ici le texte de feedback qui s’affichera en même temps que la solution.

[explanation]

#### Text Input
Question avec champ de réponse libre. La réponse peut inclure du texte, des nombres et des caractères spéciaux tels que des signes de ponctuation.


Enoncé


= Réponse correcte souhaitée


[explanation]

Texte de feedback qui s’affichera en même temps que la solution.

[explanation]


#### Numerical Input 
Enoncé

= valeur attendue +- tolérance autorisée


Exemples&nbsp;:

Enter the numerical value of Pi:

= 3.14159 +- .02


Enter the approximate value of 502*9:

= 4518 +- 15%


Enter the number of fingers on a human hand:

= 5


[explanation]

Texte de feedback qui s’affichera en même temps que la solution 

[explanation]


#### Multiple Choice & Numerical Input 
Question à choix multiple avec champ numérique pour préciser sa réponse.

<img src="Images/multiple-choice-numerical-input.jpg" width=""/>


### Autres exercices possibles sur FUN

#### Glisser-déplacer 
Répondre à une question / résoudre un problème en plaçant des éléments texte ou images à un endroit spécifique (exemples d’utilisation : annoter un schéma, identifier des personnes sur une photo, identifier des lieux sur une carte, etc.) 

#### Sélection d’une zone sur une image
Réponse à une question par clic sur une image. 

### Exemples de mises en page spécifiques

#### Liste de champs textes associés à une lettre*
<img src="Images/champs-texte-lettre.jpg" width=""/>

#### Tableau avec listes déroulantes
<img src="Images/tableau-listes-deroulantes.jpg" width=""/>
 
#### Tableau avec images et listes déroulantes 
<img src="Images/tableau-images-listes.jpg" width=""/>

#### Tableau avec images et cases à cocher
<img src="Images/tableau-images-cases.jpg" width=""/>

## Export pdf et html du cours

En fin de session pour exporter les pages de cours markdown en PDFs, il suffit d'utiliser le script `export.sh`. Les fichiers exportés se trouvent dans `export/html` ou `export/pdf`.

** Requirements : **

- pandoc
- wkhtmltopdf

** Utilisation : **

```bash
./export.sh
```



